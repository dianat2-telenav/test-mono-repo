Data pipeline - Adapter
---
Adapter is composed of 2 'phases':

1. extractors - each extractor is responsible to extract the value of a certain tag
2. rules - based on the extracted value, the rule is responsible to update the tag key/value/both accordingly

The adapter is part of data-pipeline, and its purpose is to prepare the parquets for NGX compilation so the map can be used for navigation.
The adapter is run after a series of other components have done their job, like standardization, road classification or topology corrections.
Basicaly it should be the last step in the pipeline. In this step many NGX specific rules are applied to the way sections, way sections
which are also cleaned up of unnecesarry data, the output is then used to generate the final pbf.

To build the project run:
```
mvn clean package -Pshade
```

##### Adapter Job

Job format:

```
    spark-submit-2.3.0 \
    --master yarn \
    --deploy-mode cluster \
    --driver-memory 5G \
    --executor-memory 15G \
    --executor-cores 4 \
    --conf spark.default.parallelism=400 \
    --queue production \
    --class com.telenav.adapter.NgxWaySectionsAdapterJob \
     ${ADAPTER_JAR}\
    way_sections_parquet=${NAME_STANDARDIZER_OUTPUT} \
    output_dir=${ADAPTER_OUTPUT}
```

Input Structure:
```
root
 |-- WAY_ID: long (nullable = false)
 |-- SEQUENCE_NUMBER: short (nullable = true)
 |-- TAGS: map (nullable = false)
 |    |-- key: string
 |    |-- value: string (valueContainsNull = false)
 |-- NODES: array (nullable = false)
 |    |-- element: struct (containsNull = false)
 |    |    |-- ID: long (nullable = false)
 |    |    |-- TAGS: map (nullable = false)
 |    |    |    |-- key: string
 |    |    |    |-- value: string (valueContainsNull = false)
 |    |    |-- LATITUDE: double (nullable = false)
 |    |    |-- LONGITUDE: double (nullable = false)
```

Output Structure:
```
root
 |-- WAY_ID: long (nullable = false)
 |-- SEQUENCE_NUMBER: short (nullable = true)
 |-- TAGS: map (nullable = false)
 |    |-- key: string
 |    |-- value: string (valueContainsNull = false)
 |-- NODES: array (nullable = false)
 |    |-- element: struct (containsNull = false)
 |    |    |-- ID: long (nullable = false)
 |    |    |-- TAGS: map (nullable = false)
 |    |    |    |-- key: string
 |    |    |    |-- value: string (valueContainsNull = false)
 |    |    |-- LATITUDE: double (nullable = false)
 |    |    |-- LONGITUDE: double (nullable = false)
```