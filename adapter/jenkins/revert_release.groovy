def revertRelease(String initialVersion, String newVersion, String projectName) {
    def mvnHome = tool 'Maven 3.3.9'
    sh "${mvnHome}/bin/mvn versions:use-releases"
    sh "${mvnHome}/bin/mvn -N versions:update-child-modules versions:set -DnewVersion=${initialVersion}"
    sh "git commit -am \"Revert release changes due to release failure\""
    sh "git push"
    sh "${mvnHome}/bin/mvn versions:commit"
    def releaseTag = "${projectName}-${newVersion}"
    sh "git tag -d ${releaseTag}"
    sh "git push origin :refs/tags/${releaseTag}"
}