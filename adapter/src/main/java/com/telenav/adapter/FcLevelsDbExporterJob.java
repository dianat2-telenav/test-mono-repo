package com.telenav.adapter;

import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.map.entity.WaySection;
import com.telenav.ost.ArgumentContainer;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import scala.Tuple2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.telenav.adapter.component.fc.TagService.HIGHWAY_TAG;
import static com.telenav.datapipelinecore.unidb.RoadCategoryTagKey.FUNCTIONAL_CLASS;

/**
 * The current job gets as in put way sections and maps them to a .csv file
 * where each record has 2 fields: fc, geometry.
 * The geometry field is used to be able to plot the way section by their fc using CartoDB or QGIS.
 * The FCs used for plotting can either be the way sections own fcs OR they can be replaced with fcs coming from
 * an input file.
 *
 * @author liviuc .
 */
public class FcLevelsDbExporterJob {

    private static final Map<String, DbCsvData> bbCsvDataMap = new HashMap<>();

    static {
        bbCsvDataMap.put("carto",
                new DbCsvData(",", "\"LINESTRING (", ")\""));
        bbCsvDataMap.put("qgis",
                new DbCsvData("|", "LINESTRING (", ")"));
    }

    public static void main(String[] args) {

        final ArgumentContainer argumentContainer = new ArgumentContainer(args);
        final String waySectionsPath = argumentContainer.getRequired("way_sections_parquet");
        final String sidefilePath = argumentContainer.getOptional("sidefile_path", StringUtils.EMPTY);
        final Integer fc1Repartition = Integer.parseInt(argumentContainer.getOptional("fc1_repartition", "1"));
        final Integer fc2Repartition = Integer.parseInt(argumentContainer.getOptional("fc2_repartition", "1"));
        final Integer fc3Repartition = Integer.parseInt(argumentContainer.getOptional("fc3_repartition", "1"));
        final Integer fc4Repartition = Integer.parseInt(argumentContainer.getOptional("fc4_repartition", "1"));
        final Integer fc5Repartition = Integer.parseInt(argumentContainer.getOptional("fc5_repartition", "1"));
        final String db = argumentContainer.getOptional("db", "qgis");
        final String outputPath = argumentContainer.getRequired("output_dir");
        final String withOsmData = argumentContainer.getOptional("with_osm_data", "false");
        final String runLocal = argumentContainer.getOptional("run_local", "false");

        final DbCsvData dbCsvData = bbCsvDataMap.get(db);

        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("FC to DB Job")
                .runLocal(Boolean.parseBoolean(runLocal))
                .build();
        final JavaSparkContext sparkContext = jobInitializer.javaSparkContext();

        JavaRDD<WaySection> waySections =
                RddReader.WaySectionReader.read(sparkContext, waySectionsPath)
                        .repartition(800)
                        .cache();

        final String geometryPrefix = dbCsvData.getGeometryPrefix();
        final String geometrySuffix = dbCsvData.getGeometrySuffix();
        final String separator = dbCsvData.getSeparator();

        if (!sidefilePath.equalsIgnoreCase(StringUtils.EMPTY)) {
            final JavaPairRDD<Long, String> fcsByWayIds = sparkContext.textFile(sidefilePath)
                    .mapToPair(line -> {

                        final String[] split = line.split(",");
                        final long wayId = Long.parseLong(split[0]);
                        final String fc = split[1];

                        return new Tuple2<Long, String>(wayId, fc);
                    });

            final JavaPairRDD<Long, WaySection> waySectionsByWayIds =
                    waySections.mapToPair(waySection -> new Tuple2<Long, WaySection>(waySection.strictIdentifier().wayId(), waySection));

            waySections = waySectionsByWayIds.leftOuterJoin(fcsByWayIds)
                    .map(tuple -> {

                        final WaySection waySection = tuple._2()._1();
                        final Optional<String> optionalFc = tuple._2()._2();

                        if (optionalFc.isPresent()) {
                            return waySection.withTag(FUNCTIONAL_CLASS.unwrap(), optionalFc.get());
                        }
                        return waySection.withoutTag(FUNCTIONAL_CLASS.unwrap());
                    }).cache();
        }

        final boolean addOsmData = Boolean.parseBoolean(withOsmData);

        final JavaPairRDD<String, String> byFc = waySections
                .mapToPair(waySection -> {

                    final Long waySectionId = waySection.id();

                    final String fc = waySection.tags().get(FUNCTIONAL_CLASS.unwrap());

                    final String geometry = waySection.nodes()
                            .stream()
                            .map(node -> new StringBuilder()
                                    .append(node.longitude().asDegrees())
                                    .append(" ")
                                    .append(node.latitude().asDegrees())
                                    .toString()
                            ).collect(Collectors.joining(","));
                    final String lineGeometry = geometryPrefix + geometry + geometrySuffix;

                    final StringBuilder record = new StringBuilder();

                    if (addOsmData) {
                        record.append(waySection.strictIdentifier().wayId())
                                .append(separator)
                                .append(waySection.id())
                                .append(separator)
                                .append(waySection.tags().get(HIGHWAY_TAG))
                                .append(separator);
                    }
                    record.append(fc)
                            .append(separator)
                            .append(lineGeometry);

                    return new Tuple2<String, String>(fc + "-" + waySectionId, record.toString());

                }).sortByKey(true)
                .cache();

        final List<String> fcs = Arrays.asList("1", "2", "3", "4", "5", "null");

        for (final String fc : fcs) {

            int re = fc1Repartition;

            if (fc.equals("2")) {
                re = fc2Repartition;
            }

            if (fc.equals("3")) {
                re = fc3Repartition;
            }

            if (fc.equals("4")) {
                re = fc4Repartition;
            }

            if (fc.equals("5")) {
                re = fc5Repartition;
            }

            final String fcKeyData = fc + "-";
            byFc.filter(tuple -> tuple._1().contains(fcKeyData))
                    .map(tuple -> tuple._2())
                    .repartition(re)
                    .saveAsTextFile(outputPath + "/" + db + "/" + fc);
        }
    }

    private static class DbCsvData {

        private String separator;
        private String geometryPrefix;
        private String geometrySuffix;

        public DbCsvData(final String separator,
                         final String geometryPrefix,
                         final String geometrySuffix) {
            this.separator = separator;
            this.geometryPrefix = geometryPrefix;
            this.geometrySuffix = geometrySuffix;
        }

        public String getSeparator() {
            return separator;
        }

        public DbCsvData setSeparator(String separator) {
            this.separator = separator;
            return this;
        }

        public String getGeometryPrefix() {
            return geometryPrefix;
        }

        public DbCsvData setGeometryPrefix(String geometryPrefix) {
            this.geometryPrefix = geometryPrefix;
            return this;
        }

        public String getGeometrySuffix() {
            return geometrySuffix;
        }

        public DbCsvData setGeometrySuffix(String geometrySuffix) {
            this.geometrySuffix = geometrySuffix;
            return this;
        }
    }
}
