package com.telenav.adapter;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.datapipelinecore.osm.OsmRelationTagKey;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.StandardEntity;
import com.telenav.map.entity.TagAdjuster;
import com.telenav.map.entity.WaySection;
import com.telenav.map.storage.WaySectionSchema;
import com.telenav.map.storage.read.RowToWaySectionConverter;
import com.telenav.map.storage.write.WaySectionToRowConverter;
import com.telenav.ost.ArgumentContainer;
import com.telenav.ost.spark.storage.Schema;
import com.telenav.ost.spark.storage.read.ParquetReader;
import com.telenav.ost.spark.storage.write.ParquetWriter;
import com.telenav.pipelinevalidator.runner.PipelineValidatorRunner;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.apache.spark.sql.Row;
import org.apache.spark.util.CollectionAccumulator;
import scala.Tuple2;

import java.util.function.Function;

import static com.telenav.datapipelinecore.unidb.RoadCategoryTagKey.FUNCTIONAL_CLASS;

/**
 * The current job adds FCs from an external source to way-sections or removes existing FCs if there is nothing to add
 * from the external source
 *
 * @author liviuc .
 */
public class FcLevelsIntegrator {

    public static void main(String[] args) {

        final ArgumentContainer argumentContainer = new ArgumentContainer(args);
        final String waySectionsPath = argumentContainer.getRequired("way_sections_parquet");
        final String sidefilePath = argumentContainer.getOptional("sidefile_path", StringUtils.EMPTY);
        final String outputPath = argumentContainer.getRequired("output_dir");
        final String logPath = argumentContainer.getRequired("log_dir");
        final String repartitionAsString = argumentContainer.getOptional("repartition", "800");
        final Integer repartition = Integer.valueOf(repartitionAsString);
        final boolean runLocal = Boolean.parseBoolean(argumentContainer.getOptional("run_local", "false"));

        try(final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("FC Integrator")
                .runLocal(runLocal)
                .build()) {
            final JavaSparkContext sparkContext = jobInitializer.javaSparkContext();

            final JavaRDD<String> fcs = sparkContext.textFile(sidefilePath).cache();

            WaySectionsIntegrator waySectionsIntegrator = new WaySectionsIntegrator(waySectionsPath, outputPath, repartition);
            waySectionsIntegrator.run(sparkContext, fcs, logPath);

            PipelineValidatorRunner.builder()
                    .waySectionsPath(waySectionsIntegrator.getOutputPath())
                    .sourceJobName(FcLevelsIntegrator.class.getSimpleName())
                    .reportPath(outputPath + "/validation_report")
                    .runLocal(runLocal)
                    .build()
                    .run();
        }

    }

    private abstract static class Integrator<E extends StandardEntity & TagAdjuster<E>> {

        private final String inputPath;
        private final String outputPath;
        private final Integer repartition;

        public Integrator(final String inputPath,
                          final String outputPath,
                          final Integer repartition) {
            this.inputPath = inputPath;
            this.outputPath = outputPath;
            this.repartition = repartition;
        }

        public String getOutputPath() {
            return outputPath + "/" + entitiesName() + "/parquet";
        }

        public void run(final JavaSparkContext sparkContext,
                        final JavaRDD<String> fcs,
                        final String logPath) {

            final CollectionAccumulator<String> logs = sparkContext.sc().collectionAccumulator(logPath);
            JavaRDD<E> entities = new ParquetReader<>(sparkContext,
                    fromRowConverter(), inputPath)
                    .read()
                    .repartition(repartition)
                    .cache();

            final JavaPairRDD<Long, String> fcsByEntityIds = fcs.mapToPair(line -> {

                final String[] split = line.split(",");
                final long wayId = Long.parseLong(split[0]);
                final String fc = split[1];

                return new Tuple2<>(wayId, fc);
            });

            final JavaPairRDD<Long, E> entitiesByWayIds = byId(entities);

            entities = entitiesByWayIds.leftOuterJoin(fcsByEntityIds)
                    .map(tuple -> {

                        final E entity = tuple._2()._1();
                        final Optional<String> optionalFc = tuple._2()._2();

                        if (optionalFc.isPresent()) {
                            if (entity.tags().containsKey(OsmTagKey.HIGHWAY.unwrap())
                                    || entity.hasTag(OsmRelationTagKey.ROUTE.unwrap(), "ferry")
                                    || entity.hasTag(UnidbTagKey.RAIL_FERRY.unwrap(), "yes")) {
                                return entity.withTag(FUNCTIONAL_CLASS.unwrap(), optionalFc.get());
                            } else {
                                long osmWayId = entity instanceof WaySection ?
                                        ((WaySection) entity).strictIdentifier().wayId() :
                                        entity.id();
                                logs.add("FC level with value " + optionalFc.get()
                                        + " received for way with OSM id " + osmWayId);
                            }
                        }
                        return entity.withoutTag(FUNCTIONAL_CLASS.unwrap());
                    }).cache();

            new ParquetWriter.Builder<E>()
                    .sparkContext(sparkContext)
                    .converter(toRowConverter())
                    .schema(schema())
                    .path(getOutputPath())
                    .saveMode("Overwrite")
                    .build()
                    .write(entities);

            if (logPath != null) {
                sparkContext
                        .parallelize(logs.value())
                        .coalesce(1)
                        .saveAsTextFile(logPath + "/" + entitiesName());
            }
        }

        protected abstract String entitiesName();

        protected abstract Schema schema();

        protected abstract JavaPairRDD<Long, E> byId(JavaRDD<E> entities);

        protected abstract Function<Row, E> fromRowConverter();

        protected abstract Function<E, Row> toRowConverter();
    }

    private static class WaySectionsIntegrator extends Integrator<WaySection> {

        public WaySectionsIntegrator(final String inputPath,
                                     final String outputPath,
                                     final Integer repartition) {
            super(inputPath, outputPath, repartition);
        }

        @Override
        protected String entitiesName() {
            return "way_sections";
        }

        @Override
        protected Schema schema() {
            return new WaySectionSchema();
        }

        @Override
        protected JavaPairRDD<Long, WaySection> byId(final JavaRDD<WaySection> entities) {
            return entities.mapToPair(waySection -> new Tuple2<>(waySection.strictIdentifier().wayId(), waySection));
        }

        @Override
        protected Function<Row, WaySection> fromRowConverter() {
            return new RowToWaySectionConverter();
        }

        @Override
        protected Function<WaySection, Row> toRowConverter() {
            return new WaySectionToRowConverter();
        }
    }

}
