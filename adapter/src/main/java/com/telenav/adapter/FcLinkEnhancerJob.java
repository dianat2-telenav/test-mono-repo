package com.telenav.adapter;

import com.telenav.adapter.component.fc.FcLinkService;
import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.map.entity.WaySection;
import com.telenav.ost.ArgumentContainer;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

@Slf4j
public class FcLinkEnhancerJob {

    public static void main(String[] args) {

        final ArgumentContainer argumentContainer = new ArgumentContainer(args);
        final String sectionedWaysPath = argumentContainer.getRequired("way_sections_parquet");
        final String outputPath = argumentContainer.getRequired("output_dir");

        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Adapter Job - FC Link Enhancer")
                .runLocal(Boolean.parseBoolean("false"))
                .build();

        final JavaSparkContext javaSparkContext = jobInitializer.javaSparkContext();

        final JavaRDD<WaySection> waySections = RddReader.WaySectionReader.read(javaSparkContext, sectionedWaysPath)
                .cache();

        final JavaRDD<WaySection> result = FcLinkService.assignFcToLinks(waySections, javaSparkContext);

        RddWriter.WaySectionWriter.write(javaSparkContext, result, outputPath + "/way_sections/parquet");
    }
}
