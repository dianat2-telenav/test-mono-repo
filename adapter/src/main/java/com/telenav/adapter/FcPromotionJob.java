package com.telenav.adapter;

import com.telenav.adapter.component.fc.FcPromotionService;
import com.telenav.adapter.component.fc.TagService;
import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.map.entity.WaySection;
import com.telenav.ost.ArgumentContainer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;

public class FcPromotionJob {

    public static void main(String[] args) {

        final ArgumentContainer argumentContainer = new ArgumentContainer(args);
        final String waySectionsPath = argumentContainer.getRequired("way_sections_parquet");
        final String outputPath = argumentContainer.getRequired("output_dir");
        final String runLocal = argumentContainer.getOptional("run_local", "false");

        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.sql.parquet.binaryAsString", "true");
        sparkConf.setMaster("local[1]");
        sparkConf.setAppName("Test");

        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Promotion Component")
                .runLocal(Boolean.parseBoolean(runLocal))
                .build();

        final JavaSparkContext javaSparkContext = jobInitializer.javaSparkContext();

        // Read way-sections.
        final JavaRDD<WaySection> waySections = RddReader.WaySectionReader.read(javaSparkContext, waySectionsPath);

        // Distinct isos.
//        final List<String> isos = waySections.map(waySection -> TagService.iso(waySection.tags()))
//                .filter(Objects::nonNull)
//                .distinct()
//                .collect();

        // Output data by country.
        JavaRDD<WaySection> waySectionsWithPromotion = javaSparkContext.emptyRDD();
        for (final String iso : Arrays.asList("USA")) {
            final JavaRDD<WaySection> byCountry =
                    waySections.filter(waySection -> iso.equalsIgnoreCase(TagService.iso(waySection.tags())));
            final JavaRDD<WaySection> byCountryPromoted =
                    FcPromotionService.closeGaps2(byCountry, javaSparkContext);
            waySectionsWithPromotion = waySectionsWithPromotion.union(byCountryPromoted);
        }

        RddWriter.WaySectionWriter.write(javaSparkContext, waySectionsWithPromotion, outputPath + "/way_sections/parquet");
    }
}
