package com.telenav.adapter;

import com.telenav.adapter.component.extractors.WayAdminLevelExtractor;
import com.telenav.adapter.component.fc.FcLinkService;
import com.telenav.adapter.component.fc.FcService;
import com.telenav.adapter.component.repository.ResourceRepository;
import com.telenav.adapter.rules.DoubleDigitizedRoadRule;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.datapipelinecore.bean.Relation;
import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.RawRelation;
import com.telenav.map.entity.WaySection;
import com.telenav.ost.ArgumentContainer;
import com.telenav.pipelinevalidator.runner.PipelineValidatorRunner;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.util.CollectionAccumulator;
import scala.Tuple2;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.telenav.adapter.component.fc.TagService.TURN_LANES;
import static com.telenav.adapter.component.fc.TagService.TYPE_LANES;
import static com.telenav.adapter.component.repository.ResourceRepository.getSpeedLimitsEU;

/**
 * @author roxanal
 */
@Slf4j
public class NgxWaySectionsAdapterJob {

    public static void main(String[] args) throws IOException {

        final ArgumentContainer argumentContainer = new ArgumentContainer(args);
        final String sectionedWaysPath = argumentContainer.getRequired("way_sections_parquet");
        final String relationsPath = argumentContainer.getRequired("relations_parquet");
        final String outputPath = argumentContainer.getRequired("output_dir");
        final String logDir = argumentContainer.getOptional("log_dir", null);
        final String runLocal = argumentContainer.getOptional("run_local", "false");
        // In order to obtain helper info:
        final String fcFunctionalityEnabled = argumentContainer.getOptional("fc_functionality_enabled", "false");
        final String saveWaySectionsWithFunctionalClass = argumentContainer.getOptional("save_ws_with_fc", "false");
        final String logCorruptedWaySections = argumentContainer.getOptional("log_corrupted_way_sections", "true");
        final String saveWaySectionsWithTypeLanes = argumentContainer.getOptional("save_ws_with_lanes_type", "false");

        try(final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Adapter Job")
                .runLocal(Boolean.parseBoolean(runLocal))
                .build()) {

            final JavaSparkContext javaSparkContext = jobInitializer.javaSparkContext();

            final JavaRDD<Relation> relations = RddReader.RelationReader.read(javaSparkContext, relationsPath);

            final JavaRDD<RawRelation> rawRelations = RddReader.RawRelationReader.read(javaSparkContext, relationsPath);
            final Map<Long, String> wayIdToAdminLevel = rawRelations.flatMapToPair(new WayAdminLevelExtractor())
                    .collectAsMap();
            final Map<Long, String> broadcastedWayIdToAdminLevel = javaSparkContext.broadcast(wayIdToAdminLevel)
                    .value();

            final JavaRDD<WaySection> waySections = RddReader.WaySectionReader.read(javaSparkContext, sectionedWaysPath);

            final JavaRDD<WaySection> adaptedWaySections = new DoubleDigitizedRoadRule(javaSparkContext).enhance(waySections);

            final JavaRDD<WaySection> sectionedWays = adaptedWaySections
                    .map(FcService::fixIso)
                    .cache();

            final Boolean fcFunctionality = Boolean.parseBoolean(fcFunctionalityEnabled);

            Map<Long, List<String>> wayIdsToIsoFc = null;

            if (fcFunctionality) {
                wayIdsToIsoFc = FcService.wayIdsToIsoFc(relations,
                        sectionedWays,
                        javaSparkContext);
            }

            CollectionAccumulator<String> invalidLaneValues = null;
            CollectionAccumulator<Long> invalidTurnLanesWayIds = null;
            if (Boolean.parseBoolean(logCorruptedWaySections)) {
                final SparkContext sparkContext = javaSparkContext.sc();
                invalidLaneValues = sparkContext.collectionAccumulator("invalidLaneValues");
                invalidTurnLanesWayIds = sparkContext.collectionAccumulator("invalidTurnLanesWayIds");
            }

            JavaRDD<WaySection> adaptedSectionedWays =
                    new NgxWaySectionsAdapterApplication(getSpeedLimitByUsStates(relations),
                            getSpeedLimitsEU(),
                            broadcastedWayIdToAdminLevel)
                            .fcFunctionality(fcFunctionality)
                            .wayIdsToIsoFc(wayIdsToIsoFc)
                            .invalidLaneValues(invalidLaneValues)
                            .invalidTurnLanesWayIds(invalidTurnLanesWayIds)
                            .adapt(sectionedWays)
                            .persist(StorageLevel.MEMORY_ONLY());

            if (fcFunctionality) {
                if (Boolean.parseBoolean(saveWaySectionsWithFunctionalClass)) {
                    saveWaySectionsWithFcClass(adaptedSectionedWays, outputPath);
                }
                adaptedSectionedWays =
                        FcLinkService.assignFcToLinks(adaptedSectionedWays, javaSparkContext);
            }

            RddWriter.WaySectionWriter.write(javaSparkContext,
                    adaptedSectionedWays,
                    outputPath + "/way_sections/parquet"
            );

            if (Boolean.parseBoolean(logCorruptedWaySections)) {
                javaSparkContext.parallelize(invalidLaneValues.value().stream().distinct().sorted().collect(Collectors.toList()))
                        .coalesce(1)
                        .saveAsTextFile(outputPath + "/way_sections/invalid-lane-values");
                javaSparkContext.parallelize(invalidTurnLanesWayIds.value().stream().distinct().sorted().collect(Collectors.toList()))
                        .coalesce(1)
                        .saveAsTextFile(outputPath + "/way_sections/invalid-turn-lanes-way-ids");
            }

            if (Boolean.parseBoolean(saveWaySectionsWithTypeLanes)) {
                saveWaySectionsWithTypeLanes(adaptedSectionedWays, outputPath);
            }

            PipelineValidatorRunner.builder()
                    .waySectionsPath(outputPath + "/way_sections/parquet")
                    .sourceJobName(NgxWaySectionsAdapterJob.class.getSimpleName())
                    .reportPath(outputPath + "/validation_report")
                    .runLocal(Boolean.parseBoolean(runLocal))
                    .build()
                    .run();
        }
    }

    private static void saveWaySectionsWithFcClass(final JavaRDD<WaySection> adaptedSectionedWays,
                                                   final String outputPath) {
        adaptedSectionedWays.filter(ws -> ws.tags().containsKey(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap()))
                .mapToPair(ws -> {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(ws.strictIdentifier().wayId())
                            .append(",")
                            .append(ws.tags().get(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap()));
                    return new Tuple2<Long, String>(ws.strictIdentifier().wayId(), sb.toString());
                })
                .sortByKey(true)
                .coalesce(1)
                .map(tuple -> tuple._2())
                .saveAsTextFile(outputPath + "/way_sections/csv/fc");
    }

    private static void saveWaySectionsWithTypeLanes(final JavaRDD<WaySection> adaptedSectionedWays,
                                                     final String outputPath) {
        adaptedSectionedWays.filter(ws -> ws.tags().containsKey(TYPE_LANES))
                .mapToPair(ws -> {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("type:lanes :" + ws.tags().get(TYPE_LANES));
                    if (ws.tags().containsKey(TURN_LANES)) {
                        sb.append(" " + TURN_LANES + " ").append(ws.tags().get(TURN_LANES));
                    }
                    if (ws.tags().containsKey("turn:lanes:forward")) {
                        sb.append(" " + "turn:lanes:forward" + " ").append(ws.tags().get("turn:lanes:forward"));
                    }
                    if (ws.tags().containsKey("turn:lanes:backward")) {
                        sb.append(" " + "turn:lanes:backward" + " ").append(ws.tags().get("turn:lanes:backward"));
                    }
                    return new Tuple2<Long, String>(ws.strictIdentifier().wayId(), sb.toString());
                })
                .sortByKey(true)
                .map(tuple -> tuple._2())
                .saveAsTextFile(outputPath + "/way_sections/text/turn-lanes");
    }

    // Map of mas speed for US states
    // Relation id (US state), max speed in mph
    private static Map<Long, Integer> getSpeedLimitByUsStates(final JavaRDD<Relation> relations)
            throws IOException {
        Map<String, String> speedLimitRange = ResourceRepository.getSpeedLimitRange();
        return new HashMap<>(
                relations
                        .filter(relation -> relation.getTags().containsKey("iso")
                                && relation.getTags().get("iso").equals("USA"))
                        .filter(relation -> relation.getTags().containsKey("admin_level")
                                && relation.getTags().get("admin_level").equals("L2"))
                        .filter(relation -> relation.getTags().containsKey("name:eng"))
                        .mapToPair(relation -> new Tuple2<>(relation.getId(),
                                Integer.getInteger(speedLimitRange.get(relation.getTags().get("name:eng")))))
                        .collectAsMap());
    }
}