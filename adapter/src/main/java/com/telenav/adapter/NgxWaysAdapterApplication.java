package com.telenav.adapter;

import com.telenav.adapter.component.extractors.*;
import com.telenav.adapter.component.extractors.adminTags.*;
import com.telenav.adapter.component.mapper.OpenStreetMapToUniDbBridgeMapper;
import com.telenav.adapter.rules.*;
import com.telenav.adapter.rules.whitelist.NgxWhiteList;
import com.telenav.datapipelinecore.enhancer.ChainedEnhancer;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.map.entity.RawWay;
import com.telenav.relationAdapter.rules.HeightFixerRule;
import org.apache.spark.api.java.JavaRDD;


/**
 * @author dianat2
 */
public class NgxWaysAdapterApplication {

    public JavaRDD<RawWay> adapt(final JavaRDD<RawWay> ways) {

        final Enhancer<RawWay> adapterEnhancer =
                new ChainedEnhancer.Builder<RawWay>(new TollRule<>(new TollExtractor()))
                        .enhancer(new AdminTagsRule<>(
                                new AdminL1LeftExtractor(),
                                new AdminL1RightExtractor(),
                                new AdminL2LeftExtractor(),
                                new AdminL2RightExtractor(),
                                new AdminL3LeftExtractor(),
                                new AdminL3RightExtractor()))
                        .enhancer(new MotorCarRule<>())
                        .enhancer(new BridgeRule<>(new BridgeExtractor(), new OpenStreetMapToUniDbBridgeMapper()))
                        .enhancer(new AccessPedsRule<>(new FootExtractor()))
                        .enhancer(new HighwayWayFixerRule(new HighwayExtractor()))
                        .enhancer(new NameRule<>()) //needs to be before language tag rule (not strictly)
                        .enhancer(new LanguageTagRule<>(new LanguageExtractor()))
                        .enhancer(new CountryIsoTagRule<>(new CountryIsoExtractor()))
                        .enhancer(new TimezoneTagRule<>(new TimezoneExtractor()))
                        .enhancer(new HeightFixerRule<>())
                        .enhancer(new CartoLineRule<>())    //should come before railway cleanup
                        .enhancer(new RiverFixerRule<>())
                        .enhancer(new RailwayCleanupRule<>()) // do not add rt, rst, fc, sc tags after this rule
                        .enhancer(new CleanNavigableTagRule())
                        .enhancer(new CleanupRule<>(new NgxWhiteList()))
                        .enhancer(new CleanRouteRule<>(new RouteExtractor()))
                        .build();
        return ways.map(adapterEnhancer::apply);
    }

}