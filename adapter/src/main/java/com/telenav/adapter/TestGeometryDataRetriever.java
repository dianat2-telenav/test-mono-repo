package com.telenav.adapter;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.ost.ArgumentContainer;
import com.telenav.relationenhancer.core.dto.GeometryCollection;
import com.telenav.relationenhancer.core.utils.RegionUtil;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;


public class TestGeometryDataRetriever {

    public static void main(String[] args) {

        final ArgumentContainer argumentContainer = new ArgumentContainer(args);
        final String regionGeometryPath = argumentContainer.getRequired("region_geometry_object");
        final String outputPath = argumentContainer.getRequired("output_dir");
        final String runLocal = argumentContainer.getOptional("run_local", "false");

        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Adapter Job")
                .runLocal(Boolean.valueOf(runLocal))
                .build();

        final JavaSparkContext sparkContext = jobInitializer.javaSparkContext();

        final JavaRDD<GeometryCollection> geometryCollections =
                RegionUtil.geometryCollectionRead(sparkContext, regionGeometryPath);

        final JavaRDD<GeometryCollection> testData =
                geometryCollections.filter(geometryCollection -> geometryCollection.getRelationId().longValue() == 396500L ||
                        geometryCollection.getRelationId().longValue() == 112150L);

        testData.saveAsObjectFile(outputPath);
    }
}
