package com.telenav.adapter.bean;

/**
 * @author liviuc
 */
public enum DrivingSide {

    LEFT,
    RIGHT
}
