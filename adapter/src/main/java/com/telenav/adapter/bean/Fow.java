package com.telenav.adapter.bean;

public enum Fow {

    UNDEFINED,
    MOTORWAY,
    MULTIPLE_CARRIAGEWAY,
    SINGLE_CARRIAGEWAY,
    ROUNDABOUT,
    TRAFFICSQUARE,
    SLIPROAD,
    OTHER
}
