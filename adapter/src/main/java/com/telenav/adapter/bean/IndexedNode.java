package com.telenav.adapter.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author liviuc
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndexedNode implements Serializable {

    private long nodeId;
    private long wayId;
    private int index;
}
