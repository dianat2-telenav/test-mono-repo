package com.telenav.adapter.bean;

/**
 * Enum containing all the FC categories.
 *
 * @author petrum
 */
public enum RoadFunctionalClass {

    FIRST_CLASS(1),
    SECOND_CLASS(2),
    THIRD_CLASS(3),
    FOURTH_CLASS(4),
    FIFTH_CLASS(5);

    /**
     * Retrun the numeric identifier by the FC Name
     *
     * @param roadFunctionalClass the type of the FC
     * @return The identifier for the given functional class
     */
    public static int identifierFor(final RoadFunctionalClass roadFunctionalClass) {
        return roadFunctionalClass.identifier;
    }

    private final int identifier;

    RoadFunctionalClass(final int identifier) {
        this.identifier = identifier;
    }
}
