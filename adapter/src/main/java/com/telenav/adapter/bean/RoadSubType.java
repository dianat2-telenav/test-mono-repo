package com.telenav.adapter.bean;

import java.util.HashMap;
import java.util.Map;


/**
 * Enum containing all the RST categories.
 *
 * @author petrum
 */
public enum RoadSubType {

    ROUNDABOUT(0),
    DEFAULT(1),
    MAIN_ROAD(2),
    CONNECTING_ROAD(3),
    INTERSECTION_LINK(4),
    RAMP(5),
    SERVICE(6),
    TUNNEL(11),
    BRIDGE(12);

    private static final Map<RoadSubType, Integer> PRIORITY = new HashMap<>();
    static {
        PRIORITY.put(DEFAULT, 1);
        PRIORITY.put(MAIN_ROAD, 2);
        PRIORITY.put(TUNNEL, 3);
        PRIORITY.put(BRIDGE, 4);
        PRIORITY.put(ROUNDABOUT, 5);
        PRIORITY.put(CONNECTING_ROAD, 6);
        PRIORITY.put(SERVICE, 7);
        PRIORITY.put(INTERSECTION_LINK, 8);
        PRIORITY.put(RAMP, 9);
    }

    /**
     * Get the road sub type numerical identifier
     *
     * @param subType the road subtype to retrieve the numerical identifier for
     * @return The identifier for the given road sub-type
     */
    public static int identifierFor(final RoadSubType subType) {
        return subType.identifier;
    }

    private final int identifier;

    RoadSubType(final int identifier) {
        this.identifier = identifier;
    }


    public static Integer priorityFor(final RoadSubType roadSubType) {
        return PRIORITY.get(roadSubType);
    }
}
