package com.telenav.adapter.bean;

/**
 * Enum containing all the SC categories. These categories are defined base on the speed allowed.
 *
 * @author petrum
 */
public enum SpeedCategory {

    SPEED_CAT_GREATER_THAN_130(1), // > 130 KPH
    SPEED_CAT_BETWEEN_101_130(2), // [101,130] KPH
    SPEED_CAT_BETWEEN_91_100(3), // [91,100] KPH
    SPEED_CAT_BETWEEN_71_90(5), // [71,90] KPH
    SPEED_CAT_BETWEEN_51_70(7), // [51,70] KPH
    SPEED_CAT_BETWEEN_31_50(11), // [31,50] KPH
    SPEED_CAT_BETWEEN_11_30(14), // [11,30] KPH
    SPEED_CAT_LESS_THAN_11(15); // < 11 KPH

    private final int identifier;


    SpeedCategory(final int identifier) {
        this.identifier = identifier;
    }


    public static int identifierFor(final SpeedCategory speedCategory) {
        return speedCategory.identifier;
    }

    /**
     * Calculate the speed category for a given speed.
     *
     * @param speed Speed in km/h
     * @return The speed category of the road.
     */
    public static SpeedCategory getSpeedCategoryForSpeed(final double speed) {
        SpeedCategory speedCategory;
        if (speed > 130) {
            speedCategory = SPEED_CAT_GREATER_THAN_130;
        } else if (speed >= 101 && speed <= 130) {
            speedCategory = SPEED_CAT_BETWEEN_101_130;
        } else if (speed >= 91 && speed <= 100) {
            speedCategory = SPEED_CAT_BETWEEN_91_100;
        } else if (speed >= 71 && speed <= 90) {
            speedCategory = SPEED_CAT_BETWEEN_71_90;
        } else if (speed >= 51 && speed <= 70) {
            speedCategory = SPEED_CAT_BETWEEN_51_70;
        } else if (speed >= 31 && speed <= 50) {
            speedCategory = SPEED_CAT_BETWEEN_31_50;
        } else if (speed >= 11 && speed <= 30) {
            speedCategory = SPEED_CAT_BETWEEN_11_30;
        } else {
            speedCategory = SPEED_CAT_LESS_THAN_11;
        }
        return speedCategory;
    }
}
