package com.telenav.adapter.bean;

/**
 * @author roxanal
 */
public class Tag {

    private final String key;
    private final String value;

    public Tag(final String key, final String value) {
        this.key = key;
        this.value = value;
    }

    public String key() {
        return key;
    }

    public String value() {
        return value;
    }
}