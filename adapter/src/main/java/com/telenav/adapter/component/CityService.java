package com.telenav.adapter.component;

import com.telenav.adapter.entity.dto.MemberRelation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author liviuc
 */
public class CityService {

    private static final Set<String> CITY_ADMIN_LEVEL = new HashSet<>(Arrays.asList("6"));

    public static Boolean isCityMemberRelation(final MemberRelation memberRelation) {
        return CITY_ADMIN_LEVEL.contains(memberRelation.getRelationTags().get("admin_level"));
    }
}
