package com.telenav.adapter.component;

import com.telenav.adapter.bean.IndexedNode;
import com.telenav.adapter.func.NodeReferenceFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;

import java.util.List;

import static org.apache.spark.sql.functions.*;

/**
 * Class that indexes nodes to their corresponding raw-ways.
 * I.e. returns data identifying the node, it's corresponding raw-way and the index of the node withing the way.
 *
 * @author liviuc
 */
public class ComposerDataset {

    private final boolean failOnMissingNode;

    public ComposerDataset(boolean failOnMissingNode) {
        this.failOnMissingNode = failOnMissingNode;
    }

    /**
     * Indexes nodes by their correspoing raw-ways.
     *
     * @param rawWays with schema:
     *  root
     *  |-- id: long (nullable = true)
     *  |-- tags: array (nullable = true)
     *  |    |-- element: struct (containsNull = true)
     *  |    |    |-- key: string (nullable = true)
     *  |    |    |-- value: string (nullable = true)
     *  |-- nodes: array (nullable = true)
     *  |    |-- element: struct (containsNull = true)
     *  |    |    |-- index: integer (nullable = true)
     *  |    |    |-- nodeId: long (nullable = true)
     * @param nodes with schema:
     *  root
     *  |-- WAY_ID: long (nullable = true)
     *  |-- SEQUENCE_NUMBER: short (nullable = true)
     *  |-- TAGS: map (nullable = true)
     *  |    |-- key: string
     *  |    |-- value: string (valueContainsNull = true)
     *  |-- NODES: array (nullable = true)
     *  |    |-- element: struct (containsNull = true)
     *  |    |    |-- ID: long (nullable = true)
     *  |    |    |-- TAGS: map (nullable = true)
     *  |    |    |    |-- key: string
     *  |    |    |    |-- value: string (valueContainsNull = true)
     *  |    |    |-- LATITUDE: double (nullable = true)
     *  |    |    |-- LONGITUDE: double (nullable = true)
     * @return indexed nodes.
     */
    public Dataset<Row> compose(final Dataset<Row> rawWays,
                                final Dataset<Row> nodes) {

        final Dataset<Row> indexedNodesByWayIds = wayNodesByWayId(rawWays, nodes);
        return rawWays.join(indexedNodesByWayIds,
                indexedNodesByWayIds.col("wayId").equalTo(rawWays.col("id")))
                .withColumnRenamed("tags", "wayTags")
                .drop("id")
                .drop("nodes");
    }

    private Dataset<Row> wayNodesByWayId(final Dataset<Row> rawWays,
                                         final Dataset<Row> nodes) {

        final Dataset<IndexedNode> indexedNodes =
                rawWays.flatMap(IndexedNodeMapper::map, Encoders.bean(IndexedNode.class));

        Dataset<Row> nodesWithIndexedNodes =
                indexedNodes.join(nodes,
                        nodes.col("id").equalTo(indexedNodes.col("nodeId")),
                        "left_outer");

        nodesWithIndexedNodes = nodesWithIndexedNodes
                .map(new NodeReferenceFunction(failOnMissingNode), RowEncoder.apply(nodesWithIndexedNodes.schema()));

        return nodesWithIndexedNodes
                .groupBy("wayId")
                .agg(collect_list(struct(
                        col("id"),
                        col("tags"),
                        col("latitude"),
                        col("longitude"),
                        col("index"))).as("indexedNodes"));
    }
}
