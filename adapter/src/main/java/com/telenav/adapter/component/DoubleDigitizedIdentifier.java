package com.telenav.adapter.component;

import com.telenav.map.entity.WaySection;
import com.telenav.adapter.bean.TwoPointWaySection;
import com.telenav.adapter.func.DoubleDigitizedWaySectionIdPair;
import com.telenav.adapter.func.DoubleDigitizedWaySectionId;
import com.telenav.adapter.func.WaySectionToTwoPointWaySectionWithKey;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *  Identifies way section ids corresponding to rst=2 (= double digitized).
 *  Identifies way id pairs corresponding to rst=2 (= double digitized).
 *
 *  Rules for double digitized roads:
 *      - they are parallel
 *      - same name (some exceptions)
 *      - same highway type
 *      - oneway (mandatory)
 */
public class DoubleDigitizedIdentifier implements Serializable {

    private static final Function<TwoPointWaySection, List<TwoPointWaySection>> ACC = twoPointWaySection -> {
        final List<TwoPointWaySection> twoPointWaySections = new ArrayList<>();
        twoPointWaySections.add(twoPointWaySection);
        return twoPointWaySections;
    };
    private static final Function2<List<TwoPointWaySection>, TwoPointWaySection, List<TwoPointWaySection>> ADD
            = (list, twoPointWaySection) -> {
        list.add(twoPointWaySection);
        return list;
    };
    private static final Function2<List<TwoPointWaySection>, List<TwoPointWaySection>, List<TwoPointWaySection>> MERGE =
            (left, right) -> {
                left.addAll(right);
                return left;
            };
    private final JavaRDD<WaySection> waySections;

    public DoubleDigitizedIdentifier(final JavaRDD<WaySection> waySections) {
        this.waySections = waySections;
    }

    /**
     * Flat maps the way section to pairs made out of two point way sections and a descriptive-double-digitized-roads-condition key.
     * Combines the two point way sections by key.
     * Provides ids of double digitized way sections.
     *
     * @return Identifies way section ids corresponding to rst=2 (= double digitized).
     */
    public JavaRDD<Long> get() {
        return waySections
                .flatMapToPair(new WaySectionToTwoPointWaySectionWithKey())
                .combineByKey(ACC, ADD, MERGE)
                .flatMap(new DoubleDigitizedWaySectionId());
    }

    /**
     * Flat maps the way section to pairs made out of two point way sections and a descriptive-double-digitized-roads-condition key.
     * Combines the two point way sections by key.
     * Provides pair ids of way ids of double digitized ways.
     *
     *  @return Identifies way id pairs corresponding to rst=2 (= double digitized).
     */
    public JavaPairRDD<Long, Long> getPaired() {
        return waySections
                .flatMapToPair(new WaySectionToTwoPointWaySectionWithKey())
                .combineByKey(ACC, ADD, MERGE)
                .flatMapToPair(new DoubleDigitizedWaySectionIdPair());
    }
}