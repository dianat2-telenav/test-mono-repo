package com.telenav.adapter.component;

import com.telenav.map.entity.Node;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import com.telenav.map.geometry.Point;
import com.telenav.map.geometry.PolyLine;
import com.telenav.adapter.bean.TwoPointWaySection;
import scala.Tuple2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class DoubleDigitizedWaySectionHelper {

    private Tuple2<String, List<TwoPointWaySection>> tuple;
    private Map<Long, Long> countedTwoPointWaySections;
    private Map<TwoPointWaySection, Tuple2<TwoPointWaySection, Double>> minimumDistanceMap;
    private static final double MAXIMUM_DISTANCE_BETWEEN_DOUBLE_DIGITIZED_ROADS_M = 10d;

    public DoubleDigitizedWaySectionHelper(final Tuple2<String, List<TwoPointWaySection>> tuple) {
        this.tuple = tuple;
    }

    public void compute() {

        final List<TwoPointWaySection> twoPointWaySections = tuple._2();

        this.minimumDistanceMap = minimumDistanceMap(twoPointWaySections);

        //count mini way section belonging to the same way section
        this.countedTwoPointWaySections = twoPointWaySections.stream()
                .map(twoPointWaySection -> twoPointWaySection.getWaySectionId())
                .collect(groupingBy(java.util.function.Function.identity(), counting()));
    }

    /**
     *
     * @param twoPointWaySections A list of objects of type TwoPointWaySection.
     * @return A map consisting of two point way section to it's corresponding double digitized two point way section,
     * based on minimum distance between them.
     */
    public Map<TwoPointWaySection, Tuple2<TwoPointWaySection, Double>>
    minimumDistanceMap(final List<TwoPointWaySection> twoPointWaySections) {
        final Map<TwoPointWaySection, Tuple2<TwoPointWaySection, Double>> minimumDistanceMap = new HashMap<>();

        for (TwoPointWaySection left : twoPointWaySections) {
            for (TwoPointWaySection right : twoPointWaySections) {
                if (!validForComparing(left, right)) {
                    continue;
                }

                final double distanceBetweenSections =
                        distanceBetweenSections(left, right);
                if (distanceBetweenSections > MAXIMUM_DISTANCE_BETWEEN_DOUBLE_DIGITIZED_ROADS_M) {
                    continue;
                }

                /*
                    Just a way of organizing our distance map such that
                */
                final TwoPointWaySection minimumWaySection =
                        (left.getWaySectionId() < right.getWaySectionId()) ? left : right;
                final TwoPointWaySection maximumWaySection =
                        (minimumWaySection == left) ?
                                right : left;

                put(minimumDistanceMap, minimumWaySection, maximumWaySection, distanceBetweenSections);
            }
        }

        return minimumDistanceMap;
    }

    public Map<String, Long> getFirstToSecondPairWaySectionToWaySectionCount() {
        return minimumDistanceMap.entrySet()
                .stream()
                .map(entry -> entry.getKey().getWaySectionId() +
                        ":" +
                        entry.getValue()._1().getWaySectionId())
                .collect(groupingBy(java.util.function.Function.identity(), counting()));
    }

    public Map<String, Long> getSecondToFirstWaySectionFromPairToWaySectionCount() {
        return minimumDistanceMap.entrySet()
                .stream()
                .map(entry -> entry.getValue()._1().getWaySectionId() +
                        ":" +
                        entry.getKey().getWaySectionId())
                .collect(groupingBy(Function.identity(), counting()));
    }

    /**
     * @param left TwoPointWaySection.
     * @param right TwoPointWaySection.
     * @return distance between the mid points of the two point way sections.
     */
    private double distanceBetweenSections(final TwoPointWaySection left,
                                           final TwoPointWaySection right) {
        return new PolyLine(Arrays.asList(midPoint(left), midPoint(right)))
                .length()
                .asMeters();
    }

    /**
     * Determines validity of being double digitized by with respect to:
     *  - not belonging to the same way (check done via way id)
     *  - having different moving directions
     *  - NOT having any nodes in common
     *
     * @param left two point way section.
     * @param right two point way section.,
     * @return validity for being double digitized together.
     */
    private boolean validForComparing(final TwoPointWaySection left,
                                      final TwoPointWaySection right) {
        return left.getWayId().longValue() != right.getWayId().longValue() &&
                differentMovingDirections(left, right) &&
                noCommonNodes(left, right);
    }

    /**
     * Puts the two point way section to its corresponding double digitized two point way section.
     * The correspondence is based on distance. When multiple possibilities exists, the minimum distance
     * one is picked up.
     *
     * @param minimumDistanceMap .
     * @param minimumWaySection TwoPointWaySection.
     * @param maximumWaySection TwoPointWaySection.
     * @param distanceBetweenSections .
     */
    private void put(final Map<TwoPointWaySection, Tuple2<TwoPointWaySection, Double>> minimumDistanceMap,
                     final TwoPointWaySection minimumWaySection,
                     final TwoPointWaySection maximumWaySection,
                     final double distanceBetweenSections) {
        if (!minimumDistanceMap.containsKey(minimumWaySection)) {
            minimumDistanceMap.put(minimumWaySection,
                    new Tuple2<TwoPointWaySection, Double>(maximumWaySection, distanceBetweenSections));
        } else {
            final Double distance = minimumDistanceMap.get(minimumWaySection)._2();
            if (distanceBetweenSections < distance) {
                minimumDistanceMap.remove(minimumWaySection)._2();
            }
            minimumDistanceMap.put(minimumWaySection,
                    new Tuple2<TwoPointWaySection, Double>(maximumWaySection, distanceBetweenSections));
        }
    }

    /**
     * No description shall be provided.
     * The method's title speaks for itself :)
     *
     * @param left TwoPointWaySection.
     * @param right TwoPointWaySection.
     * @return true if they have NO common nodes.
     */
    private boolean noCommonNodes(final TwoPointWaySection left,
                                  final TwoPointWaySection right) {

        final Node leftFromNode = left.getFromNode();
        final Node leftToNode = left.getToNode();

        final Node rightFromNode = right.getFromNode();
        final Node rightToNode = right.getToNode();

        return (leftToNode.id() != rightFromNode.id()) &&
                (rightToNode.id() != leftFromNode.id()) &&
                (leftToNode.id() != rightToNode.id()) &&
                (leftFromNode.id() != rightFromNode.id());
    }

    private Point midPoint(final TwoPointWaySection twoPointWaySection) {
        return new Point(
                Latitude.degrees(midLat(twoPointWaySection)),
                Longitude.degrees(midLon(twoPointWaySection))
        );
    }

    private double midLat(final TwoPointWaySection twoPointWaySection) {
        return (twoPointWaySection.getToNode().latitude().asDegrees() +
                twoPointWaySection.getFromNode().latitude().asDegrees()) / 2;
    }

    private double midLon(final TwoPointWaySection twoPointWaySection) {
        return (twoPointWaySection.getToNode().longitude().asDegrees() +
                twoPointWaySection.getFromNode().longitude().asDegrees()) / 2;
    }

    /**
     * Determines if two two point way sections have different moving direction.
     * This is done by determining the direction between the from and to nodes
     * corresponding between of the two point way section.
     *
     * @param left TwoPointWaySection
     * @param right TwoPointWaySection
     * @return true if the two point way sections have a different direction of motion.
     */
    private boolean differentMovingDirections(final TwoPointWaySection left,
                                              final TwoPointWaySection right) {

        final Latitude leftFromLatitude = left.getFromNode().latitude();
        final Latitude leftToLatitude = left.getToNode().latitude();

        final Longitude leftFromLongitude = left.getFromNode().longitude();
        final Longitude leftToLongitude = left.getToNode().longitude();

        final Latitude rightFromLatitude = right.getFromNode().latitude();
        final Latitude rightToLatitude = right.getToNode().latitude();

        final Longitude rightFromLongitude = right.getFromNode().longitude();
        final Longitude rightToLongitude = right.getToNode().longitude();

        boolean leftFromSouthToNorth = (leftToLatitude.asDegrees() - leftFromLatitude.asDegrees()) > 0;
        boolean leftFromWestToEast = (leftToLongitude.asDegrees() - leftFromLongitude.asDegrees()) > 0;

        boolean rightFromSouthToNorth = (rightToLatitude.asDegrees() - rightFromLatitude.asDegrees()) > 0;
        boolean rightFromWestToEast = (rightToLongitude.asDegrees() - rightFromLongitude.asDegrees()) > 0;

        return !(leftFromWestToEast && rightFromWestToEast && leftFromSouthToNorth && rightFromSouthToNorth);
    }

    public Map<Long, Long> getCountedTwoPointWaySections() {
        return countedTwoPointWaySections;
    }
}
