package com.telenav.adapter.component;

import com.telenav.ost.spark.storage.Column;
import com.telenav.ost.spark.storage.Schema;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;

import java.util.stream.Stream;

import static org.apache.spark.sql.types.DataTypes.LongType;

/**
 * @author liviuc
 */
public class NodeIdMapper implements MapFunction<Long, Row> {

    public static final NodeIdSchema NODE_ID_SCHEMA = new NodeIdSchema();
    public static final ExpressionEncoder<Row> ROW_ENCODER =
            RowEncoder.apply(NodeIdMapper.NODE_ID_SCHEMA.asStructType());

    @Override
    public Row call(final Long nodeId) {
        return RowFactory.create(new Object[]{nodeId});
    }

    static class NodeIdSchema extends Schema {

        @Override
        protected Stream<Column> columns() {
            return Stream.of(new Column("nodeId", LongType, false));
        }
    }
}
