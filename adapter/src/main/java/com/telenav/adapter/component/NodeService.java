package com.telenav.adapter.component;

import com.telenav.adapter.component.fc.MultipleNodeTypeFunction;
import com.telenav.adapter.component.fc.NodeTypeFromWayFunction;
import com.telenav.adapter.component.fc.NodeTypeFromWaySectionFunction;
import com.telenav.adapter.component.fc.NodeTypeFunction;
import lombok.Getter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.util.CollectionAccumulator;

import static com.telenav.adapter.utils.OsmInfo.NODE_MEMBER_TYPE;
import static org.apache.spark.sql.functions.*;

/**
 * Service responsible with nodes related operations
 *
 * @author liviuc
 */
@Getter
public class NodeService {

    protected final CollectionAccumulator<String> nodeTypeCollisionLog;

    public NodeService(final SparkSession sparkSession) {
        this.nodeTypeCollisionLog =
                sparkSession.sparkContext().collectionAccumulator("nodeTypeCollisionLog");
    }

    /**
     * @param memberType .
     * @return true if the member type is a identified as node.
     */
    public static boolean isNodeMemberType(final String memberType) {
        return NODE_MEMBER_TYPE.equalsIgnoreCase(memberType);
    }

    /**
     * @param nodesDs                 nodes dataset with the following schema:
     *                                root
     *                                |-- ID: long (nullable = true)
     *                                |-- TAGS: array (nullable = true)
     *                                |    |-- element: struct (containsNull = true)
     *                                |    |    |-- key: string (nullable = true)
     *                                |    |    |-- value: string (nullable = true)
     *                                |-- LATITUDE: double (nullable = true)
     *                                |-- LONGITUDE: double (nullable = true)
     * @param nodeCityCenterRelations node member relations with the following schema:
     *                                root
     *                                |-- nodeId: long (nullable = false)
     *                                |-- memberRelations: array (nullable = true)
     *                                |    |-- element: struct (containsNull = true)
     *                                |    |    |-- relationId: long (nullable = false)
     *                                |    |    |-- tags: array (nullable = false)
     *                                |    |    |    |-- element: struct (containsNull = false)
     *                                |    |    |    |    |-- key: string (nullable = false)
     *                                |    |    |    |    |-- value: string (nullable = true)
     *                                |    |    |-- member: struct (nullable = false)
     *                                |    |    |    |-- id: long (nullable = false)
     *                                |    |    |    |-- role: string (nullable = false)
     *                                |    |    |    |-- type: string (nullable = false)
     * @return joined node with city center relations with the following schema:
     * root
     * |-- id: long (nullable = true)
     * |-- latitude: double (nullable = true)
     * |-- longitude: double (nullable = true)
     * |-- nodeTags: map (nullable = true)
     * |    |-- key: string
     * |    |-- value: string (valueContainsNull = true)
     * |-- memberRelations: array (nullable = true)
     * |    |-- element: array (containsNull = true)
     * |    |    |-- element: struct (containsNull = true)
     * |    |    |    |-- relationId: long (nullable = false)
     * |    |    |    |-- tags: array (nullable = false)
     * |    |    |    |    |-- element: struct (containsNull = false)
     * |    |    |    |    |    |-- key: string (nullable = false)
     * |    |    |    |    |    |-- value: string (nullable = true)
     * |    |    |    |-- member: struct (nullable = false)
     * |    |    |    |    |-- id: long (nullable = false)
     * |    |    |    |    |-- role: string (nullable = false)
     * |    |    |    |    |-- type: string (nullable = false)
     */
    public static Dataset<Row> joinNodesToCityCenterRelations(final Dataset<Row> nodesDs,
                                                              final Dataset<Row> nodeCityCenterRelations) {
        return nodesDs.join(nodeCityCenterRelations,
                nodesDs.col("id").equalTo(nodeCityCenterRelations.col("nodeId")),
                "left_outer")
                .groupBy("id")
                .agg(first("tags").as("nodeTags"),
                        first("latitude").as("latitude"),
                        first("longitude").as("longitude"),
                        collect_list("memberRelations").as("memberRelations"));
    }

    private Dataset<Row> navnTypeEnhancementNodeData(final Dataset<Row> waySections,
                                                     final Dataset<Row> ways) {
        final ExpressionEncoder<Row> encoder =
                RowEncoder.apply(new NodeTypeFunction.NodeTypeSchema().asStructType());

        final Dataset<Row> nodeDataFromWays =
                ways.flatMap(new NodeTypeFromWayFunction(), encoder)
                        .dropDuplicates();

        final Dataset<Row> nodeDataFromWaySections =
                waySections.flatMap(new NodeTypeFromWaySectionFunction(), encoder)
                        .dropDuplicates();

        return nodeDataFromWaySections.union(nodeDataFromWays)
                .groupBy("nodeId")
                .agg(collect_list(
                        struct(col("nodeType"),
                                col("navn"))).as("nodeData"))
                .map(new MultipleNodeTypeFunction(), encoder);
    }

    /**
     * The current method receives datasets for the following entities:
     * - nodes
     * - way-sections (navigable ways)
     * - raw-ways (non-navigable ways)
     *
     * @param nodes       with schema:
     *                    root
     *                    |-- id: long (nullable = true)
     *                    |-- latitude: double (nullable = true)
     *                    |-- longitude: double (nullable = true)
     *                    |-- tags: array (nullable = true)
     *                    |    |-- element: struct (containsNull = true)
     *                    |    |    |-- key: string (nullable = true)
     *                    |    |    |-- value: string (nullable = true)
     * @param waySections with schema:
     *                    root
     *                    |-- WAY_ID: long (nullable = true)
     *                    |-- SEQUENCE_NUMBER: short (nullable = true)
     *                    |-- TAGS: map (nullable = true)
     *                    |    |-- key: string
     *                    |    |-- value: string (valueContainsNull = true)
     *                    |-- NODES: array (nullable = true)
     *                    |    |-- element: struct (containsNull = true)
     *                    |    |    |-- ID: long (nullable = true)
     *                    |    |    |-- TAGS: map (nullable = true)
     *                    |    |    |    |-- key: string
     *                    |    |    |    |-- value: string (valueContainsNull = true)
     *                    |    |    |-- LATITUDE: double (nullable = true)
     *                    |    |    |-- LONGITUDE: double (nullable = true)
     *                    root
     *                    |-- id: long (nullable = true)
     *                    |-- tags: array (nullable = true)
     *                    |    |-- element: struct (containsNull = true)
     *                    |    |    |-- key: string (nullable = true)
     *                    |    |    |-- value: string (nullable = true)
     *                    |-- nodes: array (nullable = true)
     *                    |    |-- element: struct (containsNull = true)
     *                    |    |    |-- index: integer (nullable = true)
     *                    |    |    |-- nodeId: long (nullable = true)
     * @param ways        ways with schema:
     *                    root
     *                    |-- id: long (nullable = false)
     *                    |-- tags: array (nullable = false)
     *                    |    |-- element: struct (containsNull = false)
     *                    |    |    |-- key: string (nullable = false)
     *                    |    |    |-- value: string (nullable = true)
     *                    |-- nodes: array (nullable = false)
     *                    |    |-- element: struct (containsNull = false)
     *                    |    |    |-- index: integer (nullable = false)
     *                    |    |    |-- nodeId: long (nullable = false)
     * @return dataset of rows
     */
    public Dataset<Row> enhanceNodesFromWaysSections(final Dataset<Row> nodes,
                                                     final Dataset<Row> waySections,
                                                     final Dataset<Row> ways) {
        final Dataset<Row> navnNodes = navnTypeEnhancementNodeData(waySections, ways);
        return join(nodes, navnNodes);
    }

    /**
     * This method enhances nodes with the unidb 'navn' key and 'type':'shape_point'.
     *
     * @param nodes       .
     * @param shapePoints .
     * @return nodes enhanced with the unidb 'navn' key and 'type':'shape_point'.
     */
    private Dataset<Row> join(final Dataset<Row> nodes,
                              final Dataset<Row> shapePoints) {
        final ExpressionEncoder<Row> nodesEncoder = RowEncoder.apply(nodes.schema());
        return nodes.join(shapePoints,
                shapePoints.col("nodeId").equalTo(nodes.col("id")),
                "left_outer")
                .map(new ShapePointEnhancer(nodeTypeCollisionLog), nodesEncoder);
    }
}
