package com.telenav.adapter.component;

import com.telenav.adapter.func.RelationWithIsoEnhancementFunc;
import com.telenav.datapipelinecore.bean.Member;
import com.telenav.datapipelinecore.bean.Relation;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.telenav.adapter.component.fc.FcExemptionsWhiteList.*;
import static com.telenav.adapter.component.fc.FcInfo.FcRelatedRelationConditions.NETWORK_RELATION_CONDITONS;
import static com.telenav.adapter.component.fc.IsoFcTagService.ISO_FC_TAG;
import static com.telenav.adapter.component.fc.TagService.FC_TAG;

/**
 * @author liviuc .
 */
public class RelationService {
    /*
        Helper functions.
     */
    private static final Function<String, List<String>> ACC = str -> {
        final List<String> strs = new LinkedList<>();
        strs.add(str);
        return strs;
    };
    private static final Function2<List<String>, String, List<String>> ADD = (list, str) -> {
        list.add(str);
        return list;
    };
    private static final Function2<List<String>, List<String>, List<String>> MERGE = (left, right) -> {
        left.addAll(right);
        return left.stream().distinct().collect(Collectors.toList());
    };

    private static Function<Relation, Boolean> ROUTE_RELATION_FILTER = relation -> NETWORK_RELATION_CONDITONS.isTrue(relation);
    private static Function<Relation, Boolean> HAS_ISO_FC_TAGS =
            relation -> relation.getTags().containsKey(FC_TAG)
                    //relation.getTags().containsKey(FC_TAG) && relation.getTags().containsKey("network")
            ;
    private static PairFlatMapFunction<Relation, Long, String> WAY_IDS_TO_ISO_FC = relation -> {

        final List<String> isoFcValues = isoFcValues(relation);

        return relation.getMembers()
                .stream()
                .filter(member -> "way".equalsIgnoreCase(member.getMemberType()))
                .flatMap(member -> memberIdToIsoFcs(member, isoFcValues))
                .iterator();
    };

    private static Stream<Tuple2<Long, String>> memberIdToIsoFcs(final Member member,
                                                                 final List<String> isoFcValues) {
        final List<Tuple2<Long, String>> res = new ArrayList<>();
        for (final String identifier : isoFcValues) {
            res.add(new Tuple2<Long, String>(member.getId(), identifier));
        }
        return res.stream();
    }

    private static List<String> isoFcValues(final Relation relation) {

        final Map<String, String> tags = relation.getTags();

        final List<String> fcRelevantValues = new ArrayList<>(2);

        for (final Map.Entry<String, String> entry : tags.entrySet()) {

            if (entry.getKey().equalsIgnoreCase("network") ||
                    entry.getKey().equalsIgnoreCase(FC_TAG)) {

                fcRelevantValues.add(entry.getValue());

            }
        }

        return fcRelevantValues;
    }

    public static JavaRDD<Relation> withFcData(final JavaRDD<Relation> relations,
                                               final Map<String, Map<String, String>> routeRelationsMappingBroadcast) {
        final JavaRDD<Relation> filter = relations
                .filter(ROUTE_RELATION_FILTER)
                //.filter(relation -> relation.getId() == 2332808)
                //.filter(relation -> relation.getTags().containsKey("name") && relation.getTags().get("name").contains("Highway 97"))
                .map(new RelationWithIsoEnhancementFunc(routeRelationsMappingBroadcast))
                .filter(HAS_ISO_FC_TAGS);
        return filter;
    }

    public static JavaPairRDD<Long, List<String>> wayIdsToNetworks(final JavaRDD<Relation> withIsoFc) {

        return withIsoFc.flatMapToPair(WAY_IDS_TO_ISO_FC)
                .distinct()
                .combineByKey(ACC, ADD, MERGE);
    }
}
