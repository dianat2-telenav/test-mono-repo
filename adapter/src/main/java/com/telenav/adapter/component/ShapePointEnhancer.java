package com.telenav.adapter.component;

import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.util.CollectionAccumulator;
import scala.collection.JavaConverters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static com.telenav.adapter.component.fc.NodeTypeFunction.NodeTypeSchema.*;

/**
 * @author liviuc
 */
public class ShapePointEnhancer implements MapFunction<Row, Row> {

    private final CollectionAccumulator<String> nodeTypeCollisionLog;

    public ShapePointEnhancer(final CollectionAccumulator<String> nodeTypeCollisionLog) {
        this.nodeTypeCollisionLog = nodeTypeCollisionLog;
    }

    @Override
    public Row call(final Row node) {

        if (hasNodeId(node)) {
            return enhanceNode(node);
        }

        return node;
    }

    private boolean hasNodeId(final Row node) {
        return node.apply(node.fieldIndex(NODE_ID.name())) != null;
    }

    private Row enhanceNode(final Row node) {

        final long nodeId = node.getLong(node.fieldIndex("id"));
        final String shapePointValue = node.getString(node.fieldIndex(NODE_TYPE.name()));
        final String navn = node.getString(node.fieldIndex(NAVN.name()));

        final Map<String, String> newTags = new TreeMap<>(); // order output
        final List<Row> tagRows = node.getList(node.fieldIndex("tags"));
        tagRows.forEach(tagRow -> newTags.put(tagRow.getString(0), tagRow.getString(1))); // key, value

        if (navn != null) {
            newTags.put(UnidbTagKey.NAVN.unwrap(), navn);
        }

        if (!newTags.containsKey(UnidbTagKey.TYPE.unwrap())) {
            newTags.put(UnidbTagKey.TYPE.unwrap(), shapePointValue); // never override existing type like 'city_center'
        } else {
            nodeTypeCollisionLog.add(nodeId + "," + newTags.get(UnidbTagKey.TYPE.unwrap()) + "," + shapePointValue);
        }

        final List<Row> newTagRows = new ArrayList<>(newTags.size());
        newTags.forEach((key, value) -> newTagRows.add(RowFactory.create(key, value)));

        return RowFactory.create(nodeId,
                JavaConverters.asScalaBufferConverter(newTagRows).asScala(),
                node.getDouble(node.fieldIndex("latitude")),
                node.getDouble(node.fieldIndex("longitude"))
        );
    }
}
