package com.telenav.adapter.component.attributes.enhancement;

import com.telenav.adapter.component.attributes.enumeration.Detection;
import lombok.Builder;

import java.util.List;

/**
 * @author liviuc
 */
public class DefaultAttributeEnhancement extends Enhancement<String> {

    public DefaultAttributeEnhancement(final String id,
                                       final String... tags) {
        super(id, tags);
        this.detection = Detection.ATTRIBUTE;
    }

    @Builder
    public static class SidefileBasedBuilder extends Enhancement.SidefileBasedBuilder<String> {

        private String id;
        private String key;
        private String value;

        public List<Enhancement<String>> enhancements() {
            final Enhancement<String> enhancement =
                    new DefaultAttributeEnhancement(id, key, value);
            return singletonMutableList(enhancement);
        }
    }
}
