package com.telenav.adapter.component.attributes.enhancement;

import com.telenav.adapter.component.attributes.enumeration.Detection;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.telenav.adapter.component.attributes.constant.Constants.RESTRICTION_TYPE_TAG;

/**
 * Abstraction representing sidefile retrieved data.
 *
 * @author liviuc
 */
public abstract class Enhancement<Identifier> implements Serializable {

    protected Identifier identifier;
    protected Map<String, String> enhancementTags;
    protected Detection detection;

    public Enhancement(final Identifier id,
                       final String... tags) {
        this.identifier = id;
        tags(tags);
    }

    private void tags(final String... tags) {
        this.enhancementTags = new HashMap<>(tags.length / 2 + 1);
        for (int i = 0; i < tags.length; i += 2) {
            if (!StringUtils.isEmpty(tags[i + 1])) {
                this.enhancementTags.put(tags[i], tags[i + 1]);
            }
        }
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public Map<String, String> getEnhancementTags() {
        return enhancementTags;
    }

    public Detection getDetection() {
        return detection;
    }

    public boolean isRestriction() {
        return getEnhancementTags().containsKey(RESTRICTION_TYPE_TAG);
    }

    public void setEnhancementTags(final Map<String, String> enhancementTags) {
        this.enhancementTags = enhancementTags;
    }

    public static class SidefileBasedBuilder<Identifier> {

        protected List<Enhancement<Identifier>> singletonMutableList(final Enhancement<Identifier> enhancement) {
            final List<Enhancement<Identifier>> result = new ArrayList<>();
            result.add(enhancement);
            return result;
        }
    }
}