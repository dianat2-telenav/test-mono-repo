package com.telenav.adapter.component.attributes.enhancement;

import java.util.List;

/**
 * The current component processes sidefile records and transforms them in {@link Enhancement}s.
 *
 * @author liviuc
 */
@FunctionalInterface
public interface EnhancementService<Identifier> {

    EnhancementService<String> TURN_RESTRICTION_SERVICE = (record) ->
            new TurnRestrictionEnhancement.SidefileBasedBuilder()
                    .turnRestrictionType(record[0])
                    .data(record[1])
                    .enhancement();

    EnhancementService<String> LANES = (record) -> {

        String dividerLanes = "";
        if (record.length >= 4) {
            dividerLanes = record[3];
        }

        LanesEnhancement.SidefileBasedBuilder.SidefileBasedBuilderBuilder sidefileBasedBuilderBuilder =
                LanesEnhancement.SidefileBasedBuilder
                        .builder()
                        .waySectionIdentifier(record[1])
                        .direction(record[2])
                        .dividerLanes(dividerLanes);
        if (record.length > 4) {
            sidefileBasedBuilderBuilder = sidefileBasedBuilderBuilder.turnLanes(record[4]);
        }
        if (record.length > 5) {
            sidefileBasedBuilderBuilder = sidefileBasedBuilderBuilder.typeLanes(record[5]);
        }
        return sidefileBasedBuilderBuilder.build()
                .enhancements();
    };

    EnhancementService<String> SPEED_LIMIT = (record) ->
            SpeedLimitEnhancement.SidefileBasedBuilder
                    .builder()
                    .maxSpeed(record[1])
                    .waySectionIdentifier(record[2])
                    .direction(record[3])
                    .build()
                    .enhancements();

    EnhancementService<String> ONEWAY = (record) ->
            OnewayEnhancement.SidefileBasedBuilder
                    .builder()
                    .waySectionIdentifier(record[1])
                    .direction(record[2])
                    .build()
                    .enhancements();

    EnhancementService<String> ATTRIBUTE = (record) ->
            DefaultAttributeEnhancement.SidefileBasedBuilder
                    .builder()
                    .id(record[0])
                    .key(record[1])
                    .value(record[2])
                    .build()
                    .enhancements();

    List<Enhancement<Identifier>> enhancement(String[] record);
}
