package com.telenav.adapter.component.attributes.enhancement;

import com.telenav.adapter.component.attributes.enumeration.Detection;
import com.telenav.adapter.component.fc.TagService;
import lombok.Builder;

import java.util.List;

import static com.telenav.adapter.component.fc.TagService.LANES;
import static com.telenav.adapter.component.fc.TagService.TURN_LANES;

/**
 * @author liviuc
 */
public class LanesEnhancement extends Enhancement<String> {

    public LanesEnhancement(final String id,
                            final String... tags) {
        super(id, tags);
        this.detection = Detection.LANES;
    }

    @Builder
    public static class SidefileBasedBuilder extends Enhancement.SidefileBasedBuilder<String> {

        private String waySectionIdentifier;
        private String direction;
        private String dividerLanes;
        private String turnLanes;
        private String typeLanes;

        public List<Enhancement<String>> enhancements() {

            final Enhancement<String> dividerLanesEnhancement =
                    new LanesEnhancement(waySectionIdentifier,
                            "type", LANES,
                            "direction", direction,
                            TagService.DIVIDER_LANES, dividerLanes,
                            TURN_LANES, turnLanes,
                            TagService.TYPE_LANES, typeLanes);

            return singletonMutableList(dividerLanesEnhancement);
        }
    }
}
