package com.telenav.adapter.component.attributes.enhancement;

import com.telenav.adapter.component.attributes.enumeration.Detection;
import lombok.Builder;

import java.util.List;

/**
 * @author liviuc
 */
public class OnewayEnhancement extends Enhancement<String> {

    public OnewayEnhancement(final String id,
                            final String... tags) {
        super(id, tags);
        this.detection = Detection.ONEWAY;
    }

    @Builder
    public static class SidefileBasedBuilder extends Enhancement.SidefileBasedBuilder<String> {

        private String waySectionIdentifier;
        private String direction;

        public List<Enhancement<String>> enhancements() {

            final String onewayDirection = (Boolean.parseBoolean(direction) ? "yes" : "-1");

            final Enhancement<String> dividerLanesEnhancement =
                    new OnewayEnhancement(waySectionIdentifier,
                            "type", "oneway",
                            "oneway", onewayDirection);

            return singletonMutableList(dividerLanesEnhancement);
        }
    }
}
