package com.telenav.adapter.component.attributes.enhancement;

import com.telenav.adapter.component.attributes.enumeration.Detection;
import lombok.Builder;

import java.util.List;

/**
 * @author liviuc
 */
public class SpeedLimitEnhancement extends Enhancement<String> {

    public SpeedLimitEnhancement(final String id,
                                 final String... tags) {
        super(id, tags);
        this.detection = Detection.SPEED_LIMIT;
    }

    @Builder
    public static class SidefileBasedBuilder extends Enhancement.SidefileBasedBuilder<String> {

        private String maxSpeed;
        private String waySectionIdentifier;
        private String direction;

        public List<Enhancement<String>> enhancements() {

            String maxSpeedKey = "";
            if (Boolean.parseBoolean(direction)) {
                maxSpeedKey = "maxspeed:forward";
            } else {
                maxSpeedKey = "maxspeed:backward";
            }

            final Enhancement<String> dividerLanesEnhancement =
                    new SpeedLimitEnhancement(waySectionIdentifier,
                            "type", "speed_limit",
                            maxSpeedKey, maxSpeed);

            return singletonMutableList(dividerLanesEnhancement);
        }
    }
}
