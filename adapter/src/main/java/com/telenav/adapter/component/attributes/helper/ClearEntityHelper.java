package com.telenav.adapter.component.attributes.helper;

import com.telenav.map.entity.StandardEntity;
import com.telenav.map.entity.TagAdjuster;

import java.io.Serializable;
import java.util.Set;

/**
 * Removes tags with specific keys from a {@link StandardEntity}.
 * 
 * @author liviuc
 */
public class ClearEntityHelper<E extends StandardEntity & TagAdjuster<E>> implements Serializable {

    private final Set<String> blackList;

    /**
     *
     * @param blackList tag keys of tags to be removed from the entity.
     */
    public ClearEntityHelper(final Set<String> blackList) {
        this.blackList = blackList;
    }

    /**
     * Clears entities of the tags specified in the black list.
     *
     * @param entity input entities.
     * @return entity without "black listed" tags.
     */
    public E clear(final E entity) {
        return entity.withoutTags(blackList);
    }
}
