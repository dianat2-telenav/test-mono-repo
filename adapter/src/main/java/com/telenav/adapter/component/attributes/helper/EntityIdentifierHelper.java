package com.telenav.adapter.component.attributes.helper;

import com.telenav.map.entity.StandardEntity;
import com.telenav.map.entity.WaySection;

import java.io.Serializable;

import static com.telenav.adapter.component.attributes.constant.Constants.WAY_SECTION_STRICT_IDENTIFIER_SEPARATOR;

/**
 * Component responsible for retrieving an entity specific identifier used for matching
 * entities with enhancements
 *
 * @author liviuc
 */
@FunctionalInterface
public interface EntityIdentifierHelper<E extends StandardEntity, Identifier> extends Serializable {

    /**
     * The easiest way to identify way sections is by their strict identifier (way id, firs node id, last node id).
     */
    EntityIdentifierHelper WAY_SECTIONS_IDENTIFIER_HELPER =
            (EntityIdentifierHelper<WaySection, String> & Serializable) waySection ->
                    waySection.strictIdentifier().wayId() + WAY_SECTION_STRICT_IDENTIFIER_SEPARATOR +
                            waySection.strictIdentifier().fromNodeId() + WAY_SECTION_STRICT_IDENTIFIER_SEPARATOR +
                            waySection.strictIdentifier().toNodeId();

    /**
     * By default entities can be identified by their id.
     */
    EntityIdentifierHelper DEFAULT_IDENTIFIER_HELPER =
            (EntityIdentifierHelper<StandardEntity, String> & Serializable) e -> String.valueOf(e.id());

    /**
     * Returns an identifier specific for each entity type (could be the entity id or something else
     * - way section strict identifier (made out off way id, first node id and last node id) for way sections for eg.
     *
     * @param e input entity.
     * @return entity identifier.
     */
    Identifier identifier(E e);
}
