package com.telenav.adapter.component.attributes.helper;

import com.telenav.adapter.component.attributes.enhancement.Enhancement;
import com.telenav.map.entity.StandardEntity;
import com.telenav.map.entity.TagAdjuster;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import scala.Serializable;
import scala.Tuple2;

import java.util.List;
import java.util.Map;

/**
 * The current component joins the enhancements to their proper entity and performs additional enhancement logic.
 * Although the principle of separation of concerns might be violated (as besides joining, enhancement logic is
 * sneaked "under the hood", this is done for performance reasons - for eg. just clearing entities of tags that will be added
 * and then joining is more time consuming.
 *
 * @author liviuc
 */
public class JoinService<E extends StandardEntity & TagAdjuster<E>, Identifier> implements Serializable {

    private final EntityIdentifierHelper<E, Identifier> identifierService;
    private final ClearEntityHelper<E> clearEntityHelper;
    private final EntityEnhancerHelper<E, Identifier> entityEnhancerService;

    /**
     *
     * @param identifierService helper component to identify each entity type {@link EntityIdentifierHelper}.
     * @param clearEntityHelper helper component to clear each entity type of attributes
     *                          that will be introduced from detections {@link ClearEntityHelper}.
     * @param entityEnhancerService helper component to enhance each entity type {@link EntityEnhancerHelper}.
     */
    public JoinService(final EntityIdentifierHelper<E, Identifier> identifierService,
                       final ClearEntityHelper<E> clearEntityHelper,
                       final EntityEnhancerHelper<E, Identifier> entityEnhancerService) {
        this.identifierService = identifierService;
        this.clearEntityHelper = clearEntityHelper;
        this.entityEnhancerService = entityEnhancerService;
    }

    /**
     * Enhances entities by performing a left outer join.
     *
     * @param entities entities to be enhanced
     * @param enhancementsByEntitiesIds enhancement by entities identifiers.
     * @return enhanced entities.
     */
    public JavaRDD<E> join(final JavaRDD<E> entities,
                           final JavaPairRDD<Identifier, List<Enhancement<Identifier>>> enhancementsByEntitiesIds) {

        final JavaPairRDD<Identifier, E> entitiesById =
                entities.mapToPair(e -> byIdentifier(clear(e)));

        return entitiesById.leftOuterJoin(enhancementsByEntitiesIds)
                .map(tuple -> {

                    final E entity = tuple._2()._1();

                    final Optional<List<Enhancement<Identifier>>> enhancementsOptional = tuple._2()._2();

                    if (enhancementsOptional.isPresent()) {
                        return enhance(tuple._2()._1(), enhancementsOptional.get());
                    }

                    return entity;
                });
    }

    /**
     * Enhances entities by performing a broadcast join.
     *
     * @param entities entities to enhance.
     * @param enhancementsByEntitiesIds enhancements by entity identifiers
     * @param javaSparkContext spark context.
     * @return enhanced entities.
     */
    public JavaRDD<E> broadcastJoin(final JavaRDD<E> entities,
                                    final JavaPairRDD<Identifier, List<Enhancement<Identifier>>> enhancementsByEntitiesIds,
                                    final JavaSparkContext javaSparkContext) {

        final Map<Identifier, List<Enhancement<Identifier>>> enhancementsByEntityIdsBroadcast =
                enhancementsByEntityIdBroadCast(enhancementsByEntitiesIds, javaSparkContext);

        return entities.map(entity -> {

            final Identifier identifier = identifier(entity);

            final List<Enhancement<Identifier>> enhancements = enhancementsByEntityIdsBroadcast.get(identifier);

            final E clearedEntity = clear(entity);

            return enhance(clearedEntity, enhancements);
        });
    }

    private Map<Identifier, List<Enhancement<Identifier>>>
    enhancementsByEntityIdBroadCast(final JavaPairRDD<Identifier, List<Enhancement<Identifier>>> enhancementsByEntitiesIds,
                                    final JavaSparkContext javaSparkContext) {

        final Map<Identifier, List<Enhancement<Identifier>>> enhancementsByEntityIds =
                enhancementsByEntitiesIds.collectAsMap();

        return javaSparkContext.broadcast(enhancementsByEntityIds).getValue();
    }

    private Tuple2<Identifier, E> byIdentifier(final E e) {
        final Identifier identifier = identifier(e);
        return new Tuple2<>(identifier, e);
    }

    private Identifier identifier(final E e) {
        return identifierService.identifier(e);
    }

    private E enhance(final E e,
                      final List<Enhancement<Identifier>> enhancements) {
        return entityEnhancerService.enhance(e, enhancements);
    }

    private E clear(final E e) {
        return clearEntityHelper.clear(e);
    }
}
