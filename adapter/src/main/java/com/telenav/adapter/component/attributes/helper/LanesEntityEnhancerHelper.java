package com.telenav.adapter.component.attributes.helper;

import com.telenav.adapter.component.attributes.enhancement.Enhancement;
import com.telenav.map.entity.StandardEntity;
import com.telenav.map.entity.TagAdjuster;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static com.telenav.adapter.component.fc.TagService.*;

/**
 * This component enhances an entity by adding lanes related tags from it's corresponding {@link Enhancement}s.
 *
 * @author liviuc
 */
public class LanesEntityEnhancerHelper<E extends StandardEntity & TagAdjuster<E>, Identifier> implements Serializable {

    private static final String DEFAULT_TURN_LANE_VALUE = "0";
    private static final String DEFAULT_TYPE_LANE_VALUE = "1";
    private static final String TURN_LANE_LANE_TYPE_VALUE = "2048";
    private static final String LANE_SEPARATOR_REGEXP = "\\|";
    private static final String LANE_SEPARATOR = "|";
    private static final String FRONT_TRAILING_DEFAULT_TURN_LANE = "0|";
    private static final String BACK_TRAILING_DEFAULT_TURN_LANE = "|0";

    /**
     * Core method for lane related enhancement. Lane enhancement represents a particular
     * enhancement case, as multiple tags are affected.
     *
     * @param laneEnhancements - enhancements to be added as tags
     * @param isOneway - if the way is one way
     * @return - enhanced tags
     */
    public Map<String, String> fromLaneEnhancements(final List<Enhancement<Identifier>> laneEnhancements,
                                                    final boolean isOneway) {
        /*
            If no lane enhancements are present, return empty map.
         */
        if (laneEnhancements.isEmpty()) {
            return Collections.emptyMap();
        }
        /*
            If there's only one enhancement, use a specific type of logic.
         */
        if (laneEnhancements.size() == 1) {
            return fromSingleLaneEnhancement(laneEnhancements, isOneway);
        }
        /*
            If there are two lane enhancements, then add them together into
            final lane related tags.
         */
        if (laneEnhancements.size() == 2) {
            return fromDoubleLaneEnhancements(laneEnhancements);
        }
        /*
         *  If more than 3 lane enhancements are provided for the same way section
         *  then something's wrong.
         */
        throw new RuntimeException();
    }

    private Map<String, String> fromDoubleLaneEnhancements(final List<Enhancement<Identifier>> laneEnhancements) {
        return fromDoubleLaneEnhancements(laneEnhancements.get(0), laneEnhancements.get(1));
    }

    private Map<String, String> fromDoubleLaneEnhancements(final Enhancement<Identifier> forward,
                                                           final Enhancement<Identifier> backward) {
        if (!positiveDirection(forward.getEnhancementTags())) {
            fromDoubleLaneEnhancements(backward, forward);
        }
        final Map<String, String> forwardEnhancementTags = forward.getEnhancementTags();
        final String forwardDividerLanes = forwardEnhancementTags.get(DIVIDER_LANES);
        String forwardTurnLanes = forwardEnhancementTags.get(TURN_LANES);
        String forwardTypeLanes = forwardEnhancementTags.get(TYPE_LANES);
        final String[] forwardLaneDividers = splitLanes(forwardDividerLanes);
        final int numberOfForwardLanesTag = forwardLaneDividers.length + 1;

        final Map<String, String> backwardEnhancementTags = backward.getEnhancementTags();
        final String backwardDividerLanes = backwardEnhancementTags.get(DIVIDER_LANES);
        String backwardTurnLanes = backwardEnhancementTags.get(TURN_LANES);
        String backwardTypeLanes = backwardEnhancementTags.get(TYPE_LANES);
        final String[] backWardLaneDividers = splitLanes(backwardDividerLanes);
        final int numberOfBackwardLanesTag = backWardLaneDividers.length + 1;

        final int totalNumberOfLanes = numberOfForwardLanesTag + numberOfBackwardLanesTag;

        final Map<String, String> newTags = new HashMap<>();
        if (!StringUtils.isEmpty(forwardDividerLanes) && !StringUtils.isEmpty(backwardDividerLanes)) {
            putIfNonNull(newTags, DIVIDER_LANES, forwardDividerLanes + LANE_SEPARATOR + backwardDividerLanes);
        }

        // If we have either forward turn lanes or backward turn lanes, we merge them together.
        String turnLanes = merge(forwardTurnLanes, backwardTurnLanes, DEFAULT_TURN_LANE_VALUE);
        putIfNonNull(newTags, TURN_LANES, turnLanes);

        // If we have either forward turn lanes or backward type lanes, we merge them together.
        String typeLanes = merge(forwardTypeLanes, backwardTypeLanes, DEFAULT_TYPE_LANE_VALUE);
        // Perform several adjustments with respect to turn lanes.
        typeLanes = adjustTypeLanes(typeLanes, turnLanes);
        putIfNonNull(newTags, TYPE_LANES, typeLanes);

        putIfNonNull(newTags, LANES, String.valueOf(totalNumberOfLanes));
        putIfNonNull(newTags, LANES_FORWARD, String.valueOf(numberOfForwardLanesTag));
        putIfNonNull(newTags, LANES_BACKWARD, String.valueOf(numberOfBackwardLanesTag));
        putIfNonNull(newTags, LANE_CAT, laneCat(totalNumberOfLanes));

        return newTags;
    }

    private String merge(String forwardLanes,
                         String backwardLanes,
                         final String defaultLaneValue) {
        if (!StringUtils.isEmpty(forwardLanes) || !StringUtils.isEmpty(backwardLanes)) {

            // Assign 0 (undefined) to the empty direction turn lane.
            if (StringUtils.isEmpty(forwardLanes)) {
                forwardLanes = defaultLaneValue;
            }
            if (StringUtils.isEmpty(backwardLanes)) {
                backwardLanes = defaultLaneValue;
            }

            // Merge the two direction turn lanes.
            String mergedLanes = forwardLanes + LANE_SEPARATOR + backwardLanes;
            // Add the turn lanes if they have also other values than undefined.
            if (!allZero(splitLanes(mergedLanes))) {
                return mergedLanes;
            }
        }
        return null;
    }

    private String adjustTypeLanes(final String typeLanes,
                                   final String turnLanes) {
        // If there are no turnLanes, then no typeLanes can be inferred.
        if (StringUtils.isEmpty(turnLanes)) {
            return typeLanes;
        }

        final String[] turnLanesSplit = splitLanes(turnLanes);
        // If there are no turnLanes, then no typeLanes can be inferred.
        if (allZero(turnLanesSplit)) {
            return typeLanes;
        }

        String[] newTypeLanesArr = null;
        // If the type lanes is empty, then construct it solely based on turn lanes.
        if (StringUtils.isEmpty(typeLanes)) {
            newTypeLanesArr = turnLanesSplit;

            for (int i = 0; i < newTypeLanesArr.length; i++) {
                if (!isZero(newTypeLanesArr[i])) {
                    newTypeLanesArr[i] = TURN_LANE_LANE_TYPE_VALUE;
                } else {
                    newTypeLanesArr[i] = DEFAULT_TYPE_LANE_VALUE;
                }
            }

        } else {
            /*
                Otherwise, preserve the type lanes, but adjust the value of those lanes
                who are turn lanes.
             */
            final String[] typeLanesArr = splitLanes(typeLanes);
            newTypeLanesArr = new String[typeLanesArr.length];

            for (int i = 0; i < typeLanesArr.length; i++) {
                if (i >= turnLanesSplit.length) {
                    newTypeLanesArr[i] = DEFAULT_TURN_LANE_VALUE;
                    continue;
                }
                if (!isZero(turnLanesSplit[i])) {
                    newTypeLanesArr[i] = TURN_LANE_LANE_TYPE_VALUE;
                } else {
                    newTypeLanesArr[i] = (isZero(turnLanesSplit[i])) ? DEFAULT_TYPE_LANE_VALUE : typeLanesArr[i];
                }
            }

        }
        return Arrays.stream(newTypeLanesArr)
                .collect(Collectors.joining(LANE_SEPARATOR));
    }

    private void putIfNonNull(final Map<String, String> tags,
                              final String key,
                              final String value) {
        if (!StringUtils.isEmpty(value)) {
            tags.put(key, value);
        }
    }

    /**
     * The lane cat rule is based on the following mapping:
     * 1 lane -> lane cat 1
     * 2, 3 lane -> lane cat 2
     * >= 4 -> lane cat 3
     *
     * @param totalNumberOfLanes .
     * @return lane cat
     */
    private String laneCat(int totalNumberOfLanes) {
        if (totalNumberOfLanes == 0) {
            return null;
        }
        if (totalNumberOfLanes == 1) {
            return "1";
        }
        if (totalNumberOfLanes < 4) {
            return "2";
        }
        return "3";
    }

    private Map<String, String> fromSingleLaneEnhancement(final List<Enhancement<Identifier>> laneEnhancements,
                                                          final boolean isOneway) {
        final Enhancement<Identifier> laneEnhancement = laneEnhancements.get(0);

        final Map<String, String> enhancementTags = laneEnhancement.getEnhancementTags();

        final String dividerLanes = enhancementTags.get(DIVIDER_LANES);
        String turnLanes = enhancementTags.get(TURN_LANES);
        String typeLanes = enhancementTags.get(TYPE_LANES);

        final Map<String, String> newTags = new HashMap<>();

        final boolean positiveDirection = positiveDirection(enhancementTags);
        int totalNumberOfLanes = 1;
        /*
            If the way section is a oneway, then we keep the lane related values
            as we get them and add lanes:backward / lanes:forward only based on these tags.
         */
        if (isOneway) {

            putIfNonNull(newTags, DIVIDER_LANES, dividerLanes);
            if (!StringUtils.isEmpty(turnLanes)) {
                if (allZero(splitLanes(turnLanes))) {
                    turnLanes = null;
                }
                putIfNonNull(newTags, TURN_LANES, turnLanes);
            }

            typeLanes = adjustTypeLanes(typeLanes, turnLanes);
            putIfNonNull(newTags, TYPE_LANES, typeLanes);

            // The number of lanes is equal to the number of deviders + 1;
            final String[] laneDividers = splitLanes(dividerLanes);
            totalNumberOfLanes = laneDividers.length + 1;

            final String numberOfLanesTag = String.valueOf(totalNumberOfLanes);

            putIfNonNull(newTags, LANES, numberOfLanesTag);

            // Check the direction of the way section.
            if (positiveDirection) {

                putIfNonNull(newTags, LANES_FORWARD, numberOfLanesTag);

            } else {

                putIfNonNull(newTags, LANES_BACKWARD, numberOfLanesTag);

            }

        } else {
            /*
             * If it's not a oneway, the approach is similar with the previous case,
             * however we also add 1 lane for the opposite direction - presume the opposite
             * direction has solely one lane.
             */
            putIfNonNull(newTags, DIVIDER_LANES, dividerLanes);

            final String[] laneDividers = splitLanes(dividerLanes);

            // The number of lanes is equal to the number of dividers + 1 + 1 presumed lane from the opposite direction;
            totalNumberOfLanes = laneDividers.length + 2;

            final String numberOfLanesTag = String.valueOf(totalNumberOfLanes);

            putIfNonNull(newTags, LANES, numberOfLanesTag);

            final String turnLanesFromNonOnewayValue = turnLanesFromNonOnewayValue(turnLanes);

            enhanceOnewayLanes(turnLanesFromNonOnewayValue,
                    positiveDirection,
                    dividerLanes,
                    laneDividers,
                    turnLanes,
                    typeLanes,
                    newTags);
        }

        putIfNonNull(newTags, LANE_CAT, laneCat(totalNumberOfLanes));

        return newTags;
    }

    private void enhanceOnewayLanes(String turnLanesFromNonOnewayValue,
                                    boolean positiveDirection,
                                    String dividerLanes,
                                    String[] laneDividers,
                                    String turnLanes,
                                    String typeLanes,
                                    final Map<String, String> tags) {
        if (!StringUtils.isEmpty(turnLanesFromNonOnewayValue)) {
            // If turn lanes are made out only of zeros, we can omit the turn:lanes tag.
            if (allZero(splitLanes(turnLanesFromNonOnewayValue))) {

                turnLanesFromNonOnewayValue = null;

            } else {
                // Add the presumed lane as not specified - i.e. 0.
                turnLanesFromNonOnewayValue =
                        backTrail(BACK_TRAILING_DEFAULT_TURN_LANE, turnLanesFromNonOnewayValue);
            }
        }
        putIfNonNull(tags, TURN_LANES, turnLanesFromNonOnewayValue);
        putIfNonNull(tags, DIVIDER_LANES, dividerLanes);

        if (!StringUtils.isEmpty(typeLanes)) {
            // Add the presumed lane as not specified - i.e. 0.
            String newTypeLanes = null;
            if (positiveDirection) {

                newTypeLanes = backTrail(BACK_TRAILING_DEFAULT_TURN_LANE, typeLanes);

            } else {

                newTypeLanes = frontTrail(FRONT_TRAILING_DEFAULT_TURN_LANE, typeLanes);

            }
            // Adjust the lane types according to the turn lanes.
            newTypeLanes = adjustTypeLanes(newTypeLanes, turnLanes);

            putIfNonNull(tags, TYPE_LANES, newTypeLanes);
        }

        if (positiveDirection) {

            putIfNonNull(tags, LANES_FORWARD, String.valueOf(laneDividers.length + 1));
            putIfNonNull(tags, LANES_BACKWARD, "1");

        } else {

            putIfNonNull(tags, LANES_BACKWARD, "1");
            putIfNonNull(tags, LANES_FORWARD, String.valueOf(laneDividers.length + 1));

        }
    }

    private String backTrail(final String trailValue,
                             final String value) {
        return value + trailValue;
    }

    private String frontTrail(final String trailValue,
                              final String value) {
        return trailValue + value;
    }

    private boolean positiveDirection(final Map<String, String> tags) {
        return "true".equalsIgnoreCase(tags.get("direction"));
    }

    private String turnLanesFromNonOnewayValue(final String turnLanes) {
        if (StringUtils.isEmpty(turnLanes)) {
            return null;
        }
        final String[] turnLanesSplit = splitLanes(turnLanes);
        if (!allZero(turnLanesSplit)) {
            return turnLanes;
        }
        return null;
    }

    private String[] splitLanes(final String lanesString) {
        if (StringUtils.isEmpty(lanesString)) {
            return new String[]{};
        }
        return lanesString.split(LANE_SEPARATOR_REGEXP);
    }

    private boolean allZero(final String[] turnLanesSplit) {
        for (int i = 0; i < turnLanesSplit.length; i++) {
            if (!isZero(turnLanesSplit[i])) {
                return false;
            }
        }
        return true;
    }

    private boolean isZero(final String lane) {
        return "0".equalsIgnoreCase(lane);
    }
}
