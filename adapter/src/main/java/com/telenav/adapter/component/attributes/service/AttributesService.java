package com.telenav.adapter.component.attributes.service;

import com.telenav.adapter.component.attributes.enhancement.Enhancement;
import com.telenav.adapter.component.attributes.enhancement.EnhancementService;
import com.telenav.adapter.component.attributes.enumeration.Detection;
import com.telenav.adapter.component.attributes.helper.ClearEntityHelper;
import com.telenav.adapter.component.attributes.helper.EntityEnhancerHelper;
import com.telenav.adapter.component.attributes.helper.EntityIdentifierHelper;
import com.telenav.adapter.component.attributes.helper.JoinService;
import com.telenav.map.entity.StandardEntity;
import com.telenav.map.entity.TagAdjuster;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.util.CollectionAccumulator;
import scala.Tuple2;

import java.io.Serializable;
import java.util.*;

import static com.telenav.adapter.component.attributes.constant.Constants.RECORD_SEPARATOR;

/**
 * The current service matches the enhancement data to their corresponding entities.
 *
 * @author liviuc
 */
public abstract class AttributesService<E extends StandardEntity & TagAdjuster<E>, Identifier>
        implements Serializable {

    protected final Set<String> configSpecifiedTagKeys;
    protected final boolean broadcastJoin;
    protected final CollectionAccumulator<String> logs;
    private final Function<Enhancement<Identifier>, List<Enhancement<Identifier>>> ACC =
            (Function<Enhancement<Identifier>, List<Enhancement<Identifier>>> & Serializable)
                    (e) -> {
                        final List<Enhancement<Identifier>> list = new ArrayList<>();
                        list.add(e);
                        return list;
                    };
    private final Function2<List<Enhancement<Identifier>>, Enhancement<Identifier>, List<Enhancement<Identifier>>> ADD =
            (Function2<List<Enhancement<Identifier>>, Enhancement<Identifier>, List<Enhancement<Identifier>>> & Serializable)
                    (list, e) -> {
                        list.add(e);
                        return list;
                    };
    private final Function2<List<Enhancement<Identifier>>, List<Enhancement<Identifier>>, List<Enhancement<Identifier>>> MERGE =
            (Function2<List<Enhancement<Identifier>>, List<Enhancement<Identifier>>, List<Enhancement<Identifier>>> & Serializable)
                    (left, right) -> {
                        left.addAll(right);
                        return left;
                    };

    /**
     * @param configSpecifiedTagKeys describes the tag keys / detection types that are taken in consideration for enhancement
     *                               - these preexisting tag keys will be removed from entities before enhancement to assure
     *                               that the enhanced entities do not contain attributes from multiple sources.
     * @param broadcastJoin          describes whether to perform a broadcast join or not.
     * @param logs                   logs.
     */
    public AttributesService(final Set<String> configSpecifiedTagKeys,
                             final boolean broadcastJoin,
                             final CollectionAccumulator<String> logs) {
        this.configSpecifiedTagKeys = configSpecifiedTagKeys;
        this.broadcastJoin = broadcastJoin;
        this.logs = logs;
    }

    protected abstract EntityIdentifierHelper<E, Identifier> identifierService();

    /**
     * Core method that:
     * - maps sidefile records to {@link Enhancement}s and groups them by an entity identifier.
     * - joins entities and enhancements.
     *
     * @param entities         input entities.
     * @param enhancements     input enhancements (i.e. sidefile records).
     * @param javaSparkContext spark context.
     * @return enhanced entities.
     */
    protected JavaRDD<E> process(final JavaRDD<E> entities,
                                 final JavaRDD<String> enhancements,
                                 final JavaSparkContext javaSparkContext) {

        final JavaPairRDD<Identifier, List<Enhancement<Identifier>>> enhancementsByEntitiesIds =
                enhancementsByEntitiesIds(enhancements);

        final JoinService<E, Identifier> joinService = joinService();

        if (broadcastJoin) {
            return joinService.broadcastJoin(entities, enhancementsByEntitiesIds, javaSparkContext);
        }
        return joinService.join(entities, enhancementsByEntitiesIds);
    }

    /**
     * Transforms a sidefile record into {@link Enhancement} data.
     *
     * @param sidefileRecord sidefile row.
     * @return enhancement abstraction.
     */
    private List<Enhancement<Identifier>> enhancement(final String sidefileRecord) {
        final String[] split = sidefileRecord.split(RECORD_SEPARATOR);

        final String detectionType = split[0];
        final Detection detection = Detection.detection(detectionType);
        final EnhancementService enhancementService = DetectionServiceProvider.enhancementService(detection);

        return enhancementService.enhancement(split);
    }

    private void configSpecifiedTags(final Enhancement entityEnhancement) {

        if (entityEnhancement.getDetection() != Detection.ATTRIBUTE) {
            return;
        }

        final Map<String, String> enhancementTags = entityEnhancement.getEnhancementTags();

        final Map<String, String> newTags = new HashMap<>();
        for (final Map.Entry<String, String> entry : enhancementTags.entrySet()) {
            final String key = entry.getKey();
            if (configSpecifiedTagKeys.contains(entry.getKey())) {
                newTags.put(key, entry.getValue());
            }
        }
        entityEnhancement.setEnhancementTags(newTags);
    }

    protected JoinService<E, Identifier> joinService() {

        final ClearEntityHelper<E> clearEntityHelper = new ClearEntityHelper<>(configSpecifiedTagKeys);

        final EntityEnhancerHelper<E, Identifier> entityEnhancerService = new EntityEnhancerHelper<>();

        return new JoinService<>(identifierService(), clearEntityHelper, entityEnhancerService);
    }

    protected JavaPairRDD<Identifier, List<Enhancement<Identifier>>> enhancementsByEntitiesIds(final JavaRDD<String> enhancements) {
        return enhancements.flatMapToPair(str -> byEntitiesIdsWithConfigSpecifiedTags(str))
                .combineByKey(ACC, ADD, MERGE);
    }

    private List<Tuple2<Identifier, Enhancement<Identifier>>>
    byEntitiesIdsWithConfigSpecifiedTags(final List<Enhancement<Identifier>> entityEnhancements) {

        final List<Tuple2<Identifier, Enhancement<Identifier>>> result = new ArrayList<>();

        for (final Enhancement<Identifier> entityEnhancement : entityEnhancements) {

            configSpecifiedTags(entityEnhancement);

            if (!entityEnhancement.getEnhancementTags().isEmpty()) {
                final Tuple2<Identifier, Enhancement<Identifier>> tuple =
                        new Tuple2<>(entityEnhancement.getIdentifier(), entityEnhancement);
                result.add(tuple);
            }
        }

        return result;
    }

    private Iterator<Tuple2<Identifier, Enhancement<Identifier>>> byEntitiesIdsWithConfigSpecifiedTags(final String record) {
        final List<Enhancement<Identifier>> entityEnhancements = enhancement(record);

        final List<Tuple2<Identifier, Enhancement<Identifier>>> enhancementsByEntityId =
                byEntitiesIdsWithConfigSpecifiedTags(entityEnhancements);

        return enhancementsByEntityId.iterator();
    }
}
