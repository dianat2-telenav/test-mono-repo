package com.telenav.adapter.component.attributes.service;

import com.telenav.adapter.component.attributes.enhancement.EnhancementService;
import com.telenav.adapter.component.attributes.enumeration.Detection;

import java.util.HashMap;
import java.util.Map;

/**
 * The current component retrieves {@link EnhancementService}s based on {@link Detection} types.
 *
 * @author liviuc
 */
public class DetectionServiceProvider {

    private static final Map<Detection, EnhancementService> DETECTION_SERVICES_BY_DETECTIONS = new HashMap<>();

    static {
        DETECTION_SERVICES_BY_DETECTIONS.put(Detection.TURN_RESTRICTION, EnhancementService.TURN_RESTRICTION_SERVICE);
        DETECTION_SERVICES_BY_DETECTIONS.put(Detection.LANES, EnhancementService.LANES);
        DETECTION_SERVICES_BY_DETECTIONS.put(Detection.SPEED_LIMIT, EnhancementService.SPEED_LIMIT);
        DETECTION_SERVICES_BY_DETECTIONS.put(Detection.ONEWAY, EnhancementService.ONEWAY);
        DETECTION_SERVICES_BY_DETECTIONS.put(Detection.ATTRIBUTE, EnhancementService.ATTRIBUTE);
    }

    /**
     * Provides an {@link EnhancementService} based on the provideded {@link Detection}
     *
     * @param detection detection.
     * @return service responsible for processing the provided detection.
     */
    public static EnhancementService enhancementService(final Detection detection) {
        return DETECTION_SERVICES_BY_DETECTIONS.get(detection);
    }
}
