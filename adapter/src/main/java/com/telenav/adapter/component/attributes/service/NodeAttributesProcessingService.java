package com.telenav.adapter.component.attributes.service;

import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.map.entity.Node;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.util.CollectionAccumulator;

import java.util.Set;

/**
 * * The current service is responsible for enhancing noes with sidefile data (attributes or detections).
 *
 * @author liviuc
 */
public class NodeAttributesProcessingService extends AttributesProcessingService<Node, String> {

    /**
     * @param nodesPath          input path of nodes to be enhanced.
     * @param nodesOutputPath    output path of enhanced nodes.
     * @param nodesSidefilePaths sidefiles input path.
     * @param nodesConfigPath    config file input path.
     * @param broadcastJoin      parameter describing whether to perform a broadcast join or not.
     * @param logs               logs.
     */
    public NodeAttributesProcessingService(final String nodesPath,
                                           final String nodesOutputPath,
                                           final String nodesSidefilePaths,
                                           final String nodesConfigPath,
                                           final boolean broadcastJoin,
                                           final CollectionAccumulator<String> logs) {
        super(nodesPath,
                nodesOutputPath,
                nodesSidefilePaths,
                nodesConfigPath,
                broadcastJoin,
                logs);
    }

    @Override
    protected void write(final JavaRDD<Node> rdd,
                         final String outputPath,
                         final JavaSparkContext javaSparkContext) {
        RddWriter.NodeWriter.write(javaSparkContext, rdd, outputPath);
    }

    @Override
    protected JavaRDD<Node> read(final String nodesPath,
                                 final JavaSparkContext javaSparkContext) {
        return RddReader.NodeReader.read(javaSparkContext, nodesPath);
    }

    @Override
    protected AttributesService<Node, String> attributesService(final Set<String> blackList) {
        return new NodeAttributeService(blackList, broadcastJoin);
    }
}
