package com.telenav.adapter.component.attributes.service;

import com.telenav.adapter.component.attributes.enhancement.Enhancement;
import com.telenav.adapter.component.attributes.helper.EntityIdentifierHelper;
import com.telenav.adapter.component.attributes.helper.JoinService;
import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.map.entity.RawRelation;
import com.telenav.map.entity.WaySection;
import com.telenav.relationenhancer.core.turnrestriction.common.RestrictionEnhancerJob;
import com.telenav.relationenhancer.core.turnrestriction.common.bean.DetectedRestriction;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.util.CollectionAccumulator;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.telenav.adapter.component.attributes.helper.EntityIdentifierHelper.DEFAULT_IDENTIFIER_HELPER;

/**
 * The current service matches the enhancement data to relations.
 *
 * @author liviuc
 */
public class RelationAttributeService extends AttributesService<RawRelation, String> {

    private final String waySectionsInputPath;
    private final String detectionsSource;

    /**
     * @param configSpecifiedTagKeys describes the tag keys / detection types that are taken in consideration for node enhancement
     *                               (see {@link AttributesService}).
     * @param broadcastJoin          describes whether to perform a broadcast join or not.
     * @param trWaySectionsPath      input path for way-sections used for turn restriction enhancement.
     * @param detectionsSource       source of external enhancements.
     * @param logs                   logs.
     */
    public RelationAttributeService(final Set<String> configSpecifiedTagKeys,
                                    final boolean broadcastJoin,
                                    final String trWaySectionsPath,
                                    final String detectionsSource,
                                    final CollectionAccumulator<String> logs) {
        super(configSpecifiedTagKeys, broadcastJoin, logs);
        this.waySectionsInputPath = trWaySectionsPath;
        this.detectionsSource = detectionsSource;
    }

    @Override
    protected EntityIdentifierHelper<RawRelation, String> identifierService() {
        return DEFAULT_IDENTIFIER_HELPER;
    }

    /**
     * Specific processing method for relations:
     * - retrieves enhancements by relation identifier
     * - removes relations that need to be replaced with new detections (for eg. turn-restrictions)
     * - joins remaining relations with proper enhancements
     * - creates new relations from enhancements that represent new relations (for eg. turn-restrictions)
     * - joins the enhanced relations and the newly created relations
     * - assigns ids to newly created relations {@link RestrictionEnhancerJob} from the map-relation-enhancer project
     *
     * @param relations        input relations.
     * @param enhancements     input enhancements (i.e. sidefile records).
     * @param javaSparkContext spark context.
     *
     * @return A rdd of processed raw relations.
     */
    @Override
    protected JavaRDD<RawRelation> process(JavaRDD<RawRelation> relations,
                                           final JavaRDD<String> enhancements,
                                           final JavaSparkContext javaSparkContext) {
        // Enhancements by relation ids.
        final JavaPairRDD<String, List<Enhancement<String>>> enhancementsByRelationsIds =
                enhancementsByEntitiesIds(enhancements);

        // Relations that will be enhanced by joining with enhancements.
        final JavaRDD<RawRelation> withJoin = toProcessWithJoin(relations);

        // Join/
        final JoinService<RawRelation, String> joinService = joinService();

        if (broadcastJoin) {
            return joinService.broadcastJoin(withJoin, enhancementsByRelationsIds, javaSparkContext);
        }
        // Relations enhanced by joining with enhancements.
        final JavaRDD<RawRelation> joined = joinService.join(withJoin, enhancementsByRelationsIds);

        // Read way sections.
        final JavaRDD<WaySection> waySections = readWaySections(javaSparkContext);

        // DetectedRestriction created from enhancements.
        final JavaRDD<DetectedRestriction> withoutJoin = toProcessWithoutJoin(enhancementsByRelationsIds, detectionsSource);

        // Assign ids to relations (the ones created from enhancements do not have ids).
        return RestrictionEnhancerJob.runWithDetectedRestrictions(waySections,
                joined,
                withoutJoin,
                logs);
    }

    private JavaRDD<WaySection> readWaySections(final JavaSparkContext javaSparkContext) {
        if (StringUtils.isEmpty(waySectionsInputPath)) {
            return javaSparkContext.emptyRDD();
        }
        return RddReader.WaySectionReader.read(javaSparkContext, waySectionsInputPath);
    }

    private JavaRDD<DetectedRestriction> toProcessWithoutJoin(final JavaPairRDD<String, List<Enhancement<String>>> enhancements,
                                                              final String detectionsSource) {

        return TurnRestrictionsHelper.turnRestrictions(enhancements)
                .map(e -> TurnRestrictionsHelper.detectedRestriction(e, detectionsSource, logs))
                .filter(Objects::nonNull);
    }

    private JavaRDD<RawRelation> toProcessWithJoin(final JavaRDD<RawRelation> relations) {
        return relations.filter(TurnRestrictionsHelper::isTurnRestrictionRelation);
    }
}
