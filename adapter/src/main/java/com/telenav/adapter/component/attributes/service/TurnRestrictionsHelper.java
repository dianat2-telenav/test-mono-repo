package com.telenav.adapter.component.attributes.service;

import com.telenav.adapter.component.attributes.enhancement.Enhancement;
import com.telenav.adapter.component.attributes.enumeration.Detection;
import com.telenav.map.entity.RawRelation;
import com.telenav.map.entity.WaySectionIdentifier;
import com.telenav.relationenhancer.core.turnrestriction.DetectionBuilderHelper;
import com.telenav.relationenhancer.core.turnrestriction.common.RestrictionConstructor;
import com.telenav.relationenhancer.core.turnrestriction.common.bean.DetectedRestriction;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.util.CollectionAccumulator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.telenav.adapter.component.attributes.constant.Constants.*;

/**
 * The current component is used to perform turn-restrictions related operations.
 *
 * @author liviuc
 */
public class TurnRestrictionsHelper {

    private static final String ESCAPE = "\\";

    /**
     * Filters turn-restriction related enhancements
     *
     * @param enhancements sidefile enhancements.
     * @return turn-restriction enhancements.
     */
    public static JavaRDD<Enhancement<String>> turnRestrictions(final JavaPairRDD<String, List<Enhancement<String>>> enhancements) {
        return enhancements.map(tuple -> {

            final List<Enhancement<String>> enhancementsList = tuple._2();

            final List<Enhancement<String>> filteredEnhancements = new ArrayList<>();
            for (final Enhancement<String> enhancement : enhancementsList) {
                if (enhancement.isRestriction()) {
                    filteredEnhancements.add(enhancement);
                }
            }

            if (!filteredEnhancements.isEmpty()) {
                return filteredEnhancements;
            }
            return Collections.EMPTY_LIST;
        }).filter(list -> !list.isEmpty())
                .flatMap(enhancementList -> enhancementList.iterator());
    }

    /**
     * @param relation raw relation.
     * @return true if the rew relation is a turn-restriction
     */
    public static Boolean isTurnRestrictionRelation(final RawRelation relation) {
        return !"restriction".equals(relation.tags().get("type"));
    }

    /**
     * Builds a {@link DetectedRestriction} from an enhancement.
     *
     * @param enhancement Abstraction representing sidefile retrieved data.
     * @param detectionsSource A string representing the detection source.
     * @param logs Accumulator object for logs.
     * @return A restriction that was detected.
     */
    public static DetectedRestriction detectedRestriction(final Enhancement<String> enhancement,
                                                          final String detectionsSource,
                                                          final CollectionAccumulator<String> logs) {

        if (Detection.TURN_RESTRICTION != enhancement.getDetection()) {
            return null;
        }

        final Map<String, String> enhancementTags = enhancement.getEnhancementTags();
        final String turRestrictionType = enhancementTags.get(RESTRICTION_TYPE_TAG);
        final String turnRestrictionData = enhancementTags.get(RESTRICTION_DATA_TAG);

        final List<WaySectionIdentifier> members =
                DetectionBuilderHelper.convertWaySectionIdentifiers(turnRestrictionData,
                        ESCAPE,
                        WAY_SECTION_TURN_RESTRICTION_INFO_SEPARATOR,
                        WAY_SECTION_STRICT_IDENTIFIER_SEPARATOR);

        Boolean firstMemberDirection;

        try {

            firstMemberDirection =
                    DetectionBuilderHelper.extractFirstMemberDirection(turnRestrictionData,
                            ESCAPE,
                            WAY_SECTION_TURN_RESTRICTION_INFO_SEPARATOR,
                            WAY_SECTION_STRICT_IDENTIFIER_SEPARATOR);

        } catch (final Exception e) {

            logs.add("Couldn't build DetectedRestriction from " + turnRestrictionData + " " + e.getMessage());
            return null;
        }

        return construct(members, firstMemberDirection, turnRestrictionData, turRestrictionType, detectionsSource, logs);
    }

    private static DetectedRestriction construct(final List<WaySectionIdentifier> members,
                                                 final Boolean firstMemberDirection,
                                                 final String turnRestrictionData,
                                                 final String turRestrictionType,
                                                 final String detectionsSource,
                                                 final CollectionAccumulator<String> logs) {
        try {
            return RestrictionConstructor.builder()
                    .members(members)
                    .forward(firstMemberDirection)
                    .restrictionId(-1L)
                    .originalRow(turnRestrictionData)
                    .turnType(turRestrictionType)
                    .detectionSource(detectionsSource)
                    .build()
                    .construct()
                    .get();

        } catch (final Exception e) {

            logs.add("Couldn't build DetectedRestriction from " + turnRestrictionData + " " + e.getMessage());
            return null;
        }
    }
}
