package com.telenav.adapter.component.attributes.service;

import com.telenav.adapter.component.attributes.helper.EntityIdentifierHelper;
import com.telenav.map.entity.RawWay;

import java.util.Set;

import static com.telenav.adapter.component.attributes.helper.EntityIdentifierHelper.DEFAULT_IDENTIFIER_HELPER;

/**
 * The current service matches the enhancement data to raw ways.
 *
 * @author liviuc
 */
public class WayAttributeService extends AttributesService<RawWay, String> {

    /**
     * @param configSpecifiedTagKeys describes the tag keys / detection types that are taken in consideration for raw way enhancement
     *                               (see {@link AttributesService}).
     * @param broadcastJoin          describes whether to perform a broadcast join or not.
     */
    public WayAttributeService(final Set<String> configSpecifiedTagKeys,
                               final boolean broadcastJoin) {
        super(configSpecifiedTagKeys, broadcastJoin, null);
    }

    @Override
    protected EntityIdentifierHelper<RawWay, String> identifierService() {
        return DEFAULT_IDENTIFIER_HELPER;
    }
}
