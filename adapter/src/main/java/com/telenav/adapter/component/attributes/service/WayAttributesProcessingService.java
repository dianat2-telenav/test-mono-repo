package com.telenav.adapter.component.attributes.service;

import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.map.entity.RawWay;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.util.CollectionAccumulator;

import java.util.Set;

/**
 * The current service is responsible for enhancing raw ways with sidefile data (attributes or detections)
 *
 * @author liviuc
 */
public class WayAttributesProcessingService extends AttributesProcessingService<RawWay, String> {

    /**
     * Initializes a new instance of WayAttributesProcessingService using sidefile data.
     *
     * @param waysPath input path of entities to be enhanced.
     * @param waysOutputPath output path of enhanced raw-ways.
     * @param waysSidefilePaths sidefiles input path.
     * @param waysConfigPath config file input path.
     * @param broadcastJoin config file input path.
     * @param broadcastJoin parameter describing whether to perform a broadcast join or not.
     * @param logs logs.
     */
    public WayAttributesProcessingService(final String waysPath,
                                          final String waysOutputPath,
                                          final String waysSidefilePaths,
                                          final String waysConfigPath,
                                          final boolean broadcastJoin,
                                          final CollectionAccumulator<String> logs) {
        super(waysPath,
                waysOutputPath,
                waysSidefilePaths,
                waysConfigPath,
                broadcastJoin,
                logs);
    }

    @Override
    protected void write(final JavaRDD<RawWay> rdd,
                         final String outputPath,
                         final JavaSparkContext javaSparkContext) {
        RddWriter.RawWaysWriter.write(javaSparkContext, rdd, outputPath);
    }

    @Override
    protected JavaRDD<RawWay> read(final String rawWaysPath,
                                   final JavaSparkContext javaSparkContext) {
        return RddReader.RawWayReader.read(javaSparkContext, rawWaysPath);
    }


    @Override
    protected AttributesService<RawWay, String> attributesService(final Set<String> blackList) {
        return new WayAttributeService(blackList, broadcastJoin);
    }
}
