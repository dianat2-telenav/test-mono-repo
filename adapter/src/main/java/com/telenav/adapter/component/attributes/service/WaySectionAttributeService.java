package com.telenav.adapter.component.attributes.service;

import com.telenav.adapter.component.attributes.helper.EntityIdentifierHelper;
import com.telenav.map.entity.WaySection;

import java.util.Set;

import static com.telenav.adapter.component.attributes.helper.EntityIdentifierHelper.WAY_SECTIONS_IDENTIFIER_HELPER;

/**
 * The current service matches the enhancement data to way-sections.
 *
 * @author liviuc
 */
public class WaySectionAttributeService extends AttributesService<WaySection, String> {

    /**
     * @param configSpecifiedTagKeys describes the tag keys / detection types that are taken in consideration for way-section enhancement
     *                               (see {@link AttributesService}).
     * @param broadcastJoin          describes whether to perform a broadcast join or not.
     */
    public WaySectionAttributeService(final Set<String> configSpecifiedTagKeys, final boolean broadcastJoin) {
        super(configSpecifiedTagKeys, broadcastJoin, null);
    }

    @Override
    protected EntityIdentifierHelper<WaySection, String> identifierService() {
        return WAY_SECTIONS_IDENTIFIER_HELPER;
    }
}
