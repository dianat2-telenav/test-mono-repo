package com.telenav.adapter.component.attributes.service;

import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.map.entity.WaySection;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.util.CollectionAccumulator;

import java.util.Set;

/**
 * The current service is responsible for enhancing way-sections with sidefile data (attributes or detections)
 *
 * @author liviuc
 */
public class WaySectionAttributesProcessingService extends AttributesProcessingService<WaySection, String> {

    /**
     *
     * @param waySectionsPath input path of way-sections to be enhanced.
     * @param waySectionsOutputPath output path of enhanced way-sections.
     * @param waySectionsSidefilePaths sidefiles input path.
     * @param waySectionsConfigPath config file input path.
     * @param broadcastJoin parameter describing whether to perform a broadcast join or not.
     * @param logs logs.
     */
    public WaySectionAttributesProcessingService(final String waySectionsPath,
                                                 final String waySectionsOutputPath,
                                                 final String waySectionsSidefilePaths,
                                                 final String waySectionsConfigPath,
                                                 final boolean broadcastJoin,
                                                 final CollectionAccumulator<String> logs) {
        super(waySectionsPath,
                waySectionsOutputPath,
                waySectionsSidefilePaths,
                waySectionsConfigPath,
                broadcastJoin,
                logs);
    }

    @Override
    protected void write(final JavaRDD<WaySection> rdd,
                         final String outputPath,
                         final JavaSparkContext javaSparkContext) {
        RddWriter.WaySectionWriter.write(javaSparkContext, rdd, outputPath);
    }

    @Override
    protected JavaRDD<WaySection> read(final String waySectionsPath,
                                       final JavaSparkContext javaSparkContext) {
        return RddReader.WaySectionReader.read(javaSparkContext, waySectionsPath);
    }

    @Override
    protected AttributesService<WaySection, String> attributesService(final Set<String> blackList) {
        return new WaySectionAttributeService(blackList, broadcastJoin);
    }
}
