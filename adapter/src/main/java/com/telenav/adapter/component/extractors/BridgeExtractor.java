package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;

/**
 * Extractor for the 'bridge' tag value.
 * @see SingleTagExtractor
 *
 * @author Andrei Puf
 */
public class BridgeExtractor extends SingleTagExtractor{
    /**
     * Initializes the BridgeExtractor using the 'bridge' tag key.
     */
    public BridgeExtractor() {
        super(OsmTagKey.BRIDGE.unwrap());
    }
}
