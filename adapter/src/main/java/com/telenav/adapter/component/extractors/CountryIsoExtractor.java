package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_COUNTRY_ISO_CODE;


/**
 * Extract the 'tn__country_iso_code' tag's value
 *
 * @author dianat2
 */
public class CountryIsoExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'tn__country_iso_code' tag!");
        if (tags.containsKey(TN_COUNTRY_ISO_CODE.unwrap())) {
            return Optional.of(tags.get(TN_COUNTRY_ISO_CODE.unwrap()));
        }
        return Optional.empty();
    }
}