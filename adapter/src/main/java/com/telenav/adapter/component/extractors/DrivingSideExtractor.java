package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_DRIVING_SIDE;


/**
 * Extract the 'tn__adas_driving_side' tag's value from a way section
 *
 * @author ioanao
 */
public class DrivingSideExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting the driving side!");

        final String drivingSide = tags.get(TN_DRIVING_SIDE.unwrap());
        return drivingSide != null ? Optional.of(drivingSide) : Optional.empty();
    }
}