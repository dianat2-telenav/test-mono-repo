package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_DST;


/**
 * Extract the 'tn__dst' tag's value
 *
 * @author dianat2
 */
public class DstExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'tn__dst' tag!");
        if (tags.containsKey(TN_DST.unwrap())) {
            return Optional.of(tags.get(TN_DST.unwrap()));
        }
        return Optional.empty();
    }
}