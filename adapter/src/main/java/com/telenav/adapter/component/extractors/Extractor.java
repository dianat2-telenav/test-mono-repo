package com.telenav.adapter.component.extractors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;


/**
 * Interface for all specific road types com.telenav.adapter.component.extractors
 *
 * @author roxanal
 */
public interface Extractor<T> extends Serializable {

     Logger LOGGER = LogManager.getLogger(Extractor.class);

    String FERRY_VALUE = "ferry";
    String YES_VALUE = "yes";
    String NO_VALUE = "no";
    String ROUNDABOUT_VALUE = "roundabout";
    String PRIVATE_VALUE = "private";
    String SHUTTLE_TRAIN_VALUE = "shuttle_train";

    /**
     * Extracts the value(s) / key(s) of a certain tag
     *
     * @param tags a way section's tags
     * @return the value(s)/ key(s) of a certain tag
     */
    Optional<T> extract(final Map<String, String> tags);
}