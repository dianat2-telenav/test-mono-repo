package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;

/**
 * Extracts the 'foot' way tag.
 *
 * @author andriyz
 */
public class FootExtractor extends SingleTagExtractor {

    public FootExtractor() {
        super(OsmTagKey.FOOT.unwrap());
    }

}
