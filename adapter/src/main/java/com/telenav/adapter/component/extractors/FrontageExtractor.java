package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

/**
 * Extracts the 'frontage_road' way tag and change to 'frontage'
 */
public class FrontageExtractor implements Extractor<String> {
    private static final String OSM_FRONTAGE_ROAD_TAG_KEY = "frontage_road";

    @Override
    public Optional<String> extract(Map<String, String> tags) {
        return tags.containsKey(OSM_FRONTAGE_ROAD_TAG_KEY) ? Optional.of(tags.get(OSM_FRONTAGE_ROAD_TAG_KEY)) : Optional.empty();
    }
}