package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmRelationTagKey;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.adapter.bean.SpeedCategory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmHighwayValue.*;


/**
 * Extracts the speed category from the 'highway' tags. Ferry routes are considered special cases.
 *
 * @author petrum
 */
public class HighwayBasedSpeedCategoryExtractor implements Serializable, Extractor<SpeedCategory> {

    private static final long serialVersionUID = 1L;
    private static final Map<OsmHighwayValue, SpeedCategory> speedCategoryForHighway = new HashMap<>();

    static {
        speedCategoryForHighway.put(MOTORWAY, SpeedCategory.SPEED_CAT_BETWEEN_101_130);
        speedCategoryForHighway.put(MOTORWAY_LINK, SpeedCategory.SPEED_CAT_BETWEEN_51_70);
        speedCategoryForHighway.put(TRUNK, SpeedCategory.SPEED_CAT_BETWEEN_91_100);
        speedCategoryForHighway.put(TRUNK_LINK, SpeedCategory.SPEED_CAT_BETWEEN_51_70);
        speedCategoryForHighway.put(PRIMARY, SpeedCategory.SPEED_CAT_BETWEEN_71_90);
        speedCategoryForHighway.put(PRIMARY_LINK, SpeedCategory.SPEED_CAT_BETWEEN_31_50);
        speedCategoryForHighway.put(SECONDARY, SpeedCategory.SPEED_CAT_BETWEEN_51_70);
        speedCategoryForHighway.put(SECONDARY_LINK, SpeedCategory.SPEED_CAT_BETWEEN_31_50);
        speedCategoryForHighway.put(RESIDENTIAL, SpeedCategory.SPEED_CAT_BETWEEN_31_50);
        speedCategoryForHighway.put(RESIDENTIAL_LINK, SpeedCategory.SPEED_CAT_BETWEEN_31_50);
        speedCategoryForHighway.put(SERVICE, SpeedCategory.SPEED_CAT_BETWEEN_11_30);
        speedCategoryForHighway.put(TERTIARY, SpeedCategory.SPEED_CAT_BETWEEN_31_50);
        speedCategoryForHighway.put(TERTIARY_LINK, SpeedCategory.SPEED_CAT_BETWEEN_31_50);
        speedCategoryForHighway.put(ROAD, SpeedCategory.SPEED_CAT_BETWEEN_11_30);
        speedCategoryForHighway.put(TRACK, SpeedCategory.SPEED_CAT_BETWEEN_11_30);
        speedCategoryForHighway.put(UNCLASSIFIED, SpeedCategory.SPEED_CAT_BETWEEN_31_50);
        speedCategoryForHighway.put(UNDEFINED, SpeedCategory.SPEED_CAT_BETWEEN_11_30);
        speedCategoryForHighway.put(UNKNOWN, SpeedCategory.SPEED_CAT_BETWEEN_11_30);
    }

    /**
     * Extracts the speed category from the way section tags. Ferry is a special case.
     *
     * @param tags the way section tags as a map
     * @return the Optional of SC deduced by applying a set of rules on the tags. Empty optional if no rule can be
     * applied.
     */
    public Optional<SpeedCategory> extract(final Map<String, String> tags) {
        if (FERRY_VALUE.equals(tags.get(OsmRelationTagKey.ROUTE.unwrap()))) {
            return Optional.of(SpeedCategory.SPEED_CAT_BETWEEN_11_30);
        }
        if (SHUTTLE_TRAIN_VALUE.equals(tags.get(OsmRelationTagKey.ROUTE.unwrap()))) {
            return Optional.of(SpeedCategory.SPEED_CAT_BETWEEN_51_70);
        }
        final String highwayValue = tags.get(OsmTagKey.HIGHWAY.unwrap());
        if (highwayValue != null) {
            final Optional<OsmHighwayValue> enumHighwayValue = OsmHighwayValue.optionalValueOf(highwayValue);
            if (enumHighwayValue.isPresent()) {
                SpeedCategory sc = speedCategoryForHighway.get(enumHighwayValue.get());
                if (sc != null) {
                    return Optional.of(sc);
                }
            }
        }
        return Optional.of(SpeedCategory.SPEED_CAT_LESS_THAN_11);
    }
}
