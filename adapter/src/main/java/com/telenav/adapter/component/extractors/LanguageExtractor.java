package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_LANGUAGE_CODE;


/**
 * Extract the language from tags
 *
 * @author roxanal
 */
public class LanguageExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting the language!");

        final String language = tags.get(TN_LANGUAGE_CODE.unwrap());
        return language != null ? Optional.of(language) : Optional.empty();
    }
}