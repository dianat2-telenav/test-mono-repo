package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.adapter.bean.SpeedCategory;
import com.telenav.adapter.utils.SpeedConverter;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;



/**
 * Extracts the speed category from the maxspeed ('tn__maxspeed:forward' and 'tn__maxspeed:backward' or
 * 'tn__images_maxspeed:backward' and 'tn__images_maxspeed:forward') tags.
 *
 * @author petrum
 */
public class MaxSpeedBasedSpeedCategoryExtractor implements Serializable, Extractor<SpeedCategory> {

    @Override
    public Optional<SpeedCategory> extract(final Map<String, String> tagMap) {
        final String speedUnit = tagMap.get(TnTagKey.TN_SPEED_UNIT.unwrap());

        String maxSpeedForward = tagMap.get(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES));
        maxSpeedForward =
                maxSpeedForward != null ? maxSpeedForward : tagMap.get(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix());
        maxSpeedForward =
                maxSpeedForward != null ? maxSpeedForward : tagMap.get(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.ISA));

        String maxSpeedBackward = tagMap.get(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES));
        maxSpeedBackward =
                maxSpeedBackward != null ? maxSpeedBackward : tagMap.get(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix());
        maxSpeedBackward =
                maxSpeedBackward != null ? maxSpeedBackward : tagMap.get(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.ISA));

        final Optional<SpeedCategory> forwardSpeedCategory = getSpeedCategoryForSpeed(maxSpeedForward, speedUnit);
        final Optional<SpeedCategory> backwardSpeedCategory = getSpeedCategoryForSpeed(maxSpeedBackward, speedUnit);

        // If both tags (for forward and backward directions) are present
        if (forwardSpeedCategory.isPresent() && backwardSpeedCategory.isPresent()) {
            // the equality between them is checked. If they are different the maxspeed criterion is considered
            // ambiguous and therefore they will not be used as a criterion for speed categorization.
            return backwardSpeedCategory.get().equals(forwardSpeedCategory.get()) ? backwardSpeedCategory : Optional.empty();
            // Otherwise the category is calculated based on just one of them.
        } else {
            return backwardSpeedCategory.isPresent() ? backwardSpeedCategory : forwardSpeedCategory;
        }
    }

    private Optional<SpeedCategory> getSpeedCategoryForSpeed(final String speed, final String currentSpeedUnit) {
        if (speed != null && currentSpeedUnit != null) {
            double finalSpeed = currentSpeedUnit.equals(DefaultTagValue.MILES.unwrap()) ?
                    SpeedConverter.unitConvert(speed, DefaultTagValue.KM.unwrap()) : Double.parseDouble(speed);
            return Optional.of(SpeedCategory.getSpeedCategoryForSpeed(finalSpeed));
        }
        return Optional.empty();
    }
}
