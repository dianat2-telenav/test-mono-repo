package com.telenav.adapter.component.extractors;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class MaxSpeedExtractor implements Extractor<Map<String, String>> {
    private static final Set<String> MAX_SPEED_TAGS_KEY = new HashSet<>(Arrays.asList("maxspeed",
            "maxspeed:advisory:backward", "maxspeed:advisory:forward", "maxspeed:backward",
            "maxspeed:backward:conditional", "maxspeed:forward", "maxspeed:forward:conditional"));

    @Override
    public Optional<Map<String, String>> extract(Map<String, String> tags) {
        final Map<String, String> refTags = tags.keySet()
                .stream()
                .filter(MAX_SPEED_TAGS_KEY::contains)
                .collect(Collectors.toMap(key -> key, tags::get));
        return refTags.isEmpty() ? Optional.empty() : Optional.of(refTags);
    }
}
