package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;


/**
 * Extracts the "motor_vehicle" tag
 * @author raresd
 */
public class MotorVehicleExtractor extends SingleTagExtractor {

    public MotorVehicleExtractor() {
        super(OsmTagKey.MOTOR_VEHICLE.unwrap());
    }

}
