package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmTagKey.NAME;

/**
 * Extract the 'name' tag's value
 *
 * @author liviuc
 */
public class NameExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'name' tag!");
        if (tags.containsKey(NAME.unwrap())) {
            return Optional.of(tags.get(NAME.unwrap()));
        }
        return Optional.empty();
    }
}
