package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Extracts the one way tag's value
 *
 * @author roxanal
 */
public class OneWayExtractor implements Extractor<Map<String, String>> {

    @Override
    public Optional<Map<String, String>> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'oneway' tag's value!");
        final Map<String, String> oneWayTags = tags.keySet()
                .stream()
                .filter(this::isOnewayTagKey)
                .collect(Collectors.toMap(key -> key, tags::get));

        return oneWayTags.isEmpty() ? Optional.empty() : Optional.of(oneWayTags);
    }

    private boolean isOnewayTagKey(final String key) {
        return key.equals(OsmTagKey.ONEWAY.unwrap())
                || key.equals(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES))
                || key.equals(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES));
    }
}