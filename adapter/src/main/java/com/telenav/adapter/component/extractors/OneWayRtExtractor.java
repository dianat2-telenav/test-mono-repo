package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;


/**
 * Extracts the oneway tag value. The value can be extracted from the OSM tag ('oneway') or from the Telenav detections
 * tags ('tn__probes_oneway' and 'tn__images_oneway'). If 'tn__images_oneway' is present than its value will be returned.
 * If OSM 'oneway' tag is present than its value will be returned. Otherwise 'tn__probes_oneway' tag will be checked.
 *
 * @author petrum
 */
public class OneWayRtExtractor implements Serializable, Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tagMap) {
        final String osmOneWay = tagMap.get(OsmTagKey.ONEWAY.unwrap());
        final String probeOneWay = tagMap.get(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES));
        final String imageOneWay = tagMap.get(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES));
        final String finalOneWayValue = imageOneWay != null ? imageOneWay : osmOneWay != null ? osmOneWay : probeOneWay;
        return finalOneWayValue != null ? Optional.of(finalOneWayValue) : Optional.empty();
    }
}
