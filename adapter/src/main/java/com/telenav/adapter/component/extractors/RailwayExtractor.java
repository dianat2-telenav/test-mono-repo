package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.unidb.UnidbTagKey.RAILWAY;

/**
 * @author irinap
 */
public class RailwayExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(Map<String, String> tags) {
        return tags.containsKey(RAILWAY.unwrap()) ? Optional.of(tags.get(RAILWAY.unwrap())) : Optional.empty();
    }
}
