package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.telenav.datapipelinecore.osm.OsmTagKey.REF;


/**
 * Extract the 'tn__ref' tag's key(s)
 *
 * @author roxanal
 */
public class RefExtractor implements Extractor<Map<String, String>> {

    @Override
    public Optional<Map<String, String>> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'tn__ref(_index)' tags!");
        final Map<String, String> refTags = tags.keySet()
                .stream()
                .filter(tagKey -> tagKey.startsWith(REF.telenavPrefix()))
                .collect(Collectors.toMap(key -> key, tags::get));
        return refTags.isEmpty() ? Optional.empty() : Optional.of(refTags);
    }
}