package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmRelationTagKey;
import com.telenav.adapter.bean.RoadFunctionalClass;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmHighwayValue.*;
import static com.telenav.datapipelinecore.osm.OsmTagKey.HIGHWAY;


/**
 * Extracts the functional class from the way section tags.
 *
 * @author petrum
 */
public class RoadFunctionalClassExtractor implements Serializable, Extractor<RoadFunctionalClass> {

    private static final Map<OsmHighwayValue, RoadFunctionalClass> roadFunctionalClassForHighway = new HashMap<>();
    private static final HighwayExtractor HIGHWAY_EXTRACTOR = new HighwayExtractor();

    static {
        roadFunctionalClassForHighway.put(MOTORWAY, RoadFunctionalClass.FIRST_CLASS);
        roadFunctionalClassForHighway.put(MOTORWAY_LINK, RoadFunctionalClass.FIRST_CLASS);
        roadFunctionalClassForHighway.put(TRUNK, RoadFunctionalClass.SECOND_CLASS);
        roadFunctionalClassForHighway.put(TRUNK_LINK, RoadFunctionalClass.SECOND_CLASS);
        roadFunctionalClassForHighway.put(PRIMARY, RoadFunctionalClass.SECOND_CLASS);
        roadFunctionalClassForHighway.put(PRIMARY_LINK, RoadFunctionalClass.SECOND_CLASS);
        roadFunctionalClassForHighway.put(SECONDARY, RoadFunctionalClass.THIRD_CLASS);
        roadFunctionalClassForHighway.put(SECONDARY_LINK, RoadFunctionalClass.THIRD_CLASS);
        roadFunctionalClassForHighway.put(TERTIARY, RoadFunctionalClass.FOURTH_CLASS);
        roadFunctionalClassForHighway.put(TERTIARY_LINK, RoadFunctionalClass.FOURTH_CLASS);
        roadFunctionalClassForHighway.put(RESIDENTIAL, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(RESIDENTIAL_LINK, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(UNCLASSIFIED, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(SERVICE, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(ROAD, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(TRACK, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(UNDEFINED, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(UNKNOWN, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(LIVING_STREET, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(PRIVATE, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(FOOTWAY, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(PEDESTRIAN, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(STEPS, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(BRIDLEWAY, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(CONSTRUCTION, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(PATH, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(CYCLEWAY, RoadFunctionalClass.FIFTH_CLASS);
        roadFunctionalClassForHighway.put(BUS_GUIDEWAY, RoadFunctionalClass.FIFTH_CLASS);
    }

    //TODO service has special cases discuss after POC and add them

    /**
     * Extracts the functional class from the way section tags.
     * Ferry is treated as a special case.
     *
     * @param tags the way section tags as a map
     * @return Optional of FC deduced by applying a set of rules on the tags. Empty optional if no rules apply
     */
    public Optional<RoadFunctionalClass> extract(final Map<String, String> tags) {
        if (FERRY_VALUE.equals(tags.get(OsmRelationTagKey.ROUTE.unwrap()))) {
            return Optional.of(RoadFunctionalClass.SECOND_CLASS);
        }
        if (SHUTTLE_TRAIN_VALUE.equals(tags.get(OsmRelationTagKey.ROUTE.unwrap()))) {
            return Optional.of(RoadFunctionalClass.SECOND_CLASS);
        }
        Optional<String> highwayValue = HIGHWAY_EXTRACTOR.extract(tags);
        if(highwayValue.isPresent()) {
            final Optional<OsmHighwayValue> enumTagValue = OsmHighwayValue.optionalValueOf(tags.get(HIGHWAY.unwrap()));
            if (enumTagValue.isPresent()) {
                final RoadFunctionalClass functionalClass = roadFunctionalClassForHighway.get(enumTagValue.get());
                return Optional.of(functionalClass);
            }
        }
        return Optional.of(RoadFunctionalClass.FIFTH_CLASS);
    }
}
