package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_SHIELD_ICON;


/**
 * Extract the 'tn__shield_icon(_index)' tag's
 *
 * @author martap
 */
public class RoadShieldExtractor implements Extractor<Map<String, String>> {

    @Override
    public Optional<Map<String, String>> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'tn__shield_icon(_index)' tags!");
        final Map<String, String> rsTags =
                tags.keySet().stream().filter(tagKey -> tagKey.startsWith(TN_SHIELD_ICON.unwrap()))
                .collect(Collectors.toMap(key -> key, tags::get));
        return rsTags.isEmpty() ? Optional.empty() : Optional.of(rsTags);
    }
}