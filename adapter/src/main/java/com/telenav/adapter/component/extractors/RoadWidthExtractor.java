package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;

import java.util.Map;
import java.util.Optional;


/**
 * Extracts the 'width' tag's value
 *
 * @author roxanal
 */
public class RoadWidthExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'road width' tag's value!");
        if (tags.containsKey(OsmTagKey.WIDTH.unwrap())) {
            return Optional.of(tags.get(OsmTagKey.WIDTH.unwrap()));
        }
        return Optional.empty();
    }
}