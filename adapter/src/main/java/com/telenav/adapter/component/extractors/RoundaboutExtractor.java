package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmTagKey.JUNCTION;


/**
 * Extracts the 'roundabout' way tag
 *
 * @author roxanal
 */
public class RoundaboutExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'roundabout' tag!");
        if (hasRoundaboutTags(tags)) {
            return Optional.of(tags.get(JUNCTION.unwrap()));
        }
        return Optional.empty();
    }

    private boolean hasRoundaboutTags(final Map<String, String> tags) {
        return tags.containsKey(JUNCTION.unwrap()) && tags.get(JUNCTION.unwrap()).equals("roundabout");
    }
}