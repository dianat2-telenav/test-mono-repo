package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;

/**
 * Extracts the "route" tag
 *
 * @author petrum
 */
public class RouteExtractor extends SingleTagExtractor {

    public RouteExtractor() {
        super(OsmTagKey.ROUTE.unwrap());
    }
}
