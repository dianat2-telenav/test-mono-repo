package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;

/**
 * Extractor for surface tag.
 * @see SingleTagExtractor
 * @see Extractor
 *
 * @author Andrei Puf
 */
public class SurfaceExtractor extends SingleTagExtractor{

    /**
     * Initializes the SurfaceExtractor.
     */
    public SurfaceExtractor() {
        super(OsmTagKey.SURFACE.unwrap());
    }

}
