package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_TIMEZONE;


/**
 * Extract the language from tags
 *
 * @author dianat2
 */
public class TimezoneExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting the timezone!");

        final String timezone = tags.get(TN_TIMEZONE.unwrap());
        return timezone != null ? Optional.of(timezone) : Optional.empty();
    }
}