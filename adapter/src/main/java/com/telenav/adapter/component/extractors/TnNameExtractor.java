package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmTagKey.NAME;


/**
 * Extract the 'tn__name' tag's value
 *
 * @author roxanal
 */
public class TnNameExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'tn__name' tag!");
        if (tags.containsKey(NAME.telenavPrefix())) {
            return Optional.of(tags.get(NAME.telenavPrefix()));
        }
        return Optional.empty();
    }
}