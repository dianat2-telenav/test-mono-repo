package com.telenav.adapter.component.extractors;

import static com.telenav.datapipelinecore.osm.OsmTagKey.TOLL;


/**
 * Extracts the 'toll' way tag
 *
 * @author catalinm
 */
public class TollExtractor extends SingleTagExtractor {

    public TollExtractor() {
        super(TOLL.unwrap());
    }
}