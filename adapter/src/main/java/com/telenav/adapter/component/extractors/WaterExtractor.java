package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;

/**
 * Extracts the 'water' way tag.
 *
 * @author volodymyrl
 */
public class WaterExtractor extends SingleTagExtractor {

    public WaterExtractor() {
        super(OsmTagKey.WATER.unwrap());
    }
}
