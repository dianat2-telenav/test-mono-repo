package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.unidb.UnidbTagKey.WATERWAY;

/**
 * @author irinap
 */
public class WaterwayExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(Map<String, String> tags) {
        return tags.containsKey(WATERWAY.unwrap()) ? Optional.of(tags.get(WATERWAY.unwrap())) : Optional.empty();
    }
}
