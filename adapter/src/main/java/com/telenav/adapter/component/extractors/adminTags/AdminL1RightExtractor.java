package com.telenav.adapter.component.extractors.adminTags;

import com.telenav.adapter.component.extractors.Extractor;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_ADMIN_L1_RIGHT;


/**
 * Extract the 'tn__l1:right' tag's value
 *
 * @author dianat2
 */
public class AdminL1RightExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'tn__l1:right' tag!");
        if (tags.containsKey(TN_ADMIN_L1_RIGHT.unwrap())) {
            return Optional.of(tags.get(TN_ADMIN_L1_RIGHT.unwrap()));
        }
        return Optional.empty();
    }
}