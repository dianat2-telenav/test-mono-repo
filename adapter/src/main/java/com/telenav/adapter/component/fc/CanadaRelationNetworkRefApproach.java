package com.telenav.adapter.component.fc;

import com.telenav.map.entity.WaySection;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.telenav.adapter.component.fc.FcInfo.FC_SECOND;

/**
 * @author liviuc .
 */
public class CanadaRelationNetworkRefApproach {

    public static final String NO_FC_FLAG_VALUE = "6";
    public static final int NO_FC_FLAG_INT_VALUE = 0;

    /*
        National highways by province
     */
    // Alberta
    private static final List<String> ALBERTA_NATIONAL_HIGHWAYS = Arrays.asList(
            "1", "2", "3", "4", "9", "15", "16", "28", "28A", "35", "43", "49", "58", "63", "201", "216"
    );
    // British Columbia
    private static final List<String> BRITISH_COLUMBIA_NATIONAL_HIGHWAYS = Arrays.asList(
            "1", "2", "3", "4", "5", "11", "15", "16", "17", "19", "37", "95", "97", "97A", "97B", "97C", "99", "101"
    );
    // Manitoba
    private static final List<String> MANITOBA_NATIONAL_HIGHWAYS = Arrays.asList(
            "PTH 1", "PTH 6", "PTH 10", "PTH 16", "PTH 60", "PTH 75", "PTH 100", "PTH 101", "PTH 90", "PTH 190"
    );
    // New Brunswick
    private static final List<String> NEW_BRUNSWICK_NATIONAL_HIGHWAYS = Arrays.asList(
            "1", "2", "7", "8", "11", "15", "16", "17", "95", "111"
    );
    // Newfoundland and Labrador
    private static final List<String> NEWFOUNDLAND_AND_LABRADOR_NATIONAL_HIGHWAYS = Arrays.asList(
            "1", "2", "40", "100", "340", "350", "450A", "430", "500", "510"
    );
    // Northwest Territories
    private static final List<String> NORTHWEST_TERRITORRIES_NATIONAL_HIGHWAYS = Arrays.asList(
            "1", "2", "3", "4", "8"
    );
    // Northwest Territories
    private static final List<String> NORTHWEST_TERRITORRIES_PROVINCIAL_HIGHWAYS = Arrays.asList(
            "5", "6", "7", "9"
    );
    // Ontario
    private static final List<String> ONTARIO_NATIONAL_HIGHWAYS = Arrays.asList(
            "401", "402", "403", "405", "409", "410", "416", "417", "420", "427", "3", "6", "8", "10", "16", "19", "24", "26", "34", "61",
            "77", "101", "108", "115", "137", "138", "144", "QEW", "66", "69", "71", "17", "12", "11", "7", "417", "400"
    );
    // Nova Scotia
    private static final List<String> NOVA_SCOTIA_NATIONAL_HIGHWAYS = Arrays.asList(
            "NS 101", "NS 102", "NS 103", "NS 104", "NS 118", "NS 111", "NS 125", "303", "104", "105", "106"
    );
    // Prince Edward Islands
    private static final List<String> PRINCE_EDWARD_ISLANDS_NATIONAL_HIGHWAYS = Arrays.asList(
            "1", "1A", "2", "3"
    );
    // Quebec
    private static final List<String> QUEBEC_NATIONAL_HIGHWAYS = Arrays.asList(
            "5", "10", "13", "30", "31", "35", "50", "55", "70", "73", "440", "520", "540", "610", "640", "720", "109", "111", "112", "132",
            "133", "138", "139", "155", "161", "169", "170", "173", "175", "201", "204", "344", "389", "route de la baie James", "117", "185", "85",
            "40", "25", "20", "15"
    );
    // Yukon
    private static final List<String> YUKON_NATIONAL_HIGHWAYS = Arrays.asList(
            "1", "2", "5", "37"
    );
    private static final List<String> YUKON_PROVINCIAL_HIGHWAYS = Arrays.asList(
            "3", "4", "6", "7", "8", "9", "10", "11", "14", "15"
    );
    // Saskatchewan
    private static final List<String> SASKATCHEWAN_NATIONAL_HIGHWAYS = Arrays.asList(
            "1", "16", "2", "6", "7", "10", "11", "39"
    );
    /*
        FC 2 ref conditions
     */
    // Alberta
    private static final Predicate<List<String>> CANADA_ALBERTA_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:AB") || isoFcs.contains("CA:AB:primary") || isoFcs.contains("CA:AB:secondary") || isoFcs.contains("CA:AB:tertiary");
    // British Columbia
    private static final Predicate<List<String>> CANADA_BRITISH_COLUMBIA_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:BC") || isoFcs.contains("CA:BC:primary") || isoFcs.contains("CA:BC:secondary") || isoFcs.contains("CA:BC:tertiary");
    // Manitoba
    private static final Predicate<List<String>> CANADA_MANITOBA_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:MB") || isoFcs.contains("CA:MB:primary") || isoFcs.contains("CA:MB:secondary") || isoFcs.contains("CA:MB:tertiary") ||
                    isoFcs.contains("CA:MB:PTH") || isoFcs.contains("CA:MB:PR");
    // New Brunswick
    private static final Predicate<List<String>> CANADA_NEW_BRUNSWICK_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:NB:primary") || isoFcs.contains("CA:NB:secondary") || isoFcs.contains("CA:NB:tertiary")
                    || isoFcs.contains("CA:NB");
    // Newfoundland and Labrador
    private static final Predicate<List<String>> CANADA_NEWFOUNDLAND_AND_LABRADOR_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:NL") || isoFcs.contains("CA:NL:primary") || isoFcs.contains("CA:NL:secondary") || isoFcs.contains("CA:NL:tertiary")
                    || isoFcs.contains("CA:NL:R");
    // Northwest Territories
    private static final Predicate<List<String>> CANADA_NORTHWEST_TERRITORIES_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:NT") || isoFcs.contains("CA:NT:primary") || isoFcs.contains("CA:NT:secondary") || isoFcs.contains("CA:NT:tertiary");
    // Ontario
    private static final Predicate<List<String>> CANADA_ONTARIO_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:ON") || isoFcs.contains("CA:ON:primary") || isoFcs.contains("CA:ON:secondary") || isoFcs.contains("CA:ON:tertiary")
                    || isoFcs.contains("CA:ON:PR");
    // Nova Scotia
    private static final Predicate<List<String>> CANADA_NOVA_SCOTIA_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:NS") || isoFcs.contains("CA:NS:primary") || isoFcs.contains("CA:NS:secondary") || isoFcs.contains("CA:NS:tertiary")
                    || isoFcs.contains("CA:NS:H") || isoFcs.contains("CA:NS:T");
    // Prince Edwards Islands
    private static final Predicate<List<String>> CANADA_PRINCE_EDWARD_ISLANDS_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:PE") || isoFcs.contains("CA:PE:primary") || isoFcs.contains("CA:PE:secondary") || isoFcs.contains("CA:PE:tertiary");
    // Quebec
    private static final Predicate<List<String>> CANADA_QUEBEC_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:QC") || isoFcs.contains("CA:QC:primary") || isoFcs.contains("CA:QC:secondary") || isoFcs.contains("CA:QC:tertiary") ||
                    isoFcs.contains("CA:QC:A") || isoFcs.contains("CA:QC:R");
    // Yukon
    private static final Predicate<List<String>> CANADA_YUKON_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:YT") || isoFcs.contains("CA:YT:primary") || isoFcs.contains("CA:YT:secondary") || isoFcs.contains("CA:YT:tertiary");
    private static final Predicate<List<String>> CANADA_YUKON_FC_3_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:YT") || isoFcs.contains("CA:YT:primary") || isoFcs.contains("CA:YT:secondary") || isoFcs.contains("CA:YT:tertiary");
    // Saskatchewan
    private static final Predicate<List<String>> CANADA_SASKATCHEWAN_FC_2_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:SK") || isoFcs.contains("CA:SK:primary") || isoFcs.contains("CA:SK:secondary") || isoFcs.contains("CA:SK:tertiary");
    /*
        FC 3 ref conditions
     */
    private static final Predicate<List<String>> CANADA_NORTHWEST_TERRITORIES_FC_3_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:NT") || isoFcs.contains("CA:NT:primary") || isoFcs.contains("CA:NT:secondary") || isoFcs.contains("CA:NT:tertiary");
    private static final Predicate<List<String>> CANADA_SASKATCHEWAN_FC_3_CHECK_REF_CONDITION = (isoFcs) ->
            isoFcs.contains("CA:SK") || isoFcs.contains("CA:SK:primary") || isoFcs.contains("CA:SK:secondary") || isoFcs.contains("CA:SK:tertiary");
    /*
        Ref conditions list
     */
    private static final List<Predicate<List<String>>> REF_CONDITIONS = Arrays.asList(
            // Alberta
            CANADA_ALBERTA_FC_2_CHECK_REF_CONDITION,
            // British Columbia
            CANADA_BRITISH_COLUMBIA_FC_2_CHECK_REF_CONDITION,
            // Manitoba
            CANADA_MANITOBA_FC_2_CHECK_REF_CONDITION,
            // New Brunswick
            CANADA_NEW_BRUNSWICK_FC_2_CHECK_REF_CONDITION,
            // Newfoundland and Labrador
            CANADA_NEWFOUNDLAND_AND_LABRADOR_FC_2_CHECK_REF_CONDITION,
            // Northwest Territories
            CANADA_NORTHWEST_TERRITORIES_FC_2_CHECK_REF_CONDITION,
            CANADA_NORTHWEST_TERRITORIES_FC_3_CHECK_REF_CONDITION,
            // Ontario
            CANADA_ONTARIO_FC_2_CHECK_REF_CONDITION,
            // Nova Scotia
            CANADA_NOVA_SCOTIA_FC_2_CHECK_REF_CONDITION,
            // Prince Edward Islands
            CANADA_PRINCE_EDWARD_ISLANDS_FC_2_CHECK_REF_CONDITION,
            // Quebec
            CANADA_QUEBEC_FC_2_CHECK_REF_CONDITION,
            //CANADA_QUEBEC_FC_3_CHECK_REF_CONDITION,
            // Yukon
            CANADA_YUKON_FC_2_CHECK_REF_CONDITION,
            // Saskatchewan
            CANADA_SASKATCHEWAN_FC_2_CHECK_REF_CONDITION);
    /*
        Ref conditions highways map
     */
    private static final Map<Predicate<List<String>>, List<String>> refConditionsHighwaysMap = Stream.of(new Object[][]{
            // Alberta
            {CANADA_ALBERTA_FC_2_CHECK_REF_CONDITION, ALBERTA_NATIONAL_HIGHWAYS},
            // British Columbia
            {CANADA_BRITISH_COLUMBIA_FC_2_CHECK_REF_CONDITION, BRITISH_COLUMBIA_NATIONAL_HIGHWAYS},
            // Manitoba
            {CANADA_MANITOBA_FC_2_CHECK_REF_CONDITION, MANITOBA_NATIONAL_HIGHWAYS},
            // New Brunswick
            {CANADA_NEW_BRUNSWICK_FC_2_CHECK_REF_CONDITION, NEW_BRUNSWICK_NATIONAL_HIGHWAYS},
            // Newfoundland and Labrador
            {CANADA_NEWFOUNDLAND_AND_LABRADOR_FC_2_CHECK_REF_CONDITION, NEWFOUNDLAND_AND_LABRADOR_NATIONAL_HIGHWAYS},
            // Northwest Territories
            {CANADA_NORTHWEST_TERRITORIES_FC_2_CHECK_REF_CONDITION, NORTHWEST_TERRITORRIES_NATIONAL_HIGHWAYS},
            {CANADA_NORTHWEST_TERRITORIES_FC_3_CHECK_REF_CONDITION, NORTHWEST_TERRITORRIES_PROVINCIAL_HIGHWAYS},
            // Ontario
            {CANADA_ONTARIO_FC_2_CHECK_REF_CONDITION, ONTARIO_NATIONAL_HIGHWAYS},
            // Nova Scotia
            {CANADA_NOVA_SCOTIA_FC_2_CHECK_REF_CONDITION, NOVA_SCOTIA_NATIONAL_HIGHWAYS},
            // Prince Edward Islands
            {CANADA_PRINCE_EDWARD_ISLANDS_FC_2_CHECK_REF_CONDITION, PRINCE_EDWARD_ISLANDS_NATIONAL_HIGHWAYS},
            // Quebec
            {CANADA_QUEBEC_FC_2_CHECK_REF_CONDITION, QUEBEC_NATIONAL_HIGHWAYS},
            //{CANADA_QUEBEC_FC_3_CHECK_REF_CONDITION, QUEBEC_POVINCIAL_HIGHWAYS},
            // Yukon
            {CANADA_YUKON_FC_2_CHECK_REF_CONDITION, YUKON_NATIONAL_HIGHWAYS},
            {CANADA_YUKON_FC_3_CHECK_REF_CONDITION, YUKON_PROVINCIAL_HIGHWAYS},
            // Saskatchewan
            {CANADA_SASKATCHEWAN_FC_2_CHECK_REF_CONDITION, SASKATCHEWAN_NATIONAL_HIGHWAYS}
    }).collect(Collectors.toMap(data -> (Predicate<List<String>>) data[0], data -> (List<String>) data[1]));
    /*
        Province highway fc map
     */
    private static final Map<Predicate<List<String>>, String> provinceHighwaysFcMap = Stream.of(new Object[][]{
            // Alberta
            {CANADA_ALBERTA_FC_2_CHECK_REF_CONDITION, "2"},
            // British Columbia
            {CANADA_BRITISH_COLUMBIA_FC_2_CHECK_REF_CONDITION, "2"},
            // Manitoba
            {CANADA_MANITOBA_FC_2_CHECK_REF_CONDITION, "2"},
            // New Brunswick
            {CANADA_NEW_BRUNSWICK_FC_2_CHECK_REF_CONDITION, "2"},
            // Newfoundland and Labrador
            {CANADA_NEWFOUNDLAND_AND_LABRADOR_FC_2_CHECK_REF_CONDITION, "2"},
            // Northwest Territories
            {CANADA_NORTHWEST_TERRITORIES_FC_2_CHECK_REF_CONDITION, "2"},
            {CANADA_NORTHWEST_TERRITORIES_FC_3_CHECK_REF_CONDITION, "3"},
            // Ontario
            {CANADA_ONTARIO_FC_2_CHECK_REF_CONDITION, "2"},
            // Nova Scotia
            {CANADA_NOVA_SCOTIA_FC_2_CHECK_REF_CONDITION, "2"},
            // Prince Edward Islands
            {CANADA_PRINCE_EDWARD_ISLANDS_FC_2_CHECK_REF_CONDITION, "2"},
            // Quebec
            {CANADA_QUEBEC_FC_2_CHECK_REF_CONDITION, "2"},
            //{CANADA_QUEBEC_FC_3_CHECK_REF_CONDITION, "3"},
            // Yukon
            {CANADA_YUKON_FC_2_CHECK_REF_CONDITION, "2"},
            {CANADA_YUKON_FC_3_CHECK_REF_CONDITION, "3"},
            // Saskatchewan
            {CANADA_SASKATCHEWAN_FC_2_CHECK_REF_CONDITION, "2"},
            {CANADA_SASKATCHEWAN_FC_3_CHECK_REF_CONDITION, "3"}
    }).collect(Collectors.toMap(data -> (Predicate<List<String>>) data[0], data -> (String) data[1]));
    /*
        Highway series retrievers by province
     */
    // Alberta
    private static HighwaySeriesFcRetrieval ALBERTA_HIGHWAY_SERIES = ref -> {

        final Integer refAsInt = refAsInt(ref);

        if (refAsInt == 0) {

            return NO_FC_FLAG_VALUE;

        }

        if (refAsInt >= 1 && refAsInt <= 216) {

            return FcInfo.FC_THIRD;

        } else if (refAsInt >= 500 && refAsInt <= 986) {

            return FcInfo.FC_FOURTH;
        }

        return NO_FC_FLAG_VALUE;
    };
    // Alberta
    private static HighwaySeriesFcRetrieval YUKON_HIGHWAY_SERIES = ref -> {

        if (!YUKON_NATIONAL_HIGHWAYS.contains(ref)) {

            return NO_FC_FLAG_VALUE;

        }

        return FcInfo.FC_FOURTH;
    };
    // British Columbia
    private static HighwaySeriesFcRetrieval BRITISH_COLUMBIA_HIGHWAY_SERIES = ref -> {

        final Integer refAsInt = refAsInt(ref);

        if (refAsInt == 0) {

            return NO_FC_FLAG_VALUE;

        }

        if (refAsInt >= 1 && refAsInt <= 499 &&
                refAsInt != 40 &&
                refAsInt != 41 &&
                refAsInt != 59 &&
                refAsInt != 62 &&
                refAsInt != 103) {

            return FcInfo.FC_THIRD;

        }

        return FcInfo.FC_FOURTH;
    };
    // Quebec
    private static HighwaySeriesFcRetrieval QUEBEC_HIGHWAY_SERIES = ref -> {

        if (QUEBEC_NATIONAL_HIGHWAYS.contains(ref)) {
            return NO_FC_FLAG_VALUE;
        }

        final Integer refAsInt = refAsInt(ref);

        if (refAsInt == 0) {

            return NO_FC_FLAG_VALUE;

        }

        if (refAsInt > 100 && refAsInt < 201) {

            return FcInfo.FC_THIRD;

        }

        if (refAsInt > 200) {

            return FcInfo.FC_FOURTH;

        }

        return NO_FC_FLAG_VALUE;
    };
    // Ontario
    private static HighwaySeriesFcRetrieval ONTARIO_HIGHWAY_SERIES = ref -> {

        if (ONTARIO_NATIONAL_HIGHWAYS.contains(ref)) {
            return NO_FC_FLAG_VALUE;
        }

        final Integer refAsInt = refAsInt(ref);

        if (refAsInt == 0) {

            return NO_FC_FLAG_VALUE;

        }

        if (refAsInt < 700) {

            return FcInfo.FC_THIRD;

        }

        if (refAsInt > 700 && refAsInt <= 811) {

            return FcInfo.FC_FOURTH;

        }

        if (refAsInt > 7000) {

            return FcInfo.FC_FIFTH;

        }

        return NO_FC_FLAG_VALUE;
    };
    // Prince Edwards Islands
    private static HighwaySeriesFcRetrieval PRINCE_EDWARDS_ISLANDS_SERIES = ref -> {

        if (PRINCE_EDWARD_ISLANDS_NATIONAL_HIGHWAYS.contains(ref)) {
            return NO_FC_FLAG_VALUE;
        }

        final Integer refAsInt = refAsInt(ref);

        if (refAsInt == 0) {

            return NO_FC_FLAG_VALUE;

        }

        if (refAsInt == 142 || refAsInt <= 4) {

            return FcInfo.FC_THIRD;

        }

        if (refAsInt >= 101) {

            return FcInfo.FC_FOURTH;

        }

        return NO_FC_FLAG_VALUE;
    };
    // Newfoundland and Labrador
    private static HighwaySeriesFcRetrieval NEWFOUNDLAND_AND_LABRADOR_SERIES = ref -> {

        if (NEWFOUNDLAND_AND_LABRADOR_NATIONAL_HIGHWAYS.contains(ref)) {
            return NO_FC_FLAG_VALUE;
        }

        final Integer refAsInt = refAsInt(ref);

        if (refAsInt == 0) {

            return NO_FC_FLAG_VALUE;

        }

        if (refAsInt == 142 || refAsInt <= 4) {

            return FcInfo.FC_THIRD;

        }

        if (refAsInt >= 101) {

            return FcInfo.FC_FOURTH;

        }

        return NO_FC_FLAG_VALUE;
    };
    // Manitoba
    private static HighwaySeriesFcRetrieval MANITOBA_SERIES = ref -> {

        if (MANITOBA_NATIONAL_HIGHWAYS.contains(ref)) {
            return FC_SECOND;
        }

        if (ref.contains("PTH ")) {
            return FcInfo.FC_THIRD;
        }

        if (ref.contains("PR ")) {
            return FcInfo.FC_FOURTH;
        }

        return NO_FC_FLAG_VALUE;
    };
    // Saskatchewan
    private static HighwaySeriesFcRetrieval SASKATCHEWAN_SERIES = ref -> {

        if (SASKATCHEWAN_NATIONAL_HIGHWAYS.contains(ref)) {
            return NO_FC_FLAG_VALUE;
        }

        final Integer refAsInt = refAsInt(ref);

        if (refAsInt == 0) {

            return NO_FC_FLAG_VALUE;

        }

        if (refAsInt < 600) {

            return FcInfo.FC_THIRD;

        }

        if (refAsInt >= 600) {

            return FcInfo.FC_FOURTH;

        }

        return NO_FC_FLAG_VALUE;
    };
    /*
        Province highway series check map
     */
    private static final Map<Predicate<List<String>>, List<HighwaySeriesFcRetrieval>> provinceHighwaySeriesCheckMap = Stream.of(new Object[][]{
            // Alberta
            {CANADA_ALBERTA_FC_2_CHECK_REF_CONDITION, Arrays.asList(ALBERTA_HIGHWAY_SERIES)},
            {CANADA_BRITISH_COLUMBIA_FC_2_CHECK_REF_CONDITION, Arrays.asList(BRITISH_COLUMBIA_HIGHWAY_SERIES)},
            {CANADA_ONTARIO_FC_2_CHECK_REF_CONDITION, Arrays.asList(ONTARIO_HIGHWAY_SERIES)},
            {CANADA_QUEBEC_FC_2_CHECK_REF_CONDITION, Arrays.asList(QUEBEC_HIGHWAY_SERIES)},
            {CANADA_PRINCE_EDWARD_ISLANDS_FC_2_CHECK_REF_CONDITION, Arrays.asList(PRINCE_EDWARDS_ISLANDS_SERIES)},
            {CANADA_SASKATCHEWAN_FC_2_CHECK_REF_CONDITION, Arrays.asList(SASKATCHEWAN_SERIES)},
            {CANADA_MANITOBA_FC_2_CHECK_REF_CONDITION, Arrays.asList(MANITOBA_SERIES)},
            {CANADA_YUKON_FC_2_CHECK_REF_CONDITION, Arrays.asList(YUKON_HIGHWAY_SERIES)},

    }).collect(Collectors.toMap(data -> (Predicate<List<String>>) data[0], data -> (List<HighwaySeriesFcRetrieval>) data[1]));

    public static String fc(final WaySection waySection,
                            final List<String> isoFcs,
                            String prevFc) {

        final Map<String, String> tags = waySection.tags();

        if (prevFc == null ||
                !prevFc.equals(FcInfo.FC_FIRST)) {

            final String[] refs = TagHelper.split(TagService.ref(tags));

            final List<Predicate<List<String>>> refConditions = refCondition(isoFcs);

            if (!refConditions.isEmpty()) {

                for (final Predicate<List<String>> refCondition : refConditions) {

                    final String regionHighwayFc = provinceHighwaysFcMap.get(refCondition);

                    final List<String> regionHighways = refConditionsHighwaysMap.get(refCondition);

                    prevFc = fc(regionHighways, refs, regionHighwayFc);

                    if (prevFc != NO_FC_FLAG_VALUE) {
                        break;
                    }

                    final List<HighwaySeriesFcRetrieval> highwaySeriesFcRetrieval = provinceHighwaySeriesCheckMap.get(refCondition);

                    prevFc = fc(highwaySeriesFcRetrieval, refs);

                    if (prevFc != NO_FC_FLAG_VALUE) {
                        break;
                    }
                }
            }
        }

        return prevFc;
    }

    private static List<Predicate<List<String>>> refCondition(final List<String> isoFcs) {

        if (isoFcs == null || isoFcs.isEmpty()) {

            return Collections.emptyList();

        }

        final List<Predicate<List<String>>> refConditions = new LinkedList<>();

        for (final Predicate<List<String>> refCondition : REF_CONDITIONS) {

            if (refCondition.test(isoFcs)) {

                refConditions.add(refCondition);
            }

        }

        return refConditions;
    }

    public static String fc(final List<String> highways,
                            final String[] refs,
                            final String fc) {

        for (final String ref : refs) {

            if (highways.contains(ref)) {

                return fc;
            }
        }

        return NO_FC_FLAG_VALUE;
    }

    private static String fc(final List<HighwaySeriesFcRetrieval> highwaySeriesFcRetrieval,
                             final String[] refs) {

        if (highwaySeriesFcRetrieval == null) {
            return NO_FC_FLAG_VALUE;
        }

        for (final String ref : refs) {

            for (final HighwaySeriesFcRetrieval seriesFcRetrieval : highwaySeriesFcRetrieval) {

                final String fc = seriesFcRetrieval.fc(ref);

                if (!fc.equals(NO_FC_FLAG_VALUE)) {
                    return fc;
                }
            }
        }

        return NO_FC_FLAG_VALUE;
    }

    private static String REF_REPLACE_REGEXP = "[a-zA-Z]";

    private static Integer refAsInt(String ref) {

        ref = ref.replaceAll(REF_REPLACE_REGEXP, StringUtils.EMPTY)
                .trim();

        if (StringUtils.isEmpty(ref)) {
            return NO_FC_FLAG_INT_VALUE;
        }

        try {

            return Integer.parseInt(ref);

        } catch (NumberFormatException e) {

            return NO_FC_FLAG_INT_VALUE;
        }
    }

    @FunctionalInterface
    interface HighwaySeriesFcRetrieval {

        String fc(final String ref);
    }
}
