package com.telenav.adapter.component.fc;

import java.util.Arrays;
import java.util.function.Predicate;

/**
 * @author liviuc
 */
public class ConditionsConjunction<T> implements Conditions<T> {

    private final Predicate<T>[] conditions;

    public ConditionsConjunction(final Predicate<T>... conditions) {
        this.conditions = conditions;
    }

    public boolean isTrue(final T t) {
        return Arrays.stream(conditions)
                .map(condition -> condition.test(t))
                .allMatch(val -> val == true);
    }
}
