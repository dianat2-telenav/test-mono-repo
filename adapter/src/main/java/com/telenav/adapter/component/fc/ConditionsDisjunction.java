package com.telenav.adapter.component.fc;

import java.util.Arrays;

/**
 * @author liviuc
 */
public class ConditionsDisjunction<T> implements Conditions<T> {

    private final Conditions<T>[] conjunctions;

    public ConditionsDisjunction(final Conditions<T>... conjunctions) {
        this.conjunctions = conjunctions;
    }

    public boolean isTrue(final T t) {
        return Arrays.stream(conjunctions)
                .map(condition -> condition.isTrue(t))
                .anyMatch(val -> val == true);
    }
}
