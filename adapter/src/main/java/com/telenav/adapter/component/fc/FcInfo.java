package com.telenav.adapter.component.fc;

import com.telenav.adapter.component.repository.ResourceRepository;
import com.telenav.datapipelinecore.bean.Relation;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static com.telenav.adapter.bean.FcCountry.*;
import static com.telenav.adapter.component.fc.FcExemptionsWhiteList.*;
import static com.telenav.adapter.component.fc.TagService.*;

/**
 * @author liviuc
 */
public class FcInfo {

    /*
        The FCs
    */
    public static final String FC_FIFTH = "5";
    public static final String FC_FOURTH = "4";
    public static final String FC_THIRD = "3";
    public static final String FC_SECOND = "2";
    public static final String FC_FIRST = "1";
    public static final List<String> ROUTES_ISO_WHITELIST = Arrays.asList(USA.name(), MEX.name(), CAN.name());
    /*
        FC detection approach.
     */
    public static final List<String> RELATIONS_BASED_APPROACH = Arrays.asList(USA.name(), CAN.name());
    public static final List<String> REF_BASED_APPROACH = Arrays.asList(USA.name(), MEX.name());
    public static final List<String> WHITELIST_BASED_APPROACH = Arrays.asList(USA.name(), CAN.name());
    /*
       Road network detection priority (in case a regexp matches to multiple networks).
    */
    public static final List<String> USA_FC_PRIORITY = Arrays.asList(FC_FIRST, FC_SECOND, FC_THIRD, FC_FOURTH);
    public static final List<String> MEX_FC_PRIORITY = Arrays.asList(FC_FIRST, FC_SECOND, FC_THIRD);
    public static final List<String> CAN_FC_PRIORITY = Arrays.asList(FC_FIRST, FC_SECOND, FC_THIRD, FC_FOURTH);
    /*
        In case no fc detection is successful, this value corresponding to a specific country is assigned.
     */
    public static final Map<String, Integer> DOWNGRADE_THRESHOLD_BY_COUNTRY = new HashMap<String, Integer>() {{
        put(CAN.name(), 5);
        put(USA.name(), 5);
        put(MEX.name(), 4);
    }};
    /*
        Resource info.
    */
    public static List<String> ROUTES_RELATIONS_ISO_WHITELIST;

    static {
        try {

            ROUTES_RELATIONS_ISO_WHITELIST = ResourceRepository.getFcIsos();

        } catch (final IOException e) {

            throw new RuntimeException(e);
        }
    }

    /**
     * This class is used to detect relations that help in detecting fcs.
     *
     * @author liviuc
     */
    public static class FcRelatedRelationConditions {

        /*
            Network relation related.
        */
        private static final Predicate<Relation> hasNetworkKey = relation -> hasKey(relation.getTags(), NETWORK_TAG);
        private static final Predicate<Relation> isRouteType = relation -> hasKeyWithValue(relation.getTags(), TYPE_TAG, "route");
        private static final Predicate<Relation> isRoadRoute = relation -> hasKeyWithValue(relation.getTags(), ROUTE_TAG, "road");

        private static final Predicate<Relation> isRuleExempted = relation -> ruleExempted(relation);

        private static final ConditionsConjunction<Relation> NETWORK_RELATION_CONDITON =
                new ConditionsConjunction<Relation>(hasNetworkKey, isRouteType, isRoadRoute);
        private static final ConditionsConjunction<Relation> RULE_EXEMPTED_RELATION_CONDITION =
                new ConditionsConjunction<Relation>(isRuleExempted);
        /*
            Network relation condition.
        */
        public static final ConditionsDisjunction<Relation> NETWORK_RELATION_CONDITONS =
                new ConditionsDisjunction<Relation>(NETWORK_RELATION_CONDITON, RULE_EXEMPTED_RELATION_CONDITION);

        /*
            Relation with ISO related.
        */
        public static Predicate<Relation> isExemptedName = relation -> RULE_EXEMPTED_NAMES.contains(name(relation.getTags()));

        public static Predicate<Relation> isExemptedRefConditionedByNetwork = relation ->
                EXEMPTED_REFS.contains(ref(relation.getTags())) &&
                        EXEMPTED_NETWORKS.contains(network(relation.getTags()));

        private static boolean hasKeyWithValue(final Map<String, String> tags,
                                               final String key,
                                               final String value) {
            return hasKey(tags, key) && value.equalsIgnoreCase(tags.get(key));
        }

        private static boolean hasKey(final Map<String, String> tags,
                                      final String key) {
            return tags.containsKey(key);
        }
    }

}
