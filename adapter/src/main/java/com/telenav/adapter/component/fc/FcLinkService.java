package com.telenav.adapter.component.fc;

import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.*;
import java.util.stream.Collectors;

import static com.telenav.adapter.component.fc.FcService.NAVIGABLE_WAYS;
import static com.telenav.adapter.component.fc.TagService.*;

/**
 * @author liviuc .
 */
@Slf4j
public class FcLinkService {

    private static final Function<List<LightWeightWaySection>, List<LightWeightWaySection>> ACC = str -> str;
    private static final Function2<List<LightWeightWaySection>, List<LightWeightWaySection>, List<LightWeightWaySection>> ADD = (list, str) -> {
        list.addAll(str);
        return list.stream().distinct().collect(Collectors.toList());
    };
    private static final Function2<List<LightWeightWaySection>, List<LightWeightWaySection>, List<LightWeightWaySection>> MERGE = (left, right) -> {
        left.addAll(right);
        return left.stream().distinct().collect(Collectors.toList());
    };
    private static final PairFunction<LightWeightWaySection, Long, List<LightWeightWaySection>> BY_ID_AS_LIST = waySection -> {
        final List<LightWeightWaySection> list = new ArrayList<>();
        list.add(waySection);
        return new Tuple2<>(waySection.getId(), list);
    };
    private static final int FC_DEFAULT_VALUE = 5;

    private static JavaRDD<WaySection> navigable(final JavaRDD<WaySection> adaptedWaySections) {
        return adaptedWaySections.filter(waySection -> NAVIGABLE_WAYS.contains(waySection.tags().get(HIGHWAY_TAG)));
    }

    private static JavaRDD<WaySection> nonNavigable(final JavaRDD<WaySection> adaptedWaySections) {
        return adaptedWaySections.filter(waySection -> !NAVIGABLE_WAYS.contains(waySection.tags().get(HIGHWAY_TAG)));
    }

    public static JavaRDD<WaySection> assignFcToLinks(final JavaRDD<WaySection> waySections,
                                                      final JavaSparkContext javaSparkContext) {

        /*
            FC assignment is performed only for navigable ways.
            We filter out the non navigable ones in order to decrease the amount of
            processed data, thus increasing performance.
         */
        final JavaRDD<WaySection> nonNavigableWaySections = nonNavigable(waySections);

        // Filter just the required data for processing.
        final JavaRDD<WaySection> navigableWaySections = navigable(waySections).cache();

        // Filter links.
        final JavaRDD<WaySection> links = navigableWaySections.filter(FcLinkService::link).cache();

        /*
            Filter last node id of links - because it MIGHT be the first
            node of a non-link highway, thus we detect the destination highway of the link,
            thus later on we can assign the FC of the destination highway to the succession of links
            leading to it.
         */
        final JavaRDD<Long> linksLastNodeIds = links.map(FcLinkService::lastNodeId).distinct();

        // Broadcast the links as a set (to increase performance when calling the .contains(...) method.
        final Set<Long> linksLastNodeIdsBroadcast = set(linksLastNodeIds, javaSparkContext);

        // The "stop values" are ids of nodes that are the end of a link, but the start of a non-link.
        final JavaRDD<Long> nodeIdsStopValues = navigableWaySections.flatMap(waySection -> stopValuesForNodeIds(linksLastNodeIdsBroadcast, waySection));

        // Broadcast the "stop values".
        final Set<Long> stopValues = set(nodeIdsStopValues, javaSparkContext);

        // We group the links by their first node id - i.e. the entry point to a link succession.
        final JavaPairRDD<Long, List<LightWeightWaySection>> linksByFirstNodeId =
                links.flatMapToPair(FcLinkService::byFirstNodeIdDWithNonOneWaysDuplication)
                        .combineByKey(ACC, ADD, MERGE);

        // Broadcast them.
        final Map<Long, List<LightWeightWaySection>> linksByFirstNodeIdBroadCast = map(linksByFirstNodeId, javaSparkContext);

        // Filter the last way section part of a non-link before entering a link.
        final JavaRDD<WaySection> lastHighwayBeforeMotorwayJunctionWaySections =
                navigableWaySections.filter(FcLinkService::nonLinkNonOnewayMotorwayJunction)
                        .flatMap(FcLinkService::motorwayJunctions);

        /*
            Compute link paths - i.e. link successions all the way from exiting a non-link and entering a non-link
            via links.
         */
        final JavaRDD<List<List<LightWeightWaySection>>> linkPaths = lastHighwayBeforeMotorwayJunctionWaySections
                .map(waySection -> paths(FcService.lightWeight(waySection), linksByFirstNodeIdBroadCast, stopValues))
                .filter(list -> !list.isEmpty())
                .cache();

        // Filter the last node ids of the link paths.
        final JavaRDD<Long> lasNodeIdsOfPaths = linkPaths.flatMap(FcLinkService::lastNodeIds).distinct();

        // Broadcast them.
        final Set<Long> lastNodeIdsOfPathsBroadcast = set(lasNodeIdsOfPaths, javaSparkContext);

        // Get non links by 1st node ids of the link paths.
        final JavaPairRDD<Long, List<LightWeightWaySection>> nonLinksByFirstNodeId =
                navigableWaySections.filter(waySection -> lastNodeIdsOfPathsBroadcast.contains(firstNodeId(waySection)))
                        .flatMapToPair(FcLinkService::byFirstNodeIdDWithNonOneWaysDuplication)
                        .combineByKey(ACC, ADD, MERGE);

        // Broadcast them.
        final Map<Long, List<LightWeightWaySection>> nonLinksByFirstNodeIdBroadcast =
                javaSparkContext.broadcast(nonLinksByFirstNodeId.collectAsMap()).value();

        /*
             Given a link path (succession) composed by multiple links that end up
             into non-links entry points we assign that path the most significant
             FC detected at the end.
             For links that are part of multiple link paths we assign them the most significant FC
             coming from the end of one of the paths the link is part of.
             We store this information as a pair made out of the link id and the enhanced FC.
         */
        final JavaPairRDD<Long, String> enhancedLinksById = enhancedFcByWaySectionId(linkPaths, nonLinksByFirstNodeIdBroadcast);

        // We broadcast those pairs
        final Map<Long, String> enhancedLinksByIdBroadcast = map(enhancedLinksById, javaSparkContext);

        /*
            If a navigable way section is detected to have a corresponding LINK!!! enhanced FC,
            then that means that the navigable way section is a link and we enhance it with the detected FC.
            If not we just leave the way section as it is.
         */
        final JavaRDD<WaySection> enhancedNavigableWaySections = navigableWaySections.map(waySection -> {
            final String enhancedFc = enhancedLinksByIdBroadcast.get(waySection.id());
            return (enhancedFc != null) ? waySection.withTag(FC_TAG, enhancedFc) : waySection;
        });

        /*
            Join the navigable link enhanced way sections with the non-navigable way sections
            to prevent data loss.
         */
        return enhancedNavigableWaySections.union(nonNavigableWaySections);
    }

    private static JavaPairRDD<Long, String> enhancedFcByWaySectionId(final JavaRDD<List<List<LightWeightWaySection>>> linkPaths,
                                                                      final Map<Long, List<LightWeightWaySection>> nonLinksByFirstNodeIdBroadcast) {
        // Enhance the links with the FC corresponding to the end of the path - i.e. destination non-link
        return linkPaths.flatMap(paths -> enhancePaths(paths, nonLinksByFirstNodeIdBroadcast))
                .mapToPair(BY_ID_AS_LIST)
                .combineByKey(ACC, ADD, MERGE)
                .mapToPair(tuple -> {
                    final List<LightWeightWaySection> enhancedWaySections = tuple._2();
                    /*
                        For links part of multiple paths we keep just the most significant FC.
                        This we make sure we only have one value per link, regardless of how many paths
                        the link is part of AND we also avoid handle the previous duplication trick
                        for non-oneway way sections
                     */
                    return new Tuple2<>(tuple._1(), String.valueOf(mostSignificantFc(enhancedWaySections)));
                });
    }

    private static int mostSignificantFc(final List<LightWeightWaySection> enhancedWaySections) {
        return enhancedWaySections.stream()
                .mapToInt(FcLinkService::fcOrDefaultValue)
                .min()
                .getAsInt();
    }

    private static int fcOrDefaultValue(final LightWeightWaySection waySection) {
        if (waySection.hasFc()) {
            try {
                return Integer.parseInt(waySection.getFc());
            } catch (final NumberFormatException nfe) {
                log.error(nfe.getMessage() + " " + waySection.getFc());
            }
        }
        return FC_DEFAULT_VALUE;
    }

    private static Iterator<LightWeightWaySection> enhancePaths(final List<List<LightWeightWaySection>> paths,
                                                                final Map<Long, List<LightWeightWaySection>> nonLinksByFirstNodeIdBroadcast) {

        final Map<Long, Integer> fcsByWaySectionId = new HashMap<>();

        final Set<LightWeightWaySection> enhancedLinks = new HashSet<>();

        for (final List<LightWeightWaySection> path : paths) {

            final long lastNodeOfPathId = lastNodeId(path.get(0));

            final List<LightWeightWaySection> pathEndNonLink = nonLinksByFirstNodeIdBroadcast.get(lastNodeOfPathId);

            short mostSignificantFc = 5;

            LightWeightWaySection endLink = null;

            if (pathEndNonLink == null || pathEndNonLink.isEmpty()) {
                continue;
            }

            for (final LightWeightWaySection nonLink : pathEndNonLink) {

                if (nonLink.hasFc()) {

                    final Short nonLinkFc = Short.valueOf(nonLink.getFc());

                    if (nonLinkFc <= mostSignificantFc) {

                        mostSignificantFc = nonLinkFc;
                        endLink = nonLink;
                    }
                }
            }

            if (endLink != null) {

                path.add(0, endLink);

            }

            final int nonLinkEndFc = Math.max(fcOrDefaultValue(path.get(0)), fcOrDefaultValue(path.get(path.size() - 1)));

            for (int i = 1; i < path.size() - 1; i++) {

                final long waySectionId = path.get(i).getId();

                if (!fcsByWaySectionId.containsKey(waySectionId)) {

                    fcsByWaySectionId.put(waySectionId, nonLinkEndFc);

                } else if (fcsByWaySectionId.get(waySectionId) > nonLinkEndFc) {

                    fcsByWaySectionId.put(waySectionId, nonLinkEndFc);

                }
            }
        }

        for (final List<LightWeightWaySection> path : paths) {

            for (int i = 1; i < path.size() - 1; i++) {

                LightWeightWaySection link = path.get(i);

                if (!enhancedLinks.contains(link)) {

                    final Integer linkFc = fcsByWaySectionId.get(link.getId());

                    link.setFc(String.valueOf(linkFc));

                    enhancedLinks.add(link);
                }
            }
        }

        return enhancedLinks.iterator();
    }

    private static List<List<LightWeightWaySection>> paths(final LightWeightWaySection start,
                                                           final Map<Long, List<LightWeightWaySection>> waySectionsByFirstNodeId,
                                                           final Set<Long> stopValues) {
        /*
            Given the start way section,
            we retrieve the nex way sections based on the former's last node
            and the latters' first node.
         */
        final List<LightWeightWaySection> successiveWaySections = waySectionsByFirstNodeId.get(start.getLastNode().getId());

        final List<List<LightWeightWaySection>> paths = new LinkedList<>();

        // If somehow there's no next link, then we return an empty path.
        if (successiveWaySections == null || successiveWaySections.isEmpty()) {

            return paths;

        }
        /*
            Iterate through the successive way sections,
            building up a new path made out of the start and each of the next way sections.
         */
        for (final LightWeightWaySection next : successiveWaySections) {

            //log.error("current: " + start.strictIdentifier() + " next: " + next.strictIdentifier());

            /*
                Sometimes we might end up "running in circle".
                To avoid this case, we keep track of the previously "visited" way sections.
             */
            final Set<Long> visitedIds = new HashSet<>();
            visitedIds.add(start.getId());
            visitedIds.add(next.getId());

            paths.addAll(path(next, waySectionsByFirstNodeId, stopValues, visitedIds));

            for (final List<LightWeightWaySection> path : paths) {

                path.add(start);
            }
        }

        return paths;
    }

    private static Iterator<Tuple2<Long, List<LightWeightWaySection>>> byFirstNodeIdDWithNonOneWaysDuplication(final WaySection waySection) {

        final LightWeightWaySection lightWeightWaySection = FcService.lightWeight(waySection);

        if (isOneway(waySection)) {

            final List<LightWeightWaySection> list = new ArrayList<>();
            list.add(lightWeightWaySection);

            final List<Tuple2<Long, List<LightWeightWaySection>>> result = new ArrayList<>();
            result.add(new Tuple2<>(lightWeightWaySection.getFirstNode().getId(), list));
            return result.iterator();

        }
        /*
            In order to be able to "iterate" to the links we need a movement direction.
            The best way to achieve this is to simulate NON-oneway links as oneway links.
            We achieve this by duplicating the link and switching the first and the last nodes.
         */
        final List<LightWeightWaySection> linkWaySections = new ArrayList<>();
        linkWaySections.add(lightWeightWaySection);
        linkWaySections.add(lightWeightWaySection.switchNodes());

        final List<Tuple2<Long, List<LightWeightWaySection>>> result = new ArrayList<>();
        for (final LightWeightWaySection linkWaySection : linkWaySections) {

            final List<LightWeightWaySection> linkWaySectionResult = new ArrayList<>();
            linkWaySectionResult.add(linkWaySection);
            result.add(new Tuple2<>(linkWaySection.getFirstNode().getId(), linkWaySectionResult));

        }
        return result.iterator();
    }

    private static Iterator<Long> lastNodeIds(final List<List<LightWeightWaySection>> paths) {
        return paths.stream()
                .map(path -> lastNodeId(path.get(0)))
                .collect(Collectors.toList())
                .iterator();
    }

    private static Long lastNodeId(final LightWeightWaySection lightWeightWaySection) {
        return lightWeightWaySection.getLastNode().getId();
    }

    private static Iterator<WaySection> motorwayJunctions(final WaySection waySection) {

        final boolean firsNodeMotorwayJunction = isMotorwayJunction(firstNode(waySection));
        final boolean lastNodeMotorwayJunction = isMotorwayJunction(lastNode(waySection));

        if (firsNodeMotorwayJunction && lastNodeMotorwayJunction) {

            return Arrays.asList(
                    waySection.replaceNodes(Arrays.asList(firstNode(waySection), lastNode(waySection).withoutTag(HIGHWAY_TAG))),
                    waySection.replaceNodes(Arrays.asList(lastNode(waySection), firstNode(waySection).withoutTag(HIGHWAY_TAG)))
            ).iterator();
        }

        return Arrays.asList(waySection).iterator();
    }

    private static boolean nonLinkNonOnewayMotorwayJunction(final WaySection waySection) {

        final boolean nonLink = FcService.nonLink(waySection);

        return isMotorwayJunction(lastNode(waySection)) && nonLink ||
                isMotorwayJunction(firstNode(waySection)) && nonLink &&
                        !isOneway(waySection);
    }

    private static boolean isMotorwayJunction(final Node node) {
        return node.hasTag(HIGHWAY_TAG, MOTORWAY_JUNCTION_VALUE);
    }

    private static Iterator<Long> stopValuesForNodeIds(final Set<Long> linksLastNodeIdsBroadcast,
                                                       final WaySection waySection) {

        final boolean linkConnectedToWaySection = linksLastNodeIdsBroadcast.contains(firstNodeId(waySection));

        if (isOneway(waySection) && linkConnectedToWaySection && FcService.nonLink(waySection)) {

            final List<Long> ids = new ArrayList<>();

            ids.add(firstNodeId(waySection));

            return ids.iterator();
        }

        final boolean linkConnectedToWaySectionFromOtherEnd = linksLastNodeIdsBroadcast.contains(lastNodeId(waySection));

        if ((linkConnectedToWaySection || linkConnectedToWaySectionFromOtherEnd) && FcService.nonLink(waySection)) {

            final List<Long> ids = new ArrayList<>();

            ids.add(firstNodeId(waySection));

            ids.add(lastNodeId(waySection));

            return ids.iterator();
        }

        return new ArrayList<Long>().iterator();
    }

    private static Node firstNode(final WaySection waySection) {
        return waySection.firstNode();
    }

    private static Node lastNode(final WaySection waySection) {
        return waySection.lastNode();
    }

    private static boolean isOneway(final WaySection waySection) {
        return waySection.hasTag(ONEWAY_TAG, "yes");
    }

    private static List<List<LightWeightWaySection>> path(final LightWeightWaySection current,
                                                          final Map<Long, List<LightWeightWaySection>> waySectionsByFirstNodeId,
                                                          final Set<Long> stopValues,
                                                          final Set<Long> visited) {

        // Get the successive way sections from the current one.
        final List<LightWeightWaySection> successiveWaySections = waySectionsByFirstNodeId.get(current.getLastNode().getId());

        final List<List<LightWeightWaySection>> result = new LinkedList<>();

        // If there's no successive element we return a path made out of just the current element.
        if (successiveWaySections == null || stopValues.contains(current.getLastNode().getId())) {

            final List<LightWeightWaySection> singleElementList = new ArrayList<>(1);
            singleElementList.add(current);

            result.add(singleElementList);

            return result;

        }

        /*
            Iterate through the successive way sections,
            building up a new path made out of the current and each of the next way sections.
         */
        for (final LightWeightWaySection next : successiveWaySections) {

            // In case we've visited this next way section already, we don't follow this path again.
            if (visited.contains(next.getId())) {

                final List<LightWeightWaySection> path = new ArrayList<>();
                path.add(current);
                result.add(path);
                continue;

            }

            if (next.getId() == current.getId()) {

                final List<LightWeightWaySection> path = new ArrayList<>();
                path.add(current);
                result.add(path);

            } else {

                // We check if we must stop at the next way section
                final boolean stop = stopValues.contains(next.getFirstNode().getId());

                // If so, we have a path and we add the current and the next elements to it.
                if (stop) {
                    final List<LightWeightWaySection> path = new ArrayList<>();
                    path.add(next);
                    path.add(current);
                    // And also add the path to the paths list.
                    result.add(path);
                } else {

                    //log.error("current: " + current.strictIdentifier() + " next: " + next.strictIdentifier());

                    // If not, we continue the process.
                    visited.add(next.getId());

                    final List<List<LightWeightWaySection>> paths = path(next, waySectionsByFirstNodeId, stopValues, visited);

                    for (final List<LightWeightWaySection> path : paths) {

                        path.add(current);

                    }

                    result.addAll(paths);
                }
            }
        }

        return result;
    }

    private static Set<Long> set(final JavaRDD<Long> values,
                                 final JavaSparkContext javaSparkContext) {
        return new HashSet<>(javaSparkContext.broadcast(values.collect()).value());
    }

    private static Map map(final JavaPairRDD values,
                           final JavaSparkContext javaSparkContext) {
        return javaSparkContext.broadcast(values.collectAsMap()).value();
    }

    private static Long lastNodeId(final WaySection waySection) {
        return lastNode(waySection).id();
    }

    private static Long firstNodeId(final WaySection waySection) {
        return firstNode(waySection).id();
    }

    private static Boolean link(final WaySection waySection) {
        return waySection.hasKey(HIGHWAY_TAG) && waySection.tags().get(HIGHWAY_TAG).contains(LINK_SUFFIX_VALUE);
    }
}
