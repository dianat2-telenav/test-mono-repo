package com.telenav.adapter.component.fc;

import com.telenav.adapter.utils.connectivity.connectivity.checker.GraphConnectivityChecker;
import com.telenav.adapter.utils.connectivity.connectivity.checker.UndirectedGraphConnectivityChecker;
import com.telenav.adapter.utils.connectivity.graph.NodeVertex;
import com.telenav.adapter.utils.connectivity.graph.WayEdge;
import com.telenav.adapter.utils.connectivity.graph.WaySectionEdge;
import com.telenav.adapter.utils.connectivity.graph.builder.UndirectedGraphBuilder;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import com.telenav.map.geometry.Point;
import com.telenav.map.geometry.PolyLine;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.jgrapht.alg.connectivity.ConnectivityInspector;
import org.jgrapht.graph.DefaultUndirectedGraph;
import scala.Tuple2;
import scala.Tuple3;

import java.util.*;

import static com.telenav.adapter.component.fc.TagService.FC_TAG;
import static com.telenav.adapter.utils.connectivity.service.ConnectivityCheckerService.graphData;
import static com.telenav.datapipelinecore.unidb.RoadCategoryTagKey.FUNCTIONAL_CLASS;

/**
 * @author liviuc
 */
@Slf4j
public class FcPromotionService {

    private static final Function<LightWeightWaySection, List<LightWeightWaySection>> ACC = waySection -> {
        final List<LightWeightWaySection> waySections = new LinkedList<>();
        waySections.add(waySection);
        return waySections;
    };
    private static final Function2<List<LightWeightWaySection>, LightWeightWaySection, List<LightWeightWaySection>> ADD = (list, waySection) -> {
        list.add(waySection);
        return list;
    };
    private static final Function2<List<LightWeightWaySection>, List<LightWeightWaySection>, List<LightWeightWaySection>> MERGE = (left, right) -> {
        left.addAll(right);
        return left;
    };
    private static final Function<LightWeightWay, List<LightWeightWay>> ACC2 = way -> {
        final List<LightWeightWay> ways = new LinkedList<>();
        ways.add(way);
        return ways;
    };
    private static final Function2<List<LightWeightWay>, LightWeightWay, List<LightWeightWay>> ADD2 = (list, way) -> {
        list.add(way);
        return list;
    };
    private static final Function2<List<LightWeightWay>, List<LightWeightWay>, List<LightWeightWay>> MERGE2 = (left, right) -> {
        left.addAll(right);
        return left;
    };
    private static Function<String, List<String>> ACC3 = str -> {
        final List<String> list = new LinkedList<>();
        list.add(str);
        return list;
    };
    private static Function2<List<String>, String, List<String>> ADD3 = (list, str) -> {
        list.add(str);
        return list;
    };
    private static Function2<List<String>, List<String>, List<String>> MERGE3 = (left, right) -> {
        left.addAll(right);
        return left;
    };

    public static JavaRDD<WaySection> removeShortComponents(final JavaRDD<WaySection> waySections,
                                                            final JavaSparkContext javaSparkContext) {
        // Filter just the required data for processing.
        final JavaRDD<WaySection> navigableWaySections = FcService.navigable(waySections);
        final JavaRDD<WaySection> nonLinks = FcService.nonLinks(navigableWaySections)
                .cache();

        final JavaRDD<LightWeightWay> ways =
                nonLinks.mapToPair(waySection -> new Tuple2<Long, LightWeightWaySection>(waySection.strictIdentifier().wayId(), FcService.lightWeight(waySection)))
                        .combineByKey(ACC, ADD, MERGE)
                        .map(tuple -> lightweightWay(tuple._2()));

        // Combine way sections by way id.
        final JavaPairRDD<Long, List<LightWeightWay>> waysByNodes =
                ways.flatMapToPair(way -> Arrays.asList(new Tuple2<Long, LightWeightWay>(way.getFirstNode().getId(), way),
                        new Tuple2<Long, LightWeightWay>(way.getLastNode().getId(), way))
                        .iterator()
                ).filter(Objects::nonNull)
                        //       .filter(tuple -> tuple._1() == null || tuple._2() == null)
                        .combineByKey(ACC2, ADD2, MERGE2);

        final Map<Long, List<LightWeightWay>> waysByNodesBroadcast = waysByNodes.collectAsMap();
        //javaSparkContext.broadcast(waysByNodes.collectAsMap()).value();

        // =====================
        final JavaRDD<Tuple3<String, String, WaySectionEdge>> graphData = graphData(nonLinks);

        final DefaultUndirectedGraph graph =
                new UndirectedGraphBuilder<String, WaySectionEdge>().buildGraph(graphData, WaySectionEdge.class);

        // Compute undirected graph geojsons.
        final GraphConnectivityChecker undirectedGraphConnectivityChecker =
                new UndirectedGraphConnectivityChecker<String, WaySectionEdge>().graph(graph);

        final List<Set<String>> connectedSets = undirectedGraphConnectivityChecker.connectedSets();

        // We iterate through each fc component and if it's length is less than a threshold we downgrade it
        final Set<String> connectedSetsEnds = new HashSet<>(connectedSets.size());
        for (final Set<String> connectedSet : connectedSets) {
            final Iterator<String> iterator = connectedSet.iterator();

            final String previousString = iterator.next();
            final Double[] previousCoordinates = toCoordinates(previousString);

            double length = 0;

            boolean lengthExceeded = false;
            while (iterator.hasNext()) {
                final String currentString = iterator.next();
                final Double[] currentCoordinates = toCoordinates(currentString);

                final PolyLine line = new PolyLine(new Point(Latitude.degrees(currentCoordinates[0]), Longitude.degrees(currentCoordinates[1])),
                        new Point(Latitude.degrees(previousCoordinates[0]), Longitude.degrees(previousCoordinates[1])));

                length += line.length().asKilometers();

                if (length > 10) {
                    lengthExceeded = true;
                    break;
                }
            }

            if (!lengthExceeded) {
                final Iterator<String> iterator1 = connectedSet.iterator();
                connectedSetsEnds.add(iterator1.next());
                while (iterator.hasNext()) {
                    final String next = iterator1.next();
                    if (!iterator.hasNext()) {
                        connectedSetsEnds.add(next);
                    }
                }
            }
        }

        final Set<String> connectedSetsEndsBroadcast = javaSparkContext.broadcast(connectedSetsEnds).getValue();
        final Map<Long, String> fcByWayId = ways.filter(way -> "1".equals(way.getFc()))
                .filter(way -> {
                    final String coordinates = way.getFirstNode().getLatitude() + "," + way.getFirstNode().getLongitude();
                    return connectedSetsEndsBroadcast.contains(coordinates);
                }).mapToPair(way -> new Tuple2<Long, String>(way.getId(), "2"))
                .collectAsMap();

        final Map<Long, String> fcByWayIdBroadcast = javaSparkContext.broadcast(fcByWayId).getValue();

        return waySections.map(waySection -> {

            final String fc = fcByWayIdBroadcast.get(waySection.strictIdentifier().wayId());
            if (fc != null) {
                return waySection.withTag(FC_TAG, fc);
            }
            return waySection;
        });
    }

    public static JavaRDD<WaySection> closeGaps2(final JavaRDD<WaySection> waySections,
                                                final JavaSparkContext javaSparkContext) {
        // Filter just the required data for processing.
        final JavaRDD<WaySection> navigableWaySections = FcService.navigable(waySections);
        final JavaRDD<WaySection> nonLinks = FcService.nonLinks(navigableWaySections)
                .cache();

        final JavaRDD<LightWeightWay> ways =
                nonLinks.mapToPair(waySection -> new Tuple2<Long, LightWeightWaySection>(waySection.strictIdentifier().wayId(), FcService.lightWeight(waySection)))
                        .combineByKey(ACC, ADD, MERGE)
                        .map(tuple -> lightweightWay(tuple._2()));

        final JavaPairRDD<String, List<LightWeightWay>> waysByGeohash = ways.mapToPair(way -> {
            final String geohash5 = GeohashUtils.geohash5(way.getFirstNode().getLatitude(), way.getFirstNode().getLongitude());
            return new Tuple2<String, LightWeightWay>(geohash5, way);
        }).combineByKey(ACC2, ADD2, MERGE2);

        final Map<Long, String> promotedFcsByWayId = waysByGeohash.flatMapToPair(tuple -> {

            final List<LightWeightWay> lightWeightWays = tuple._2();
            final Map<Long, List<LightWeightWay>> waysByNodes = new HashMap<>();

            final List<LightWeightWay> withFc1 = new LinkedList<>();

            for (int i = 0; i < lightWeightWays.size(); i++) {
                final LightWeightWay lightWeightWay = lightWeightWays.get(i);
                if ("1".equals(lightWeightWay.getFc())) {
                    withFc1.add(lightWeightWay);
                }
                if (waysByNodes.containsKey(lightWeightWay.getFirstNode().getId())) {
                    final List<LightWeightWay> incidentWays = waysByNodes.get(lightWeightWay.getFirstNode().getId());
                    incidentWays.add(lightWeightWay);
                    waysByNodes.put(lightWeightWay.getFirstNode().getId(), incidentWays);
                } else {
                    final List<LightWeightWay> incidentWays = new LinkedList<>();
                    incidentWays.add(lightWeightWay);
                    waysByNodes.put(lightWeightWay.getFirstNode().getId(), incidentWays);
                }
                if (waysByNodes.containsKey(lightWeightWay.getLastNode().getId())) {
                    final List<LightWeightWay> incidentWays = waysByNodes.get(lightWeightWay.getLastNode().getId());
                    incidentWays.add(lightWeightWay);
                    waysByNodes.put(lightWeightWay.getLastNode().getId(), incidentWays);
                } else {
                    final List<LightWeightWay> incidentWays = new LinkedList<>();
                    incidentWays.add(lightWeightWay);
                    waysByNodes.put(lightWeightWay.getLastNode().getId(), incidentWays);
                }
            }

            final DefaultUndirectedGraph<NodeVertex, WayEdge> graph = buildGraph(lightWeightWays);
            final ConnectivityInspector<NodeVertex, WayEdge> connectivityInspector = new ConnectivityInspector<>(graph);
            final List<Set<NodeVertex>> connectedSets = connectivityInspector.connectedSets();

            final Set<String> connectedSetsEnds = new HashSet<>(connectedSets.size());
            for (final Set<NodeVertex> connectedSet : connectedSets) {
                final Iterator<NodeVertex> iterator = connectedSet.iterator();
                final NodeVertex first = iterator.next();
                connectedSetsEnds.add(first.getLatitude() + "," + first.getLongitude());
                while (iterator.hasNext()) {
                    final NodeVertex next = iterator.next();
                    if (!iterator.hasNext()) {
                        connectedSetsEnds.add(next.getLatitude() + "," + next.getLongitude());
                    }
                }
            }

            final Map<Long, String> enhancements = new HashMap<>();
            for (final LightWeightWay lightWeightWay : lightWeightWays) {
                final String coordinates = lightWeightWay.getFirstNode().getLatitude() + "," + lightWeightWay.getFirstNode().getLongitude();
                if (connectedSetsEnds.contains(coordinates)) {
                    final Dfs dfs = dfs(lightWeightWay, waysByNodes, new HashSet<Long>(), 0, 5);
                    if (dfs != null && dfs.enhancements.size() > 0) {
                        enhancements.putAll(dfs.enhancements);
                    }
                }
            }

            final List<Tuple2<Long, String>> result = new LinkedList<>();
            for (final Map.Entry<Long, String> entry : enhancements.entrySet()) {
                result.add(new Tuple2<Long, String>(entry.getKey(), entry.getValue()));
            }
            return result.iterator();
        }).distinct()
                .collectAsMap();

        final Map<Long, String> promotedFcsByWayIdBroadcast = javaSparkContext.broadcast(promotedFcsByWayId).value();

        return waySections.map(waySection -> {
            if (promotedFcsByWayIdBroadcast.containsKey(waySection.strictIdentifier().wayId())) {
                return waySection.withTag(FC_TAG, promotedFcsByWayIdBroadcast.get(waySection.strictIdentifier().wayId()));
            }
            return waySection;
        });
    }

    private static DefaultUndirectedGraph<NodeVertex, WayEdge> buildGraph(final List<LightWeightWay> lightWeightWays) {
        final DefaultUndirectedGraph<NodeVertex, WayEdge> graph = new
                DefaultUndirectedGraph<>(WayEdge.class);
        for (final LightWeightWay lightWeightWay : lightWeightWays) {
            if ("1".equals(lightWeightWay.getFc())) {
                final WayEdge wayEdge = FcService.toWayEdge(lightWeightWay);
                for (final Tuple3<String, String, WayEdge> graphDatum : graphData(wayEdge)) {
                    graph.addVertex(graphDatum._3().getNodes().get(0));
                    graph.addVertex(graphDatum._3().getNodes().get(1));
                    graph.addEdge(graphDatum._3().getNodes().get(0), graphDatum._3().getNodes().get(1), graphDatum._3());
                }
            }
        }
        return graph;
    }

    public static JavaRDD<WaySection> closeGaps(final JavaRDD<WaySection> waySections,
                                                final JavaSparkContext javaSparkContext) {
        // Filter just the required data for processing.
        final JavaRDD<WaySection> navigableWaySections = FcService.navigable(waySections);
        final JavaRDD<WaySection> nonLinks = FcService.nonLinks(navigableWaySections)
                .cache();

        final JavaRDD<LightWeightWay> ways =
                nonLinks.mapToPair(waySection -> new Tuple2<Long, LightWeightWaySection>(waySection.strictIdentifier().wayId(), FcService.lightWeight(waySection)))
                        .combineByKey(ACC, ADD, MERGE)
                        .map(tuple -> lightweightWay(tuple._2()));

        // Combine way sections by way id.
        final JavaPairRDD<Long, List<LightWeightWay>> waysByNodes =
                ways.flatMapToPair(way -> Arrays.asList(new Tuple2<Long, LightWeightWay>(way.getFirstNode().getId(), way),
                        new Tuple2<Long, LightWeightWay>(way.getLastNode().getId(), way))
                        .iterator()
                ).filter(Objects::nonNull)
                        .combineByKey(ACC2, ADD2, MERGE2);

        final Map<Long, List<LightWeightWay>> waysByNodesBroadcast = waysByNodes.collectAsMap();
        //javaSparkContext.broadcast(waysByNodes.collectAsMap()).value();

        // =====================
        final JavaRDD<Tuple3<String, String, WaySectionEdge>> graphData = graphData(nonLinks);

        final DefaultUndirectedGraph graph =
                new UndirectedGraphBuilder<String, WaySectionEdge>().buildGraph(graphData, WaySectionEdge.class);

        // Compute undirected graph geojsons.
        final GraphConnectivityChecker undirectedGraphConnectivityChecker =
                new UndirectedGraphConnectivityChecker<String, WaySectionEdge>().graph(graph);

        final List<Set<String>> connectedSets = undirectedGraphConnectivityChecker.connectedSets();

        final Set<String> connectedSetsEnds = new HashSet<>(connectedSets.size());
        for (final Set<String> connectedSet : connectedSets) {
            final Iterator<String> iterator = connectedSet.iterator();
            connectedSetsEnds.add(iterator.next());
            while (iterator.hasNext()) {
                final String next = iterator.next();
                if (!iterator.hasNext()) {
                    connectedSetsEnds.add(next);
                }
            }
        }
        //final Set<String> connectedSetsEndsBroadcast = javaSparkContext.broadcast(connectedSetsEnds).value();

        final Map<Long, String> fcByWayId = ways.filter(way -> "1".equals(way.getFc()))
                .filter(way -> {
                    final String coordinates = way.getFirstNode().getLatitude() + "," + way.getFirstNode().getLongitude();
                    return connectedSetsEnds.contains(coordinates);
                }).map(way -> dfs(way, waysByNodesBroadcast, new HashSet<Long>(), 0, 5))
                .filter(dfs -> dfs != null && dfs.enhancements.size() > 0)
                .flatMapToPair(map -> {
                    final List<Tuple2<Long, String>> result = new ArrayList<>();
                    for (final Map.Entry<Long, String> entry : map.enhancements.entrySet()) {
                        result.add(new Tuple2<Long, String>(entry.getKey(), entry.getValue()));
                    }

                    return result.iterator();
                }).combineByKey(ACC3, ADD3, MERGE3)
                .mapToPair(tuple -> {
                    final Long wayId = tuple._1();
                    final List<String> fcs = tuple._2();
                    int resultFc = 5;
                    for (final String fc : fcs) {
                        final int fcShort = Integer.parseInt(fc);
                        resultFc = Math.min(resultFc, fcShort);
                    }
                    return new Tuple2<Long, String>(wayId, String.valueOf(resultFc));
                }).collectAsMap();

        final Map<Long, String> fcByWayIdBroadcast = javaSparkContext.broadcast(fcByWayId).value();

        final JavaRDD<WaySection> waySectionsWithPromotion = waySections.map(waySection -> {
            final String promotionFc = fcByWayIdBroadcast.get(waySection.strictIdentifier().wayId());
            if (promotionFc != null) {
                waySection = waySection.withTag(FUNCTIONAL_CLASS.unwrap(), promotionFc);
            }
            return waySection;
        });

        return waySectionsWithPromotion;
    }

    private static Dfs dfs(final LightWeightWay way,
                           final Map<Long, List<LightWeightWay>> waysByNodesBroadcast,
                           final Set<Long> idsTrack,
                           final int depth,
                           final int depthThreshold) {
        if (depth == depthThreshold || way == null) {
            return null;
        }

        //final List<LightWeightWay> neighbors = new ArrayList<>();
        final List<LightWeightWay> lastNodeNeighbors = waysByNodesBroadcast.get(way.getLastNode().getId());
//        if (lastNodeNeighbors != null) {
//            for (final LightWeightWay lightWeightWay : lastNodeNeighbors) {
//                if (lightWeightWay.getId() != way.getId() && !idsTrack.contains(lightWeightWay.getId())) {
//                    neighbors.add(lightWeightWay);
//                }
//            }
//        }
//        try {
//            final List<LightWeightWay> lightWeightWays = waysByNodesBroadcast.get(way.getLastNode().getId());
//            for (final LightWeightWay lightWeightWay : lightWeightWays) {
//                if (lightWeightWay.getId() != way.getId() && !idsTrack.contains(lightWeightWay.getId())) {
//                    neighbors.add(lightWeightWay);
//                }
//            }
//        } catch (Exception e) {
//            throw new RuntimeException(e.getClass().getName() +
//                    " message: " + e.getMessage() +
//                    " waysByNodesBroadcast: " + (waysByNodesBroadcast == null) +
//                    " wayId: " + way.getId() +
//                    " way.getLastNode(): " + (way.getLastNode() == null) +
//                    ((way.getLastNode() != null) ? (" way.getLastNode().getId(): " + (way.getLastNode().getId())) : "") +
//                    " waysByNodesBroadcast.get(way.getLastNode().getId()): " + waysByNodesBroadcast.get(way.getLastNode().getId()));
//        }
        final List<LightWeightWay> firstNodeNeighbors = waysByNodesBroadcast.get(way.getFirstNode().getId());
//        if (firstNodeNeighbors != null) {
//            for (final LightWeightWay lightWeightWay : firstNodeNeighbors) {
//                if (lightWeightWay.getId() != way.getId() && !idsTrack.contains(lightWeightWay.getId())) {
//                    neighbors.add(lightWeightWay);
//                }
//            }
//        }

        final Queue<LightWeightWay> toCheck = new LinkedList<>();
        ins(way, firstNodeNeighbors, idsTrack, toCheck);
        ins(way, lastNodeNeighbors, idsTrack, toCheck);
        outs(way, firstNodeNeighbors, idsTrack, toCheck);
        outs(way, lastNodeNeighbors, idsTrack, toCheck);

        idsTrack.add(way.getId());
        Dfs result = null;
        while (!toCheck.isEmpty()) {
            final LightWeightWay neighbor = toCheck.poll();
            if (idsTrack.contains(neighbor.getId())) {
                continue;
            }
            if ("1".equals(neighbor.getFc())) {
                return new Dfs();
            } else {
                result = dfs(neighbor, waysByNodesBroadcast, idsTrack, depth + 1, depthThreshold);
                if (result != null) {
                    result.enhancements.put(neighbor.getId(), "1");
                }
            }
        }
        return result;
    }

    private static void outs(final LightWeightWay way,
                             final List<LightWeightWay> outgoing,
                             final Set<Long> idsTrack,
                             final Queue<LightWeightWay> toCheck) {
        if (outgoing == null) {
            return;
        }
        for (final LightWeightWay out : outgoing) {
            if (out.getId() == way.getId() || idsTrack.contains(out.getId())) {
                continue;
            }
            final boolean outPositiveMovementDirection = positiveMovementDirection(out);
            if (!isOneway(out)) {
                toCheck.add(out);
            } else if (way.getLastNode().getId() == out.getFirstNode().getId() && outPositiveMovementDirection) {
                toCheck.add(out);
            } else if (way.getLastNode().getId() == out.getLastNode().getId() && !outPositiveMovementDirection) {
                toCheck.add(out);
            }
        }
    }

    private static void ins(final LightWeightWay way,
                            final List<LightWeightWay> incoming,
                            final Set<Long> idsTrack,
                            final Queue<LightWeightWay> toCheck) {
        if (incoming == null) {
            return;
        }
        for (final LightWeightWay in : incoming) {
            if (in.getId() == way.getId() || idsTrack.contains(in.getId())) {
                continue;
            }
            final boolean inPositiveMovementDirection = positiveMovementDirection(in);
            if (!isOneway(in)) {
                toCheck.add(in);
            } else if (way.getFirstNode().getId() == in.getFirstNode().getId() && !inPositiveMovementDirection) {
                toCheck.add(in);
            } else if (way.getFirstNode().getId() == in.getLastNode().getId() && inPositiveMovementDirection) {
                toCheck.add(in);
            }
        }
    }

    private static boolean positiveMovementDirection(final LightWeightWay way) {
        if ("-1".equalsIgnoreCase(way.getOneway())) {
            return false;
        }
        return true;
    }

    private static boolean isOneway(final LightWeightWay way) {
        if (way.getOneway() == null || "no".equals(way.getOneway())) {
            return false;
        }
        return true;
    }

    private static Double[] toCoordinates(final String coordinatesAsString) {
        final String[] split = coordinatesAsString.split(",");
        final Double[] coordinates = new Double[2];
        coordinates[0] = Double.parseDouble(split[0]);
        coordinates[1] = Double.parseDouble(split[1]);
        return coordinates;
    }

    private static LightWeightWay lightweightWayWithNodes(final List<LightWeightWaySection> waySections) {
        LightWeightWaySection first = null;
        LightWeightWaySection last = null;
        //final int size = waySections.size();

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (final LightWeightWaySection weightWaySection : waySections) {
            if (weightWaySection.getSequenceNumber() < min) {
                min = weightWaySection.getSequenceNumber();
            }
            if (weightWaySection.getSequenceNumber() > max) {
                max = weightWaySection.getSequenceNumber();
            }
        }

        for (final LightWeightWaySection waySection : waySections) {
            if (waySection.getSequenceNumber() == min) {
                first = waySection;
            }
            if (waySection.getSequenceNumber() == max) {
                last = waySection;
            }
        }
        return LightWeightWay.builder()
                .id(first.getWayId())
                .fc(first.getFc())
                .firstNode(first.getFirstNode())
                .lastNode(last.getLastNode())
                //.nodes(nodes)
                .oneway(first.getOneway()) // All the way sections inherit the oneway attribute from the way, so any way section would do for retrieving the oneway attribute.
                .geohash(GeohashUtils.geohash5(first.getFirstNode().getLatitude(), first.getFirstNode().getLongitude()))
                .build();
    }

    private static LightWeightWay lightweightWay(final List<LightWeightWaySection> waySections) {
        LightWeightWaySection first = null;
        LightWeightWaySection last = null;
        //final int size = waySections.size();

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (final LightWeightWaySection weightWaySection : waySections) {
            if (weightWaySection.getSequenceNumber() < min) {
                min = weightWaySection.getSequenceNumber();
            }
            if (weightWaySection.getSequenceNumber() > max) {
                max = weightWaySection.getSequenceNumber();
            }
        }

        for (final LightWeightWaySection waySection : waySections) {
            if (waySection.getSequenceNumber() == min) {
                first = waySection;
            }
            if (waySection.getSequenceNumber() == max) {
                last = waySection;
            }
        }
        return LightWeightWay.builder()
                .id(first.getWayId())
                .fc(first.getFc())
                .firstNode(first.getFirstNode())
                .lastNode(last.getLastNode())
                .oneway(first.getOneway()) // All the way sections inherit the oneway attribute from the way, so any way section would do for retrieving the oneway attribute.
                .geohash(GeohashUtils.geohash5(first.getFirstNode().getLatitude(), first.getFirstNode().getLongitude()))
                .build();
    }

    private static class Dfs {
        private final Map<Long, String> enhancements = new HashMap<>();
    }
}
