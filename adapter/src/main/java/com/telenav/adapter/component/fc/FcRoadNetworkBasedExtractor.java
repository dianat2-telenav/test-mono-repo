package com.telenav.adapter.component.fc;

import com.telenav.adapter.exception.MissingIsoException;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.*;
import java.util.regex.Pattern;

import static com.telenav.adapter.bean.FcCountry.*;
import static com.telenav.adapter.component.fc.FcInfo.*;

@FunctionalInterface
public interface FcRoadNetworkBasedExtractor extends Serializable {

    Set<String> MEX_FC_1_ROUTES = new HashSet<>(Arrays.asList(
            "MEX 2D",
            "MEX 15D",
            "CHIH 39", // Statal level, but upgraded to avoid a federal toll road
            "MEX 57D",
            "MEX 95D",
            "MEX 185D",
            "MEX 200D"
    ));

    Set<String> MEX_FC_1_NAMES = new HashSet<>(Arrays.asList(
            "Carretera Nacional Acapulco-Zihuatanejo",
            "Prolongación Diego Hurtado de Mendoza",
            "Calzada Pie de la Cuesta",
            "Carretera Nacional Acapulco-Zihuatanejo",
            "México 200",

            "Libramiento Norte",
            "Calle Libramiento Norte Poniente",
            "Muna - Felipe Carrillo",
            "Anillo Periférico Licenciado Manuel Berzunza",
            "Calzada al Pacífico",
            "Avenida Cuauhtémoc",

            "Mazatlán-Tepic",

            "Carretera Federal MEX-80",
            "MEX-80 Guadalajara - Autlán de Navarro",
            "MEX-80 El Corcobado - Barra de Navidad",
            "Carretera Guadalajara -Barra de Navidad",
            "Calle Rafaél Palomera, Libramiento de Melaque",

            "Calle Primavera",
            "Cale Emiliano Zapata",

            "Autopista Colima - Manzanillo",
            "Carretera Tecomán - Boca de Pascuales",

            "Carretera Nacional Acapulco-Zihuatanejo",
            "Carretera Nacional México, Avenida Lázaro Cárdenas",

            "Carretera Tuxtla Gutiérrez-Angostura",
            "Carretera a la Angostura",

            "Carretera Federal Villahermosa-Teapa",
            "Federal",

            "Avenida Lázaro Cárdenas del Río",
            "Paraíso - Comalcalco",
            "Calle 5 de Febrero",

            "Carretera Tzucacab-Catmís",
            "Carretera Muna-Felipe Carrillo Puerto",

            "Anillo Periférico Licenciado Manuel Berzunza"
    ));

    Set<String> USA_FC_2_REFS = new HashSet<>(Arrays.asList("I-169", "I169", "I 169", "I 2", "I-2", "I2"));

    FcRoadNetworkBasedExtractor USA_EXTRACTOR =
            (tags, routesMapping) -> {

                final String ref = TagService.ref(tags);

                if (ref != null) {
                    final String[] refs = ref.split(";");
                    for (String rf : refs) {
                        if (USA_FC_2_REFS.contains(rf)) {
                            return FC_SECOND;
                        }
                    }
                }

                final String iso = TagService.iso(tags);

                final Map<String, List<String>> routesByFcs = routesMapping.get(iso);

                if (routesByFcs != null) {

                    final List<String> fcPriorityByCountry = fcPriorityByCountry(iso);

                    String fc = TagHelper.fcFromTag(ref, fcPriorityByCountry, routesByFcs);

                    if (!StringUtils.isEmpty(fc)) {
                        return fc;
                    }

                    fc = UsaTigerFccApproach.fc(tags, fcPriorityByCountry, routesByFcs);

                    if (!StringUtils.isEmpty(fc)) {
                        return fc;
                    }

                    return FC_FIFTH;
                }

                throwMissingIsoException(USA.name());

                // Unreachable statement:
                return null;
            };

    FcRoadNetworkBasedExtractor MEX_EXTRACTOR =
            (tags, routesMapping) -> {

                final String iso = TagService.iso(tags);
                final String ref = TagService.ref(tags);

                final Map<String, List<String>> routesByFcs = routesMapping.get(iso);

                if (routesByFcs != null && ref != null) {

                    final String[] routesFromTag = TagHelper.split(ref);

                    for (String fc : fcPriorityByCountry(iso)) {

                        final List<String> routesRegexp = routesByFcs.get(fc);

                        for (final String route : routesFromTag) {

                            final String trimedRoute = route.trim();

                            final String name = TagService.name(tags);

                            if (MEX_FC_1_NAMES.contains(name) || MEX_FC_1_ROUTES.contains(route)) {

                                return FC_FIRST;

                            } else if ("Carretera Estatal".equalsIgnoreCase(name)) {

                                return FC_SECOND;

                            }

                            for (final String routeRegexp : routesRegexp) {

                                if (Pattern.matches(routeRegexp, trimedRoute)) {

                                    return fc;
                                }
                            }


                        }
                    }

                    return FcHighwayBasedExtractor.fcByCountry(iso, tags);
                }

                throwMissingIsoException(MEX.name());

                // Unreachable statement:
                return null;
            };

    static void throwMissingIsoException(final String iso) {
        throw new MissingIsoException("ISO must be equal to " + iso);
    }

    static List<String> fcPriorityByCountry(final String iso) {

        switch (iso) {
            case "USA":
                return USA_FC_PRIORITY;
            case "MEX":
                return MEX_FC_PRIORITY;
            case "CAN":
                return CAN_FC_PRIORITY;
            default:
                return Collections.emptyList();
        }
    }

    String fc(final Map<String, String> tags,
              final Map<String, Map<String, List<String>>> routesMapping);
}
