package com.telenav.adapter.component.fc;

import com.google.common.collect.ImmutableList;
import com.telenav.adapter.component.RelationService;
import com.telenav.adapter.component.WaySectionService;
import com.telenav.adapter.utils.connectivity.graph.NodeVertex;
import com.telenav.adapter.utils.connectivity.graph.WayEdge;
import com.telenav.adapter.utils.connectivity.graph.WaySectionEdge;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.datapipelinecore.bean.Relation;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;
import java.util.*;

import static com.telenav.adapter.component.fc.TagService.*;

/**
 * @author liviuc
 */
public class FcService {

    public static final Set<String> NAVIGABLE_WAYS = new HashSet<>(ImmutableList.of(
            "motorway",
            "motorway_link",
            "trunk",
            "trunk_link",
            "primary",
            "primary_link",
            "secondary",
            "secondary_link",
            "tertiary",
            "tertiary_link",
            "residential",
            "service",
            "unclassified"
    ));
    private static final Map<String, String> NAMES_OF_WAYS_TO_FIX = new HashMap<String, String>() {{
        put("Vancouver – Blaine Highway", "CAN");
        put("Vancouver – Blaine Highway (NEXUS Lane)", "CAN");
    }};
    private static final Map<Long, String> IDS_OF_WAYS_TO_FIX = new HashMap<Long, String>() {{
        put(751315493L, "CAN");
        put(32047865L, "CAN");
        put(32047865L, "CAN");
        put(19714355L, "USA");
        put(37319544L, "USA");
        put(37319544L, "USA");
        put(476630370L, "USA");
        put(90546627L, "USA");
        put(476630371L, "USA");
        put(598775339L, "USA");
        put(598775365L, "USA");
        put(598775317L, "USA");
        put(598775315L, "USA");
        put(598775319L, "USA");
        put(476630370L, "USA");
    }};

    public static String downgradeFc() {
        return "5";
    }

    public static Map<Long, List<String>> wayIdsToIsoFc(final JavaRDD<Relation> relations,
                                                        final JavaRDD<WaySection> sectionedWays,
                                                        final JavaSparkContext javaSparkContext) throws IOException {

        final Map<String, Map<String, String>> routeRelationsMappingBroadcast =
                WaySectionService.routeRelationsMappingBroadcast(sectionedWays, javaSparkContext);

        final JavaRDD<Relation> withIsoFc = RelationService.withFcData(relations, routeRelationsMappingBroadcast);

        final JavaPairRDD<Long, List<String>> wayIdsToNetworks = RelationService.wayIdsToNetworks(withIsoFc);

        return javaSparkContext.broadcast(wayIdsToNetworks.collectAsMap()).value();
    }

    public static WaySection fixIso(final WaySection waySection) {

        final Map<String, String> tags = waySection.tags();

        final String name = TagService.name(tags);

        if (NAMES_OF_WAYS_TO_FIX.keySet().contains(name)) {

            final String correctIso = NAMES_OF_WAYS_TO_FIX.get(name);

            if (!correctIso.equalsIgnoreCase(TagService.iso(tags))) {

                return waySection.withTag("iso", correctIso);

            }
        }

        final long wayId = waySection.strictIdentifier().wayId();

        if (IDS_OF_WAYS_TO_FIX.keySet().contains(wayId)) {

            final String correctIso = IDS_OF_WAYS_TO_FIX.get(wayId);

            if (!correctIso.equalsIgnoreCase(TagService.iso(tags))) {

                return waySection.withTag("iso", correctIso);

            }
        }

        return waySection;
    }

    public static WayEdge toWayEdge(final LightWeightWay lightWeightWay) {

        final List<NodeVertex> nodeVertices = new ArrayList<>(2);
        nodeVertices.add(nodeVertex(lightWeightWay.getFirstNode()));
        nodeVertices.add(nodeVertex(lightWeightWay.getLastNode()));

        return WayEdge.builder()
                .id(lightWeightWay.getId())
                .nodes(nodeVertices)
                .oneway(lightWeightWay.getOneway())
                .build();
    }

    public static WaySectionEdge toWaySectionEdge(final WaySection waySection) {

        final List<NodeVertex> nodeVertices = new ArrayList<>(waySection.nodes().size());

        for (final Node node : waySection.nodes()) {

            nodeVertices.add(nodeVertex(node));

        }
        return WaySectionEdge.builder()
                .id(waySection.id())
                .nodes(nodeVertices)
                .tags(waySection.tags())
                .build();
    }


    private static NodeVertex nodeVertex(final LightWeightNode node) {
        return new NodeVertex(node.getId(),
                node.getLatitude(),
                node.getLongitude(),
                Collections.emptyMap());
    }

    private static NodeVertex nodeVertex(final Node node) {
        return new NodeVertex(node.id(),
                node.latitude().asDegrees(),
                node.longitude().asDegrees(),
                Collections.emptyMap());
    }

    public static LightWeightWaySection lightWeightWithNodes(final WaySection waySection) {
        return LightWeightWaySection.builder()
                .id(waySection.id())
                .sequenceNumber(waySection.sequenceNumber())
                .wayId(waySection.strictIdentifier().wayId())
                .fc(waySection.tags().get(FC_TAG))
                .oneway(waySection.tags().get("oneway"))
                .firstNode(FcService.lightWeight(waySection.firstNode()))
                .lastNode(FcService.lightWeight(waySection.lastNode()))
                .nodes(FcService.lightWeight(waySection.nodes()))
                .build();
    }

    private static List<LightWeightNode> lightWeight(final List<Node> nodes) {
        final List<LightWeightNode> leightweightNodes = new LinkedList<>();
        for (final Node node : nodes) {
            leightweightNodes.add(lightWeight(node));
        }
        return leightweightNodes;
    }

    public static LightWeightWaySection lightWeight(final WaySection waySection) {
        return LightWeightWaySection.builder()
                .id(waySection.id())
                .sequenceNumber(waySection.sequenceNumber())
                .wayId(waySection.strictIdentifier().wayId())
                .fc(waySection.tags().get(FC_TAG))
                .oneway(waySection.tags().get("oneway"))
                .firstNode(FcService.lightWeight(waySection.firstNode()))
                .lastNode(FcService.lightWeight(waySection.lastNode()))
                .build();
    }

    public static LightWeightNode lightWeight(final Node node) {
        return LightWeightNode.builder()
                .id(node.id())
                .latitude(node.latitude().asDegrees())
                .longitude(node.longitude().asDegrees())
                .build();
    }

    public static boolean isNavigable(final WaySection waySection) {
        return waySection.hasKey(HIGHWAY_TAG) &&
                NAVIGABLE_WAYS.contains(waySection.tags().get(HIGHWAY_TAG));
    }


    public static JavaRDD<WaySection> nonLinks(final JavaRDD<WaySection> waySections) {
        return waySections.filter(FcService::nonLink);
    }

    public static boolean nonLink(final WaySection waySection) {
        return waySection.hasKey(HIGHWAY_TAG) ? !waySection.tags().get(HIGHWAY_TAG).contains(LINK_SUFFIX_VALUE) : true;
    }

    public static JavaRDD<WaySection> navigable(final JavaRDD<WaySection> adaptedWaySections) {
        return adaptedWaySections.filter(waySection -> NAVIGABLE_WAYS.contains(waySection.tags().get(HIGHWAY_TAG)));
    }
}
