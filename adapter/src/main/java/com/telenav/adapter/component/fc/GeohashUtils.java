package com.telenav.adapter.component.fc;

import ch.hsr.geohash.GeoHash;

/**
 * @author liviuc
 */
public class GeohashUtils {

    public static final int GEOHASH_5 = 5;

    public static String geohash5(final double latDeg, final double lonDeg) {
        return geohash(latDeg, lonDeg, GEOHASH_5);
    }

    private static String geohash(final double latDeg,
                                  final double lonDeg,
                                  final int precision) {
        return GeoHash.withCharacterPrecision(latDeg, lonDeg, precision)
                .toBase32();
    }
}
