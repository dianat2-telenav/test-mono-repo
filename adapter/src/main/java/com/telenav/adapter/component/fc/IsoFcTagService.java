package com.telenav.adapter.component.fc;

import static com.telenav.adapter.component.fc.TagService.NETWORK_TAG;

/**
 * @author liviuc .
 */
public class IsoFcTagService {

    public static final String ISO_FC_TAG = "iso:fc";
    public static final String SEPARATOR = ":";

    public static String isoTagValue(final String exemptedNetworkIso,
                                     final String exemptedNetworkFc) {
        return exemptedNetworkIso + SEPARATOR + exemptedNetworkFc;
    }

    public static String isoTagKey(final String isoFcTag,
                                   final String exemptedNetworkIso,
                                   final String exemptedNetworkFc) {
        return isoFcTag + SEPARATOR + exemptedNetworkIso + SEPARATOR + exemptedNetworkFc;
    }

    public static String isoFcNetworkKey() {
        return ISO_FC_TAG + SEPARATOR + NETWORK_TAG;
    }
}
