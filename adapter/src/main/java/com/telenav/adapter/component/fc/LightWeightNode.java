package com.telenav.adapter.component.fc;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * This class is meant to hold specific node information.
 * It is used in order to shuffle just the data needed for the @{FcLinkService},
 * thus increasing shuffling performance.
 *
 * @author liviuc
 */
@Builder
@Data
public class LightWeightNode implements Serializable {

    private long id;
    private double latitude;
    private double longitude;
}
