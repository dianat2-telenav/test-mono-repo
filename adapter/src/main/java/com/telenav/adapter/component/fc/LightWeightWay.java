package com.telenav.adapter.component.fc;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * This class is meant to hold specific way information.
 * It is used in order to shuffle just the data needed for the @{FcLinkService},
 * thus increasing shuffling performance.
 *
 * @author liviuc
 */
@Builder
@Data
public class LightWeightWay implements Serializable {

    private long id;
    private LightWeightNode firstNode;
    private LightWeightNode lastNode;
    private LightWeightNode nodes;
    private String fc;
    private String oneway;
    private String geohash;

}
