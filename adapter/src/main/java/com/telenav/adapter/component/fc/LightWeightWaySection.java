package com.telenav.adapter.component.fc;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * This class is meant to hold specific way section information.
 * It is used in order to shuffle just the data needed for the @{FcLinkService},
 * thus increasing shuffling performance.
 *
 * @author liviuc
 */
@Builder
@Data
public class LightWeightWaySection implements Serializable {

    private long id;
    private long wayId;
    private short sequenceNumber;
    private LightWeightNode firstNode;
    private LightWeightNode lastNode;
    private List<LightWeightNode> nodes;
    private String fc;
    private String oneway;

    public LightWeightWaySection switchNodes() {
        return LightWeightWaySection.builder()
                .id(id)
                .firstNode(lastNode)
                .lastNode(firstNode)
                .fc(fc)
                .build();
    }

    public boolean hasFc() {
        return !StringUtils.isEmpty(fc);
    }
}