package com.telenav.adapter.component.fc;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import scala.collection.mutable.WrappedArray;

import java.util.List;

/**
 * @author liviuc
 */
public class MultipleNodeTypeFunction implements MapFunction<Row, Row> {

    @Override
    public Row call(final Row row) {

        final long nodeId = row.getLong(0);

        String nodeType = "shape_point";
        String navn = null;

        final List<Row> nodeData = row.getList(1);
        for (final Row nodeDatum : nodeData) {

            final String nodeTypeFromData = nodeDatum.getString(0);
            if ("node".equals(nodeTypeFromData)) {
                nodeType = nodeTypeFromData;
            }

            final String navnFromData = nodeDatum.getString(1);
            if (navnFromData != null) {
                navn = navnFromData;
            }
        }

        return RowFactory.create(nodeId, nodeType, navn);
    }
}
