package com.telenav.adapter.component.fc;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.Row;

import java.util.Iterator;

/**
 * @author liviuc
 */
public class NodeTypeFromWayFunction implements FlatMapFunction<Row, Row> {

    @Override
    public Iterator<Row> call(final Row row) {
        return NodeTypeFunction.call(row, "nodeId", "nodes", "tags");
    }
}
