package com.telenav.adapter.component.fc;

import com.telenav.adapter.component.repository.ResourceRepository;
import com.telenav.adapter.exception.MissingIsoException;
import com.telenav.map.entity.WaySection;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.telenav.adapter.component.fc.FcRoadNetworkBasedExtractor.MEX_EXTRACTOR;
import static com.telenav.adapter.component.fc.FcRoadNetworkBasedExtractor.USA_EXTRACTOR;

/**
 * @author liviuc .
 */
@Slf4j
public class RefBased extends FcApproach {

    private Map<String, Map<String, List<String>>> routesMapping;

    public RefBased(final List<String> countries) {
        super(countries);
        try {
            this.routesMapping = routesMapping(ResourceRepository.getRoutesMapping());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String fc(final WaySection waySection) {

        final Map<String, String> tags = waySection.tags();

        return fc(TagService.iso(tags), tags);
    }

    private Map<String, Map<String, List<String>>> routesMapping(final Map<String, Map<String, String>> resourceRoutesMapping) {

        final Map<String, Map<String, List<String>>> routesMapping = new HashMap<>();

        for (final Map.Entry<String, Map<String, String>> routesByFcByCountry : resourceRoutesMapping.entrySet()) {

            final String country = routesByFcByCountry.getKey();

            final Map<String, List<String>> routesListByFc = new HashMap<>();

            for (final Map.Entry<String, String> routesByFc : routesByFcByCountry.getValue().entrySet()) {

                final String fc = routesByFc.getKey();

                final List<String> routes = Arrays.asList(routesByFc.getValue().split("\\|"));

                routesListByFc.put(fc, routes);
            }

            routesMapping.put(country, routesListByFc);
        }

        return routesMapping;
    }

    private String fc(final String iso,
                      final Map<String, String> tags) {

        if (iso == null) {
            return fc(tags);
        }

        try {

            switch (iso) {
                case "USA":
                    return USA_EXTRACTOR.fc(tags, routesMapping);
                case "MEX":
                    return MEX_EXTRACTOR.fc(tags, routesMapping);
                default:
                /*
                    Some countries don't have the roads referenced to a road network (eg. Canada).
                    In that case, we just provide the fc after it's standard rule
                 */
                    return fc(tags);
            }

        } catch (final MissingIsoException e) {

            log.error(e.getMessage());
            return fc(tags);

        }
    }
}
