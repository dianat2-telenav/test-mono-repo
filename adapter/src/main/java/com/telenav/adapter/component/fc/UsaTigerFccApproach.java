package com.telenav.adapter.component.fc;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

import static com.telenav.adapter.component.fc.TagService.TIGER_CFCC;
import static com.telenav.adapter.component.fc.TagService.TIGER_NAME_BASE;

/**
 * @author liviuc .
 */
public class UsaTigerFccApproach {

    public static String tigerCfcc(String tigerCfcc) {

        if (tigerCfcc != null) {

            tigerCfcc = tigerCfcc.replace(" ", "");
            tigerCfcc = tigerCfcc.replace(":", ";");

            String[] tigerCfccSplit = null;

            if (tigerCfcc.contains(";")) {
                tigerCfccSplit = tigerCfcc.split(";");
            } else {
                tigerCfccSplit = new String[]{tigerCfcc};
            }

            if (tigerCfccSplit != null) {

                final OptionalInt maxTigerCfcc = maxTigerCfcc(tigerCfccSplit);

                if (maxTigerCfcc.isPresent()) {

                    final int tigerCfccInt = maxTigerCfcc.getAsInt();

                    if (tigerCfccInt == 0) {

                        return FcInfo.FC_FIFTH;
                    }

                    if (tigerCfccInt >= 11 && tigerCfccInt < 41) {

                        return FcInfo.FC_THIRD;
                    }


                    if (tigerCfccInt >= 41 && tigerCfccInt < 51) {

                        return FcInfo.FC_FOURTH;
                    }

                    if (tigerCfccInt >= 51 && tigerCfccInt <= 75) {

                        return FcInfo.FC_FIFTH;
                    }
                }
            }
        }

        return FcInfo.FC_FIFTH;
    }

    private static OptionalInt maxTigerCfcc(final String[] tigerCfccSplit) {

        return Arrays.stream(tigerCfccSplit).filter(val -> !StringUtils.isEmpty(val))
                .map(val -> {
                    try {
                        return Integer.parseInt(val.substring(1).trim());
                    } catch (Exception e) {
                        return null;
                    }
                }).filter(Objects::nonNull)
                .mapToInt(val -> val)
                .max();
    }

    public static String tigerCfccNameBase(final String tigerCfccNameBase,
                                           final List<String> fcPriorityByCountry,
                                           final Map<String, List<String>> routesByFcs) {

        return TagHelper.fcFromTag(tigerCfccNameBase, fcPriorityByCountry, routesByFcs);
    }

    public static String fc(final Map<String, String> tags,
                            final List<String> fcPriorityByCountry,
                            final Map<String, List<String>> routesByFcs) {
        final String tigerCfcc = tags.get(TIGER_CFCC);

        String tigerCfccFc = null;

        if (!StringUtils.isEmpty(tigerCfcc)) {

            tigerCfccFc = UsaTigerFccApproach.tigerCfcc(tigerCfcc);

        }

        final String tigerCfccNameBase = tags.get(TIGER_NAME_BASE);

        String tigerCfccNameBaseFc = null;

        if (!StringUtils.isEmpty(tigerCfccNameBase)) {

            tigerCfccNameBaseFc =
                    UsaTigerFccApproach.tigerCfccNameBase(tigerCfccNameBase,
                            fcPriorityByCountry,
                            routesByFcs);

        }

        if (!StringUtils.isEmpty(tigerCfccFc) && !StringUtils.isEmpty(tigerCfccNameBaseFc)) {

            if (tigerCfccFc.compareTo(tigerCfccNameBaseFc) > 0) {
                return tigerCfccFc;
            }
            return tigerCfccNameBaseFc;

        } else if (!StringUtils.isEmpty(tigerCfccFc)) {

            return tigerCfccFc;

        } else if (!StringUtils.isEmpty(tigerCfccNameBaseFc)) {

            return tigerCfccNameBaseFc;

        }

        return null;
    }
}
