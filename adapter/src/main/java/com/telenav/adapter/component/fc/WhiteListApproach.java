package com.telenav.adapter.component.fc;

import com.telenav.map.entity.WaySection;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liviuc .
 */
public class WhiteListApproach extends FcApproach {

    private static final Map<Long, String> CAN_WAY_ID_WHITELIST = new HashMap() {{
        put(35225397L, "1");
        put(17365421L, "1");
        put(17365421L, "1");
    }};

    private static final Map<Long, String> USA_WAY_ID_WHITELIST = new HashMap() {{
        put(17765499, "1");
        put(37319544L,"1");
        put(476630371L,"1");
        put(90546627L,"1");
        put(598775339L,"1");
        put(598775365L,"1");
        put(598775317L,"1");
        put(598775315L, "1");
        put(598775321L, "1");
        put(598775319L, "1");
        put(476630370L, "1");
        put(37319544L, "1");
        put(510843976L, "1");
        put(476630371L, "1");
        put(598775339L, "1");
        put(598775365, "1");
        put(598775317L, "1");
        put(598775315L, "1");
        put(598775319L, "1");
        put(476630370L, "1");
        put(91536919L, "1");
    }};

    private static final Map<Long, String> CAN_WAY_NAME_WHITELIST = new HashMap() {{
        put("International Crossing", "1");
        put("Rainbow Bridge", "2");
        put("Peace Bridge", "1");
    }};

    public WhiteListApproach(List<String> countries) {
        super(countries);
    }

    public boolean isValidForCountry(final String iso) {
        return true;
    }

    @Override
    public String fc(final WaySection waySection) {

        final Map<String, String> tags = waySection.tags();

        return fc(TagService.iso(tags), waySection);
    }

    private String fc(final String iso, final WaySection waySection) {

        final long wayId = waySection.strictIdentifier().wayId();

        final String name = waySection.tags().get("name");

        if ("CAN".equalsIgnoreCase(iso)) {

            String fc = CAN_WAY_ID_WHITELIST.get(wayId);

            if (fc != null) {
                return fc;
            }

            fc = CAN_WAY_NAME_WHITELIST.get(name);

            return fc;
        }

        String fc = CAN_WAY_ID_WHITELIST.get(wayId);

        if (fc != null) {
            return fc;
        }

        return USA_WAY_ID_WHITELIST.get(wayId);
    }
}
