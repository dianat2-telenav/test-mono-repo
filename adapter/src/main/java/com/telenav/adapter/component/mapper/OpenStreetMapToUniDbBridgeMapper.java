package com.telenav.adapter.component.mapper;

import com.telenav.datapipelinecore.osm.OsmTagValue;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;

import java.util.*;
/**
 * Bridge Open Street Map tag values to Bridge UniDb tag values mapper.
 *
 * @author Andrei Puf
 */
public class OpenStreetMapToUniDbBridgeMapper extends OpenStreetMapToUniDbMapper {

    /** Set of values that will be unified to "yes" */
    public static final Set<OsmTagValue> YES_BRIDGE_VALUES = new HashSet<>();

    /** Map of UniDb tag values as key and a set of convertible to that value Open Street Map tag values. */
    private static final Map<UnidbTagValue,Set<OsmTagValue>> UNI_DB_TO_OSM_MAP = new HashMap();
    static {
        YES_BRIDGE_VALUES.add(OsmTagValue.YES);
        YES_BRIDGE_VALUES.add(OsmTagValue.SOURCEBOARDWALK);
        YES_BRIDGE_VALUES.add(OsmTagValue.COVERED);
        YES_BRIDGE_VALUES.add(OsmTagValue.SKYBRIDGE);
        YES_BRIDGE_VALUES.add(OsmTagValue.OVERPASS);
        YES_BRIDGE_VALUES.add(OsmTagValue.MOVABLE);
        YES_BRIDGE_VALUES.add(OsmTagValue.TRUSS);
        YES_BRIDGE_VALUES.add(OsmTagValue.TRESTLE);
        YES_BRIDGE_VALUES.add(OsmTagValue.AQUEDUCT);
        YES_BRIDGE_VALUES.add(OsmTagValue.VIADUCT);
        YES_BRIDGE_VALUES.add(OsmTagValue.BOARDWALK);
        YES_BRIDGE_VALUES.add(OsmTagValue.CANTILEVER);
        YES_BRIDGE_VALUES.add(OsmTagValue.SIMPLE_BRUNEL);
        YES_BRIDGE_VALUES.add(OsmTagValue.SUSPENSION);
        YES_BRIDGE_VALUES.add(OsmTagValue.TRUE);
        YES_BRIDGE_VALUES.add(OsmTagValue.BENT);
        YES_BRIDGE_VALUES.add(OsmTagValue.PONTOON);
        YES_BRIDGE_VALUES.add(OsmTagValue.FOOTBRIDGE);
        YES_BRIDGE_VALUES.add(OsmTagValue.PIER);
        YES_BRIDGE_VALUES.add(OsmTagValue.LOG_BRIDGE);
        YES_BRIDGE_VALUES.add(OsmTagValue.BRIDGE);
        YES_BRIDGE_VALUES.add(OsmTagValue.CAUSEWAY);

        UNI_DB_TO_OSM_MAP.put(UnidbTagValue.YES, YES_BRIDGE_VALUES);
    }

    public Optional<UnidbTagValue> map(String accessValue) {
        return super.map(UNI_DB_TO_OSM_MAP, accessValue);
    }
}
