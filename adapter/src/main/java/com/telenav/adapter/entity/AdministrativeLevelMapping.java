package com.telenav.adapter.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class AdministrativeLevelMapping implements Serializable {

    private String region;
    private String country;
    private String l1_Admin_Type;
    private String l2_Admin_Type;
    private String l3_Admin_Type;
    private String l4_Admin_Type;
    private String l5_Admin_Type;
}
