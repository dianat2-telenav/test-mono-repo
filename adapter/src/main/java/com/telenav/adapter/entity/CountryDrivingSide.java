package com.telenav.adapter.entity;

import com.telenav.adapter.bean.DrivingSide;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CountryDrivingSide {

    private String countryName;
    private DrivingSide drivingSide;
}
