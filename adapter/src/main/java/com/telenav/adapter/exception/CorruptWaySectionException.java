package com.telenav.adapter.exception;

/**
 * Exception aimed for turn lanes implementation logic.
 *
 * @author liviuc
 */
public class CorruptWaySectionException extends RuntimeException {

    public CorruptWaySectionException(final String message) {
        super(message);
    }
}
