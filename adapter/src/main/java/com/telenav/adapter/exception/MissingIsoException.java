package com.telenav.adapter.exception;

public class MissingIsoException extends RuntimeException {

    public MissingIsoException(final String message) {
        super(message);
    }
}
