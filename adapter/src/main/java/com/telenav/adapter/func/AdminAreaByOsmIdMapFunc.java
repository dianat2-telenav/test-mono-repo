package com.telenav.adapter.func;

import com.telenav.adapter.utils.RowUtils;
import com.telenav.map.storage.RawOsmEntitySchema;
import com.telenav.ost.spark.storage.Column;
import com.telenav.ost.spark.storage.Schema;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.types.DataTypes;
import scala.collection.JavaConverters;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

/**
 * @author liviuc
 */
public class AdminAreaByOsmIdMapFunc implements MapFunction<Row, Row> {

    public static Schema ADMIN_AREA_BY_ID_SCHEMA = new AdminAreaByIdSchema();

    @Override
    public Row call(final Row relation) {
        final Map<String, String> tagMap = new TreeMap<>(); // order output
        final List<Row> tagRows = relation.getList(relation.fieldIndex("tags"));
        tagRows.forEach(tagRow -> tagMap.put(tagRow.getString(0), tagRow.getString(1))); // key, value

        if (tagMap.containsKey("osm_relation_id")) {
            return RowFactory.create(new Object[]{
                    Long.valueOf(tagMap.get("osm_relation_id")),
                    JavaConverters.asScalaBufferConverter(RowUtils.toTagsList(tagMap)).asScala()
            });
        }
        return null;
    }

    public static class AdminAreaByIdSchema extends Schema {
        public static final Column ORIGINAL_OSM_RELATION_ID;
        public static final Column ADMIN_AREA_RELATION_TAGS;

        static {
            ORIGINAL_OSM_RELATION_ID = new Column("originalOsmRelationId", DataTypes.LongType, false);
            ADMIN_AREA_RELATION_TAGS = new Column("adminAreaRelationTags", DataTypes.createArrayType((new RawOsmEntitySchema.TagSchema()).asStructType(), false), false);
        }

        public AdminAreaByIdSchema() {
        }

        protected Stream<Column> columns() {
            return Stream.of(ORIGINAL_OSM_RELATION_ID, ADMIN_AREA_RELATION_TAGS);
        }
    }

}
