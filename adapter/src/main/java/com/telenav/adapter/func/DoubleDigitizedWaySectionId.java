package com.telenav.adapter.func;

import com.telenav.adapter.bean.TwoPointWaySection;
import com.telenav.adapter.component.DoubleDigitizedWaySectionHelper;
import org.apache.spark.api.java.function.FlatMapFunction;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *  Identifies way section ids corresponding to rst=2 (= double digitized).
 */
public class DoubleDigitizedWaySectionId
        implements FlatMapFunction<Tuple2<String, List<TwoPointWaySection>>, Long> {

    @Override
    public Iterator<Long> call(final Tuple2<String, List<TwoPointWaySection>> tuple) {

        final DoubleDigitizedWaySectionHelper doubleDigitizedWaySectionHelper =
                new DoubleDigitizedWaySectionHelper(tuple);
        doubleDigitizedWaySectionHelper.compute();

        // Count two point way sections belonging to the same way section.
        final Map<Long, Long> countedTwoPointWaySections = doubleDigitizedWaySectionHelper.getCountedTwoPointWaySections();

        /*
            WaySection to corresponding double digitized WaySection (key)
            to
            count the two point way section mapping between the two way sections.
         */
        final Map<String, Long> firstToSecondWaySectionFromPairToWaySectionCount
                = doubleDigitizedWaySectionHelper.getFirstToSecondPairWaySectionToWaySectionCount();
        final Map<String, Long> secondToFirstWaySectionFromPairToWaySectionCount
                = doubleDigitizedWaySectionHelper.getSecondToFirstWaySectionFromPairToWaySectionCount();

        final List<Long> firstWaySecondIds =
                doubleDigitizedWaySectionIds(firstToSecondWaySectionFromPairToWaySectionCount, countedTwoPointWaySections);
        final List<Long> secondWaySecondIds
                = doubleDigitizedWaySectionIds(secondToFirstWaySectionFromPairToWaySectionCount, countedTwoPointWaySections);
        firstWaySecondIds.addAll(secondWaySecondIds);

        return firstWaySecondIds.iterator();
    }

    private List<Long> doubleDigitizedWaySectionIds(final Map<String, Long> waySectionFromPairToWaySectionCount,
                                                    final Map<Long, Long> countedTwoPointWaySections) {
        final List<Long> doubleDigitizedWaySectionIds = new ArrayList<>();
        countedTwoPointWaySections.entrySet()
                .forEach(entry -> {
                    final Long waySectionId = entry.getKey();
                    if (!doubleDigitizedWaySectionIds.contains(waySectionId)) {
                        final Long twoPointWaySectionCount = entry.getValue();
                        final boolean toAddAsDoubleDigitized =
                                getCount(waySectionFromPairToWaySectionCount, waySectionId, twoPointWaySectionCount);
                        if (toAddAsDoubleDigitized) {
                            doubleDigitizedWaySectionIds.add(waySectionId);
                        }

                    }
                });
        return doubleDigitizedWaySectionIds;
    }

    private boolean getCount(final Map<String, Long> waySectionFromPairToWaySectionCount,
                             final Long leftWaySectionId,
                             final Long twoPointWaySectionCount) {
        final String leftWaySectionIdAsString = String.valueOf(leftWaySectionId);
        for (final Map.Entry<String, Long> pairedWaySectionIdsEntry : waySectionFromPairToWaySectionCount.entrySet()) {
            final String key = pairedWaySectionIdsEntry.getKey();
            if (key.contains(leftWaySectionIdAsString + ":")) {
                if (pairedWaySectionIdsEntry.getValue() >= twoPointWaySectionCount / 2) {
                    return true;
                }
            }
        }
        return false;
    }
}
