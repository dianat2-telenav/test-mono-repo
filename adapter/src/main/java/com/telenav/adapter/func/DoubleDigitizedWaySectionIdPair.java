package com.telenav.adapter.func;

import com.telenav.map.entity.Node;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import com.telenav.map.geometry.Point;
import com.telenav.map.geometry.PolyLine;
import com.telenav.adapter.bean.TwoPointWaySection;
import com.telenav.adapter.component.DoubleDigitizedWaySectionHelper;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import scala.Tuple2;

import java.util.*;

/**
 *  Identifies way id pairs corresponding to rst=2 (= double digitized).
 */
public class DoubleDigitizedWaySectionIdPair
        implements PairFlatMapFunction<Tuple2<String, List<TwoPointWaySection>>, Long, Long> {

    @Override
    public Iterator<Tuple2<Long, Long>> call(final Tuple2<String, List<TwoPointWaySection>> tuple) {

        final DoubleDigitizedWaySectionHelper doubleDigitizedWaySectionHelper =
                new DoubleDigitizedWaySectionHelper(tuple);
        doubleDigitizedWaySectionHelper.compute();

        //count mini way section belonging to the same way section
        final Map<Long, Long> countedTwoPointWaySections = doubleDigitizedWaySectionHelper.getCountedTwoPointWaySections();

        final Map<String, Long> firstToSecondWaySectionFromPairToWaySectionCount
                = doubleDigitizedWaySectionHelper.getFirstToSecondPairWaySectionToWaySectionCount();
        final Map<String, Long> secondToFirstWaySectionFromPairToWaySectionCount
                = doubleDigitizedWaySectionHelper.getSecondToFirstWaySectionFromPairToWaySectionCount();

        final Map<Long, Long> firstWaySecondIds =
                doubleDigitizedWaySectionIds(firstToSecondWaySectionFromPairToWaySectionCount, countedTwoPointWaySections);
        final Map<Long, Long> secondWaySecondIds
                = doubleDigitizedWaySectionIds(secondToFirstWaySectionFromPairToWaySectionCount, countedTwoPointWaySections);

        final List<Tuple2<Long, Long>> list = new ArrayList<>();
        firstWaySecondIds.entrySet()
                .stream()
                .forEach(entry -> {

                    if (secondWaySecondIds.containsKey(entry.getValue())) {
                        if (secondWaySecondIds.get(entry.getValue()).longValue() == entry.getKey().longValue()) {
                            list.add(new Tuple2<Long, Long>(entry.getKey(), entry.getValue()));
                        }
                    }
                });

        return firstWaySecondIds.entrySet()
                .stream()
                .map(entry -> new Tuple2<Long, Long>(entry.getKey(), entry.getValue()))
                .iterator();
    }

    private Map<Long, Long> doubleDigitizedWaySectionIds(final Map<String, Long> waySectionFromPairToWaySectionCount,
                                                         final Map<Long, Long> countedTwoPointWaySections) {
        final Map<Long, Long> doubleDigitizedWaySectionIds = new HashMap<>();

        if (countedTwoPointWaySections != null) {
            countedTwoPointWaySections.entrySet()
                    .forEach(entry -> {
                        final Long waySectionId = entry.getKey();
                        if (!doubleDigitizedWaySectionIds.containsKey(waySectionId)) {
                            final Long twoPointWaySectionCount = entry.getValue();

                            if (!waySectionFromPairToWaySectionCount.isEmpty()) {
                                final String pairId =
                                        getPairId(waySectionFromPairToWaySectionCount, waySectionId, twoPointWaySectionCount);
                                if (pairId != null) {
                                    doubleDigitizedWaySectionIds.put(waySectionId, Long.parseLong(pairId));
                                }
                            }
                        }
                    });
        }
        return doubleDigitizedWaySectionIds;
    }

    private String getPairId(final Map<String, Long> waySectionFromPairToWaySectionCount,
                             final Long leftWaySectionId,
                             final Long twoPointWaySectionCount) {
        long max = 0;
        String maxPairId = null;

        final String leftWaySectionIdAsString = String.valueOf(leftWaySectionId);
        for (final Map.Entry<String, Long> pairedWaySectionIdsEntry : waySectionFromPairToWaySectionCount.entrySet()) {
            final String key = pairedWaySectionIdsEntry.getKey();
            if (key.contains(leftWaySectionIdAsString + ":")) {
                final Long count = pairedWaySectionIdsEntry.getValue();
                if (count >= twoPointWaySectionCount / 2) {
                    if (count > max) {
                        max = count;
                        maxPairId = key.split(":")[1];
                    }
                }
            }
        }
        return maxPairId;
    }

    private Map<TwoPointWaySection, Tuple2<TwoPointWaySection, Double>>
    minimumDistanceMap(final List<TwoPointWaySection> twoPointWaySections) {
        final Map<TwoPointWaySection, Tuple2<TwoPointWaySection, Double>> minimumDistanceMap = new HashMap<>();

        for (TwoPointWaySection left : twoPointWaySections) {
            for (TwoPointWaySection right : twoPointWaySections) {
                if (!validForComparing(left, right)) {
                    continue;
                }

                final double distanceBetweenSections =
                        distanceBetweenSections(left, right);
                if (distanceBetweenSections > 10) {
                    continue;
                }

                /*
                    Just a way of organizing our distance map such that
                */
                final TwoPointWaySection minimumWaySection =
                        (left.getWaySectionId() < right.getWaySectionId()) ? left : right;
                final TwoPointWaySection maximumWaySection =
                        (minimumWaySection == left) ?
                                right : left;

                put(minimumDistanceMap, minimumWaySection, maximumWaySection, distanceBetweenSections);
            }
        }

        return minimumDistanceMap;
    }

    private void put(final Map<TwoPointWaySection, Tuple2<TwoPointWaySection, Double>> minimumDistanceMap,
                     final TwoPointWaySection minimumWaySection,
                     final TwoPointWaySection maximumWaySection,
                     final double distanceBetweenSections) {
        if (!minimumDistanceMap.containsKey(minimumWaySection)) {
            minimumDistanceMap.put(minimumWaySection,
                    new Tuple2<TwoPointWaySection, Double>(maximumWaySection, distanceBetweenSections));
        } else {
            final Double distance = minimumDistanceMap.get(minimumWaySection)._2();
            if (distanceBetweenSections < distance) {
                minimumDistanceMap.remove(minimumWaySection)._2();
            }
            minimumDistanceMap.put(minimumWaySection,
                    new Tuple2<TwoPointWaySection, Double>(maximumWaySection, distanceBetweenSections));
        }
    }

    private boolean validForComparing(final TwoPointWaySection left,
                                      final TwoPointWaySection right) {
        return left.getWayId().longValue() != right.getWayId().longValue() &&
                differentMovingDirections(left, right) &&
                noCommonNodes(left, right);
    }

    private double distanceBetweenSections(final TwoPointWaySection left,
                                           final TwoPointWaySection right) {
        return new PolyLine(Arrays.asList(midPoint(left), midPoint(right)))
                .length()
                .asMeters();
    }

    private Point midPoint(final TwoPointWaySection twoPointWaySection) {
        return new Point(
                Latitude.degrees(midLat(twoPointWaySection)),
                Longitude.degrees(midLon(twoPointWaySection))
        );
    }

    private double midLat(final TwoPointWaySection twoPointWaySection) {
        return (twoPointWaySection.getToNode().latitude().asDegrees() +
                twoPointWaySection.getFromNode().latitude().asDegrees()) / 2;
    }

    private double midLon(final TwoPointWaySection twoPointWaySection) {
        return (twoPointWaySection.getToNode().longitude().asDegrees() +
                twoPointWaySection.getFromNode().longitude().asDegrees()) / 2;
    }

    private boolean noCommonNodes(final TwoPointWaySection left,
                                  final TwoPointWaySection right) {

        final Node leftFromNode = left.getFromNode();
        final Node leftToNode = left.getToNode();

        final Node rightFromNode = right.getFromNode();
        final Node rightToNode = right.getToNode();

        return (leftToNode.id() != rightFromNode.id()) &&
                (rightToNode.id() != leftFromNode.id()) &&
                (leftToNode.id() != rightToNode.id()) &&
                (leftFromNode.id() != rightFromNode.id());
    }

    private boolean differentMovingDirections(final TwoPointWaySection left,
                                              final TwoPointWaySection right) {

        final Latitude leftFromLatitude = left.getFromNode().latitude();
        final Latitude leftToLatitude = left.getToNode().latitude();

        final Longitude leftFromLongitude = left.getFromNode().longitude();
        final Longitude leftToLongitude = left.getToNode().longitude();

        final Latitude rightFromLatitude = right.getFromNode().latitude();
        final Latitude rightToLatitude = right.getToNode().latitude();

        final Longitude rightFromLongitude = right.getFromNode().longitude();
        final Longitude rightToLongitude = right.getToNode().longitude();


        boolean leftFromSouthToNorth = (leftToLatitude.asDegrees() - leftFromLatitude.asDegrees()) > 0;
        boolean leftFromWestToEast = (leftToLongitude.asDegrees() - leftFromLongitude.asDegrees()) > 0;

        boolean rightFromSouthToNorth = (rightToLatitude.asDegrees() - rightFromLatitude.asDegrees()) > 0;
        boolean rightFromWestToEast = (rightToLongitude.asDegrees() - rightFromLongitude.asDegrees()) > 0;


        return !(leftFromWestToEast && rightFromWestToEast && leftFromSouthToNorth && rightFromSouthToNorth);
    }
}

