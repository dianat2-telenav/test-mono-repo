package com.telenav.adapter.func;

import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Row;

/**
 * The current class performs a sanity check to validate whether ane non-existing node
 * is referenced from a raw-way.
 * This case can occur if the data is corrupted or map data with different data is used
 * for nodes and raw-ways when running the job.
 *
 * @author liviuc
 */
@Slf4j
public class NodeReferenceFunction implements MapFunction<Row, Row> {

    private final boolean failOnMissingNode;

    public NodeReferenceFunction(boolean failOnMissingNode) {
        this.failOnMissingNode = failOnMissingNode;
    }

    @Override
    public Row call(final Row row) {

        Long nodeId = null;

        final Integer idFieldIndex = row.fieldIndex("id");
        if (row.get(idFieldIndex) != null) {
            nodeId = row.getLong(idFieldIndex);
        }

        if (nodeId == null) {

            final Long wayId = row.getLong(row.fieldIndex("wayId"));
            final Long nodeIdFromIndexedWay = row.getLong(row.fieldIndex("nodeId"));

            if (failOnMissingNode) {
                throw new IllegalArgumentException("Missing node: " + nodeIdFromIndexedWay + " (referenced from way " + wayId + ")");
            } else {
                log.error("Missing node: " + nodeIdFromIndexedWay + " (referenced from way " + wayId + ")");
            }
        }

        return row;
    }
}
