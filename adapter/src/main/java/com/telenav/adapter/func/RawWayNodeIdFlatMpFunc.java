package com.telenav.adapter.func;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author liviuc
 */
public class RawWayNodeIdFlatMpFunc {

    public static Iterator<Row> map(final Row waySection) {
        final List<Row> nodes = waySection.getList(waySection.fieldIndex("nodes"));

        final Row firstNodeId = id(nodes.get(0));
        final Row lastNodeId = id(nodes.get(nodes.size() - 1));

        return Arrays.asList(firstNodeId, lastNodeId)
                .iterator();
    }

    private static Row id(final Row node) {
        return RowFactory.create(node.getLong(node.fieldIndex("nodeId")));
    }
}
