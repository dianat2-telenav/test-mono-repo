package com.telenav.adapter.func;

import com.telenav.adapter.component.NodeService;
import com.telenav.adapter.utils.OsmInfo;
import com.telenav.map.storage.RawOsmEntitySchema;
import com.telenav.map.storage.RawRelationSchema;
import com.telenav.ost.spark.storage.Column;
import com.telenav.ost.spark.storage.Schema;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.types.DataTypes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author liviuc
 */
public class RelationMemberExplodeFunc {

    public static Schema RELATION_MEMBER_SCHEMA = new RelationMemberSchema();

    public static Iterator<Row> map(final Row relationRow) {

        final List<Row> result = new ArrayList<>();

        final List<Row> memberRows = relationRow.getList(relationRow.fieldIndex("members"));

        for (final Row memberRow : memberRows) {

            final String memberType = memberRow.getAs("type");

            if (NodeService.isNodeMemberType(memberType)) {

                final String role = memberRow.getAs("role");

                if (isCityCenterRole(role)) {

                    result.add(RowFactory.create(relationRow.getLong(0),
                            memberRow.getLong(0),
                            relationRow.get(1),
                            memberRow
                    ));
                }
            }
        }

        return result.iterator();
    }

    private static boolean isCityCenterRole(String role) {
        return role.equals(OsmInfo.OSM_ADMIN_CENTER_MEMBER_ROLE) || role.equals(OsmInfo.OSM_LABEL_MEMBER_ROLE);
    }

    public static class RelationMemberSchema extends Schema {
        public static final Column RELATION_ID;
        public static final Column NODE_ID;
        public static final Column RELATION_TAGS;
        public static final Column MEMBER;

        static {
            RELATION_ID = new Column("relationId", DataTypes.LongType, false);
            NODE_ID = new Column("nodeId", DataTypes.LongType, false);
            RELATION_TAGS = new Column("relationTags", DataTypes.createArrayType((new RawOsmEntitySchema.TagSchema()).asStructType(), false), false);
            MEMBER = new Column("member", new RawRelationSchema.MemberSchema().asStructType(), false);
        }

        public RelationMemberSchema() {
        }

        protected Stream<Column> columns() {
            return Stream.of(RELATION_ID, NODE_ID, RELATION_TAGS, MEMBER);
        }
    }
}
