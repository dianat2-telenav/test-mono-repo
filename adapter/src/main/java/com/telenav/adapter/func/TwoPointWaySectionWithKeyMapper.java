package com.telenav.adapter.func;

import com.telenav.map.entity.Node;
import com.telenav.adapter.bean.TwoPointWaySection;
import scala.Tuple2;

import java.math.BigDecimal;
import java.util.function.Function;

import static com.telenav.adapter.component.TwoPointWaySectionKey.KEY_SLOPE_INFO;
import static com.telenav.adapter.component.TwoPointWaySectionKey.SEPARATOR;

public class TwoPointWaySectionWithKeyMapper implements Function<TwoPointWaySection, Tuple2<String, TwoPointWaySection>> {

    private static final int APPROXIMATE_EARTH_RADIUS_KM = 6371;
    private final StringBuilder keyPrefix;

    public TwoPointWaySectionWithKeyMapper(final StringBuilder keyPrefix) {
        this.keyPrefix = keyPrefix;
    }

    @Override
    public Tuple2<String, TwoPointWaySection> apply(final TwoPointWaySection twoPointWaySection) {

        addGeometricalData(twoPointWaySection);

        final String key = keyPrefix.append(KEY_SLOPE_INFO + SEPARATOR)
                .append(twoPointWaySection.getSlope()).toString();

        return new Tuple2<String, TwoPointWaySection>(key, twoPointWaySection);
    }

    private void addGeometricalData(final TwoPointWaySection twoPointWaySection) {

        final Node fromNode = twoPointWaySection.getFromNode();
        final Node toNode = twoPointWaySection.getToNode();

        double xFrom = cartesianX(fromNode);
        double yFrom = cartesianY(fromNode);

        double xTo = cartesianX(toNode);
        double yTo = cartesianY(toNode);

        /*
            Slope m mathematical formula:
            m = (y2 - y1) / (x2 - x1)
         */
        double slope = slope(xTo, xFrom, yTo, yFrom);

        /*
            Rounding the slope in order to cope with real life cases
            when double digitized roads are not "perfectly" parallel
         */
        twoPointWaySection.setSlope(oneDigitSlope(slope));

        setLineEquationCoefficients(twoPointWaySection, slope, xFrom, yFrom);
    }

    private void setLineEquationCoefficients(final TwoPointWaySection twoPointWaySection,
                                             final double slope,
                                             final double xFrom,
                                             final double yFrom) {
        /*
            compute line equation coefficients
            (y = a * x + b) =>
            a = m
            b = - m * x1 + y1
         */

        int a = (int) slope;
        int b = (int) (-slope * xFrom + yFrom);

        twoPointWaySection.setLineEquationCoefficientA(a);
        twoPointWaySection.setLineEquationCoefficientB(b);
    }

    private double slope(final double xTo,
                         final double xFrom,
                         final double yTo,
                         final double yFrom) {
        final double x = xTo - xFrom;
        if (x != 0.0) {
            return (yTo - yFrom) / x;
        } else {
            // To avoid division by 0 when x = 0
            return 1.0 / (x / (yTo - yFrom));
        }
    }

    private double cartesianY(final Node node) {
        return APPROXIMATE_EARTH_RADIUS_KM *
                Math.cos(node.latitude().asRadians()) *
                Math.sin(node.longitude().asRadians());
    }

    private double cartesianX(final Node node) {
        return APPROXIMATE_EARTH_RADIUS_KM *
                Math.cos(node.latitude().asRadians()) *
                Math.cos(node.longitude().asRadians());
    }

    private double oneDigitSlope(final double slope) {
        return new BigDecimal(slope)
                .setScale(1, BigDecimal.ROUND_HALF_UP)
                .doubleValue();
    }

    private void thrownException(final TwoPointWaySection twoPointWaySection,
                                 final NumberFormatException nfe,
                                 final double slope,
                                 final double xTo,
                                 final double yTo,
                                 final double xFrom,
                                 final double yFrom) {
        throw new RuntimeException(nfe.getMessage() +
                " wayId=" + twoPointWaySection.getWayId() +
                " fromNodeId=" + twoPointWaySection.getFromNode().id() +
                " toNodeId=" + twoPointWaySection.getToNode().id() +
                " slope=" + slope +
                " xTo=" + xTo +
                " xFrom=" + xFrom +
                " yTo=" + yTo +
                " yFrom=" + yFrom
        );
    }
}
