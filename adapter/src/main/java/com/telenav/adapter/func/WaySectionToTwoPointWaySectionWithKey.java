package com.telenav.adapter.func;

import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.adapter.bean.TwoPointWaySection;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import scala.Tuple2;

import java.util.*;

import static com.telenav.adapter.component.TwoPointWaySectionKey.*;

/**
 * Pairs the way section with a compound key made out of:
 * -name
 * -highway
 * -oneway
 * if it has these OSM tags.
 * This key is to group two point way sections that satisfy the conditions for being candidate double digitized roads.
 */
public class WaySectionToTwoPointWaySectionWithKey implements PairFlatMapFunction<WaySection, String, TwoPointWaySection> {

    @Override
    public Iterator<Tuple2<String, TwoPointWaySection>> call(final WaySection waySection) {

        final Map<String, String> tags = waySection.tags();

        final String name = tags.get(OSM_NAME_TAG_KEY);
        final String highway = tags.get(OSM_HIGHWAY_TAG_KEY);
        final String oneway = tags.get(OSM_ONEWAY_TAG_KEY);

        /*
            Check if we have the info to create a compound key
            used for pairing double digitized roads.
         */
        if (name != null && !name.isEmpty() &&
                highway != null && !highway.isEmpty() &&
                oneway != null && oneway.equals("yes")) {

            final StringBuilder keyPrefix = keyPrefix(name, highway, oneway);

            /*
                Divide the way section to road portions defined by two consecutive points
                that belong to the way section.
             */
            final List<TwoPointWaySection> twoPointWaySections = toTwoPointWaySections(waySection);
            return twoPointWaySections.stream()
                    .map(new TwoPointWaySectionWithKeyMapper(keyPrefix))
                    .iterator();
        }
        return Collections.emptyIterator();
    }

    private StringBuilder keyPrefix(final String name,
                                    final String highway,
                                    final String oneway) {
        return new StringBuilder()
                .append(OSM_NAME_TAG_KEY + SEPARATOR).append(name)
                .append(OSM_HIGHWAY_TAG_KEY + SEPARATOR).append(highway)
                .append(OSM_ONEWAY_TAG_KEY + SEPARATOR).append(oneway);
    }

    private List<TwoPointWaySection> toTwoPointWaySections(final WaySection waySection) {

        final List<Node> nodes = waySection.nodes();

        final List<TwoPointWaySection> twoPointWaySections = new ArrayList<>();

        for (int i = 1; i < nodes.size(); i++) {

            final Node iThNode = nodes.get(i - 1);
            final Node iThMinusOneNode = nodes.get(i - 1);

            /*
                If the points that determine the two point way sections are the same,
                then the way section is actually a point. This is an inconvenient case
                ase we are looking for actual way sections, not for points.
                Therefore we do not add it.
            */
            final boolean nonIdenticalNodes = nonIdenticalNodes(iThNode, iThMinusOneNode);

            if (nonIdenticalNodes) {

                final TwoPointWaySection twoPointWaySection = TwoPointWaySection.builder()
                        .waySectionId(waySection.id())
                        .wayId(waySection.strictIdentifier().wayId())
                        .fromNode(iThNode)
                        .toNode(iThMinusOneNode)
                        .build();

                twoPointWaySections.add(twoPointWaySection);

            }
        }

        return twoPointWaySections;
    }

    /**
     * There might be cases where a two point way section is made out of
     * two different nodes, however the nodes have the same coordinates.
     *
     * @param iThNode         .
     * @param iThMinusOneNode .
     * @return true if node coordinates are identical, false otherwise.
     */
    private boolean nonIdenticalNodes(final Node iThNode, final Node iThMinusOneNode) {
        return iThNode.latitude().asDegrees() != iThMinusOneNode.latitude().asDegrees() &&
                iThNode.longitude().asDegrees() != iThMinusOneNode.longitude().asDegrees();
    }
}
