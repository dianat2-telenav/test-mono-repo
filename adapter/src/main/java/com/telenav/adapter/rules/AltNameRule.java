package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.TnAltNameExtractor;
import com.telenav.adapter.component.extractors.LanguageExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Map;
import java.util.Optional;


/**
 * Update the 'tn__alt_name' tag with the key 'alt_name:language'
 *
 * @author roxanal
 */
public class AltNameRule extends Enhancer<WaySection> {

    private final TnAltNameExtractor altNameExtractor;
    private final LanguageExtractor languageExtractor;


    public AltNameRule(final TnAltNameExtractor altNameExtractor, final LanguageExtractor languageExtractor) {
        this.altNameExtractor = altNameExtractor;
        this.languageExtractor = languageExtractor;
    }


    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> waySectionTags = waySection.tags();
        final Optional<String> waySectionAltNameTagValue = altNameExtractor.extract(waySectionTags);
        final Optional<String> languageCode = languageExtractor.extract(waySectionTags);
        //TODO should we cleanup all other alt names?
        final WaySection adaptedWaySection = waySection.withoutTag(OsmTagKey.ALT_NAME.unwrap());

        if (languageCode.isPresent() && waySectionAltNameTagValue.isPresent()) {
            return adaptedWaySection.withTag(OsmTagKey.ALT_NAME.postFixed(languageCode.get()),
                            waySectionAltNameTagValue.get());
        }
        return adaptedWaySection;
    }
}