package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.RailwayExtractor;
import com.telenav.adapter.component.extractors.WaterwayExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.map.entity.TagAdjuster;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.unidb.UnidbTagKey.BOUNDARY;
import static com.telenav.datapipelinecore.unidb.UnidbTagKey.TYPE;
import static com.telenav.datapipelinecore.unidb.UnidbTagValue.*;

/**
 * Add type=carto_line to ways having
 * railway=[rail, subway, light_rail] tag or
 * waterway=[river, wadi, canal] tag or
 * boundary=* tag
 *
 * @author irinap
 */
public class CartoLineRule<T extends TagAdjuster<T>> extends Enhancer<T> {
    private static final RailwayExtractor railwayExtractor = new RailwayExtractor();
    private static final WaterwayExtractor waterwayExtractor = new WaterwayExtractor();

    @Override
    public T apply(T entity) {
        final Map<String, String> tags = entity.tags();

        Optional<String> railwayTagOptional = railwayExtractor.extract(tags);

        if (railwayTagOptional.isPresent() &&
                (railwayTagOptional.get().equals(RAIL.unwrap())
                        || railwayTagOptional.get().equals(SUBWAY.unwrap())
                        || railwayTagOptional.get().equals(LIGHT_RAIL.unwrap()))) {
            return entity.withTag(TYPE.unwrap(), CARTO_LINE.unwrap());
        }

        Optional<String> waterwayTagOptional = waterwayExtractor.extract(tags);

        if (waterwayTagOptional.isPresent() &&
                (waterwayTagOptional.get().equals(WADI.unwrap())
                        || waterwayTagOptional.get().equals(RIVER.unwrap())
                        || waterwayTagOptional.get().equals(CANAL.unwrap()))) {
            return entity.withTag(TYPE.unwrap(), CARTO_LINE.unwrap());
        }

        if (tags.containsKey(BOUNDARY.unwrap())) {
            return entity.withTag(TYPE.unwrap(), CARTO_LINE.unwrap());
        }

        return entity;
    }
}
