package com.telenav.adapter.rules;

import java.util.Optional;
import com.telenav.adapter.component.extractors.FootExtractor;
import com.telenav.adapter.component.extractors.HighwayExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.map.entity.WaySection;

/**
 * The rule removes "foot" tag from way sections having "highway=motorway" tag
 * 
 * @author andriyz
 *
 */
public class CleanFootRule extends Enhancer<WaySection> {

    private static final long serialVersionUID = 1L;
    private final FootExtractor footExtractor = new FootExtractor();
    private final HighwayExtractor highwayExtractor = new HighwayExtractor();

    @Override
    public WaySection apply(WaySection waySection) {
        Optional<String> footValue = footExtractor.extract(waySection.tags());
        Optional<String> highwayValue = highwayExtractor.extract(waySection.tags());
        if (footValue.isPresent() && OsmHighwayValue.MOTORWAY.unwrap().equals(highwayValue.orElse(""))) {
            return waySection.withoutTag(footExtractor.getTagKey());
        }
        return waySection;
    }

}
