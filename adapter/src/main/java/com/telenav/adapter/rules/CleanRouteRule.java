package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.RouteExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.TagAdjuster;

import java.util.Map;
import java.util.Optional;

/**
 * Remove 'route' tags with value different from 'ferry'.
 *
 * @author maksymf
 */
public class CleanRouteRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private final RouteExtractor routeExtractor;

    private static final String FERRY = "ferry";

    public CleanRouteRule(final RouteExtractor routeExtractor) {
        this.routeExtractor = routeExtractor;
    }

    @Override
    public T apply(final T entity) {
        final Map<String, String> tags = entity.tags();
        final Optional<String> entityRouteTagValue = routeExtractor.extract(tags);
        if (entityRouteTagValue.map(value -> !value.equals(FERRY)).orElse(false)) {
            return entity.withoutTag(OsmTagKey.ROUTE.unwrap());
        }
        return entity;
    }
}
