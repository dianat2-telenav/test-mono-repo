package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.CountryIsoExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.TagAdjuster;

import java.util.Map;
import java.util.Optional;


/**
 * Update the 'tn__country_iso_code' tag with the key 'country_iso_code'
 *
 * @author dianat2
 */
public class CountryIsoTagRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private final CountryIsoExtractor countryIsoExtractor;

    public CountryIsoTagRule(final CountryIsoExtractor countryIsoExtractor) {
        this.countryIsoExtractor = countryIsoExtractor;
    }

    @Override
    public T apply(final T entity) {
        final Map<String, String> entityTags = entity.tags();
        final Optional<String> entityCountryIsoTagValue = countryIsoExtractor.extract(entityTags);

        final T adaptedEntity = entity.withoutTag(TnTagKey.TN_COUNTRY_ISO_CODE.unwrap());

        if (entityCountryIsoTagValue.isPresent()) {
            return adaptedEntity.withTag(TnTagKey.TN_COUNTRY_ISO_CODE.withoutTelenavPrefix(),
                    entityCountryIsoTagValue.get());
        }
        return adaptedEntity;
    }
}