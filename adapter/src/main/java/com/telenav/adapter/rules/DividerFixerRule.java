package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.map.entity.WaySection;

import java.util.Arrays;
import java.util.List;

public class DividerFixerRule extends Enhancer<WaySection> {
    private static final String DIVIDER_TAG_KEY = "divider";
    private static final List<String> DIVIDER_TAG_VALUES = Arrays.asList("1", "2", "A", "L", "N");
    private static final String DIVIDER_LEGAL_TAG_KEY = "divider_legal";
    private static final List<String> DIVIDER_LEGAL_TAG_VALUES = Arrays.asList("Y", "N");

    /**
     * According to the specification @link http://mapilot.telenav.com/docs/unidb_spec/spec/road_attributes/divider.html
     * tag 'divider' can have values from the list "1", "2", "A", "L", "N" only.
     * According to the specification @link http://mapilot.telenav.com/docs/unidb_spec/spec/road_attributes/divider.html
     * tag 'divider_legal' can have values 'Y' or 'N' only.
     *
     * @param waySection initial way section.
     * @return way section with fixed 'divider' and 'divider_legal' tags values.
     */
    @Override
    public WaySection apply(WaySection waySection) {

        if (!waySection.hasTag(DIVIDER_TAG_KEY, DIVIDER_TAG_VALUES)) {
            waySection = waySection.withoutTag(DIVIDER_TAG_KEY);
        }
        if (!waySection.hasTag(DIVIDER_LEGAL_TAG_KEY, DIVIDER_LEGAL_TAG_VALUES)) {
            waySection = waySection.withoutTag(DIVIDER_LEGAL_TAG_KEY);
        }

        return waySection;
    }
}
