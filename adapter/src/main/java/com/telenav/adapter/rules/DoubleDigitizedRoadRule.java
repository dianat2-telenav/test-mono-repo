package com.telenav.adapter.rules;

import com.telenav.map.entity.WaySection;
import com.telenav.adapter.component.DoubleDigitizedIdentifier;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.List;

/**
 * Enhances way section with rst=2.
 * This enhancement isn't done as others using extractor(s) because this information
 * cannot be inferred solely from the way section's tag, but rather via specific operations.
 *
 * @author liviuc
 */
public class DoubleDigitizedRoadRule {

    private final JavaSparkContext javaSparkContext;

    public DoubleDigitizedRoadRule(final JavaSparkContext javaSparkContext) {
        this.javaSparkContext = javaSparkContext;
    }

    public JavaRDD<WaySection> enhance(final JavaRDD<WaySection> waySections) {
        final JavaRDD<Long> pairedWaySectionIds = new DoubleDigitizedIdentifier(waySections).get();
        final List<Long> doubleDigitizedWaySectionIds = javaSparkContext.broadcast(pairedWaySectionIds.collect()).value();
        return waySections.map(waySection -> {
            if (doubleDigitizedWaySectionIds.contains(waySection.id())) {
                return waySection.withTag("rst", "2");
            }
            return waySection;
        });
    }
}
