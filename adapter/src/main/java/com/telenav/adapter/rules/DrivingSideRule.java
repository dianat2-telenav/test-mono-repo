package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.DrivingSideExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Optional;


/**
 * Add a new tag 'adas:driving_side' considering the 'tn__adas_driving_side' tag value
 *
 * @author ioanao
 */
public class DrivingSideRule extends Enhancer<WaySection> {

    private final DrivingSideExtractor drivingSideExtractor;


    public DrivingSideRule(final DrivingSideExtractor drivingSideExtractor) {
        this.drivingSideExtractor = drivingSideExtractor;
    }


    @Override
    public WaySection apply(final WaySection waySection) {
        final Optional<String> drivingSideTagValue = drivingSideExtractor.extract(waySection.tags());
        return drivingSideTagValue.isPresent() ?
                waySection.withTag(UnidbTagKey.DRIVING_SIDE.unwrap(), drivingSideTagValue.get()) : waySection;
    }
}