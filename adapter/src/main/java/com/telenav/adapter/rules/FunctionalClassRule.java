package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.WaySection;
import com.telenav.adapter.bean.RoadFunctionalClass;
import com.telenav.adapter.component.extractors.RoadFunctionalClassExtractor;

import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_FUNCTIONAL_CLASS;


/**
 * Add functional class tag {@link TnTagKey#TN_FUNCTIONAL_CLASS}. The rules are specified in {@link RoadFunctionalClassExtractor}
 *
 * @author adrianal
 */
public class FunctionalClassRule extends Enhancer<WaySection> {

    private final RoadFunctionalClassExtractor roadFCExtractor;

    public FunctionalClassRule(final RoadFunctionalClassExtractor extractor) {
        roadFCExtractor = extractor;
    }

    @Override
    public WaySection apply(final WaySection waySection) {
        final Optional<RoadFunctionalClass> roadFC = roadFCExtractor.extract(waySection.tags());
        if (roadFC.isPresent()) {
            return waySection.withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(),
                    String.valueOf(RoadFunctionalClass.identifierFor(roadFC.get())));
        }
        return waySection;
    }
}
