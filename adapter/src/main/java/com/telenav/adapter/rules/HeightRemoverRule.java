package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.map.entity.WaySection;

import java.util.Map;


/**
 * Remove height tag from navigable ways
 *
 * @author volodymyrl
 */
public class HeightRemoverRule extends Enhancer<WaySection> {

    @Override
    public WaySection apply(final WaySection waySection) {
        return waySection.withoutTag(OsmTagKey.HEIGHT.unwrap());
    }
}