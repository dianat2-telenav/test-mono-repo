package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.HighwayExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.TagAdjuster;

import java.util.Map;
import java.util.Optional;

/**
 * A rule that maps highway tags for non-sectioned ways according to the specifications.
 *
 * @author dianat2
 */
public class HighwayWayFixerRule extends Enhancer<RawWay> {

    private final HighwayExtractor highwayExtractor;

    public HighwayWayFixerRule(HighwayExtractor highwayExtractor) {
        this.highwayExtractor = highwayExtractor;
    }

    @Override
    public RawWay apply(RawWay rawWay) {
        Map<String, String> tags = rawWay.tags();
        final Optional<String> highWayValue = highwayExtractor.extract(tags);

        if (highWayValue.isPresent()) {
            switch (highWayValue.get()) {
                case "escape":
                case "busway":
                    return rawWay.withoutTag(OsmTagKey.HIGHWAY.unwrap())
                            .withTag(OsmTagKey.ACCESS.unwrap(), OsmHighwayValue.PRIVATE.unwrap());
                default:
                    return rawWay.withoutTag(OsmTagKey.HIGHWAY.unwrap());
            }
        }
        return rawWay;

    }
}