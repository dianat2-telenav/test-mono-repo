package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.HighwayExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Map;
import java.util.Optional;

/**
 * A rule that maps highway tags for sectioned ways according to the specifications.
 *
 * @author petrum
 */
public class HighwayWaySectionFixerRule extends Enhancer<WaySection> {

    private final HighwayExtractor highwayExtractor;

    public HighwayWaySectionFixerRule(HighwayExtractor highwayExtractor) {
        this.highwayExtractor = highwayExtractor;
    }

    @Override
    public WaySection apply(WaySection waySection) {
        Map<String, String> tags = waySection.tags();
        final Optional<String> highWayValue = highwayExtractor.extract(tags);

        if (highWayValue.isPresent()) {
            switch (highWayValue.get()) {
                case "motorway":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
                case "motorway_link":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY_LINK.unwrap());
                case "trunk":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK.unwrap());
                case "trunk_link":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK_LINK.unwrap());
                case "primary":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.PRIMARY.unwrap());
                case "primary_link":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.PRIMARY_LINK.unwrap());
                case "secondary":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SECONDARY.unwrap());
                case "secondary_link":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SECONDARY_LINK.unwrap());
                case "tertiary":
                case "unclassified":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TERTIARY.unwrap());
                case "tertiary_link":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TERTIARY_LINK.unwrap());
                case "residential":
                case "residential_link":
                case "living_street":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
                case "service":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SERVICE.unwrap());
                case "pedestrian":
                case "road":
                case "steps":
                case "cycleway":
                case "footway":
                case "path":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.PEDESTRIAN.unwrap());
                case "track":
                case "construction":
                case "undefined":
                case "unknown":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRACK.unwrap());
                case "raceway":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), "raceway");
                case "bridleway":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRACK.unwrap())
                            .withTag(OsmTagKey.ACCESS.unwrap(), OsmHighwayValue.PRIVATE.unwrap());
                case "bus_guideway":
                case "private":
                    return waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SERVICE.unwrap())
                            .withTag(OsmTagKey.ACCESS.unwrap(), OsmHighwayValue.PRIVATE.unwrap());
                default:
                    return waySection.withoutTag(OsmTagKey.HIGHWAY.unwrap());
            }
        }
        return waySection;

    }
}