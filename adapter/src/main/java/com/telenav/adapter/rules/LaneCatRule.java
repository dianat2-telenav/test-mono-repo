package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.LaneExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.telenav.adapter.component.fc.TagService.LANE_CAT;

public class LaneCatRule extends Enhancer<WaySection> {

    private static final String LANE_CAT_UNI_DB_KEY = LANE_CAT;
    private static final Map<Integer, String> NUM_LANES_TO_LANE_CAT;
    private static final String LANE_CAT_1 = "1";
    private static final String LANE_CAT_2 = "2";
    private static final String LANE_CAT_3 = "3";

    static {
        NUM_LANES_TO_LANE_CAT = new HashMap<>();
        NUM_LANES_TO_LANE_CAT.put(0, LANE_CAT_1);
        NUM_LANES_TO_LANE_CAT.put(1, LANE_CAT_1);
        NUM_LANES_TO_LANE_CAT.put(2, LANE_CAT_2);
        NUM_LANES_TO_LANE_CAT.put(3, LANE_CAT_3);
        NUM_LANES_TO_LANE_CAT.put(4, LANE_CAT_3);
    }

    private final LaneExtractor laneExtractor;

    public LaneCatRule(final LaneExtractor laneExtractor) {
        this.laneExtractor = laneExtractor;
    }

    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> tags = waySection.tags();
        final Optional<String> lanesForward = laneExtractor.extract(tags, OsmTagKey.LANES_FORWARD.unwrap());
        final Optional<String> lanesBackward = laneExtractor.extract(tags, OsmTagKey.LANES_BACKWARD.unwrap());
        if (!lanesForward.isPresent() && !lanesBackward.isPresent()) {
            tags.put(LANE_CAT_UNI_DB_KEY, LANE_CAT_1);
        } else {
            int forward = getNumberOfLanes(lanesForward);
            int backward = getNumberOfLanes(lanesBackward);
            tags.put(LANE_CAT_UNI_DB_KEY, getLaneCat(Math.max(forward, backward)));
        }
        return waySection;
    }

    private int getNumberOfLanes(final Optional<String> lanes) {
        try {
            return lanes.map(Integer::parseInt).orElse(0);
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }

    private String getLaneCat(final int numLanes) {
        if (NUM_LANES_TO_LANE_CAT.containsKey(numLanes)) {
            return NUM_LANES_TO_LANE_CAT.get(numLanes);
        } else if (numLanes > 0) {
            return LANE_CAT_3;
        } else {
            return LANE_CAT_1;
        }
    }
}
