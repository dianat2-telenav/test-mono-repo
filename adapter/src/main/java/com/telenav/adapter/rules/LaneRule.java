package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.LaneExtractor;
import com.telenav.adapter.component.extractors.OneWayExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.map.entity.WaySection;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;


/**
 * Add extra info to way if it contains 'lane' and 'oneway' info
 *
 * @author roxanal
 */
public class LaneRule extends Enhancer<WaySection> {

    private static final String ONEWAY_LANES_TAG_UNIDB_KEY = "oneway:lanes";
    public static final int MAXIMUM_LANE_NUMBER_PER_WAY = 30;
    private static final int MINIMUM_LANE_NUMBER_PER_WAY = 0;
    private static final int DEFAULT_LANE_NUMBER = 0;
    private static final int MAXIMUM_LANE_NUMBER_PER_WAY_PER_SENSE = 15;
    private final LaneExtractor laneExtractor;
    private final OneWayExtractor oneWayExtractor;


    public LaneRule(final LaneExtractor laneExtractor, final OneWayExtractor oneWayExtractor) {
        super(Collections.singleton(OneWayRule.class));
        this.laneExtractor = laneExtractor;
        this.oneWayExtractor = oneWayExtractor;
    }

    @Override
    public WaySection apply(WaySection waySection) {
        final Map<String, String> tags = waySection.tags();
        final Optional<Map<String, String>> oneWay = oneWayExtractor.extract(tags);
        Optional<String> lanes = laneExtractor.extract(tags, OsmTagKey.LANES.unwrap());
        Optional<String> lanesForward = laneExtractor.extract(tags, OsmTagKey.LANES_FORWARD.unwrap());
        Optional<String> lanesBackward = laneExtractor.extract(tags, OsmTagKey.LANES_BACKWARD.unwrap());
        int lanesInt = getNumberOfLanes(lanes);
        int lanesForwardInt = getNumberOfLanes(lanesForward);
        int lanesBackwardInt = getNumberOfLanes(lanesBackward);
        if (lanesInt < lanesForwardInt + lanesBackwardInt) {
            lanesForwardInt = 0;
            lanesBackwardInt = 0;
        }
        if (lanes.isPresent() && lanesInt != 0) {
            tags.replace(OsmTagKey.LANES.unwrap(), Integer.toString(lanesInt));
            if (oneWay.isPresent()) {
                final String oneWayValue = oneWay.get().get(OsmTagKey.ONEWAY.unwrap());
                if (oneWayValue.equals(DefaultTagValue.BACKWARD_ONEWAY.unwrap())) {
                    tags.put(OsmTagKey.LANES_BACKWARD.unwrap(), Integer.toString(lanesInt));
                    lanesBackwardInt = lanesInt;
                    lanesForwardInt = 0;
                } else if (oneWayValue.equals("yes")) {
                    tags.put(OsmTagKey.LANES_FORWARD.unwrap(), Integer.toString(lanesInt));
                    lanesForwardInt = lanesInt;
                    lanesBackwardInt = 0;
                } else if (lanes.get().equals("2") && oneWayValue.equals("no")) {
                    tags.put(OsmTagKey.LANES_BACKWARD.unwrap(), "1");
                    tags.put(OsmTagKey.LANES_FORWARD.unwrap(), "1");
                    lanesBackwardInt = 1;
                    lanesForwardInt = 1;
                }
            } else if (lanes.get().equals("2")) {
                tags.put(OsmTagKey.LANES_BACKWARD.unwrap(), "1");
                tags.put(OsmTagKey.LANES_FORWARD.unwrap(), "1");
                lanesBackwardInt = 1;
                lanesForwardInt = 1;
            }
        }
        if ((!oneWay.isPresent() || oneWay.get().get(OsmTagKey.ONEWAY.unwrap()).equals("no")) && lanes.isPresent()) {
            if (lanesBackward.isPresent()) {
                lanesForwardInt = lanesInt - lanesBackwardInt;
            } else if (lanesForward.isPresent()) {
                lanesBackwardInt = lanesInt - lanesForwardInt;
            }
        }

        final StringBuilder oneWayLanesTagValue = new StringBuilder();
        for (int i = 0; i < lanesForwardInt; i++) {
            oneWayLanesTagValue.append("yes");
            //put a '|' between all "yes" values and after the last one if there is any "no" value
            if (i < lanesForwardInt - 1 || lanesBackwardInt > 0) {
                oneWayLanesTagValue.append("|");
            }
        }
        for (int i = 0; i < lanesBackwardInt; i++) {
            oneWayLanesTagValue.append("-1");
            if (i < lanesBackwardInt - 1) {
                oneWayLanesTagValue.append("|");
            }
        }
        final String oneWayLanesAsString = oneWayLanesTagValue.toString();
        if (!oneWayLanesAsString.isEmpty()) {
            tags.put(ONEWAY_LANES_TAG_UNIDB_KEY, oneWayLanesAsString);
        }

        if (lanesInt == 0) {
            tags.remove(OsmTagKey.LANES.unwrap());
        }
        if (lanesForwardInt == 0) {
            tags.remove(OsmTagKey.LANES_FORWARD.unwrap());
        }
        if (lanesBackwardInt == 0) {
            tags.remove(OsmTagKey.LANES_BACKWARD.unwrap());
        }
        waySection = waySection.replaceTags(tags);
        return waySection;
    }

    /**
     * Retrieves the number of lanes.
     *
     * @param lanes An optional of string representing the raw lane number.
     *
     * @return An integer representing the number of lanes.
     */
    private int getNumberOfLanes(final Optional<String> lanes) {
        try {
            int result = lanes.map(Integer::parseInt).orElse(0);
            if (result <= MINIMUM_LANE_NUMBER_PER_WAY || result > MAXIMUM_LANE_NUMBER_PER_WAY) {
                result = DEFAULT_LANE_NUMBER;
            }
            if (result >= MAXIMUM_LANE_NUMBER_PER_WAY_PER_SENSE) {
                result = MAXIMUM_LANE_NUMBER_PER_WAY_PER_SENSE;
            }
            return result;
        } catch (NumberFormatException nfe) {
            return DEFAULT_LANE_NUMBER;
        }
    }
}