package com.telenav.adapter.rules;

import com.telenav.adapter.utils.lanes.OsmToUniDbLaneConverter;
import com.telenav.adapter.utils.lanes.OsmToUniDbLaneValueConverter;
import com.telenav.adapter.utils.turnLanes.OsmToUniDbTurnLaneValueConverter;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.telenav.adapter.component.fc.TagService.TYPE_LANES;
import static com.telenav.adapter.utils.turnLanes.OsmToUniDbTurnLanesEnhancer.TURN_LANE_SEPARATOR;

/**
 * Adds type:lanes UniDB values.
 *
 * @author liviuc
 */
public class LaneTypeRule extends Enhancer<WaySection> {

    public static final String LANE_TYPES_SEPARATOR = "|";
    public static final String REGULAR_LANE_VALUE = "1";
    public static final String TURN_LANE_VALUE = "2048";
    public static final String CORRUPTED_WAY_SECTION_KEY = "corrupted_way_section";
    public static final String HOV_LANES = "hov:lanes";
    public static final String HOV_LANES_OSM_KEY = "hov:lanes";
    public static final String HOV_LANES_FORWARD_OSM_KEY = "hov:lanes:forward";
    public static final String HOV_LANES_BACKWARD_OSM_KEY = "hov:lanes:backward";
    public static final String CENTRE_TURN_LANE_OSM_KEY = "centre_turn_lane";
    public static final String BICYCLE_LANES_OSM_KEY = "bicycle:lanes";
    public static final String BICYCLE_LANES_FORWARD_OSM_KEY = "bicycle:lanes:forward";
    public static final String BICYCLE_LANES_BACKWARD_OSM_KEY = "bicycle:lanes:backward";
    public static final String REVERSIBLE_LANES_OSM_KEY = "reversible:lanes";
    public static final String UNI_DB_TYPE_LANES_KEY = TYPE_LANES;
    private static final String HOV_LANE_VALUE = "2";
    private static final String CENTRE_TURN_LANE_VALUE = "4096";
    private static final String BICYCLE_LANE_VALUE = "65536";
    private static final String REVERSIBLE_LANE_VALUE = "4";
    private static final String HOV_REVERSIBLE_LANE_VALUE = "6";
    private static final String TN_LANE_TYPES_FORWARD = "tn_lane:types:forward";
    private static final String TN_LANE_TYPES_BACKWARD = "tn_lane:types:backward";
    private final OsmToUniDbTurnLaneValueConverter osmToUniDbTurnLaneValueConverter =
            new OsmToUniDbTurnLaneValueConverter();

    @Override
    public WaySection apply(WaySection waySection) {
        final Map<String, String> tags = waySection.tags();
        if (tags.containsKey(CORRUPTED_WAY_SECTION_KEY)) {
            return waySection.withoutTag(CORRUPTED_WAY_SECTION_KEY);
        }
        if (tags.containsKey(OsmTagKey.TURN_LANES.unwrap())) {
            waySection = getTurnLaneType(waySection, tags, OsmTagKey.TURN_LANES.unwrap());
        } else {
            boolean hasHovLanes = tags.containsKey(HOV_LANES);
            boolean hasHovLanesFw = tags.containsKey(HOV_LANES_FORWARD_OSM_KEY);
            boolean hasHovLanesBw = tags.containsKey(HOV_LANES_BACKWARD_OSM_KEY);
            boolean hasCentreTurnLanes = tags.containsKey(CENTRE_TURN_LANE_OSM_KEY);
            boolean hasBicycleLanes = tags.containsKey(BICYCLE_LANES_OSM_KEY);
            boolean hasBicycleLanesFw = tags.containsKey(BICYCLE_LANES_FORWARD_OSM_KEY);
            boolean hasBicycleLanesBw = tags.containsKey(BICYCLE_LANES_BACKWARD_OSM_KEY);
            boolean hasReversibleLanes = tags.containsKey(REVERSIBLE_LANES_OSM_KEY);

            boolean reversibleHovLanesCondition = hasHovLanes && hasReversibleLanes;
            waySection = addLaneTypesForCondition(waySection,
                    REVERSIBLE_LANES_OSM_KEY,
                    UNI_DB_TYPE_LANES_KEY,
                    HOV_REVERSIBLE_LANE_VALUE,
                    true,
                    hasHovLanes,
                    hasReversibleLanes);

            boolean extraCondition = !reversibleHovLanesCondition;
            waySection = addLaneTypesForCondition(waySection,
                    HOV_LANES_OSM_KEY,
                    UNI_DB_TYPE_LANES_KEY,
                    HOV_LANE_VALUE,
                    true,
                    hasHovLanes,
                    extraCondition);

            extraCondition &= !hasHovLanes;
            waySection = addLaneTypesForCondition(waySection,
                    HOV_LANES_FORWARD_OSM_KEY,
                    TN_LANE_TYPES_FORWARD,
                    HOV_LANE_VALUE,
                    true,
                    hasHovLanesFw,
                    extraCondition);
            waySection = addLaneTypesForCondition(waySection,
                    HOV_LANES_BACKWARD_OSM_KEY,
                    TN_LANE_TYPES_BACKWARD,
                    HOV_LANE_VALUE,
                    false,
                    hasHovLanesBw,
                    extraCondition);
            waySection = enhanceLaneTypesFromMultipleOsmLaneTypes(waySection,
                    hasHovLanesBw & extraCondition,
                    TN_LANE_TYPES_FORWARD,
                    TN_LANE_TYPES_BACKWARD);

            extraCondition = !(hasHovLanesFw || hasHovLanesBw);
            waySection = addLaneTypesForCondition(waySection,
                    REVERSIBLE_LANES_OSM_KEY,
                    UNI_DB_TYPE_LANES_KEY,
                    REVERSIBLE_LANE_VALUE,
                    true,
                    hasReversibleLanes,
                    extraCondition);

            extraCondition &= !hasReversibleLanes;
            waySection = addLaneTypesForCondition(waySection,
                    CENTRE_TURN_LANE_OSM_KEY,
                    UNI_DB_TYPE_LANES_KEY,
                    CENTRE_TURN_LANE_VALUE,
                    true,
                    hasCentreTurnLanes,
                    extraCondition);

            waySection = addLaneTypesForCondition(waySection,
                    BICYCLE_LANES_OSM_KEY,
                    UNI_DB_TYPE_LANES_KEY,
                    BICYCLE_LANE_VALUE,
                    true,
                    hasBicycleLanes,
                    extraCondition);

            extraCondition &= !hasBicycleLanes;
            waySection = addLaneTypesForCondition(waySection,
                    BICYCLE_LANES_FORWARD_OSM_KEY,
                    UNI_DB_TYPE_LANES_KEY,
                    BICYCLE_LANE_VALUE,
                    true,
                    hasBicycleLanesFw,
                    extraCondition);
            waySection = addLaneTypesForCondition(waySection,
                    BICYCLE_LANES_BACKWARD_OSM_KEY,
                    UNI_DB_TYPE_LANES_KEY,
                    BICYCLE_LANE_VALUE,
                    false,
                    hasBicycleLanesBw,
                    extraCondition);
        }
        return waySection;
    }

    private WaySection enhanceLaneTypesFromMultipleOsmLaneTypes(final WaySection waySection,
                                                                final boolean condition,
                                                                final String fw,
                                                                final String bw) {
        if (condition) {
            final Map<String, String> tags = waySection.tags();
            final String fwLaneType = tags.get(fw);
            final String bwLaneType = tags.get(bw);
            final String turnLanesValue = getLaneTypesValues(fwLaneType, bwLaneType);
            tags.put(UNI_DB_TYPE_LANES_KEY, turnLanesValue);
            tags.remove("tn_lane:types:forward");
            tags.remove("tn_lane:types:backward");
            return waySection.replaceTags(tags);
        }
        return waySection;
    }

    private String getLaneTypesValues(final String... turnLanesValuesArray) {
        return Arrays.stream(turnLanesValuesArray)
                .filter(turnLanesValue -> turnLanesValue != null && !turnLanesValue.isEmpty())
                .collect(Collectors.joining(TURN_LANE_SEPARATOR));

    }

    private WaySection getTurnLaneType(final WaySection waySection,
                                       final Map<String, String> tags,
                                       final String turnLanesOsmKey) {
        try {
            if (turnLanesOsmKey != null) {
                final String[] turnLaneType =
                        getLaneType(turnLanesOsmKey, waySection, osmToUniDbTurnLaneValueConverter);
                return addTags(waySection, UNI_DB_TYPE_LANES_KEY, turnLaneType, false);
            }
        } catch (final NumberFormatException e) {
            return waySection.replaceTags(tags);
        }
        return waySection;
    }

    private WaySection addLaneTypesForCondition(final WaySection waySection,
                                                final String laneTypeOsmKey,
                                                final String laneTypeUniDbKey,
                                                final String laneTypeValue,
                                                final boolean revertLanes,
                                                final Boolean... conditions) {
        boolean conditionSatisfied = true;
        for (final Boolean condition : conditions) {
            conditionSatisfied &= condition;
            if (!conditionSatisfied) {
                return waySection;
            }
        }
        return enhanceLaneTypes(waySection, laneTypeOsmKey, laneTypeUniDbKey, laneTypeValue, revertLanes);
    }

    private WaySection enhanceLaneTypes(final WaySection waySection,
                                        final String laneTypeOsmKey,
                                        final String laneTypeUniDbKey,
                                        final String laneTypeValue,
                                        final boolean revertLanes) {
        final OsmToUniDbLaneValueConverter osmToUniDbLaneValueConverter =
                new OsmToUniDbLaneValueConverter(laneTypeOsmKey, laneTypeValue);
        return addLaneTypesTag(waySection,
                laneTypeOsmKey,
                laneTypeUniDbKey,
                osmToUniDbLaneValueConverter,
                revertLanes);
    }

    private WaySection addLaneTypesTag(final WaySection waySection,
                                       final String osmTagKey,
                                       final String laneTypeUniDbKey,
                                       final OsmToUniDbLaneConverter osmToUniDbLaneConverter,
                                       final boolean revertLanes) {
        final String[] hovReversibleLaneType =
                getLaneType(osmTagKey, waySection, osmToUniDbLaneConverter);
        return addTags(waySection, laneTypeUniDbKey, hovReversibleLaneType, revertLanes);
    }

    private WaySection addTags(final WaySection waySection,
                               final String key,
                               final String[] laneType,
                               boolean revertLanes) {
        if (laneType != null && laneType.length > 0) {
            final List<String> laneTypesAsString = Arrays.asList(laneType);
            if (revertLanes) {
                Collections.reverse(laneTypesAsString);
            }
            final String laneTypes = laneTypesAsString.stream()
                    .map(Object::toString)
                    .collect(Collectors.joining(LANE_TYPES_SEPARATOR));
            final Map<String, String> tags = waySection.tags();
            tags.put(key, laneTypes);
            return waySection.replaceTags(tags);
        }
        return waySection;
    }

    private String[] getLaneType(final String tagKey,
                                 final WaySection waySection,
                                 final OsmToUniDbLaneConverter converter) {
        final Map<String, String> tags = waySection.tags();
        if (tags.containsKey(tagKey)) {
            final String[] compact = compact(tagKey, tags);
            return converter.convert(waySection, compact);
        }
        return null;
    }

    private String[] compact(final String tagKey, final Map<String, String> tags) {
        String lanesValue = tags.get(tagKey);
        while (lanesValue.contains(LANE_TYPES_SEPARATOR + LANE_TYPES_SEPARATOR)) {
            lanesValue = lanesValue.replace(LANE_TYPES_SEPARATOR + LANE_TYPES_SEPARATOR,
                    LANE_TYPES_SEPARATOR + "none" + LANE_TYPES_SEPARATOR);
        }
        if (lanesValue.lastIndexOf(LANE_TYPES_SEPARATOR) == lanesValue.length() - 1) {
            lanesValue += "none";
        }
        return lanesValue.split("\\" + LANE_TYPES_SEPARATOR);
    }
}
