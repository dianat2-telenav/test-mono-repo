package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.LanguageExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.TagAdjuster;

import java.util.Map;
import java.util.Optional;


/**
 * Update the 'tn__language_code' tag with the key 'language_code'
 *
 * @author dianat2
 */
public class LanguageTagRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private final LanguageExtractor languageExtractor;

    public LanguageTagRule(final LanguageExtractor languageExtractor) {
        this.languageExtractor = languageExtractor;
    }

    @Override
    public T apply(final T entity) {
        final Map<String, String> entityTags = entity.tags();
        final Optional<String> entityLanguageTagValue = languageExtractor.extract(entityTags);

        final T adaptedEntity = entity.withoutTag(TnTagKey.TN_LANGUAGE_CODE.unwrap());

        if (entityLanguageTagValue.isPresent()) {
            return adaptedEntity.withTag(TnTagKey.TN_LANGUAGE_CODE.withoutTelenavPrefix(),
                    entityLanguageTagValue.get());
        }
        return adaptedEntity;
    }
}