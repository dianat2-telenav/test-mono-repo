package com.telenav.adapter.rules;

import com.telenav.adapter.utils.ManualEditsUtil;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;


/**
 * Class which alters the 'tn__source:oneway = value' tag like so: 'tn__value:oneway = oneWayValue / missing'
 *
 * @author roxanal
 */
public class ManualOneWayRule extends Enhancer<WaySection> {

    /**
     * All tags which are of the form: 'tn__source:{feature} = value' become 'tn__value:{feature} = missing / the
     * feature tag's value'
     *
     * @param waySection - the way section to enhance
     * @return - the enhanced way section
     */
    @Override
    public WaySection apply(final WaySection waySection) {
        Map<String, String> tags = waySection.tags();
        tags = ManualEditsUtil.alterManualEditsTag(tags, OsmTagKey.ONEWAY.unwrap());
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }
}