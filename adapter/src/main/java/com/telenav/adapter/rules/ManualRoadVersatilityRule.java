package com.telenav.adapter.rules;

import com.telenav.adapter.utils.ManualEditsUtil;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Map;


/**
 * Class which alters the 'tn__source:motorcar = value'/'tn__source:motorcycle = value'  tag like so:
 * 'tn__value:motorcar = motorcarValue / missing' and 'tn__value:motorcycle = motorcycleValue / missing'
 *
 * @author roxanal
 */
public class ManualRoadVersatilityRule extends Enhancer<WaySection> {

    @Override
    public WaySection apply(final WaySection waySection) {
        Map<String, String> tags = waySection.tags();
        tags = ManualEditsUtil.alterManualEditsTag(tags, OsmTagKey.MOTORCAR.unwrap());
        tags = ManualEditsUtil.alterManualEditsTag(tags, OsmTagKey.MOTORCYCLE.unwrap());
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }
}