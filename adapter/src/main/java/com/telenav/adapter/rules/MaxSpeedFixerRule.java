package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.MaxSpeedExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.map.entity.WaySection;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import scala.Tuple2;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class MaxSpeedFixerRule extends Enhancer<WaySection> {

    private static final String SPEED_STRING_REGEX = "^(\\d*)\\D*";
    private static final String CLEANUP_REGEX = "[`'\"]";

    private static final Pattern SPEED_STRING_PATTERN =  Pattern.compile(SPEED_STRING_REGEX);

    private final MaxSpeedExtractor maxSpeedExtractor;

    public MaxSpeedFixerRule(MaxSpeedExtractor maxSpeedExtractor) {
        this.maxSpeedExtractor = maxSpeedExtractor;
    }

    /**
     * Maxspeed... tags must contain numeric values only.
     *
     * @param waySection initial way section.
     * @return way section with fixed 'maxspeed...' tags value.
     */
    @Override
    public WaySection apply(WaySection waySection) {

            Optional<Map<String, String>> maxSpeedTags = maxSpeedExtractor.extract(waySection.tags());
            if (maxSpeedTags.isPresent()) {
                Map<String, String> processed = maxSpeedTags.get().entrySet().stream()
                        .map(e -> new Tuple2<>(e.getKey(), fixSpeedValue(waySection, e)))
                        .filter(e -> StringUtils.isNotEmpty(e._2))
                        .collect(Collectors.toMap(Tuple2::_1, Tuple2::_2));
                Set<String> tagKeysToRemove = maxSpeedTags.get().keySet();
                tagKeysToRemove.removeAll(processed.keySet());
                return waySection.withTags(processed).withoutTags(tagKeysToRemove);
            }

            return waySection;
    }

    private static String fixSpeedValue(WaySection waySection, Map.Entry<String, String> entry) {
        final Matcher matcher = SPEED_STRING_PATTERN.matcher(entry.getValue().replaceAll(CLEANUP_REGEX, "").trim());
        try {
            if (matcher.find()) {
                final String groupFound = matcher.group(1);
                if (StringUtils.isNotBlank(groupFound)) {
                    int speedLimit = Integer.parseInt(groupFound);
                    return speedLimit > 0 && speedLimit < 999 ? Integer.toString(speedLimit) : null;
                }
            }
        } catch (NumberFormatException e) {
            log.warn("Wrong speed value for WaySection ID: {}. tags->'{}'={}",
                    waySection.id(),entry.getKey(), entry.getValue());
        }
        return null;
    }
}
