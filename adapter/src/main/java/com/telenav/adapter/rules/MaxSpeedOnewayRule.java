package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.TagAdjuster;
import java.util.Map;

/**
 * maxspeed:forward  Speed limit in the positive direction of the navigable link.
 * maxspeed:backward  Speed limit in the negative direction of the navigable link.
 *
 * @author olehd
 */
public class MaxSpeedOnewayRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    @Override
    public T apply(final T entity) {
        Map<String, String> tags =  entity.tags();
        if (tags.containsKey(OsmTagKey.ONEWAY.unwrap())) {
            final String oneWayValue = tags.get(OsmTagKey.ONEWAY.unwrap());
            if (oneWayValue != null) {
                if (oneWayValue.equals("yes")) {
                    tags.remove(OsmTagKey.MAXSPEED_BACKWARD.unwrap());
                    tags.remove(OsmTagKey.MAXSPEED_BACKWARD.withSource());
                } else if (oneWayValue.equals("-1")) {
                    tags.remove(OsmTagKey.MAXSPEED_FORWARD.unwrap());
                    tags.remove(OsmTagKey.MAXSPEED_FORWARD.withSource());
                }
            }
        }
        return entity.replaceTags(tags);
    }
}
