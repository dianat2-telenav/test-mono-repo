package com.telenav.adapter.rules;

import com.google.common.collect.Sets;
import com.telenav.adapter.utils.InvalidTagsCleaner;
import com.telenav.adapter.utils.OsmTagKeysArchiver;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;

import java.util.*;


/**
 *
 * @author olehd
 */
public class MaxSpeedRule extends Enhancer<WaySection> {

    private static final Set<OsmTagKey> OSM_TAGS_TO_ARCHIVE =
            Sets.newHashSet(OsmTagKey.MAXSPEED, OsmTagKey.MAXSPEED_FORWARD, OsmTagKey.MAXSPEED_BACKWARD,
                    OsmTagKey.MAXSPEED_CONDITIONAL);

    @Override
    public WaySection apply(final WaySection waySection) {
        Map<String, String> tags = new OsmTagKeysArchiver(OSM_TAGS_TO_ARCHIVE).archiveTags(waySection.tags());
        if (tags.containsKey(TnTagKey.TN_SPEED_UNIT.unwrap())) {
            tags = constructFinalTags(tags);
        }
        tags = InvalidTagsCleaner.deleteTagsWithInvalidValue(tags, OsmTagKey.MAXSPEED.unwrap());
        tags = InvalidTagsCleaner.deleteTagsWithInvalidValue(tags, OsmTagKey.MAXSPEED_FORWARD.unwrap());
        tags = InvalidTagsCleaner.deleteTagsWithInvalidValue(tags, OsmTagKey.MAXSPEED_BACKWARD.unwrap());
        tags = InvalidTagsCleaner.deleteTagsWithInvalidValue(tags, OsmTagKey.MAXSPEED_CONDITIONAL.unwrap());
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }

    private Map<String, String> constructFinalTags(final Map<String, String> tags) {
        final MultipleSourceTagRule maxSpeedForwardEnhancer = MultipleSourceTagRule.builder()
                .imageTagName(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES))
                .probeTagName(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.PROBES))
                .isaTagName(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.ISA))
                .osmTagName(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix())
                .manualTagName(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.MANUAL))
                .deletedTagName(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.DELETED))
                .finalTagName(OsmTagKey.MAXSPEED_FORWARD)
                .build();
        final MultipleSourceTagRule maxSpeedBackwardEnhancer = MultipleSourceTagRule.builder()
                .imageTagName(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES))
                .probeTagName(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.PROBES))
                .isaTagName(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.ISA))
                .osmTagName(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix())
                .manualTagName(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.MANUAL))
                .deletedTagName(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.DELETED))
                .finalTagName(OsmTagKey.MAXSPEED_BACKWARD)
                .build();
        final MultipleSourceTagRule conditionalMaxSpeedEnhancer = MultipleSourceTagRule.builder()
                .osmTagName(OsmTagKey.MAXSPEED_CONDITIONAL.telenavPrefix())
                .manualTagName(OsmTagKey.MAXSPEED_CONDITIONAL.withSource(TnSourceValue.MANUAL))
                .deletedTagName(OsmTagKey.MAXSPEED_CONDITIONAL.withSource(TnSourceValue.DELETED))
                .finalTagName(OsmTagKey.MAXSPEED_CONDITIONAL)
                .build();
        return maxSpeedForwardEnhancer
            .andThen(maxSpeedBackwardEnhancer)
            .andThen(conditionalMaxSpeedEnhancer)
            .apply(tags);
    }
}