package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.map.entity.TagAdjuster;

import java.util.*;

/**
 * Update OsmTagKey.MAXSPEED_FORWARD and OsmTagKey.MAXSPEED_BACKWARD
 * 1(Posted): Based on Posted signs or road markings
 * 2(Derived): Based on Overall country/administrative rules
 *
 * @author olehd
 */
public class MaxSpeedSourceRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    @Override
    public T apply(final T entity) {
        Map<String, String> tags = entity.tags();
        Map<String, String> newTags = new HashMap<>(tags);
        for (String tagKey : tags.keySet()) {
            if (tagKey.contains(OsmTagKey.MAXSPEED_FORWARD.withSource())
                    || tagKey.contains(OsmTagKey.MAXSPEED_BACKWARD.withSource())
                    || tagKey.contains(OsmTagKey.MAXSPEED.withSource())) {
                String tagValue = tags.get(tagKey);
                if (tagValue.equals(TnSourceValue.MANUAL.unwrap())
                        || tagValue.equals(TnSourceValue.DELETED.unwrap())
                        || tagValue.equals(TnSourceValue.IMAGES.unwrap())
                        || tagValue.equals(TnSourceValue.OSM.unwrap())
                        || tagValue.equals(TnSourceValue.POSTED.unwrap())
                ) {
                    tagValue = TnSourceValue.POSTED.unwrap();
                } else  if (tagValue.equals(TnSourceValue.PROBES.unwrap())
                        || tagValue.equals(TnSourceValue.ISA.unwrap())
                        || tagValue.equals(TnSourceValue.DERIVED.unwrap())
                ) {
                    tagValue = TnSourceValue.DERIVED.unwrap();
                } else {
                    tagValue = TnSourceValue.POSTED.unwrap(); // default
                }
                newTags.remove(tagKey);
                newTags.put(OsmTagKey.MAXSPEED.withSource(), tagValue);
            }
        }
        return entity.replaceTags(newTags);
    }

}