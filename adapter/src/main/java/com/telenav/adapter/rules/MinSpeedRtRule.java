package com.telenav.adapter.rules;

import com.telenav.adapter.utils.SpeedNormalizer;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;

import java.util.HashMap;
import java.util.Map;


/**
 * Add minspeed specific tags 'tn__minspeed', 'tn_minspeed:forward', 'tn_minspeed:backward'.
 *
 * @author petrum
 */
public class MinSpeedRtRule extends Enhancer<WaySection> {

    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> tags = new HashMap<>(waySection.tags());
        final String speedUnit = tags.get(TnTagKey.TN_SPEED_UNIT.unwrap());
        if (speedUnit != null) {
            SpeedNormalizer.normalize(tags.get(OsmTagKey.MINSPEED.unwrap()), speedUnit)
                    .ifPresent(value -> tags.put(OsmTagKey.MINSPEED.telenavPrefix(), value));
            SpeedNormalizer.normalize(tags.get(OsmTagKey.MINSPEED_FORWARD.unwrap()), speedUnit)
                    .ifPresent(value -> tags.put(OsmTagKey.MINSPEED_FORWARD.telenavPrefix(), value));
            SpeedNormalizer.normalize(tags.get(OsmTagKey.MINSPEED_BACKWARD.unwrap()), speedUnit)
                    .ifPresent(value -> tags.put(OsmTagKey.MINSPEED_BACKWARD.telenavPrefix(), value));
        }
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }
}
