package com.telenav.adapter.rules;

import com.google.common.collect.Sets;
import com.telenav.adapter.utils.OsmTagKeysArchiver;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmPostFix;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Map;
import java.util.Set;


/**
 *
 * @author ioanao
 */
public class MinSpeedRule extends Enhancer<WaySection> {

    private static final Set<OsmTagKey> OSM_TAGS_TO_ARCHIVE =
            Sets.newHashSet(OsmTagKey.MINSPEED, OsmTagKey.MINSPEED_BACKWARD, OsmTagKey.MINSPEED_FORWARD);

    @Override
    public WaySection apply(final WaySection waySection) {
        Map<String, String> tags = new OsmTagKeysArchiver(OSM_TAGS_TO_ARCHIVE).archiveTags(waySection.tags());
        if (tags.containsKey(TnTagKey.TN_SPEED_UNIT.unwrap())) {
            tags = constructFinalTags(tags);
        }
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }

    private Map<String, String> constructFinalTags(final Map<String, String> tags) {
        final MultipleSourceTagRule minSpeedEnhancer = MultipleSourceTagRule.builder()
                .osmTagName(OsmTagKey.MINSPEED.telenavPrefix()).finalTagName(OsmTagKey.MINSPEED)
                .build();
        final MultipleSourceTagRule minSpeedBackwardEnhancer = MultipleSourceTagRule.builder()
                .osmTagName(OsmTagKey.MINSPEED_BACKWARD.telenavPrefix())
                .finalTagName(OsmTagKey.MINSPEED_BACKWARD)
                .build();
        final MultipleSourceTagRule minSpeedForwardEnhancer = MultipleSourceTagRule.builder()
                .osmTagName(OsmTagKey.MINSPEED_FORWARD.telenavPrefix())
                .finalTagName(OsmTagKey.MINSPEED_FORWARD).build();
        return minSpeedForwardEnhancer
                .andThen(minSpeedBackwardEnhancer)
                .andThen(minSpeedEnhancer)
                .apply(tags);
    }
}