package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.AccessExractor;
import com.telenav.adapter.component.extractors.MotorCarExtractor;
import com.telenav.adapter.component.extractors.MotorVehicleExtractor;
import com.telenav.adapter.component.extractors.RouteExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.TagAdjuster;
import com.telenav.map.entity.WaySection;

import java.util.Map;
import java.util.Optional;


/**
 * if motorcar tag exists and has 'yes' or 'no' values and  if motor_vehicle tag and route tag exists and the value
 * of route tag is "ferry" modifiy the motorcar tag accordingly otherwise do nothing
 * <p>
 * if motorcar tag exists but has other values then 'yes' or 'no'
 * ={@literal >} if current value is 'designated', 'permissive', 'unknown' (only if highway tag in: 'unclassified', 'motorway',
 * 'motorway_link', 'trunk', 'trunk_link', 'primary', 'primary_link', 'secondary', 'secondary_link', 'tertiary',
 * 'tertiary_link', 'residential', 'service') ={@literal >} replace the existing with value 'yes'
 * ={@literal >} otherwise ={@literal >} replace the existing with value 'no'
 * <p>
 * if motorcar tag doesn't exist ={@literal >} apply the same rule as applied when motorcar tag value is 'unknown'
 *
 * @author catalinm
 */
public class MotorCarRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private static final long serialVersionUID = 1L;
    private final MotorCarExtractor motorCarExtractor = new MotorCarExtractor();
    private final RouteExtractor routeExtractor = new RouteExtractor();
    private final MotorVehicleExtractor motorVehicleExtractor = new MotorVehicleExtractor();
    private final AccessExractor accessExtractor = new AccessExractor();


    @Override
    public T apply(final T entity) {
        //if we have rawWay we remove motorcar
        if (!(entity instanceof WaySection)) {
            return entity.withoutTag(OsmTagKey.MOTORCAR.unwrap());
        }
        final Map<String, String> waySectionTags = entity.tags();
        final Optional<String> routeTag = routeExtractor.extract(waySectionTags);
        final Optional<String> motorCarTag = motorCarExtractor.extract(waySectionTags);
        final Optional<String> motorVehicleTag = motorVehicleExtractor.extract(waySectionTags);
        final Optional<String> accessTag = accessExtractor.extract(waySectionTags);

        // applied only on way sections
        if (!motorCarTag.isPresent()) {
            //way sections without motor_car
            if (accessTag.isPresent()) {
                switch (accessTag.get()) {
                    case "no":
                    case "agricultural":
                    case "forestry":
                    case "emergency":
                        return entity.withTag(OsmTagKey.MOTORCAR.unwrap(), "no");
                }
            }
            return updateWaySectionAccordingWithHighWayType(entity);
        } else {
            switch (motorCarTag.get()) {
                case "yes":
                case "no":
                    // check if exists motor_vehicle & route=ferry tag and if so update accordingly or do nothing
                    if (motorVehicleTag.isPresent() && routeTag.isPresent() && routeTag.get().equals("ferry")) {
                        // replace the existing tag value with "motor_vehicle tag"
                        String value = motorVehicleTag.get();
                        //TODO Temporary fix until MA research
                        if (value.equals("permit")) {
                            value = "yes";
                        } else if (value.equals("private")) {
                            value = "no";
                        }
                        waySectionTags.replace(OsmTagKey.MOTORCAR.unwrap(), value);
                        return entity.replaceTags(waySectionTags);
                    } else {
                        // do nothing
                        return entity;
                    }
                case "designated":
                case "permissive":
                    // replace the existing tag value with "yes"
                    waySectionTags.replace(OsmTagKey.MOTORCAR.unwrap(), "yes");
                    return entity.replaceTags(waySectionTags);
                case "unknown":
                    return updateWaySectionAccordingWithHighWayType(entity);
                default:
                    // replace the existing tag value with "no"
                    waySectionTags.replace(OsmTagKey.MOTORCAR.unwrap(), "no");
                    return entity.replaceTags(waySectionTags);
            }
        }
    }

    private T updateWaySectionAccordingWithHighWayType(final T entity) {
        final Map<String, String> waySectionTags = entity.tags();
        final String highWayTagValue = waySectionTags.get(OsmTagKey.HIGHWAY.unwrap());
        final Optional<String> motorCarTag = motorCarExtractor.extract(waySectionTags);
        final Optional<String> routeTag = routeExtractor.extract(waySectionTags);
        final Optional<String> motorVehicleTag = motorVehicleExtractor.extract(waySectionTags);

        // highway tag and route=ferry appear together
        if (highWayTagValue != null) {
            switch (highWayTagValue) {
                case "unclassified":
                case "motorway":
                case "motorway_link":
                case "trunk":
                case "trunk_link":
                case "primary":
                case "primary_link":
                case "secondary":
                case "secondary_link":
                case "tertiary":
                case "tertiary_link":
                case "residential":
                case "service":
                    if (motorCarTag.isPresent()) {
                        // replace the existing tag value with "yes"
                        waySectionTags.replace(OsmTagKey.MOTORCAR.unwrap(), "yes");
                    } else {
                        // add a motorcar tag with "yes" value
                        waySectionTags.put(OsmTagKey.MOTORCAR.unwrap(), "yes");
                    }

                    return entity.replaceTags(waySectionTags);
                default:
                    if (motorCarTag.isPresent()) {
                        // replace the existing tag value with "no"
                        waySectionTags.replace(OsmTagKey.MOTORCAR.unwrap(), "no");
                    } else {
                        // add a motorcar tag with "no" value
                        waySectionTags.put(OsmTagKey.MOTORCAR.unwrap(), "no");
                    }
                    return entity.replaceTags(waySectionTags);
            }
        } else { // ferries(way sections without highway)
            if (motorVehicleTag.isPresent() && routeTag.isPresent() && routeTag.get().equals("ferry")) { // route=ferry
                String value = motorVehicleTag.get();
                //TODO Temporary fix until MA research
                if (value.equals("permit")) {
                    value = "yes";
                } else if (value.equals("private")) {
                    value = "no";
                }
                if (motorCarTag.isPresent()) {
                    // replace the existing tag value with "motor_vehicle tag"
                    waySectionTags.replace(OsmTagKey.MOTORCAR.unwrap(), value);
                } else {
                    waySectionTags.put(OsmTagKey.MOTORCAR.unwrap(), value);
                }
            } else {
                if (motorCarTag.isPresent()) {
                    // replace the existing tag value with "no"
                    waySectionTags.replace(OsmTagKey.MOTORCAR.unwrap(), "no");
                } else {
                    waySectionTags.put(OsmTagKey.MOTORCAR.unwrap(), "no");
                }
            }
            return entity.replaceTags(waySectionTags);
        }
    }
}