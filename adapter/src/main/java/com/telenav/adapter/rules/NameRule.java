package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.LanguageExtractor;
import com.telenav.adapter.component.extractors.TnNameExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.TagAdjuster;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


/**
 * Add 'name:language' tag from 'name' and 'tn__language_code' tags to ways and way sections.
 *
 * @author irinap
 */
public class NameRule<T extends TagAdjuster<T>> extends Enhancer<T> {
    private static final LanguageExtractor LANGUAGE_EXTRACTOR = new LanguageExtractor();
    private static final TnNameExtractor TN_NAME_EXTRACTOR = new TnNameExtractor();

    @Override
    public T apply(T entity) {
        final Map<String, String> tags = entity.tags();
        final Optional<String> tnNameTagOptional = TN_NAME_EXTRACTOR.extract(tags);
        final Optional<String> languageCode = LANGUAGE_EXTRACTOR.extract(tags);

        if (tnNameTagOptional.isPresent() && languageCode.isPresent()) {
            Map<String, String> nameTags = new HashMap<>(2);
            nameTags.put(OsmTagKey.NAME.postFixed(languageCode.get()), tnNameTagOptional.get());
            nameTags.put(OsmTagKey.NAME.unwrap(), tnNameTagOptional.get());

            return entity.withTags(nameTags);
        }
        return entity;
    }
}