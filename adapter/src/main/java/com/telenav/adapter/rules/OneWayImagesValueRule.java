package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.map.entity.WaySection;

import java.util.Map;


/**
 * Class which adapts the one way value from images like so:
 * {@code if 'tn__images_oneway'='1' -> 'tn__images_oneway'='yes'} // forward oneway
 * {@code if 'tn__images_oneway'='-1' -> 'tn__images_oneway'='-1'} // backward oneway
 *
 * @author dianat2
 */
public class OneWayImagesValueRule extends Enhancer<WaySection> {

    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> tags = waySection.tags();
        final String oneWayValue = tags.get(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES));
        if (oneWayValue != null) {
            tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), adaptValue(oneWayValue));
        }
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }

    private static String adaptValue(final String oneWayValue) {
        return DefaultTagValue.FORWARD_ONEWAY.unwrap().equals(oneWayValue) ? "yes" :
                DefaultTagValue.BACKWARD_ONEWAY.unwrap().equals(oneWayValue) ? "-1" : "no";
    }
}