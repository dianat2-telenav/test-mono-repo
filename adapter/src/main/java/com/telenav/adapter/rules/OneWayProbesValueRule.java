package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.map.entity.WaySection;

import java.util.Map;


/**
 * Class which adapts the one way value like so:
 * {@code if 'tn__probes_oneway'='1' -> 'tn__images_oneway'='yes'} // forward oneway
 * {@code if 'tn__probes_oneway'='-1' -> 'tn__images_oneway'='-1'} // backward oneway
 *
 * @author dianat2
 */
public class OneWayProbesValueRule extends Enhancer<WaySection> {

    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> tags = waySection.tags();
        final String oneWayValue = tags.get(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES));
        if (oneWayValue != null) {
            tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), adaptValue(oneWayValue));
        }
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }

    private static String adaptValue(final String oneWayValue) {
        return DefaultTagValue.FORWARD_ONEWAY.unwrap().equals(oneWayValue) ? "yes" :
                DefaultTagValue.BACKWARD_ONEWAY.unwrap().equals(oneWayValue) ? "-1" : "no";
    }
}