package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.map.entity.WaySection;

import java.util.Map;


/**
 * Class which adapts the one way value like so: {@code if 'oneway'='yes' -> 'oneway'='1'}
 *
 * @author roxanal
 */
public class OneWayValueRule extends Enhancer<WaySection> {

    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> tags = waySection.tags();
        final String oneWayValue = tags.get(OsmTagKey.ONEWAY.unwrap());
        if (oneWayValue != null) {
            tags.put(OsmTagKey.ONEWAY.unwrap(), adaptValue(oneWayValue));
        }
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }

    private static String adaptValue(final String oneWayValue) {
        return oneWayValue.equals("yes") ? DefaultTagValue.FORWARD_ONEWAY.unwrap() : oneWayValue;
    }
}