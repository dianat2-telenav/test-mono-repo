package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.RailwayExtractor;
import com.telenav.adapter.component.extractors.TypeExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.map.entity.TagAdjuster;

import static com.telenav.datapipelinecore.unidb.RoadCategoryTagKey.*;
import static com.telenav.datapipelinecore.unidb.UnidbTagValue.*;

import java.util.Map;
import java.util.Optional;

/**
 * If the way section has type=carto_line and railway=* tags,
 * then remove 'rt', 'rst', 'sc', 'functional_class' tags.
 *
 * @author irinap
 */
public class RailwayCleanupRule<T extends TagAdjuster<T>> extends Enhancer<T> {
    private static final TypeExtractor typeExtractor = new TypeExtractor();
    private static final RailwayExtractor railwayExtractor = new RailwayExtractor();

    @Override
    public T apply(T entity) {
        final Map<String, String> tags = entity.tags();
        T adaptedEntity = entity;

        Optional<String> typeTagOptional = typeExtractor.extract(tags);

        if (typeTagOptional.isPresent() && typeTagOptional.get().equals(CARTO_LINE.unwrap())
                && (railwayExtractor.extract(tags)).isPresent()) {
            adaptedEntity = adaptedEntity
                    .withoutTag(ROAD_TYPE.unwrap())
                    .withoutTag(ROAD_SUBTYPE.unwrap())
                    .withoutTag(SPEED_CATEGORY.unwrap())
                    .withoutTag(FUNCTIONAL_CLASS.unwrap());
        }
        return adaptedEntity;
    }
}
