package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.RailwayExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.WaySection;

/**
 * The rule removes "railway" tag from all way-sections
 * 
 * @author andriyz
 *
 */
public class RailwayWaySectionCleanupRule extends Enhancer<WaySection> {

    private static final long serialVersionUID = 1L;
    private static final RailwayExtractor railwayExtractor = new RailwayExtractor();

    @Override
    public WaySection apply(WaySection waySection) {
        if (railwayExtractor.extract(waySection.tags()).isPresent()) {
            return waySection.withoutTag(UnidbTagKey.RAILWAY.unwrap());
        }
        return waySection;
    }
}
