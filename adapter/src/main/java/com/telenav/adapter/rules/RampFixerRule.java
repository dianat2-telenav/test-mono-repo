package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.map.entity.WaySection;

public class RampFixerRule extends Enhancer<WaySection> {

    public static final String RAMP_TAG_KEY = "ramp";

    /**
     * According to the specification @link http://mapilot.telenav.com/docs/unidb_spec/spec/road_attributes/ramp.html
     * tag 'ramp' can have values 'Y' or 'N' only.
     *
     * @param waySection initial way section.
     * @return way section with fixed 'ramp' tag value.
     */
    @Override
    public WaySection apply(WaySection waySection) {

        if (waySection.hasKey(RAMP_TAG_KEY)
                && !waySection.hasTag(RAMP_TAG_KEY, "N")
                && !waySection.hasTag(RAMP_TAG_KEY, "Y")) {
            if ("no".equals(waySection.tags().get(RAMP_TAG_KEY).toLowerCase())) {
                return waySection.withTag(RAMP_TAG_KEY, "N");
            } else {
                return waySection.withTag(RAMP_TAG_KEY, "Y");
            }
        }
        return waySection;
    }
}
