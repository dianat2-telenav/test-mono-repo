package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.LanguageExtractor;
import com.telenav.adapter.component.extractors.RefExtractor;
import com.telenav.adapter.utils.TagConstruction;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Map;
import java.util.Optional;


/**
 * Update the 'tn__ref(_index)' tag with the key 'ref(_index):eng'
 *
 * @author roxanal
 */
public class RefRule extends Enhancer<WaySection> {

    private final RefExtractor refExtractor;
    private final LanguageExtractor languageExtractor;


    public RefRule(final RefExtractor refExtractor, final LanguageExtractor languageExtractor) {
        this.refExtractor = refExtractor;
        this.languageExtractor = languageExtractor;
    }


    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> waySectionTags = waySection.tags();
        final Optional<Map<String, String>> waySectionRefTags = refExtractor.extract(waySectionTags);
        final Optional<String> languageCode = languageExtractor.extract(waySectionTags);

        //waySectionTags.remove(OsmTagKey.REF.unwrap());
        if (waySectionRefTags.isPresent() && languageCode.isPresent()) {
            waySectionRefTags.get().forEach((key, value) ->
                    waySectionTags.put(TagConstruction.suffixWithLanguage(key.substring(4), languageCode.get()), value));
        }
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), waySectionTags)
                .withSequenceNumber(waySection.sequenceNumber());
    }
}