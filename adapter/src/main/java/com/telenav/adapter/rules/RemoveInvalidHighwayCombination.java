package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.TagAdjuster;
import com.telenav.map.entity.WaySection;

public class RemoveInvalidHighwayCombination extends Enhancer<WaySection> {
    @Override
    public WaySection apply(WaySection entity) {
        if (entity.hasTag(OsmTagKey.ROUTE.unwrap(), "ferry") && entity.hasKey(OsmTagKey.HIGHWAY.unwrap())) {
            return entity.withoutTag(OsmTagKey.HIGHWAY.unwrap());
        }
        return entity;
    }
}
