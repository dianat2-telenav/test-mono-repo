package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.*;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.TagAdjuster;

import java.util.Map;
import java.util.Optional;


/**
 * Convert "water"{@code ->}"river" to "waterway"{@code ->}"river" if "type"{@code ->}"carto_line"
 *
 * @author volodymyrl
 */
public class RiverFixerRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private final static TypeExtractor typeExtractor = new TypeExtractor();
    private final static WaterExtractor waterExtractor = new WaterExtractor();

    public static final String RIVER = "river";
    public static final String CARTO_LINE = "carto_line";

    public RiverFixerRule() {
    }

    @Override
    public T apply(final T entity) {
        final Map<String, String> entityTags = entity.tags();
        final Optional<String> entityWaterTagValue = waterExtractor.extract(entityTags);
        final Optional<String> type = typeExtractor.extract(entityTags);
        if (type.map(value -> value.equals(CARTO_LINE)).orElse(false) &&
                entityWaterTagValue.map(value -> value.equals(RIVER)).orElse(false)) {
            entityTags.put(UnidbTagKey.WATERWAY.unwrap(), RIVER);
        }
        entityTags.remove(OsmTagKey.WATER.unwrap());

        return entity.replaceTags(entityTags);
    }
}