package com.telenav.adapter.rules;

import com.telenav.adapter.bean.RoadFunctionalClass;
import com.telenav.adapter.component.extractors.RoadCategoryExtractor;
import com.telenav.adapter.component.extractors.RoadFunctionalClassExtractor;
import com.telenav.adapter.component.fc.*;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.WaySection;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

import static com.telenav.adapter.component.fc.FcInfo.*;


/**
 * Update the "functional_class" value.
 *
 * @author roxanal
 */
@Slf4j
public class RoadCategoryRule extends Enhancer<WaySection> {

    private final RoadFunctionalClassExtractor roadFCExtractor;
    private final List<FcApproach> fcApproaches;

    public RoadCategoryRule(final RoadFunctionalClassExtractor roadFCExtractor,
                            final Map<Long, List<String>> wayIdsToIsoFc) {
        this.roadFCExtractor = roadFCExtractor;
        this.fcApproaches = fcApproaches(wayIdsToIsoFc);
    }

    @Override
    public WaySection apply(WaySection waySection) {

        final boolean isNavigable = FcService.isNavigable(waySection);
        final boolean isNonLink = FcService.nonLink(waySection);

        final Map<String, String> tags = waySection.tags();

        if (isNavigable && isNonLink) // links will be treated in a separate service
        {
            final String iso = TagService.iso(tags);

            /*
                If the iso is part of the isos for which we have designed the fc algorithm,
                then we apply the designed rules for the algorithm,
             */
            if (ROUTES_RELATIONS_ISO_WHITELIST.contains(iso)) {

                final List<String> resultedFcs = new ArrayList<>();

                for (final FcApproach approach : fcApproaches) {

                    if (approach.isValidForCountry(iso)) {

                        final String newFc = approach.fc(waySection);

                        if (newFc != null) {

                            resultedFcs.add(newFc);
                        }
                    }
                }

                if (!resultedFcs.isEmpty()) {
                    String min = resultedFcs.get(0);
                    for (int i = 1; i < resultedFcs.size(); i++) {
                        if (min.compareTo(resultedFcs.get(i)) > 0) {
                            min = resultedFcs.get(i);
                        }
                    }
                    tags.put(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), min);
                }
            } else {
                // Else we apply a naive mapping based on the highway type.
                final Optional<RoadFunctionalClass> roadFC = roadFCExtractor.extract(waySection.tags());
                if (roadFC.isPresent()) {
                    tags.put(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(),
                            String.valueOf(RoadFunctionalClass.identifierFor(roadFC.get())));
                }
            }
        }

        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }

    private List<FcApproach> fcApproaches(final Map<Long, List<String>> wayIdsToIsoFc) {
        final List<FcApproach> fcApproaches = new LinkedList<>();
        fcApproaches.add(0, new WhiteListApproach(WHITELIST_BASED_APPROACH));
        fcApproaches.add(1, new RelationNetworkBased(RELATIONS_BASED_APPROACH, wayIdsToIsoFc));
        fcApproaches.add(2, new RefBased(REF_BASED_APPROACH));
        return fcApproaches;
    }
}
