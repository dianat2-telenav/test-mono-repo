package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.LanguageExtractor;
import com.telenav.adapter.component.extractors.RoadShieldExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toMap;


/**
 * Adds unidb specific 'ref:language:si' tag based on the existing 'tn__shield_icon' and 'tn__language_code' tags.
 *
 * @author martap
 */
public class RoadShieldRule extends Enhancer<WaySection> {

    private final RoadShieldExtractor roadShieldExtractor;
    private final LanguageExtractor languageExtractor;


    public RoadShieldRule(final RoadShieldExtractor roadShieldExtractor, final LanguageExtractor languageExtractor) {
        this.roadShieldExtractor = roadShieldExtractor;
        this.languageExtractor = languageExtractor;
    }


    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> waySectionTags = waySection.tags();
        final Optional<Map<String, String>> waySectionRSTagValue = roadShieldExtractor.extract(waySectionTags);
        final Optional<String> languageCode = languageExtractor.extract(waySectionTags);
        if (waySectionRSTagValue.isPresent() && languageCode.isPresent()) {
            final Map<String, String> waySectionRSTagValueTN = waySectionRSTagValue.get();
            final Map<String, String> adaptedRSTags = waySectionRSTagValueTN.entrySet()
                    .stream()
                    .collect(toMap(e -> buildIndexedRSTag(e.getKey(), languageCode.get()), Map.Entry::getValue));
            return waySection.withTags(adaptedRSTags);
        }
        return waySection;
    }

    /**
     * Converts the given TN road shield tag to a unidb specific tag. For example:
     * </p>
     * tn__shield_icon_2 with languageCode=eng became ref_2:eng:si
     *
     * @param indexedRoadShieldTnTagKey the internal TN tag for road shields to convert
     * @param languageCode the language code needed to construct the unidb specific tag
     * @return the unidb specific tag key name
     */
    private String buildIndexedRSTag(final String indexedRoadShieldTnTagKey, final String languageCode) {
        final String rsTagSuffix ="si";
        final Integer index = indexFromTNShieldIconTagKey(indexedRoadShieldTnTagKey);
        return OsmTagKey.REF.postFixed(index.shortValue(), languageCode, rsTagSuffix);
    }

    private Integer indexFromTNShieldIconTagKey(final String tagKey) {
        if (!tagKey.equals(TnTagKey.TN_SHIELD_ICON.unwrap())) {
            return Integer.valueOf(tagKey.substring(tagKey.lastIndexOf("_") + 1));
        }
        return 0;
    }
}