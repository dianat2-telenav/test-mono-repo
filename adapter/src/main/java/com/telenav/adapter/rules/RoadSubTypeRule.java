package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.WaySection;
import com.telenav.adapter.bean.RoadSubType;
import com.telenav.adapter.component.extractors.RoadSubTypeExtractor;

import java.util.Optional;

/**
 * Add road sub type tag tn__road_subtype. The rules are specified in {@link RoadSubTypeExtractor}
 *
 * @author petrum
 */
public class RoadSubTypeRule extends Enhancer<WaySection> {

    private final RoadSubTypeExtractor roadSubTypeExtractor;

    public RoadSubTypeRule(final RoadSubTypeExtractor roadSubTypeExtractor) {
        this.roadSubTypeExtractor = roadSubTypeExtractor;
    }

    @Override
    public WaySection apply(final WaySection waySection) {
        final Optional<RoadSubType> roadSubType = roadSubTypeExtractor.extract(waySection.tags());
        if (roadSubType.isPresent()) {
            return waySection.withTag(RoadCategoryTagKey.ROAD_SUBTYPE.unwrap(),
                    String.valueOf(RoadSubType.identifierFor(roadSubType.get())));
        }
        return waySection;
    }
}
