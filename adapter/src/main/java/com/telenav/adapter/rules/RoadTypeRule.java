package com.telenav.adapter.rules;

import com.telenav.adapter.bean.RoadType;
import com.telenav.adapter.component.extractors.RoadTypeExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Optional;

/**
 * Add road type tag {tn__road_type. The rules are specified in {@link RoadTypeExtractor}
 *
 * @author petrum
 */
public class RoadTypeRule extends Enhancer<WaySection> {

    private final RoadTypeExtractor roadTypeExtractor;

    public RoadTypeRule(final RoadTypeExtractor extractor) {
        this.roadTypeExtractor = extractor;
    }

    @Override
    public WaySection apply(final WaySection waySection) {
        final Optional<RoadType> roadType = roadTypeExtractor.extract(waySection.tags());
        if (roadType.isPresent()) {
            return waySection.withTag(RoadCategoryTagKey.ROAD_TYPE.unwrap(),
                    String.valueOf(RoadType.identifierFor(roadType.get())));
        }
        return waySection;
    }
}

