package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.RoadWidthExtractor;
import com.telenav.adapter.utils.OsmTagKeysArchiver;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.map.entity.WaySection;

import java.util.Map;
import java.util.Optional;


/**
 * Update the 'width' tag. The value is taken from one of the following tags: 'tn__iamges_width', 'tn__osm_width'.
 *
 * @author roxanal
 */
public class RoadWidthRule extends Enhancer<WaySection> {

    private final RoadWidthExtractor extractor;


    public RoadWidthRule(final RoadWidthExtractor extractor) {
        this.extractor = extractor;
    }


    @Override
    public WaySection apply(final WaySection waySection) {
        Map<String, String> tags = waySection.tags();
        final Optional<String> roadWidthValue = extractor.extract(tags);
        if(roadWidthValue.isPresent()) {
            tags.put(OsmTagKey.WIDTH.telenavPrefix(), roadWidthValue.get());
        }

        tags = new OsmTagKeysArchiver(OsmTagKey.WIDTH).archiveTags(tags);
        tags = MultipleSourceTagRule.builder()
                .imageTagName(OsmTagKey.WIDTH.withSource(TnSourceValue.IMAGES))
                .osmTagName(OsmTagKey.WIDTH.telenavPrefix())
                .finalTagName(OsmTagKey.WIDTH)
                .build()
                .apply(tags);
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }
}