package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.RoundaboutExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Map;
import java.util.Optional;


/**
 * If the way section has the 'junction'='roundabout' tag then add a new one: 'oneway'='yes'
 *
 * @author roxanal
 */
public class RoundaboutRule extends Enhancer<WaySection> {

    private final RoundaboutExtractor roundaboutExtractor;


    public RoundaboutRule(final RoundaboutExtractor roundaboutExtractor) {
        this.roundaboutExtractor = roundaboutExtractor;
    }


    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> waySectionTags = waySection.tags();
        final Optional<String> isRoundabout = roundaboutExtractor.extract(waySectionTags);
        if (isRoundabout.isPresent()) {
            waySectionTags.put(OsmTagKey.ONEWAY.unwrap(), "yes");
        }
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), waySectionTags)
                .withSequenceNumber(waySection.sequenceNumber());
    }
}