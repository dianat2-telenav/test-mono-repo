package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.WaySection;
import com.telenav.adapter.bean.SpeedCategory;
import com.telenav.adapter.component.extractors.HighwayBasedSpeedCategoryExtractor;
import com.telenav.adapter.component.extractors.MaxSpeedBasedSpeedCategoryExtractor;

import java.util.Collections;
import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_SPEED_CATEGORY;


/**
 * Add speed category tag tn__speed_category. The rules are specified in
 * {@link HighwayBasedSpeedCategoryExtractor}.
 *
 * @author petrum
 */
public class SpeedCategoryRule extends Enhancer<WaySection> {

    private final HighwayBasedSpeedCategoryExtractor highwayBasedSpeedCategoryExtractor;
    private final MaxSpeedBasedSpeedCategoryExtractor maxSpeedBasedSpeedCategoryExtractor;

    public SpeedCategoryRule(final HighwayBasedSpeedCategoryExtractor highwayBasedSpeedCategoryExtractor,
                          final MaxSpeedBasedSpeedCategoryExtractor maxSpeedBasedSpeedCategoryExtractor) {
        super(Collections.singleton(MaxSpeedRtRule.class));
        this.highwayBasedSpeedCategoryExtractor = highwayBasedSpeedCategoryExtractor;
        this.maxSpeedBasedSpeedCategoryExtractor = maxSpeedBasedSpeedCategoryExtractor;
    }

    @Override
    public WaySection apply(final WaySection waySection) {
        Optional<SpeedCategory> speedCategory = maxSpeedBasedSpeedCategoryExtractor.extract(waySection.tags());
        if (!speedCategory.isPresent()) {
            speedCategory = highwayBasedSpeedCategoryExtractor.extract(waySection.tags());
        }
        if (speedCategory.isPresent()) {
            return waySection.withTag(RoadCategoryTagKey.SPEED_CATEGORY.unwrap(),
                    String.valueOf(SpeedCategory.identifierFor(speedCategory.get())));
        }
        return waySection;
    }
}
