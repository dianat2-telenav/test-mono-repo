package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.WaySection;


/**
 * Replace the 'tn__speed_unit' tag with the key 'speed_unit'.
 *
 * @author ioanao
 */
public class SpeedUnitRule extends Enhancer<WaySection> {

    @Override
    public WaySection apply(final WaySection waySection) {
        final String speedUnit = waySection.tags().get(TnTagKey.TN_SPEED_UNIT.unwrap());
        return speedUnit != null ? waySection.withTag(UnidbTagKey.SPEED_UNIT.unwrap(), speedUnit) : waySection;
    }
}