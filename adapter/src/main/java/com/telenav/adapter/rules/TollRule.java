package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.TollExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.TagAdjuster;

import java.util.Map;
import java.util.Optional;


/**
 * if toll cost tag exists and its value is 'no', remove it
 *
 * @author catalinm
 */
public class TollRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private final TollExtractor tollExtractor;

    public TollRule(final TollExtractor tollExtractor) {
        this.tollExtractor = tollExtractor;
    }

    @Override
    public T apply(final T entity) {
        final Map<String, String> entityTags = entity.tags();
        final Optional<String> tollValue = tollExtractor.extract(entityTags);
        if (tollValue.isPresent()) {
            entityTags.replace(OsmTagKey.TOLL.unwrap(), "yes");
            return entity.replaceTags(entityTags);
        }
        return entity;
    }
}