package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;

/**
 * @author opashkevych
 */
public class TunnelFixerRule extends Enhancer<WaySection> {

    /**
     * According to the specification @link http://mapilot.telenav.com/docs/unidb_spec/spec/road_attributes/tunnel.html
     * tag 'tunnel' can have values 'yes' or <b>null</b> only.
     *
     * @param waySection initial way section.
     * @return way section with fixed 'tunnel' tag value.
     */
    @Override
    public WaySection apply(final WaySection waySection) {

        if (waySection.hasKey(OsmTagKey.TUNNEL.unwrap())) {
            String tunnel = waySection.tags().get(OsmTagKey.TUNNEL.unwrap()).toLowerCase();
            if (tunnel.startsWith("no") || tunnel.equals("-1")) {
                return waySection.withoutTag(OsmTagKey.TUNNEL.unwrap());
            } else {
                return waySection.withTag(OsmTagKey.TUNNEL.unwrap(), "yes");
            }
        }

        return waySection;
    }
}
