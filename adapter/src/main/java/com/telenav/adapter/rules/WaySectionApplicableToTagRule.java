package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.AccessExtractor;
import com.telenav.adapter.component.extractors.MotorCarExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.datapipelinecore.util.OsmToUnidbVehicleTypeConverter;
import com.telenav.map.entity.WaySection;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.telenav.datapipelinecore.unidb.ApplicableToTagValue.*;

public class WaySectionApplicableToTagRule extends Enhancer<WaySection> {
    private final AccessExtractor accessExtractor;
    private final MotorCarExtractor motorCarExtractor;
    private static final List<String> ACCESS_VALUE = Arrays.asList("yes", "designated", "permissive");
    private static final List<String> ADDITIONAL_TAGS = Arrays.asList("tourist_bus", "minibus", "coach", "share_taxi",
            "car_sharing", "goods", "hgv_agriculated", "hgv");
    private Set<String> applicableTo = new LinkedHashSet<>();
    private List<String> exceptUnidbValues = new ArrayList<>();
    private Map<String, String> tags;
    private boolean isAccessExist;
    private boolean isAccessTagYes;
    private boolean isMotorcarNo;

    public WaySectionApplicableToTagRule(AccessExtractor accessExtractor, MotorCarExtractor motorCarExtractor) {
        this.accessExtractor = accessExtractor;
        this.motorCarExtractor = motorCarExtractor;
    }

    @Override
    public WaySection apply(WaySection waySection) {
        tags = waySection.tags();
        extractAccessTags();
        if (!applicableTo.isEmpty()) {
            waySection = waySection.withTag(UnidbTagKey.APPLICABLE_TO.unwrap(), String.join(";", applicableTo));
        } else {
            if (isAccessExist && !isAccessTagYes) {
                waySection = waySection.withTag(UnidbTagKey.APPLICABLE_TO.unwrap(), String.join(";", applicableToValues));
            }
        }
        return waySection;
    }

    private void extractAccessTags() {
        final Optional<String> accessTagValue = accessExtractor.extract(tags);
        final Optional<String> mcTagValue = motorCarExtractor.extract(tags);
        if (accessTagValue.isPresent()) {
            isAccessExist = true;
            if (ACCESS_VALUE.contains(accessTagValue.get())) {
                isAccessTagYes = true;
                applicableTo.add(ACCESS_THROUGH_TRAFFIC.unwrap());
            }
        }
        if (mcTagValue.isPresent()) {
            if (mcTagValue.get().equals("yes")) {
                applicableTo.add(MOTORCAR.unwrap());
            } else {
                isMotorcarNo = true;
                addApplicableToVehicle(Collections.singletonList(MOTORCAR.unwrap()));
            }
        }
        extractVehicleTag();
    }

    protected void extractVehicleTag() {
        Optional<String> except = Optional.ofNullable(tags.get(OsmTagKey.EXCEPT.unwrap()));
        if (except.isPresent()) {  // all but "except=*" vehicles
            Stream.of(except.get().split(";"))
                    .forEach(vehicle -> exceptUnidbValues.addAll(OsmToUnidbVehicleTypeConverter.convert(vehicle)));
            applicableTo.addAll(applicableToValues
                    .stream()
                    .filter(vehicle -> !exceptUnidbValues.contains(vehicle))
                    .collect(Collectors.toList()));
        } else { // only specified vehicles
            for (String key : tags.keySet()) {
                if (key.startsWith("restriction:")) {
                    String[] splitValues = key.split(":");
                    if (splitValues.length > 1) { // restriction:vehicle_type or restriction:vehicle_type:conditional
                        applicableTo.addAll(OsmToUnidbVehicleTypeConverter.convert(splitValues[1]));
                    }
                }
                if (!key.equals(MOTORCAR.unwrap())) {
                    addVehicleByTag(key);
                }
            }
        }
    }

    private void addApplicableToVehicle(List<String> keyTagList) {
        applicableTo.addAll(applicableToValues
                .stream()
                .distinct()
                .filter(o -> !keyTagList.contains(o))
                .collect(Collectors.toList()));
    }

    private void addVehicleByTag(String vehicle) {
        List<String> keyTagList = OsmToUnidbVehicleTypeConverter.convert(vehicle);
        if ((applicableToValues.contains(vehicle)) || (ADDITIONAL_TAGS.contains(vehicle))) {
            Optional<String> value = Optional.ofNullable(tags.get(vehicle));
            if (value.filter(ACCESS_VALUE::contains).isPresent()) {
                if (isAccessTagYes) {
                    addApplicableToVehicle(keyTagList);
                } else {
                    if (isMotorcarNo) {
                        applicableTo.removeAll(keyTagList);
                    } else {
                        applicableTo.addAll(keyTagList);
                    }
                }
            }
            if (value.filter(v -> v.equals("no")).isPresent()) {
                if (isMotorcarNo) {
                    applicableTo.removeAll(keyTagList);
                } else {
                    addApplicableToVehicle(keyTagList);
                }
            }
        }
    }
}
