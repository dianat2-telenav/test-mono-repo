package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.AccessExtractor;
import com.telenav.adapter.component.mapper.OpenStreetMapToUniDbAccessMapper;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;
import com.telenav.map.entity.TagAdjuster;

import java.util.*;

/**
 * Access tag rule for unifying various values according to the UnitDb specs
 *
 * @param <T> A way or waySection to be processed.
 * @author Andrei Puf
 */
public class WaysAccessRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private final AccessExtractor accessExtractor;
    private final OpenStreetMapToUniDbAccessMapper openStreetMapToUniDbAccessMapper;

    /**
     * Initializes the WaysAccessRule using an accessExtractor reference.
     * @param accessExtractor extractor for access tag value
     * @param openStreetMapToUniDbAccessMapper mapper
     */
    public WaysAccessRule(AccessExtractor accessExtractor,
                          OpenStreetMapToUniDbAccessMapper openStreetMapToUniDbAccessMapper) {
        this.accessExtractor = accessExtractor;
        this.openStreetMapToUniDbAccessMapper = openStreetMapToUniDbAccessMapper;
    }

    /**
     * Apply the access rule on a way reference.
     *
     * @param entity way representation (way/waySection) as it comes from OSM.
     * @return An updated way representation according to UniDb specs.
     */
    @Override
    public T apply(T entity) {
        final Map<String, String> tags = entity.tags();
        final Optional<String> osmAccessValue = accessExtractor.extract(tags);
        final Optional<UnidbTagValue> uniDbAccessValue;

        if (!osmAccessValue.isPresent()) { return entity; }

        uniDbAccessValue = openStreetMapToUniDbAccessMapper.map(osmAccessValue.get());

        if (uniDbAccessValue.isPresent()) {
            return entity.withTag(UnidbTagKey.ACCESS.unwrap(), uniDbAccessValue.get().unwrap());
        }

        return entity.withoutTag(UnidbTagKey.ACCESS.unwrap());
    }
}
