package com.telenav.adapter.rules.node;

import com.telenav.adapter.component.extractors.AccessExtractor;
import com.telenav.adapter.component.extractors.MotorCarExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.datapipelinecore.util.OsmToUnidbVehicleTypeConverter;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.telenav.datapipelinecore.unidb.ApplicableToTagValue.*;

/**
 * If the Node has the tags 'access=*','restriction=*' or any vehicles type like 'motorcar=yes' tags
 * set 'applicable_to' tag.
 *
 * @author liviuc
 */
public class ApplicableToTagRuleDatasets extends Enhancer<Map<String, String>> {

    private static final Set<String> ACCESS_VALUE = new HashSet<>(Arrays.asList("yes", "designated", "permissive"));
    private static final Set<String> ADDITIONAL_TAGS = new HashSet<>(Arrays.asList("tourist_bus", "minibus", "coach", "share_taxi",
            "car_sharing", "goods", "hgv_agriculated", "hgv"));
    private final AccessExtractor accessExtractor;
    private final MotorCarExtractor motorCarExtractor;
    private Set<String> applicableTo = new LinkedHashSet<>();
    private Set<String> exceptUnidbValues = new HashSet<>();

    private boolean isAccessTagYes;
    private boolean isMotorcarNo;

    public ApplicableToTagRuleDatasets() {
        this.accessExtractor = new AccessExtractor();
        this.motorCarExtractor = new MotorCarExtractor();
    }

    @Override
    public Map<String, String> apply(final Map<String, String> tags) {
        extractAccessTags(tags);
        if (!applicableTo.isEmpty()) {
            tags.put(UnidbTagKey.APPLICABLE_TO.unwrap(), String.join(";", applicableTo));
        }
        applicableTo = new HashSet<>();
        return tags;
    }

    private void extractAccessTags(final Map<String, String> tags) {
        final Optional<String> accessTagValue = accessExtractor.extract(tags);
        final Optional<String> mcTagValue = motorCarExtractor.extract(tags);
        if (accessTagValue.isPresent()) {
            if (ACCESS_VALUE.contains(accessTagValue.get())) {
                isAccessTagYes = true;
                applicableTo.add(ACCESS_THROUGH_TRAFFIC.unwrap());
            }
        }
        if (mcTagValue.isPresent()) {
            if (mcTagValue.get().equals("yes")) {
                applicableTo.add(MOTORCAR.unwrap());
            } else {
                isMotorcarNo = true;
                applicableTo.addAll(getApplicableToValuesExcept(Collections.singletonList(MOTORCAR.unwrap())));
            }
        }
        extractVehicleTag(tags);
    }

    protected void extractVehicleTag(final Map<String, String> tags) {
        Optional<String> except = Optional.ofNullable(tags.get(OsmTagKey.EXCEPT.unwrap()));
        if (except.isPresent()) {  // all but "except=*" vehicles
            Stream.of(except.get().split(";"))
                    .forEach(vehicle -> exceptUnidbValues.addAll(OsmToUnidbVehicleTypeConverter.convert(vehicle)));
            applicableTo.addAll(applicableToValues
                    .stream()
                    .filter(vehicle -> !exceptUnidbValues.contains(vehicle))
                    .collect(Collectors.toList()));
        } else { // only specified vehicles
            for (String key : tags.keySet()) {
                if (key.startsWith("restriction:")) {
                    String[] splitValues = key.split(":");
                    if (splitValues.length > 1) { // restriction:vehicle_type or restriction:vehicle_type:conditional
                        applicableTo.addAll(OsmToUnidbVehicleTypeConverter.convert(splitValues[1]));
                    }
                }
                if (!key.equals(MOTORCAR.unwrap())) {
                    addVehicleByTag(key, tags);
                }
            }
        }
    }

    private List<String> getApplicableToValuesExcept(final List<String> exceptList) {
        return applicableToValues
                .stream()
                .distinct()
                .filter(o -> !exceptList.contains(o))
                .collect(Collectors.toList());
    }

    private void addVehicleByTag(final String vehicle,
                                 final Map<String, String> tags) {
        List<String> keyTagList = OsmToUnidbVehicleTypeConverter.convert(vehicle);
        if ((applicableToValues.contains(vehicle)) || (ADDITIONAL_TAGS.contains(vehicle))) {
            final Optional<String> value = Optional.ofNullable(tags.get(vehicle));
            if (value.filter(v -> v.equals("no")).isPresent()) {
                if (isAccessTagYes) {
                    applicableTo.addAll(getApplicableToValuesExcept(keyTagList));
                } else {
                    if (isMotorcarNo) {
                        applicableTo.removeAll(keyTagList);
                    } else {
                        applicableTo.addAll(keyTagList);
                    }
                }
            }
            if (value.filter(ACCESS_VALUE::contains).isPresent()) {
                if (isMotorcarNo) {
                    applicableTo.removeAll(keyTagList);
                } else {
                    applicableTo.addAll(getApplicableToValuesExcept(keyTagList));
                }
            }
        }
    }
}
