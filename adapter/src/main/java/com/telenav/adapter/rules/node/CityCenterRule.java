package com.telenav.adapter.rules.node;

import com.telenav.adapter.component.CityService;
import com.telenav.adapter.component.extractors.AltNameExtractor;
import com.telenav.adapter.component.extractors.Extractor;
import com.telenav.adapter.component.extractors.cityCenterExtractor.CityCenterExtractor;
import com.telenav.adapter.component.fc.TagService;
import com.telenav.adapter.component.repository.ResourceRepository;
import com.telenav.adapter.entity.dto.MemberRelation;
import com.telenav.adapter.utils.IsoUtil;
import com.telenav.adapter.utils.LangCodeUtil;
import com.telenav.datapipelinecore.bean.Member;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.telenav.adapter.utils.OsmInfo.*;
import static com.telenav.adapter.utils.OsmKeys.*;
import static com.telenav.adapter.utils.UniDbKeys.*;

@Slf4j
public class CityCenterRule extends Enhancer<Map<String, String>> {

    private static final String ISO = "iso";
    private static final String IS_IN_COUNTRY_CODE = "is_in:country_code";
    private static final String IS_IN_COUNTRY = "is_in:country";
    /*
        Data.
     */
    private static final Map<String, String> PLACE_TAG_TO_CAT_ID_VALUE = Stream.of(new Object[][]{
            {PLACE_TAG_NEIGHBORHOOD_VALUE, "9709"},
            {PLACE_TAG_CITY_VALUE_IN_UNIDB, "4444"},
            {PLACE_TAG_HAMLET_VALUE_IN_UNIDB, "9998"},
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]));
    private static Map<String, List<String>> LANG_CODE_MAP = ResourceRepository.getLangCodesMap();

    private final List<MemberRelation> memberRelations;
    private final long nodeId;
    private final AltNameExtractor altNameExtractor;
    private final CityCenterExtractor placeExtractor;
    private final CityCenterExtractor addrStreetExtractor;
    private final double latitude;
    private final double longitude;

    /*
        Admin centre relevant value extractors.
     */
    public CityCenterRule(final long nodeId,
                          final double latitude,
                          final double longitude,
                          final List<MemberRelation> memberRelations) {
        this.nodeId = nodeId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.placeExtractor = new CityCenterExtractor(PLACE_TAG_KEY_IN_OSM);
        this.addrStreetExtractor = new CityCenterExtractor(ADDRSTREET_TAG_KEY_IN_OSM);
        this.memberRelations = memberRelations;
        this.altNameExtractor = new AltNameExtractor();
    }

    @Override
    public final Map<String, String> apply(final Map<String, String> tags) {
        if (memberRelations == null || memberRelations.isEmpty()) {
            return tags;
        }

        final MemberRelation adminCenterRelation = getAdminCenterRelation(nodeId, memberRelations);

        if (adminCenterRelation != null) {

            final Long adminCenterRelationId = adminCenterRelation.getId();
            final Map<String, String> originalOsmRelationTags = adminCenterRelation.getRelationTags();

            log.info("Enhancing node: " + nodeId + " as admin_centre for relation: " + adminCenterRelationId);

            // Enhance city center.
            enhance(tags, (tagsParam) -> enhance(tagsParam, CITY_CENTER_TAG_KEY_IN_UNIDB, CITY_CENTER_TAG_VALUE_IN_UNIDB));

            // Enhance place.
            enhanceWithExtractor(placeExtractor, tags, CityCenterRule.EnhancementCodeWithValue.PLACE_ENHANCER_FUNC);

            // Enhance cat id.
            enhance(tags, CityCenterRule.EnhancementCode.CAT_ID_ENHANCEMENT_FUNC);

            // Population: as in OSM - do nothing.

            // Enhance addr. street.
            enhanceWithExtractor(addrStreetExtractor, tags, CityCenterRule.EnhancementCodeWithValue.ADDR_STREET_ENHANCEMENT_FUNC);

            // Enhance admin level related data.
            enhanceAdminLevels(originalOsmRelationTags, tags);

            // Postal code as in OSM - do nothing.

            // Enhance ISO
            enhanceIso(tags, latitude, longitude, originalOsmRelationTags, originalOsmRelationTags);

            enhanceAdminLevel(tags, originalOsmRelationTags.get("original_osm_admin_level"));

            // Enhance name, alt_name
            enhanceName(tags);

            enhanceCapital(tags);

            enhancedCapitalOrder(tags);
        }

        return tags;
    }

    private void enhanceCapital(final Map<String, String> tags) {
        final String uniDbAdminLevel = tags.get(ADMIN_LEVEL_TAG_KEY_IN_UNIDB);
        /*
            In UniDb 'capital':'yes' is valid ONLY for country capitals,
            while in OSM for other administrative areas - i.e. keeping
            the 'capital' tag with another value than 'yes' for admin levels different than "L1" is wrong.
            Why L1? because in the "administrative-level-mapping.csv" all the L1_Admin_Types correspond to countries.
            If this changes, then the logic would need to get updated..
        */
        if (ADMIN_LEVEL_1.equals(uniDbAdminLevel)) {
            tags.put(CAPITAL_TAG_KEY_IN_UNIDB_AND_OSM, "yes");
        }
    }

    private void enhanceAdminLevel(final Map<String, String> tags,
                                   final String osmOriginalAdminLevel) {

        final String iso = tags.get("iso");
        if (StringUtils.isEmpty(iso)) {
            return;
        }

        String adminLevel = null;
        switch (iso) {
            // TODO: implement for Canada, Mexico and Europe
            case "USA":
                if ("7".equals(osmOriginalAdminLevel)) {
                    adminLevel = "L4";
                } else if ("8".equals(osmOriginalAdminLevel)) {
                    adminLevel = "L5";
                }
                break;
        }
        if (StringUtils.isEmpty(adminLevel)) {
            tags.remove("admin_level");
        } else {
            tags.put("admin_level", adminLevel);
        }
    }

    private MemberRelation getAdminCenterRelation(final long nodeId,
                                                  final List<MemberRelation> relations) {

        // Filter relations that have the node as city centre.
        final List<MemberRelation> relationsWithNodeAsAdminCentre = relations.stream()
                .filter(memberRelation -> {

                    final Member nodeMember = memberRelation.getMember();

                    final String nodeRole = nodeMember.getMemberRole();

                    return (nodeRole.equals(OSM_ADMIN_CENTER_MEMBER_ROLE) ||
                            // Some cities in Germany have this label instead of 'admin_center'
                            nodeRole.equals(OSM_LABEL_MEMBER_ROLE) &&
                                    nodeId == nodeMember.getId().longValue() &&
                                    memberRelation.getRelationTags().containsKey(ADMIN_LEVEL_TAG_KEY_IN_OSM));
                }).collect(Collectors.toList());

        for (final MemberRelation memberRelation : relationsWithNodeAsAdminCentre) {
            if (CityService.isCityMemberRelation(memberRelation)) {
                return memberRelation;
            }
        }
        return null;
    }

    private void enhanceName(final Map<String, String> newTags) {
        final String iso3 = newTags.get(ISO);
        if (StringUtils.isEmpty(iso3)) {
            return;
        }

        // name:<lang_code>: as in OSM - do nothing.
        // alt_name:<lang_code>: as in OSM - do nothing.

        final String iso2 = IsoUtil.getIso2(iso3);
        if (StringUtils.isEmpty(iso2)) {
            return;
        }
        final String lowerCaseIso2 = iso2.toLowerCase();

        // Enhance name.
        // we have a the separate NameRule for this.

        // Enhance alt_name.
        enhanceWithExtractor(altNameExtractor,
                newTags,
                (value, tags) -> LangCodeUtil.enhanceWithLangCode(value,
                        OsmTagKey.ALT_NAME.unwrap(),
                        lowerCaseIso2,
                        tags,
                        LANG_CODE_MAP));

    }

    private void enhancedCapitalOrder(final Map<String, String> tags) {

        if ("yes".equals(tags.get("capital"))) {
            return;
        }

        final String uniDbAdminLevel = tags.get(ADMIN_LEVEL_TAG_KEY_IN_UNIDB);
        if (ADMIN_LEVEL_1.equals(uniDbAdminLevel)) {
            tags.put(CAPITAL_TAG_KEY_IN_UNIDB_AND_OSM, "yes");
        }

        String capitalOrder = null;

        final String iso = tags.get("iso");
        if (!StringUtils.isEmpty(iso)) {
            // TODO: implement for Canada, Mexico and Europe
            if ("USA".equals(iso)) {
                final String capital = tags.get("capital");
                if (!StringUtils.isEmpty(capital)) {
                    switch (capital) {
                        case "4":
                            capitalOrder = "1";
                            break;
                        case "6":
                            capitalOrder = "2";
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        if (!StringUtils.isEmpty(capitalOrder)) {
            tags.put(CAPITAL_ORDER_TAG_KEY_PREFIX_IN_UNIDB + capitalOrder, "yes");
        }
        tags.remove("capital");
    }

    private void enhanceAdminLevels(final Map<String, String> adminAreaRelationTags,
                                    final Map<String, String> tags) {
        TagService.addIfNonEmpty(tags, "l1", adminAreaRelationTags.get("l1"));
        TagService.addIfNonEmpty(tags, "l2", adminAreaRelationTags.get("l2"));
        TagService.addIfNonEmpty(tags, "l3", adminAreaRelationTags.get("l3"));
        TagService.addIfNonEmpty(tags, LINK_COUNT_TAG_KEY_IN_UNIDB, adminAreaRelationTags.get(LINK_COUNT_TAG_KEY_IN_UNIDB));
    }

    private void enhanceWithExtractor(final Extractor<String> extractor,
                                      final Map<String, String> tags,
                                      final CityCenterRule.EnhancementCodeWithValue code) {
        extractor.extract(tags).ifPresent(s -> code.execute(s, tags));
    }

    private boolean enhance(final Map<String, String> tags,
                            final String key,
                            final String value) {
        tags.put(key, value);
        return true;
    }

    private boolean enhance(final Map<String, String> tags,
                            final CityCenterRule.EnhancementCode code) {
        return code.execute(tags);
    }

    private void enhanceIso(final Map<String, String> tags,
                            final double latitude,
                            final double longitude, final Map<String, String> originalOsmRelationTags,
                            final Map<String, String> adminAreaRelationTags) {
        String iso = null;
        if (!tags.containsKey(ISO)) {
            if (originalOsmRelationTags.containsKey(ISO)) {
                iso = originalOsmRelationTags.get(ISO);
            } else if (adminAreaRelationTags.containsKey(ISO)) {
                iso = adminAreaRelationTags.get(ISO);
            } else if (originalOsmRelationTags.containsKey(IS_IN_COUNTRY_CODE)) {
                iso = originalOsmRelationTags.get(IS_IN_COUNTRY_CODE);
            } else if (adminAreaRelationTags.containsKey(IS_IN_COUNTRY_CODE)) {
                iso = adminAreaRelationTags.get(IS_IN_COUNTRY_CODE);
            } else if (originalOsmRelationTags.containsKey(IS_IN_COUNTRY)) {
                iso = originalOsmRelationTags.get(IS_IN_COUNTRY);
            } else if (adminAreaRelationTags.containsKey(IS_IN_COUNTRY)) {
                iso = adminAreaRelationTags.get(IS_IN_COUNTRY);
            } else {
                iso = IsoUtil.getIso3(latitude, longitude);
            }
        }

        if (StringUtils.isEmpty(iso)) {
            return;
        }

        String iso3 = null;
        if (iso.length() == 3) {
            iso3 = iso;
        } else {
            iso3 = IsoUtil.getIso3(iso);
        }
        tags.put(ISO_TAG_KEY_IN_UNIDB, iso3);
    }

    /*
        Helper functions.
    */
    @FunctionalInterface
    public interface EnhancementCodeWithValue extends Serializable {

        CityCenterRule.EnhancementCodeWithValue PLACE_ENHANCER_FUNC = (value, tags) -> {

            if (OSM_CORESPONDENTS_FOR_UNIDB_NEIGHBORHOOD.contains(value)) {

                tags.put(PLACE_TAG_KEY_IN_UNIDB, "neighborhood");

            } else if (OSM_CORESPONDENTS_FOR_UNIDB_CITY.contains(value)) {

                tags.put(PLACE_TAG_KEY_IN_UNIDB, "city");

            } else if (OSM_CORESPONDENTS_FOR_UNIDB_HAMLET.contains(value)) {

                tags.put(PLACE_TAG_KEY_IN_UNIDB, "hamlet");

            }

            return true;
        };

        CityCenterRule.EnhancementCodeWithValue ADDR_STREET_ENHANCEMENT_FUNC = (value, tags) -> {

            final String placeTagValue = tags.get(PLACE_TAG_KEY_IN_UNIDB);

            if (placeTagValue != null) {

                final String catId = PLACE_TAG_TO_CAT_ID_VALUE.get(placeTagValue);

                if (catId != null) {

                    tags.put(CAT_ID_TAG_KEY_IN_UNIDB, catId);

                    return true;
                }
            }

            return false;
        };

        boolean execute(String value, Map<String, String> tags);

    }

    @FunctionalInterface
    private interface EnhancementCode extends Serializable {

        CityCenterRule.EnhancementCode CAT_ID_ENHANCEMENT_FUNC = (tags) -> {
            if (tags.containsKey(PLACE_TAG_KEY_IN_UNIDB)) {

                final String placeTagValue = tags.get(PLACE_TAG_KEY_IN_UNIDB);

                final String catId = PLACE_TAG_TO_CAT_ID_VALUE.get(placeTagValue);

                if (catId != null) {

                    tags.put(CAT_ID_TAG_KEY_IN_UNIDB, catId);

                    return true;
                }
            }

            return false;
        };

        boolean execute(Map<String, String> tags);

    }
}
