package com.telenav.adapter.rules.node;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class CleanNodeTagsRule extends Enhancer<Map<String, String>> {
    @Override
    public Map<String, String> apply(final Map<String, String> tags) {
        tags.remove(OsmTagKey.HIGHWAY.unwrap());
        tags.remove("crossing");
        return tags;
    }
}
