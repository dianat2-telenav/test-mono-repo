package com.telenav.adapter.rules.node;

import com.telenav.adapter.component.repository.ResourceRepository;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Pattern;

import static com.telenav.adapter.utils.IsoUtil.getIso2;

/**
 * @author roxanal
 */
@Slf4j
public class NameRule extends Enhancer<Map<String, String>> {

    private static final long serialVersionUID = 1L;
    private static final String TWO_CHARS_LOCALIZED_NAME_REGEXP = "^(name:[a-zA-Z]{2})$";
    private static Map<String, List<String>> LANG_CODE_MAP = ResourceRepository.getLangCodesMap();

    @Override
    public Map<String, String> apply(final Map<String, String> tags) {
        with3CharLangNames(tags);
        withIsoDerived3CharLangName(tags);
        return tags;
    }

    /**
     * The current method adds the 'name:3_char_lang_code>' considering the ISO if that
     * 'name:3_char_lang_code>' doesn't already exist.
     *
     * @param tags .
     */
    private void withIsoDerived3CharLangName(final Map<String, String> tags) {
        if (tags.containsKey("name")) {

            final String name = tags.get("name");

            final String iso2 = getIso2(tags);

            if (!StringUtils.isEmpty(iso2)) {
                final List<String> langCodes = LANG_CODE_MAP.get(iso2);

                if (langCodes != null) {
                    for (final String langCode : langCodes) {

                        final String twoCharsLocalizedKey = "name:" + langCode;

                        final Optional<String> threeCharsLocalizedKey = to3CharsLocalizedKey(twoCharsLocalizedKey);

                        if (threeCharsLocalizedKey.isPresent() &&
                                !tags.containsKey(threeCharsLocalizedKey)) {
                            tags.put(threeCharsLocalizedKey.get(), name);
                        }
                    }
                }
            }
        }
    }

    /**
     * The current method derives the existing 'name:<2_char_lang_code>' tags to 'name:<3_char_lang_code>' tags.
     *
     * @param tags .
     */
    private void with3CharLangNames(final Map<String, String> tags) {
        final List<String> twoCharsLocalizedKeys = getTwoCharsLocalizedKeys(tags);

        if (!twoCharsLocalizedKeys.isEmpty()) {

            for (final String twoCharsLocalizedKey : twoCharsLocalizedKeys) {

                final String value = tags.get(twoCharsLocalizedKey);

                final Optional<String> threeCharsLocalizedKey = to3CharsLocalizedKey(twoCharsLocalizedKey);

                if (threeCharsLocalizedKey.isPresent()) {
                    tags.remove(twoCharsLocalizedKey);
                    tags.put(threeCharsLocalizedKey.get(), value);
                }
            }
        }
    }

    private Optional<String> to3CharsLocalizedKey(final String twoCharsLocalizedKey) {

        final String[] split = twoCharsLocalizedKey.split(":");

        String iso3Lang = null;

        try {
            iso3Lang = Locale.forLanguageTag(split[1]).getISO3Language();
        } catch (MissingResourceException e) {
            log.error(e.getMessage());
            return Optional.empty();
        }

        return Optional.of(split[0] + ":" + iso3Lang);
    }

    private List<String> getTwoCharsLocalizedKeys(final Map<String, String> tags) {

        final List<String> result = new LinkedList<>();

        for (final String key : tags.keySet()) {

            if (isTwoCharsLocalizedName(key)) {
                result.add(key);
            }
        }
        return result;
    }

    private boolean isTwoCharsLocalizedName(final String key) {
        return Pattern.matches(TWO_CHARS_LOCALIZED_NAME_REGEXP, key);
    }
}