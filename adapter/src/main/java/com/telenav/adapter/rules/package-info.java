/**
 * Group of rules for adapting/updating and unifying tags and their values according to UniDb specs.
 */
package com.telenav.adapter.rules;