package com.telenav.adapter.rules.whitelist;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Whitelist for telenav tags, for Grab client.
 *
 * @author adrianal
 */
public class GrabWhiteList implements WhiteList {

    @Override
    public List<String> tags() {
        return new ArrayList<>(
                Arrays.asList(OsmTagKey.WIDTH.withSource(TnSourceValue.IMAGES),
                        TnTagKey.TN_LANGUAGE_CODE.unwrap(),
                        TnTagKey.TN_COUNTRY_ISO_CODE.unwrap(),
                        TnTagKey.TN_ADMIN_L1_NAME.unwrap(),
                        TnTagKey.TN_ADMIN_L2_NAME.unwrap(),
                        TnTagKey.TN_FREE_FLOW_SPEED.unwrap(),
                        TnTagKey.TN_VALIDATION_STATUS.unwrap()));
    }
}