package com.telenav.adapter.rules.whitelist;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;

import java.util.Arrays;
import java.util.List;


/**
 * Whitelist for telenav tags for NGX client.
 *
 * @author adrianal
 */
public class NgxWhiteList implements WhiteList {

    @Override
    public List<String> tags() {
        return Arrays
                .asList(TnTagKey.TN_ORIGINAL_ID.unwrap(),
                        OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES),
                        OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES),
                        OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES),
                        OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES),
                        TnTagKey.TN_LANGUAGE_CODE.unwrap(),
                        TnTagKey.TN_COUNTRY_ISO_CODE.unwrap(),
                        "tn__iso",
                        TnTagKey.TN_FREE_FLOW_SPEED.unwrap(),
                        // tn__poly_id is a temporary tag to fix self intersect_job
                        "tn__poly_id"
                );
    }
}