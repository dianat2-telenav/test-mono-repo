package com.telenav.adapter.utils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Util for language related logic.
 *
 * @author liviuc
 */
public class LangCodeUtil {

    public static boolean enhanceWithLangCode(final String name,
                                              final String tagKey,
                                              final String iso2,
                                              final Map<String, String> tags,
                                              final Map<String, List<String>> langCodesByCountry) {
        boolean enhanced = false;

        final List<String> langCodes = getLangCodes(iso2, langCodesByCountry);

        for (final String langCode : langCodes) {

            final String keyWithLangCode = tagKey + ":" + langCode;

            //if not from osm already
            if (!tags.containsKey(keyWithLangCode)) {

                tags.put(keyWithLangCode, name);

                enhanced = true;

            }
        }

        return enhanced;
    }

    public static List<String> getLangCodes(final String iso2,
                                            final Map<String, List<String>> langCodesByCountry) {
        final List<String> langCodes = langCodesByCountry.get(iso2);

        if (langCodes == null) {
            return Collections.emptyList();
        }

        return langCodes;
    }
}
