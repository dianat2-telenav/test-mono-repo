package com.telenav.adapter.utils;

import com.telenav.datapipelinecore.osm.OsmSourceTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;

import java.util.HashMap;
import java.util.Map;


/**
 * @author roxanal
 */
public class ManualEditsUtil {

    /**
     * The tag, having the pattern tn__source:{feature}, is altered by replacing the 'source' String with the
     * tag's value and the resulted tag's value will be the value of the given feature tag
     *
     * @param tags - initial map of tags
     * @param feature - an OSM tag name (e.g.: 'oneway', 'maxspeed')
     * @return - the map of tags with 'tn__source:{feature}' tag altered
     */
    public static Map<String, String> alterManualEditsTag(final Map<String, String> tags, final String feature) {
        final Map<String, String> alteredTags = new HashMap<>(tags);
        final String osmTagValue = getOsmTagValue(tags, feature);
        final String editStatus = tags.get(OsmSourceTagKey.SOURCE.telenavPrefix() + ":" + feature);
        if (editStatus != null) {
            alteredTags.remove(OsmSourceTagKey.SOURCE.telenavPrefix() + ":" + feature);
            alteredTags.put("tn__" + editStatus + "_" + feature, osmTagValue);
        }
        return alteredTags;
    }

    /**
     * Retrieves a certain tag's value. If it is missing, the 'missing' String will be returned
     * @param tags - the map of tags
     * @param tagName - the tag's name whose value is searched
     * @return - the tag's value, if it is missing, the 'missing' String will be returned
     */
    private static String getOsmTagValue(final Map<String, String> tags, final String tagName) {
        return tags.get(tagName) == null ? TnSourceValue.MISSING.unwrap() : tags.get(tagName);
    }
}