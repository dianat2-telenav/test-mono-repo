package com.telenav.adapter.utils;

import java.util.Arrays;
import java.util.List;

public class OsmInfo {

    public static final String ADMIN_LEVEL_1 = "L1";
    public static final String ADMIN_LEVEL_2 = "L2";
    public static final String ADMIN_LEVEL_3 = "L3";
    public static final String ADMIN_LEVEL_4 = "L4";
    public static final String ADMIN_LEVEL_5 = "L5";
    public static final List<String> UNIDB_ADMIN_LEVELS_TO_INFER_CAPITAL_ORDER =
            Arrays.asList(ADMIN_LEVEL_1, ADMIN_LEVEL_2, ADMIN_LEVEL_3, ADMIN_LEVEL_4, ADMIN_LEVEL_5);
    public static final String ADMIN_LEVEL_6 = "L6";
    public static final String ADMIN_LEVEL_7 = "L7";

    public static final String NODE_MEMBER_TYPE = "node";
    public static final String TELENAV_ADMIN_LEVEL_KEY_CONTENT = ":left";
    public static final String TELENAV_ADMIN_LEVEL_TAG_KEY_REGEX = "tn__l\\d.*";
    public static final List<String> OSM_CORESPONDENTS_FOR_UNIDB_NEIGHBORHOOD = Arrays.asList(new String[]{
            "neighborhood", "suburb"
    });
    public static final List<String> OSM_CORESPONDENTS_FOR_UNIDB_CITY = Arrays.asList(new String[]{
            "city", "town", "square", //???
            "city_block", "quarter"
    });
    public static final List<String> OSM_CORESPONDENTS_FOR_UNIDB_HAMLET = Arrays.asList(new String[]{
            "hamlet", "locality", "village", "isolated_dwelling", "islet", "island", "farm"
    });
    public static final String OSM_ADMIN_CENTER_MEMBER_ROLE = "admin_centre";
    public static final String OSM_LABEL_MEMBER_ROLE = "label";

    public static final String PLACE_TAG_CITY_VALUE_IN_UNIDB = "city";
    public static final String PLACE_TAG_HAMLET_VALUE_IN_UNIDB = "hamlet";
    public static final String ALT_NAME_UNIDB_KEY_EXONYM_POSTFIX = ":is_exonym";
    public static final String ALT_NAME_EXONYM_UNIDB_VALUE = "yes";
}
