package com.telenav.adapter.utils;

public class OsmKeys {

    //osm keys:
    public static final String ADDRSTREET_TAG_KEY_IN_OSM = "addr:street";
    public static final String PLACE_TAG_KEY_IN_OSM = "place";
    public static final String ADMIN_LEVEL_TAG_KEY_IN_OSM = "admin_level";
    public static final String IS_IN_STATE_CODE_TAG_KEY_IN_OSM = "is_in:state_code";
    public static final String CAPITAL_TAG_KEY_IN_OSM = "capital";

}
