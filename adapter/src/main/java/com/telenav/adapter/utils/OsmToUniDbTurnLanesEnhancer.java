package com.telenav.adapter.utils;

import com.telenav.adapter.bean.DrivingSide;
import com.telenav.adapter.component.DrivingSideFinder;
import com.telenav.adapter.component.fc.TagService;
import com.telenav.adapter.exception.CorruptWaySectionException;
import com.telenav.map.entity.TagAdjuster;
import com.telenav.map.entity.WaySection;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public interface OsmToUniDbTurnLanesEnhancer extends Serializable {

    DrivingSideFinder DRIVING_SIDE_FINDER = new DrivingSideFinder();
    String LANES_FORWARD = TagService.LANES_FORWARD;
    String LANES_BACKWARD = TagService.LANES_BACKWARD;
    String LANES_BOTH_WAYS = "lanes:both_ways";
    String TN_LANES_FORWARD = "tn_lanes:forward";
    String TN_LANES_BACKWARD = "tn_lanes:backward";
    String TN_LANES_BOTH_WAYS = "tn_lanes:both_ways";
    String TURN_LANES_FORWARD = "turn:lanes:forward";
    String TURN_LANES_BACKWARD = "turn:lanes:backward";
    String TURN_LANES_BOTH_WAYS = "turn:lanes:both_ways";
    String TURN_LANES_CENTRE_TURN_LANE = "centre_turn_lane";
    String TN_TURN_LANES_FORWARD = "tn_turn:lanes:forward";
    String TN_TURN_LANES_BACKWARD = "tn_turn:lanes:backward";
    String TN_TURN_LANES_BOTH_WAYS = "tn_turn:lanes:both_ways";
    String TN_TURN_LANES_CENTRE_TURN_LANE = "tn_centre_turn_lane";
    String TURN_LANE_SEPARATOR = "|";
    String TURN_LANE_VALUE_SEPARATOR = ";";

    Map<String, Integer> turnLanesMapping = Arrays.stream(
            new String[][]{
                    {"none", "0"},
                    {"", "0"},
                    {"through", "1"},
                    {"straight", "1"},
                    {"right", "4"},
                    {"slight_right", "2"},
                    {"sharp_right", "8"},
                    {"sharp_left", "32"},
                    {"left", "64"},
                    {"slight_left", "128"},
                    {"merge_to_left", "512"},
                    {"merge_to_right", "2048"}
            }
    ).collect(Collectors.toMap(keyMapper -> keyMapper[0],
            valueMapper -> Integer.parseInt(valueMapper[1])));

    Map<String, Integer> reverseTurnLanesMapping = Arrays.stream(
            new String[][]{
                    {"RIGHT", "16"},
                    {"LEFT", "256"}
            }
    ).collect(Collectors.toMap(keyMapper -> keyMapper[0],
            valueMapper -> Integer.parseInt(valueMapper[1])));

    String getNewTag();

    String getNewTagTemp();

    String getOldTag();

    boolean revertLanes();

    String getLaneTypeName();

    default int getNumberOfLanes(Map<String, String> tags) {
        final String lanes = getLaneTypeName();
        final String lanesValue = tags.get(lanes);
        return (lanesValue != null) ? Integer.parseInt(lanesValue) : 0;
    }

    default <T extends TagAdjuster<T>> T enhanceFromTurnLane(final Optional<String> turnLaneValue,
                                                             final T entity) {
        return enhanceFromTurnLane(turnLaneValue,
                entity,
                getNewTag(),
                getOldTag());
    }

    default <T extends TagAdjuster<T>> T enhanceFromTurnLane(final Optional<String> turnLaneValue,
                                                             final T entity,
                                                             final String newTag,
                                                             final String oldTag) {
        if (turnLaneValue.isPresent()) {
            final Map<String, String> tags = entity.tags();
            DrivingSide drivingSide;
            if (entity instanceof WaySection) {
                WaySection waySection = (WaySection) entity;
                drivingSide = DRIVING_SIDE_FINDER.getDrivingSide(waySection.firstNode());
            } else {
                drivingSide = null;
            }
            addTags(tags,
                    newTag,
                    oldTag,
                    turnLaneValue.get(),
                    drivingSide);
            return entity.replaceTags(tags);
        }
        return entity;
    }

    default <T extends TagAdjuster<T>> T enhanceFromLaneNumber(final T entity) {
        final Map<String, String> tags = entity.tags();
        final String newTag = getNewTagTemp();

        int lanesNumber = getNumberOfLanes(tags);

        final StringBuilder newTagValueSb = new StringBuilder();
        for (int i = 0; i < lanesNumber; i++) {
            newTagValueSb.append("0");
            if (i < lanesNumber - 1) {
                newTagValueSb.append(TURN_LANE_SEPARATOR);
            }
        }
        tags.put(newTag, newTagValueSb.toString());
        return entity.replaceTags(tags);
    }

    default void addTags(final Map<String, String> tags,
                         final String newTag,
                         final String oldTag,
                         final String oldValue,
                         final DrivingSide drivingSide) {
        final String compact = compact(convertTurnLanes(laneParts(oldValue), drivingSide));
        if (!compact.isEmpty()) {
            tags.put(newTag, compact);
            //TODO remove old turn lanes tag, leaving for now for testing purposes
            tags.put(oldTag, oldValue);
        }
    }

    default String compact(final LinkedList<Integer> turnLanesAsInts) {
        final Spliterator<Integer> spliterator = (revertLanes()) ?
                Spliterators.spliteratorUnknownSize(turnLanesAsInts.descendingIterator(), Spliterator.ORDERED) :
                Spliterators.spliteratorUnknownSize(turnLanesAsInts.iterator(), Spliterator.ORDERED);
        return StreamSupport
                .stream(spliterator, false)
                .map(Object::toString)
                .collect(Collectors.joining(TURN_LANE_SEPARATOR));
    }

    default LinkedList<String> laneParts(final String turnLaneValue) {
        return new LinkedList<>(Arrays.asList(turnLaneValue.split("\\" + TURN_LANE_SEPARATOR, -1)));
    }

    default LinkedList<Integer> convertTurnLanes(final LinkedList<String> turnLanes,
                                                 final DrivingSide drivingSide) {
        final LinkedList<Integer> asInts = turnLanes.stream()
                .map(turnLane -> mapTurnLaneToInt(turnLane, drivingSide))
                .collect(Collectors.toCollection(LinkedList::new));
        //some of the values are null, then do no conversion (null means string cannot be paired to int)
        return asInts.stream().allMatch(Objects::nonNull) ? asInts : new LinkedList<>();
    }

    default Integer mapTurnLaneToInt(final String turnLane,
                                     final DrivingSide drivingSide) {
        Integer intValue = null;
        /*if (turnLanesMapping.containsKey(turnLane)) {
            intValue = turnLanesMapping.get(turnLane);
        } else *///if (turnLane.contains(TURN_LANE_VALUE_SEPARATOR))
        {
            //if turn lane value has more than one value
            final List<Integer> turnLanesAsInts = Arrays
                    .stream(turnLane.split(TURN_LANE_VALUE_SEPARATOR))
                    .map(lane -> turnLaneValue(lane, drivingSide))
                    .collect(Collectors.toList());
            intValue = turnLanesAsInts.stream()
                    .mapToInt(x -> (x != null) ? x : 0)
                    .sum();
        }
        return intValue;
    }

    default Integer turnLaneValue(final String lane,
                                  final DrivingSide drivingSide) {
        Integer turnLaneValue;
        if (isReverse(lane)) {
            turnLaneValue = reverseTurnLanesMapping.get(drivingSide.name());
        } else {
            turnLaneValue = turnLanesMapping.get(lane);
        }
        if (turnLaneValue == null) {
            throw new CorruptWaySectionException("Value " + lane + " is invalid for lane");
        }
        return turnLaneValue;
    }

    default boolean isReverse(final String turnLane) {
        return "reverse".equals(turnLane);
    }
}

