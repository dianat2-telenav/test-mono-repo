package com.telenav.adapter.utils;

import com.telenav.map.storage.RawOsmEntitySchema;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RowUtils {

    public static List<Row> toTagsList(Map<String, String> map) {
        List<Row> tags = new ArrayList<>(map.size());

        map.forEach((k, v) -> tags.add(RowFactory.create(k, v)));

        return tags;
    }

    public static Map<String, String> extractTags(List<Row> tagRows) {
        final Map<String, String> tags = new LinkedHashMap<>(tagRows.size());
        String key, value;
        for (Row tagRow : tagRows) {
            key = tagRow.getAs(RawOsmEntitySchema.TagSchema.KEY.name());
            value = tagRow.getAs(RawOsmEntitySchema.TagSchema.VALUE.name());
            tags.put(key, value);
        }

        return tags;
    }
}
