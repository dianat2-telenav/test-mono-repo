package com.telenav.adapter.utils;

import com.telenav.datapipelinecore.unidb.DefaultTagValue;


/**
 * @author petrum
 */
public class SpeedConverter {

    private static final double KM_TO_MILES = 0.621371;
    private static final double MILES_TO_KM = 1.60934;

    /**
     * Convert a value to a given speed unit.
     * @param value the value that has to be converted;
     * @param unit the unit in which the given value has to be converted
     * @return the converted value
     */
    public static double  unitConvert(final String value, final String unit) throws NumberFormatException{
        return unit.equals(DefaultTagValue.MILES.unwrap()) ? Double.valueOf(value) * KM_TO_MILES :
                Double.valueOf(value) * MILES_TO_KM;
    }
}
