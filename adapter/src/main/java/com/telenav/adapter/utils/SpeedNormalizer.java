package com.telenav.adapter.utils;

import lombok.AllArgsConstructor;

import java.util.Optional;


/**
 * @author petrum
 */
@AllArgsConstructor
public class SpeedNormalizer {

    public static Optional<String> normalize(final String speed, String speedUnit) {
        if (speed != null) {
            if (speed.contains("mph") && speedUnit.equals("K")) {
                return correctSpeedLimitIfUnitsAreNotTheSame(speed, speedUnit);
            } else if (speed.contains("kph") && speedUnit.equals("M")) {
                return correctSpeedLimitIfUnitsAreNotTheSame(speed, speedUnit);
            } else {
                return fixSpeedLimitsIfUnitsAreTheSame(speed);
            }
        }
        return Optional.empty();
    }

    private static Optional<String> correctSpeedLimitIfUnitsAreNotTheSame(String speed, String speedUnit) {
        String speedValue = speed.replaceAll("[^0-9]", ""); // only numerical values are accepted
        try {
            final int value = (int) SpeedConverter.unitConvert(speedValue, speedUnit);
            return Optional.of(roundSpeed(value));
        } catch (final NumberFormatException exception) {
            return Optional.empty();
        }
    }

    private static Optional<String> fixSpeedLimitsIfUnitsAreTheSame(String speed) {

        try {
            String speedValue = speed.replaceAll("[^0-9]", ""); // only numerical values are accepted
            return Optional.of(roundSpeed(Integer.parseInt(speedValue)));
        } catch (final NumberFormatException exception) {
            return Optional.empty();
        }
    }

    private static String roundSpeed(int speed){
        speed = speed / 5;
        speed = speed * 5;
        return String.valueOf(speed);
    }
}
