package com.telenav.adapter.utils;

/**
 * @author liviuc
 */
public class StringUtils {

    public static String concat(final Object... objs) {
        final StringBuilder sb = new StringBuilder();
        for(final Object obj : objs) {
            sb.append(obj);
        }
        return sb.toString();
    }

    public static StringBuilder concat(final StringBuilder sb,
                                final Object... objs) {
        for(final Object obj : objs) {
            sb.append(obj);
        }
        return sb;
    }
}
