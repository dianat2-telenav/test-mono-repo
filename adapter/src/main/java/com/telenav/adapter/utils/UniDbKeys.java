package com.telenav.adapter.utils;

public class UniDbKeys {

    /*
        UniDB tag keys.
     */
    public static final String CITY_CENTER_TAG_KEY_IN_UNIDB = "type";
    public static final String PLACE_TAG_KEY_IN_UNIDB = "place";
    public static final String STREET_NAME_TAG_KEY_IN_UNIDB = "street_name";
    public static final String ADMIN_LEVEL_TAG_KEY_IN_UNIDB = "admin_level";
    public static final String CAPITAL_ORDER_TAG_KEY_PREFIX_IN_UNIDB = "capital_order";
    public static final String LINK_COUNT_TAG_KEY_IN_UNIDB = "link_count";
    public static final String CAT_ID_TAG_KEY_IN_UNIDB = "cat_id";
    public static final String ISO_TAG_KEY_IN_UNIDB = "iso";
    public static final String CAPITAL_TAG_KEY_IN_UNIDB_AND_OSM = "capital";
    public static final String TYPE_TAG = "type";
    public static final String NAVN_TAG = "navn";
    /*
        UniDB tag values.
     */
    public static final String CITY_CENTER_TAG_VALUE_IN_UNIDB = "city_center";

    public static final String PLACE_TAG_NEIGHBORHOOD_VALUE = "neighborhood";
}
