package com.telenav.adapter.utils.connectivity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

/**
 * @author liviuc
 */
@Getter
@Setter
@Builder
public class ConnectedComponent implements Serializable {

    private long id;
    private Set<String> vertexes;
}