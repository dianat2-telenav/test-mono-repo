package com.telenav.adapter.utils.connectivity;

import com.telenav.adapter.utils.connectivity.geojson.GeoJson;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.spark.api.java.JavaRDD;

/**
 * @author liviuc
 */
@Builder
@Getter
@Setter
public class Connectivity {

    private JavaRDD<GeoJson> geojsons;
    private ConnectivityMetaData metaData;
}
