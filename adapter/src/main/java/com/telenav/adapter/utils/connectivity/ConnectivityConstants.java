package com.telenav.adapter.utils.connectivity;

/**
 * @author liviuc
 */
public class ConnectivityConstants {

    public static final String COORDINATES_SEPARATOR = ",";
    public static final int SMALL_COMPONENT_LENGTH_THRESHOLD_KM = 2;
}
