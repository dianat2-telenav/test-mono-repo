package com.telenav.adapter.utils.connectivity.connectivity.checker;

import com.telenav.adapter.utils.connectivity.ConnectivityMetaData;
import org.jgrapht.alg.connectivity.BiconnectivityInspector;
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector;
import org.jgrapht.graph.DefaultDirectedGraph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The current class provides geojsons of the connected components of a directed graph.
 *
 * @param <V> vertices type.
 * @param <E> edges type.
 * @author liviuc
 */
public class DirectedGraphConnectivityChecker<V, E> extends GraphConnectivityChecker<DefaultDirectedGraph, V, E> {

    private KosarajuStrongConnectivityInspector<V, E> connectivityInspector;
    private BiconnectivityInspector<V, E> biconnectivityInspector;

    @Override
    public List<Set<V>> connectedSets() {
        return this.connectivityInspector.stronglyConnectedSets();
    }

    @Override
    public DirectedGraphConnectivityChecker<V, E> graph(final DefaultDirectedGraph graph) {
        this.graph = graph;
        this.connectivityInspector = new KosarajuStrongConnectivityInspector<V, E>(graph);
        this.biconnectivityInspector = new BiconnectivityInspector<V, E>(graph);
        return this;
    }

    @Override
    public ConnectivityMetaData metaData() {
        final Map<String, String> data = new HashMap<>();
        data.put("is strongly connected", String.valueOf(connectivityInspector.isStronglyConnected()));
        data.put("strongly connected components", String.valueOf(connectivityInspector.stronglyConnectedSets().size()));
        data.put("is bi-connected", String.valueOf(biconnectivityInspector.isBiconnected()));
        data.put("bi-connected components", String.valueOf(biconnectivityInspector.getConnectedComponents().size()));
        return ConnectivityMetaData.builder()
                .data(data)
                .build();
    }
}
