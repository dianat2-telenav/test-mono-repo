package com.telenav.adapter.utils.connectivity.connectivity.checker;

import com.telenav.adapter.utils.connectivity.ConnectivityMetaData;
import org.jgrapht.graph.AbstractBaseGraph;

import java.util.List;
import java.util.Set;

/**
 * The current class provides geojsons of the connected components of a graph.
 *
 * @param <T> AbstractBaseGraph.
 * @param <V> vertices type.
 * @param <E> edges type.
 * @author liviuc.
 */
public abstract class GraphConnectivityChecker<T extends AbstractBaseGraph, V, E> {

    protected T graph;

    public abstract List<Set<V>> connectedSets();

    public abstract GraphConnectivityChecker graph(T graph);

    public abstract ConnectivityMetaData metaData();
}
