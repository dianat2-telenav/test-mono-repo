package com.telenav.adapter.utils.connectivity.connectivity.checker;

import com.telenav.adapter.utils.connectivity.ConnectivityMetaData;
import org.jgrapht.alg.connectivity.ConnectivityInspector;
import org.jgrapht.graph.DefaultUndirectedGraph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The current class provides geojsons of the connected components of a undirected graph.
 *
 * @param <V> vertices type.
 * @param <E> edges type.
 * @author liviuc
 */
public class UndirectedGraphConnectivityChecker<V, E> extends GraphConnectivityChecker<DefaultUndirectedGraph, V, E> {

    private ConnectivityInspector<V, E> connectivityInspector;

    @Override
    public UndirectedGraphConnectivityChecker<V, E> graph(final DefaultUndirectedGraph graph) {
        this.graph = graph;
        this.connectivityInspector = new ConnectivityInspector<V, E>(graph);
        return this;
    }

    @Override
    public List<Set<V>> connectedSets() {
        return connectivityInspector.connectedSets();
    }

    @Override
    public ConnectivityMetaData metaData() {
        final Map<String, String> data = new HashMap<>();
        data.put("is connected", String.valueOf(connectivityInspector.isConnected()));
        data.put("connected components", String.valueOf(connectivityInspector.connectedSets().size()));
        return ConnectivityMetaData.builder()
                .data(data)
                .build();
    }
}
