package com.telenav.adapter.utils.connectivity.geojson;


import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author liviuc
 */
@Builder
@Getter
public class GeoJson implements Serializable {

    private long connectedComponentId;
    private String content;
    private long numberOfEdges;
    private boolean isSmallComponent;
}