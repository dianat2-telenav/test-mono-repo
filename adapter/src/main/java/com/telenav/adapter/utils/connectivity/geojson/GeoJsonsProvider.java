package com.telenav.adapter.utils.connectivity.geojson;

import com.telenav.adapter.utils.StringUtils;
import com.telenav.adapter.utils.connectivity.ConnectedComponent;
import com.telenav.adapter.utils.connectivity.graph.WaySectionEdge;
import com.telenav.adapter.utils.connectivity.service.VerticesDataService;
import com.telenav.adapter.utils.connectivity.connectivity.checker.GraphConnectivityChecker;
import com.telenav.adapter.utils.connectivity.graph.NodeVertex;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import com.telenav.map.geometry.Point;
import com.telenav.map.geometry.PolyLine;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import scala.Tuple2;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.telenav.adapter.utils.connectivity.ConnectivityConstants.COORDINATES_SEPARATOR;
import static com.telenav.adapter.utils.connectivity.ConnectivityConstants.SMALL_COMPONENT_LENGTH_THRESHOLD_KM;

/**
 * @author liviuc
 */
public class GeoJsonsProvider<V, E> {

    private static final Function<Double[], List<Double[]>> ACC = str -> {
        final List<Double[]> strs = new LinkedList<>();
        strs.add(str);
        return strs;
    };

    private static final Function2<List<Double[]>, Double[], List<Double[]>> ADD = (strs, str) -> {
        strs.add(str);
        return strs;
    };

    private static final Function2<List<Double[]>, List<Double[]>, List<Double[]>> MERGE = (left, right) -> {
        left.addAll(right);
        return left;
    };

    private static GeoJson geoJson(final Tuple2<Long, List<Double[]>> coordinatesData) {

        final List<Double[]> edgesCoordinates = coordinatesData._2();

        final StringBuilder sb = new StringBuilder("{\"type\":\"FeatureCollection\",\"features\":[");

        for (int i = 0; i < edgesCoordinates.size(); i++) {
            StringUtils.concat(sb,
                    "{\"geometry\":{\"type\":\"LineString\",\"coordinates\":[",
                    coordinatesStringBuilder(edgesCoordinates.get(i), COORDINATES_SEPARATOR),
                    "]}}");
            //sb.append("]},");
            //sb.append("\"type\":\"Feature\",\"properties\":{");
            //"\"id\":" + "1" + "" +
            //sb.append("}}");
            if (i < edgesCoordinates.size() - 1) {
                StringUtils.concat(sb, COORDINATES_SEPARATOR);
            }
        }
        StringUtils.concat(sb, "]}");

        return GeoJson.builder()
                .content(sb.toString())
                .isSmallComponent(isSmallComponent(edgesCoordinates))
                .connectedComponentId(coordinatesData._1())
                .numberOfEdges(edgesCoordinates.size())
                .build();
    }

    private static StringBuilder coordinatesStringBuilder(final Double[] coordinates,
                                                          final String separator) {
        final StringBuilder sb = new StringBuilder();
        int i = 0;
        for (i = 0; i < coordinates.length - 3; i += 2) {
            StringUtils.concat(sb, "[", coordinates[i], separator, coordinates[i + 1], "]", separator);
        }
        StringUtils.concat(sb, "[", coordinates[i], separator, coordinates[i + 1], "]");
        return sb;
    }

    public JavaRDD<GeoJson> geojsons(final JavaRDD<WaySectionEdge> edges,
                                     final VerticesDataService<V> verticesDataService,
                                     final GraphConnectivityChecker connectivityChecker,
                                     final JavaSparkContext javaSparkContext) {

        final List<Set<V>> connectedSets = connectivityChecker.connectedSets();

        final List<ConnectedComponent> connectedComponents =
                connectedComponents(connectedSets, verticesDataService, javaSparkContext);

        return geojsons(edges, connectedComponents);
    }

    private List<ConnectedComponent> connectedComponents(final List<Set<V>> connectedSets,
                                                         final VerticesDataService<V> verticesDataService,
                                                         final JavaSparkContext javaSparkContext) {
        return javaSparkContext.broadcast(verticesData(verticesDataService, connectedSets))
                .value();
    }

    protected List<ConnectedComponent> verticesData(final VerticesDataService<V> verticesDataService,
                                                    final List<Set<V>> connectedSets) {
        return verticesDataService.verticesData(connectedSets);
    }

    private JavaRDD<GeoJson> geojsons(final JavaRDD<WaySectionEdge> waySections,
                                      final List<ConnectedComponent> verticesDataBroadcast) {
        return waySections.mapToPair(waySectionEdge -> edgeCoordinates(waySectionEdge, verticesDataBroadcast))
                .filter(Objects::nonNull)
                .combineByKey(ACC, ADD, MERGE)
                .map(GeoJsonsProvider::geoJson);
    }

    private static Tuple2<Long, Double[]> edgeCoordinates(final WaySectionEdge waySectionEdge,
                                                   final List<ConnectedComponent> verticesDataBroadcast) {
        Tuple2<Long, Double[]> result = null;

        for (final ConnectedComponent verticesDatum : verticesDataBroadcast) {

            final NodeVertex firstNode = waySectionEdge.getNodes().get(0);
            final String firstNodeKey = nodeKey(firstNode);

            final NodeVertex lastNode = waySectionEdge.getNodes().get(waySectionEdge.getNodes().size() - 1);
            final String lastNodeKey = nodeKey(lastNode);

            final Set<String> vertexes = verticesDatum.getVertexes();
            if (vertexes.contains(firstNodeKey) && vertexes.contains(lastNodeKey)) {
                final Double[] coordinates = coordinates(waySectionEdge.getNodes());
                result = new Tuple2<>(verticesDatum.getId(), coordinates);
            }
        }

        return result;
    }

    private static boolean isSmallComponent(final List<Double[]> edgesCoordinates) {

        double totalLength = 0;

        for (int i = 0; i < edgesCoordinates.size(); i++) {

            final List<Point> points = new LinkedList<>();

            final Double[] edgeCoordinates = edgesCoordinates.get(i);
            for (int j = 0; j < edgeCoordinates.length; j += 2) {

                points.add(new Point(Latitude.degrees(edgeCoordinates[j]), Longitude.degrees(edgeCoordinates[j + 1])));

            }

            final double edgeLength = new PolyLine(points).length().asKilometers();

            if (edgeLength < SMALL_COMPONENT_LENGTH_THRESHOLD_KM) {
                totalLength += edgeLength;
            }
            if (totalLength > SMALL_COMPONENT_LENGTH_THRESHOLD_KM) {
                return false;
            }
        }

        return true;
    }

    private static String nodeKey(final NodeVertex nodeVertex) {
        return StringUtils.concat(nodeVertex.getLatitude(), COORDINATES_SEPARATOR, nodeVertex.getLongitude());
    }

    private static Double[] coordinates(final List<NodeVertex> nodes) {
        final Double[] coordinates = new Double[nodes.size() * 2];
        int pos = 0;
        for (final NodeVertex node : nodes) {
            coordinates[pos++] = node.getLongitude();
            coordinates[pos++] = node.getLatitude();
        }
        return coordinates;
    }
}
