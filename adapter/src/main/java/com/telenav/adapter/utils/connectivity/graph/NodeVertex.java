package com.telenav.adapter.utils.connectivity.graph;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

/**
 * @author liviuc
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
public class NodeVertex implements Serializable {

    private Long id;
    private double latitude;
    private double longitude;
    private Map<String, String> tags;

    public NodeVertex(final Long id,
                      final double latitude,
                      final double longitude,
                      final Map<String, String> tags) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.tags = tags;
    }
}
