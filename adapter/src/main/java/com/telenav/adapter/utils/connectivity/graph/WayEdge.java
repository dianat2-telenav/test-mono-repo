package com.telenav.adapter.utils.connectivity.graph;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author liviuc
 */
@Getter
@Setter
@Builder
public class WayEdge {

    private Long id;
    private List<NodeVertex> nodes;
    private String oneway;
}
