package com.telenav.adapter.utils.connectivity.graph;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jgrapht.graph.DefaultEdge;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author liviuc
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
public class WaySectionEdge extends DefaultEdge
        implements Serializable {

    private Long id;
    private Map<String, String> tags;
    private List<NodeVertex> nodes;
    //private String iso;

    public WaySectionEdge(final Long id,
                          final Map<String, String> tags,
                          final List<NodeVertex> nodes) {
        this.id = id;
        this.tags = tags;
        this.nodes = nodes;
    }
}