package com.telenav.adapter.utils.connectivity.graph.builder;

import org.jgrapht.graph.DefaultDirectedGraph;

/**
 * @author liviuc
 */
public class DirectedGraphBuilder<V, E> extends GraphBuilder<DefaultDirectedGraph, V, E> {

    @Override
    protected DefaultDirectedGraph<V, E> emptyGraph(final Class<E> edgeType) {
        return new DefaultDirectedGraph<V, E>(edgeType);
    }
}
