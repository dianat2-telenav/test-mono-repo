package com.telenav.adapter.utils.connectivity.graph.builder;

import com.telenav.adapter.utils.connectivity.graph.WaySectionEdge;
import org.apache.spark.api.java.JavaRDD;
import org.jgrapht.graph.AbstractBaseGraph;
import scala.Tuple2;
import scala.Tuple3;

import java.util.List;

/**
 * @author liviuc
 */
public abstract class GraphBuilder<G extends AbstractBaseGraph, V, E> {

    protected abstract G emptyGraph(final Class<E> edgeType);

    public G buildGraph(final JavaRDD<Tuple3<String, String, WaySectionEdge>> graphData,
                        final Class<E> edgeType) {
        final G graph = emptyGraph(edgeType);
        graphData.map(tuple -> new Tuple2<>(tuple._1(), tuple._2()))
                .collect()
                .forEach(tuple -> {
                    graph.addVertex(tuple._1());
                    graph.addVertex(tuple._2());
                    graph.addEdge(tuple._1(), tuple._2());
                });
        return graph;
    }
}
