package com.telenav.adapter.utils.connectivity.service;

import com.telenav.adapter.component.fc.TagService;
import com.telenav.adapter.utils.StringUtils;
import com.telenav.adapter.utils.connectivity.Connectivity;
import com.telenav.adapter.utils.connectivity.ConnectivityMetaData;
import com.telenav.adapter.utils.connectivity.connectivity.checker.DirectedGraphConnectivityChecker;
import com.telenav.adapter.utils.connectivity.connectivity.checker.GraphConnectivityChecker;
import com.telenav.adapter.utils.connectivity.connectivity.checker.UndirectedGraphConnectivityChecker;
import com.telenav.adapter.utils.connectivity.geojson.GeoJson;
import com.telenav.adapter.utils.connectivity.geojson.GeoJsonsProvider;
import com.telenav.adapter.utils.connectivity.graph.NodeVertex;
import com.telenav.adapter.utils.connectivity.graph.WayEdge;
import com.telenav.adapter.utils.connectivity.graph.WaySectionEdge;
import com.telenav.adapter.utils.connectivity.graph.builder.DirectedGraphBuilder;
import com.telenav.adapter.utils.connectivity.graph.builder.UndirectedGraphBuilder;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultUndirectedGraph;
import scala.Tuple3;

import java.util.*;
import java.util.stream.Collectors;

import static com.telenav.adapter.component.fc.TagService.FC_TAG;
import static com.telenav.adapter.utils.connectivity.ConnectivityConstants.COORDINATES_SEPARATOR;
import static org.apache.spark.sql.functions.concat;
import static org.apache.spark.sql.functions.lit;

/**
 * @author liviuc
 */
public class ConnectivityCheckerService {

    private static final VerticesDataService<String> VERTICES_DATA_SERVICE = new StringVerticesDataService();

    public static JavaRDD<Tuple3<String, String, WaySectionEdge>> graphData(final JavaRDD<WaySection> waySections) {
        // Filter the ones with fc tag.
        final JavaRDD<WaySection> fcWaySections = waySections.filter(waySection -> waySection.hasTag(FC_TAG, "1"));

        // Graph data.
        return fcWaySections.map(ConnectivityCheckerService::toWaySectionEdge)
                .flatMap(ConnectivityCheckerService::graphData)
                //.cache()
                 ;
    }

    public static JavaRDD<Tuple3<String, String, WaySectionEdge>> graphDataNoTags(final JavaRDD<WaySection> waySections) {
        // Filter the ones with fc tag.
        final JavaRDD<WaySection> fcWaySections = waySections.filter(waySection -> waySection.hasTag(FC_TAG, "1"))
                .map(waySection -> {
                    final String oneway = waySection.tags().get("oneway");
                    if (oneway != null) {
                        final Map<String, String> newMap = new HashMap<>();
                        newMap.put("oneway", oneway);
                        waySection = waySection.replaceTags(newMap);
                    } else {
                        waySection = waySection.replaceTags(new HashMap<>());
                    }
                    return waySection;
                });

        // Graph data.
        return fcWaySections.map(ConnectivityCheckerService::toWaySectionEdgeNoTags)
                .flatMap(ConnectivityCheckerService::graphData)
                //.cache()
                ;
    }

    public static Connectivity directed(final JavaSparkContext javaSparkContext,
                                        final JavaRDD<WaySection> waySections) {

        final JavaRDD<Tuple3<String, String, WaySectionEdge>> graphData = graphData(waySections);

        // Way sections with fc - i.e. future graph edges.
        final JavaRDD<WaySectionEdge> waySectionEdges = graphData.map(tuple -> tuple._3()).cache();

        final DefaultDirectedGraph directedGraph =
                new DirectedGraphBuilder<String, WaySectionEdge>().buildGraph(graphData, WaySectionEdge.class);

        // Compute directed graph geojsons.
        final GraphConnectivityChecker directedGraphConnectivityChecker =
                new DirectedGraphConnectivityChecker<String, WaySectionEdge>().graph(directedGraph);

        final ConnectivityMetaData metaData = directedGraphConnectivityChecker.metaData();
        final JavaRDD<GeoJson> geojsons =
                new GeoJsonsProvider<String, WaySectionEdge>()
                        .geojsons(waySectionEdges, VERTICES_DATA_SERVICE, directedGraphConnectivityChecker, javaSparkContext)
                        .filter(Objects::nonNull);

        return Connectivity.builder()
                .geojsons(geojsons)
                .metaData(metaData)
                .build();
    }

    public static Connectivity undirected(final JavaSparkContext javaSparkContext,
                                          final JavaRDD<WaySection> waySections) {

        final JavaRDD<Tuple3<String, String, WaySectionEdge>> graphData = graphData(waySections);

        // Way sections with fc - i.e. future graph edges.
        final JavaRDD<WaySectionEdge> waySectionEdges = graphData.map(tuple -> tuple._3()).cache();

        final DefaultUndirectedGraph undirectedGraph =
                new UndirectedGraphBuilder<String, WaySectionEdge>().buildGraph(graphData, WaySectionEdge.class);

        // Compute undirected graph geojsons.
        final GraphConnectivityChecker undirectedGraphConnectivityChecker =
                new UndirectedGraphConnectivityChecker<String, WaySectionEdge>().graph(undirectedGraph);

        final ConnectivityMetaData metaData = undirectedGraphConnectivityChecker.metaData();
        final JavaRDD<GeoJson> geojsons =
                new GeoJsonsProvider<String, WaySectionEdge>()
                        .geojsons(waySectionEdges, VERTICES_DATA_SERVICE, undirectedGraphConnectivityChecker, javaSparkContext)
                        .filter(Objects::nonNull);

        return Connectivity.builder()
                .geojsons(geojsons)
                .metaData(metaData)
                .build();
    }

    public static void saveGeojsons(final JavaRDD<GeoJson> geoJsonMeta,
                                    final String outputPath,
                                    final JavaSparkContext javaSparkContext) {

        final JavaRDD<GeoJson> cached = geoJsonMeta.cache();

        final JavaRDD<GeoJson> smallComponents = cached.filter(meta -> meta.isSmallComponent());

        final JavaRDD<GeoJson> largeComponents = cached.filter(meta -> !meta.isSmallComponent());

        final SQLContext sqlContext = new SQLContext(javaSparkContext.sc());

        saveGeojsons(smallComponents, outputPath + "/smallComponents/", sqlContext);
        saveGeojsons(largeComponents, outputPath + "/largeComponents/", sqlContext);
    }

    private static void saveGeojsons(final JavaRDD<GeoJson> components,
                                     final String outputPath,
                                     final SQLContext sqlContext) {

        final Dataset<Row> dataFrame = sqlContext.createDataFrame(components, GeoJson.class);
        dataFrame.withColumn("componentToNumberOfEdges",
                concat(dataFrame.col("connectedComponentId"), lit("_"), dataFrame.col("numberOfEdges")))
                .select("content", "componentToNumberOfEdges")
                .write()
                .mode(SaveMode.Overwrite)
                .partitionBy("componentToNumberOfEdges")
                .text(outputPath);
    }

    public static List<Tuple3<String, String, WayEdge>> graphData(final WayEdge wayEdge) {

        final NodeVertex firstNodeVertex = wayEdge.getNodes().get(0);

        final String startPoint = StringUtils.concat(firstNodeVertex.getLatitude(), COORDINATES_SEPARATOR, firstNodeVertex.getLongitude());

        final NodeVertex lastNodeVertex = wayEdge.getNodes().get(wayEdge.getNodes().size() - 1);

        final String endPoint = StringUtils.concat(lastNodeVertex.getLatitude(), COORDINATES_SEPARATOR, lastNodeVertex.getLongitude());

        final String oneway = wayEdge.getOneway();

        List<Tuple3<String, String, WayEdge>> result = null;

        // If it's a oneway we add data corresponding to an edge directed from the startPoint to the endPoint.
        if ("yes".equalsIgnoreCase(oneway)) {

            result = Collections.singletonList(new Tuple3<>(startPoint, endPoint, wayEdge));

            // If it's a oneway we add data corresponding to an edge directed from the endPoint to the startPoint.
        } else if ("-1".equalsIgnoreCase(oneway)) {

            result = Collections.singletonList(new Tuple3<>(endPoint, startPoint, wayEdge));

            // Else it's a non-oneway, so we add data corresponding to two edges directed both ways.
        } else {

            result = Arrays.asList(new Tuple3<>(startPoint, endPoint, wayEdge),
                    new Tuple3<>(endPoint, startPoint, wayEdge));

        }
        return result;
    }

    private static Iterator<Tuple3<String, String, WaySectionEdge>> graphData(final WaySectionEdge waySectionEdge) {
        final Map<String, String> tags = waySectionEdge.getTags();

        final NodeVertex firstNodeVertex = waySectionEdge.getNodes().get(0);

        final String startPoint = StringUtils.concat(firstNodeVertex.getLatitude(), COORDINATES_SEPARATOR, firstNodeVertex.getLongitude());

        final NodeVertex lastNodeVertex = waySectionEdge.getNodes().get(waySectionEdge.getNodes().size() - 1);

        final String endPoint = StringUtils.concat(lastNodeVertex.getLatitude(), COORDINATES_SEPARATOR, lastNodeVertex.getLongitude());

        final String oneway = TagService.oneway(tags);

        List<Tuple3<String, String, WaySectionEdge>> result = null;

        // If it's a oneway we add data corresponding to an edge directed from the startPoint to the endPoint.
        if ("yes".equalsIgnoreCase(oneway)) {

            result = Collections.singletonList(new Tuple3<>(startPoint, endPoint, waySectionEdge));

            // If it's a oneway we add data corresponding to an edge directed from the endPoint to the startPoint.
        } else if ("-1".equalsIgnoreCase(oneway)) {

            result = Collections.singletonList(new Tuple3<>(endPoint, startPoint, waySectionEdge));

            // Else it's a non-oneway, so we add data corresponding to two edges directed both ways.
        } else {

            result = Arrays.asList(new Tuple3<>(startPoint, endPoint, waySectionEdge),
                    new Tuple3<>(endPoint, startPoint, waySectionEdge));

        }
        return result.iterator();
    }

    private static WaySectionEdge toWaySectionEdgeNoTags(final WaySection waySection) {

        final List<NodeVertex> nodeVertices = new ArrayList<>(waySection.nodes().size());

        for (final Node node : waySection.nodes()) {

            nodeVertices.add(nodeVertex(node));

        }
        return WaySectionEdge.builder()
                .id(waySection.id())
                .nodes(nodeVertices)
                .build();
    }

    private static WaySectionEdge toWaySectionEdge(final WaySection waySection) {

        final List<NodeVertex> nodeVertices = new ArrayList<>(waySection.nodes().size());

        for (final Node node : waySection.nodes()) {

            nodeVertices.add(nodeVertex(node));

        }
        return WaySectionEdge.builder()
                .id(waySection.id())
                .nodes(nodeVertices)
                .tags(waySection.tags())
                .build();
    }

    private static NodeVertex nodeVertex(final Node node) {
        return new NodeVertex(node.id(),
                node.latitude().asDegrees(),
                node.longitude().asDegrees(),
                Collections.emptyMap());
    }

    public static void saveMetaData(final ConnectivityMetaData metaData,
                                    final String outputPath,
                                    final JavaSparkContext javaSparkContext) {
        // Use the spark way of saving.
        final JavaRDD<String> metadata = javaSparkContext.parallelize(metaData.getData()
                .entrySet()
                .stream()
                .map(entry -> entry.getKey() + ":" + entry.getValue())
                .collect(Collectors.toList()));
        metadata.coalesce(1)
                .saveAsTextFile(outputPath + "/metadata/");
    }
}
