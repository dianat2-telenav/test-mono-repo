package com.telenav.adapter.utils.connectivity.service;

import com.telenav.adapter.utils.connectivity.ConnectedComponent;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author liviuc
 */
public class StringVerticesDataService implements VerticesDataService<String> {

    public List<ConnectedComponent> verticesData(final List<Set<String>> connectedSets) {

        final List<ConnectedComponent> connectedComponents = new LinkedList<>();

        long connectedComponentId = 1;

        for (final Set<String> connectedSet : connectedSets) {

            final ConnectedComponent connectedComponent = ConnectedComponent.builder()
                    .id(connectedComponentId++)
                    .vertexes(connectedSet)
                    .build();
            connectedComponents.add(connectedComponent);

        }

        return connectedComponents;
    }
}
