package com.telenav.adapter.utils.connectivity.service;

import com.telenav.adapter.utils.connectivity.ConnectedComponent;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author liviuc
 */
public interface VerticesDataService<V> extends Serializable {

    List<ConnectedComponent> verticesData(final List<Set<V>> connectedSets);

}
