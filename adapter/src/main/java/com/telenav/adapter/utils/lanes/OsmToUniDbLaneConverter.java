package com.telenav.adapter.utils.lanes;

import com.telenav.map.entity.WaySection;

import java.io.Serializable;

public interface OsmToUniDbLaneConverter extends Serializable {

    String[] convert(WaySection waySection, String[] valuesPerLane);
}
