package com.telenav.adapter.utils.lanes;

import com.telenav.map.entity.WaySection;

import static com.telenav.adapter.rules.LaneTypeRule.REGULAR_LANE_VALUE;

public class OsmToUniDbLaneValueConverter implements OsmToUniDbLaneConverter {

    private final String osmTagKey;
    private final String uniDbTagValue;

    public OsmToUniDbLaneValueConverter(final String osmTagKey,
                                        final String uniDbTagValue) {
        this.osmTagKey = osmTagKey;
        this.uniDbTagValue = uniDbTagValue;
    }

    @Override
    public String[] convert(WaySection waySection, final String[] valuesPerLane) {
        final int length = valuesPerLane.length;
        final String[] laneTypes = new String[length];
        //inverse traversal as the lanes in OSM are counted from left to right and in UniDB it's the opposite
        for (int i = 0; i < length; i++) {
            if (valuesPerLane[i].equals("designated")) {
                laneTypes[i] = uniDbTagValue;
            } else {
                laneTypes[i] = REGULAR_LANE_VALUE;
            }
        }
        return laneTypes;
    }
}
