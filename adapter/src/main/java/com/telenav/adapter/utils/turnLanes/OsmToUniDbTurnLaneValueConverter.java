package com.telenav.adapter.utils.turnLanes;

import com.telenav.adapter.utils.lanes.OsmToUniDbLaneConverter;
import com.telenav.map.entity.WaySection;

import static com.telenav.adapter.rules.LaneTypeRule.*;

public class OsmToUniDbTurnLaneValueConverter implements OsmToUniDbLaneConverter {

    @Override
    public String[] convert(WaySection waySection, final String[] valuesPerLane) {
        final String[] laneTypes = new String[valuesPerLane.length];
        for (int i = 0; i < valuesPerLane.length; i++) {
            if (valuesPerLane[i].equals(LANE_TYPES_SEPARATOR)) {
                laneTypes[i] = "|";
                continue;
            }
            try {
                final int valuePerLane = Integer.parseInt(valuesPerLane[i]);
                if (valuePerLane == 0) {
                    laneTypes[i] = REGULAR_LANE_VALUE;
                } else {
                    laneTypes[i] = TURN_LANE_VALUE;
                }
            } catch (final NumberFormatException nfe) {
                throw new NumberFormatException(waySection.toString() + " " + nfe.getMessage());
            }
        }
        return laneTypes;
    }
}
