package com.telenav.adapter.utils.turnLanes;

import com.telenav.adapter.bean.DrivingSide;
import com.telenav.adapter.component.DrivingSideFinder;
import com.telenav.adapter.exception.CorruptWaySectionException;
import com.telenav.map.entity.TagAdjuster;
import com.telenav.map.entity.WaySection;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.util.CollectionAccumulator;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author liviuc
 */
public abstract class OsmToUniDbTurnLanesEnhancer implements Serializable {

    public static final String LANES_FORWARD = "lanes:forward";
    public static final String LANES_BACKWARD = "lanes:backward";
    public static final String LANES_BOTH_WAYS = "lanes:both_ways";
    public static final String TN_LANES_FORWARD = "tn_lanes:forward";
    public static final String TN_LANES_BACKWARD = "tn_lanes:backward";
    public static final String TN_LANES_BOTH_WAYS = "tn_lanes:both_ways";
    public static final String TURN_LANES_FORWARD = "turn:lanes:forward";
    public static final String TURN_LANES_BACKWARD = "turn:lanes:backward";
    public static final String TURN_LANES_BOTH_WAYS = "turn:lanes:both_ways";
    public static final String TURN_LANES_CENTRE_TURN_LANE = "centre_turn_lane";
    public static final String TN_TURN_LANES_FORWARD = "tn_turn:lanes:forward";
    public static final String TN_TURN_LANES_BACKWARD = "tn_turn:lanes:backward";
    public static final String TN_TURN_LANES_BOTH_WAYS = "tn_turn:lanes:both_ways";
    public static final String TN_TURN_LANES_CENTRE_TURN_LANE = "tn_centre_turn_lane";
    public static final String TURN_LANE_SEPARATOR = "|";
    public static final String TURN_LANE_VALUE_SEPARATOR = ";";

    /*
        Saving invalid values to be able to identify them on the fly
        to spare time when computing.
     */
    private static final Set<String> INVALID_LANE_VALUES_CACHE = new HashSet<>();

    private static final Map<String, Integer> TURN_LANES_MAPPING = Arrays.stream(
            new String[][]{
                    {"none", "0"},
                    {"", "0"},
                    {"through", "1"},
                    {"straight", "1"},
                    {"right", "4"},
                    {"slight_right", "2"},
                    {"sharp_right", "8"},
                    {"sharp_left", "32"},
                    {"left", "64"},
                    {"slight_left", "128"},
                    {"merge_to_left", "512"},
                    {"merge_to_right", "2048"}
            }
    ).collect(Collectors.toMap(keyMapper -> keyMapper[0],
            valueMapper -> Integer.parseInt(valueMapper[1])));

    private static final Map<String, Integer> REVERSE_TURN_LANES_MAPPING = Arrays.stream(
            new String[][]{
                    {"RIGHT", "16"},
                    {"LEFT", "256"}
            }
    ).collect(Collectors.toMap(keyMapper -> keyMapper[0],
            valueMapper -> Integer.parseInt(valueMapper[1])));
    private CollectionAccumulator<String> invalidLaneValues;

    protected OsmToUniDbTurnLanesEnhancer(final CollectionAccumulator<String> invalidLaneValues) {
        this.invalidLaneValues = invalidLaneValues;
    }

    protected abstract String getNewTag();

    protected abstract String getNewTagTemp();

    protected abstract String getOldTag();

    protected abstract boolean revertLanes();

    protected abstract String getLaneTypeName();

    private int getNumberOfLanes(Map<String, String> tags) {
        final String lanes = getLaneTypeName();
        final String lanesValue = tags.get(lanes);
        return (lanesValue != null) ? Integer.parseInt(lanesValue) : 0;
    }

    public <T extends TagAdjuster<T>> T enhanceFromTurnLane(final Optional<String> turnLaneValue,
                                                            final T entity) {
        return enhanceFromTurnLane(turnLaneValue,
                entity,
                getNewTag(),
                getOldTag());
    }

    private <T extends TagAdjuster<T>> T enhanceFromTurnLane(final Optional<String> turnLaneValueOptional,
                                                             final T entity,
                                                             final String newTag,
                                                             final String oldTag) {
        if (turnLaneValueOptional.isPresent()) {
            final Map<String, String> tags = entity.tags();
            final DrivingSide drivingSide = drivingSide(entity);
            addTags(tags,
                    newTag,
                    oldTag,
                    turnLaneValueOptional.get(),
                    drivingSide);
            return entity.replaceTags(tags);
        }
        return entity;
    }

    private DrivingSide drivingSide(final TagAdjuster entity) {
        // A little hack to avoid polymorphic over-engineering.
        if (WaySection.class.isInstance(entity)) {
            return DrivingSideFinder.getDrivingSideFromWaySection((WaySection) entity);
        }
        // Raw ways have no driving side since they are non-navigable
        return null;
    }

    public <T extends TagAdjuster<T>> T enhanceFromLaneNumber(final T entity) {
        final Map<String, String> tags = entity.tags();
        final String newTag = getNewTagTemp();

        int lanesNumber = getNumberOfLanes(tags);

        final StringBuilder newTagValueSb = new StringBuilder();
        for (int i = 0; i < lanesNumber; i++) {
            newTagValueSb.append("0");
            if (i < lanesNumber - 1) {
                newTagValueSb.append(TURN_LANE_SEPARATOR);
            }
        }
        tags.put(newTag, newTagValueSb.toString());
        return entity.replaceTags(tags);
    }

    private void addTags(final Map<String, String> tags,
                         final String newTag,
                         final String oldTag,
                         final String oldValue,
                         final DrivingSide drivingSide) {

        final LinkedList<String> lanes = laneParts(oldValue);

        checkNonInvalidValue(lanes);

        final String compact = compact(convertTurnLanes(lanes, drivingSide));
        if (!StringUtils.isEmpty(compact)) {
            tags.put(newTag, compact);
            //TODO remove old turn lanes tag, leaving for now for testing purposes
            tags.put(oldTag, oldValue);
        }
    }

    public String compact(final LinkedList<Integer> turnLanesAsInts) {
        final Spliterator<Integer> spliterator = (revertLanes()) ?
                Spliterators.spliteratorUnknownSize(turnLanesAsInts.descendingIterator(), Spliterator.ORDERED) :
                Spliterators.spliteratorUnknownSize(turnLanesAsInts.iterator(), Spliterator.ORDERED);
        return StreamSupport
                .stream(spliterator, false)
                .map(Object::toString)
                .collect(Collectors.joining(TURN_LANE_SEPARATOR));
    }

    public LinkedList<String> laneParts(final String turnLaneValue) {

        String[] split = turnLaneValue.split("\\" + TURN_LANE_SEPARATOR, -1);
        for (int i = 0 ; i < split.length; i++) {
            /*
                "fix" OSM inconsistencies to increase valid lanes.
                For eg. " left" can be safely considered "left".
             */
            split[i] = split[i].trim();
        }

        return new LinkedList<>(Arrays.asList(split));
    }

    public LinkedList<Integer> convertTurnLanes(final LinkedList<String> turnLanes,
                                                 final DrivingSide drivingSide) {
        final LinkedList<Integer> asInts = turnLanes.stream()
                .map(turnLane -> mapTurnLaneToInt(turnLane, drivingSide))
                .collect(Collectors.toCollection(LinkedList::new));
        //some of the values are null, then do no conversion (null means string cannot be paired to int)
        return asInts.stream().allMatch(Objects::nonNull) ? asInts : new LinkedList<>();
    }

    private Integer mapTurnLaneToInt(final String turnLane,
                                     final DrivingSide drivingSide) {

        //if turn lane value has more than one value
        final List<Integer> turnLanesAsInts = Arrays
                .stream(turnLane.split(TURN_LANE_VALUE_SEPARATOR))
                .map(lane -> turnLaneValue(lane, drivingSide))
                .collect(Collectors.toList());
        return turnLanesAsInts.stream()
                .mapToInt(turnLaneAsInt -> (turnLaneAsInt != null) ? turnLaneAsInt : 0)
                .sum();
    }

    private Integer turnLaneValue(final String lane,
                                  final DrivingSide drivingSide) {
        Integer turnLaneValue;
        if (isReverse(lane)) {

            turnLaneValue = REVERSE_TURN_LANES_MAPPING.get(drivingSide.name());

        } else {

            turnLaneValue = TURN_LANES_MAPPING.get(lane);
            if (turnLaneValue == null) {

                if (this.invalidLaneValues != null) {
                    this.invalidLaneValues.add(lane);
                }

                INVALID_LANE_VALUES_CACHE.add(lane);
                throwCorruptWaySectionException(lane);
            }
        }
        return turnLaneValue;
    }

    private boolean isReverse(final String turnLane) {
        return "reverse".equals(turnLane);
    }


    protected void checkNonInvalidValue(final List<String> lanes) {
        for (final String lane : lanes) {
            if (INVALID_LANE_VALUES_CACHE.contains(lane)) {
                throwCorruptWaySectionException(lane);
            }
        }
    }

    protected void throwCorruptWaySectionException(final String lane) {
        throw new CorruptWaySectionException("Value " + lane + " is invalid for lane");
    }
}

