package com.telenav.adapter.utils.turnLanes;

import org.apache.spark.util.CollectionAccumulator;

/**
 * @author liviuc
 */
public class OsmToUniDbTurnLanesForwardEnhancer extends OsmToUniDbTurnLanesEnhancer {

    public OsmToUniDbTurnLanesForwardEnhancer() {
        super(null);
    }

    public OsmToUniDbTurnLanesForwardEnhancer(final CollectionAccumulator<String> invalidLaneValues) {
        super(invalidLaneValues);
    }

    @Override
    public String getNewTag() {
        return TURN_LANES_FORWARD;
    }

    @Override
    public String getNewTagTemp() {
        return TN_TURN_LANES_FORWARD;
    }

    @Override
    public String getOldTag() {
        return "tn__original_" + TURN_LANES_FORWARD;
    }

    @Override
    public boolean revertLanes() {
        return true;
    }

    @Override
    public String getLaneTypeName() {
        return LANES_FORWARD;
    }
}
