--
--===============--
--   FC_LEVELS   --
--===============--
-- 1. Valid functional_class values are present
select count(1) > 0 from way_sections
where tags["functional_class"] in ("1", "2", "3", "4", "5");
-- 2. Invalid functional_class values are not present
select count(1) = 0 from way_sections
where tags["functional_class"] not in ("1", "2", "3", "4", "5");
-- 2. Invalid functional_class values are not present