--
-- ================= --
-- NGX_NODES_ADAPTER --
-- ================= --
-- 1. City center rule
select count(*) > 0 from nodes lateral view explode(tags) as key, value where key='type' and value = 'city_center';
--
select count(*) = 0 from (
select * from nodes lateral view explode(tags) as type, type_value where type = 'type' and type_value = 'city_center'
) lateral view explode(tags) as place, place_value where place = 'place' and place_value in('suburb', 'town', 'square', 'city_block', 'quarter', 'locality', 'village', 'isolated_dwelling', 'islet', 'island', 'farm');
--
select count(*) = 0 from (
select id, first(tags), count(*) as cat_id_count, first(city_center_count) as city_center_count from (
select id, first(tags) as tags, count(*) as city_center_count from nodes lateral view explode(tags) as type, type_value where type='type' and type_value='city_center' group by id)
lateral view explode(tags) as cat_id, cat_id_value where cat_id='cat_id' group by id
) where cat_id_count <> city_center_count;
-- 2. Name rule check
select count(*) = 0 from nodes lateral view explode(tags) as key, value where key rlike '^(name:[a-zA-Z]{2})$' and key not in('name:sh', 'name:gr');
-- 3. Clean node rule check
select count(*) = 0 from nodes lateral view explode(tags) as key, value where key in ('crossing', 'highway');
-- 4. ApplicableTo tag rule check
select count(*) = 0 from nodes lateral view explode(tags) as key, value where key="applicable_to" and value not rlike '^((access_through_traffic|bus|delivery|emergency|foot|hov|motorcar|motorcycle|taxi|truck)*(access_through_traffic;|bus;|delivery;|emergency;|foot;|hov;|motorcar;|motorcycle;|taxi;|truck;)*(access_through_traffic|bus|delivery|emergency|foot|hov|motorcar|motorcycle|taxi|truck)+)$';