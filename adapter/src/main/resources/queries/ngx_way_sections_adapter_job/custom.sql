--
--=============================--
--   NGX_WAY_SECTIONS_ADAPTER  --
--=============================--
-- 1. Valid turn:lanes values are present
SELECT COUNT(1) > 0 FROM way_sections
WHERE rlike(tags["turn:lanes"], "(\\d*\\|)?\\d+");
-- 2. Invalid turn:lanes values are not present
-- Need to be uncommented when https://jira.telenav.com:8443/browse/OPT-3215 is done
-- SELECT COUNT(1) = 0 FROM way_sections
-- WHERE NOT rlike(tags["turn:lanes"], "(\\d*\\|)?\\d+");
-- 3. All names are transformed to name:language_code tags
SELECT COUNT(1) = 0
FROM way_sections
WHERE (tags["language_code"] IS NOT NULL AND tags["name"] IS NOT NULL)
  AND tags["name"] != tags[CONCAT("name", ":", tags["language_code"])];
-- 4. All toll tag have only "yes" values
SELECT COUNT(1) = 0
FROM way_sections
WHERE tags["toll"] != "yes";
-- 5. All "junction"="roundabout" have "oneway":"yes" tag
SELECT count(*) = 0
FROM way_sections
WHERE tags["junction"] = "roundabout"
  AND (tags["oneway"] IS NULL OR tags["oneway"] != "yes");
-- 6. All telenav admin tags were removed
SELECT COUNT(1) = 0
FROM way_sections
WHERE tags["tn__l1:left"] IS NOT NULL OR
    tags["tn__l1:right"] IS NOT NULL OR
    tags["tn__l2:left"] IS NOT NULL OR
    tags["tn__l2:right"] IS NOT NULL OR
    tags["tn__l3:left"] IS NOT NULL OR
    tags["tn__l3:right"] IS NOT NULL;
-- 7. Exist uniDB admin level tags
SELECT COUNT(1) > 0
FROM way_sections
WHERE tags["l1:left"] IS NOT NULL OR
    tags["l1:right"] IS NOT NULL OR
    tags["l2:left"] IS NOT NULL OR
    tags["l2:right"] IS NOT NULL OR
    tags["l3:left"] IS NOT NULL OR
    tags["l3:right"] IS NOT NULL;
-- 8. Motorways don't have foot tag
SELECT count(*) = 0
FROM way_sections
WHERE tags["highway"] = "motorway"
  AND tags["foot"] IS NOT NULL;
-- 9. "alt_name" have same suffix as "language" value
SELECT COUNT(1) > 0
FROM way_sections
WHERE tags["language_code"] IS NOT NULL AND tags[CONCAT("alt_name", ":", tags["language_code"])] IS NOT NULL;
-- 10. "frontage" is always "yes
SELECT count(*) = 0
FROM way_sections
WHERE tags["frontage"] IS NOT NULL
  AND tags["frontage"] != "yes";
-- 11. "hov" values are from NGX values
SELECT count(1) = 0
FROM way_sections
WHERE tags["hov"] IS NOT NULL
  AND tags["hov"] NOT IN ("yes", "no", "designated");
-- 12. "dst_pattern" is present
SELECT count(1) > 0
FROM way_sections
WHERE tags["dst_pattern"] IS NOT NULL;
-- 13. "tn__dst_pattern" is removed
SELECT count(1) = 0
FROM way_sections
WHERE tags["tn__dst_pattern"] IS NOT NULL;
-- 14. "language_code" is present
SELECT count(1) > 0
FROM way_sections
WHERE tags["language_code"] IS NOT NULL;
-- 15. "tn__language_code" is removed
SELECT count(1) = 0
FROM way_sections
WHERE tags["tn__language_code"] IS NOT NULL;
-- 16. "iso" is present
SELECT count(1) > 0
FROM way_sections
WHERE tags["iso"] IS NOT NULL;
-- 17. "tn__iso" is removed
SELECT count(1) = 0
FROM way_sections
WHERE tags["tn__iso"] IS NOT NULL;
-- 18. Check if rt, rst and sc aren't not null for all navlinks
SELECT COUNT(1) = 0
FROM way_sections
WHERE tags["rt"] IS NULL
   OR tags["rst"] IS NULL
   OR tags["sc"] IS NULL;
-- 19. "tn__timezone" is removed
SELECT count(1) = 0
FROM way_sections
WHERE tags["tn__timezone"] IS NOT NULL;
-- 20. "adas:driving_side" is present
SELECT count(1) > 0
FROM way_sections
WHERE tags["adas:driving_side"] IS NOT NULL;
-- 21. "highway" doesn't have ("unclassified", "residential_link", "living_street", "road", "steps", "corridor", "cycleway", "construction", "footway", "path") values
SELECT COUNT(1) = 0
FROM way_sections
WHERE tags["highway"] IS NOT NULL
  AND tags["highway"] IN ("unclassified", "residential_link", "living_street", "road", "steps", "corridor", "cycleway", "construction", "footway", "path");
-- 22. Speed category tag has only values from enums
SELECT COUNT(1) = 0
FROM way_sections
WHERE tags["sc"] IS NOT NULL
  AND tags["sc"] NOT IN ("1", "2", "3", "5", "7", "11", "14", "15");
-- 23. Tunnel tag has only "yes" values
SELECT COUNT(1) = 0
FROM way_sections
WHERE tags["tunnel"] IS NOT NULL AND tags["tunnel"] != "yes";
-- 24. Ramp tag has only "Y" or "N" values
select count(1) = 0
from way_sections
where tags["ramp"] IS NOT NULL AND tags["ramp"] NOT IN ("Y", "N");
-- 25. All maxspeed tags have only numeric values
SELECT COUNT(1) = 0
FROM way_sections
WHERE (tags["maxspeed"] IS NOT NULL AND NOT RLIKE(tags["maxspeed"], "\\d+"))
   OR (tags["maxspeed:advisory:backward"] IS NOT NULL AND NOT RLIKE(tags["maxspeed:advisory:backward"], "\\d+"))
   OR (tags["maxspeed:advisory:forward"] IS NOT NULL AND NOT RLIKE(tags["maxspeed:advisory:forward"], "\\d+"))
   OR (tags["maxspeed:backward"] IS NOT NULL AND NOT RLIKE(tags["maxspeed:backward"], "\\d+"))
   OR (tags["maxspeed:backward:conditional"] IS NOT NULL AND NOT RLIKE(tags["maxspeed:backward:conditional"], "\\d+"))
   OR (tags["maxspeed:forward"] IS NOT NULL AND NOT RLIKE(tags["maxspeed:forward"], "\\d+"))
   OR (tags["maxspeed:forward:conditional"] IS NOT NULL AND NOT RLIKE(tags["maxspeed:forward:conditional"], "\\d+"));
-- 26. There are ref_(index):eng values
SELECT COUNT(1) > 0
FROM way_sections
         LATERAL VIEW EXPLODE(tags) way_sections
WHERE RLIKE(key, "^ref_\\d+:eng");
-- 27. tn__ref_(index) values were removed
SELECT COUNT(1) = 0
FROM way_sections
         LATERAL VIEW EXPLODE(tags) way_sections
WHERE RLIKE(key, "^tn__ref_\\d+");
-- 27. There are no tn__ tags
SELECT COUNT(1) = 0
FROM way_sections
         LATERAL VIEW EXPLODE(tags) way_sections
WHERE key LIKE "tn[_]%" AND key NOT IN ("tn__original_id", "tn__images_maxspeed:backward", "tn__images_maxspeed:forward", "tn__probes_oneway", "tn__images_oneway", "tn__language_code", "tn__iso", "tn__iso", "tn__free_flow_speed");
-- 28. There are no rt, rst, sc, functional_class tags if type="carto_line" and railway=*
SELECT COUNT(1) = 0
FROM way_sections
WHERE (tags["type"] = "carto_line" AND tags["railway"] IS NOT NULL)
  AND (tags["rt"] IS NOT NULL OR tags["rst"] IS NOT NULL OR tags["sc"] IS NOT NULL OR tags["functional_class"] IS NOT NULL);
-- 29. There are ref(_index):language:si tags
SELECT COUNT(1) > 0
FROM way_sections
         LATERAL VIEW EXPLODE(tags) way_sections
WHERE rlike(key, "ref(_\\d+)?:[a-z]{3}:si");
-- 30. Lanes tag contains only digits
SELECT COUNT(*) = 0
FROM way_sections
WHERE NOT RLIKE(tags["lanes"], "^\\d+$")
   OR NOT RLIKE(tags["lanes:forward"], "^\\d+$")
   OR NOT RLIKE(tags["lanes:backward"], "^\\d+$");
-- 31. Check if speed limit is divisible by 5
SELECT COUNT(*) = 0
FROM way_sections
WHERE tags["maxspeed:forward"] < 5 OR tags["maxspeed:forward"] % 5 != 0
OR tags["maxspeed:backward"] < 5 OR tags["maxspeed:backward"] % 5 != 0;
-- 32. Check if removed exist speed_unit tags
SELECT COUNT(*) > 0
FROM way_sections
WHERE tags["speed_unit"] IS NOT NULL;
-- 32. Check if oneway contains only "yes", "no" and  "-1" values
SELECT COUNT(*) = 0
FROM way_sections
WHERE tags["oneway"] NOT IN ("yes", "no", "-1");
-- 33. Check if form of way set correctly
SELECT COUNT(*) = 0
FROM way_sections
WHERE (tags["junction"] = "roundabout" AND tags["fow"] != "ROUNDABOUT")
   OR (tags["junction"] != "roundabout" AND tags["highway"] = "motorway" AND tags["fow"] != "MOTORWAY")
   OR (tags["junction"] != "roundabout" AND tags["highway"] IN ("motorway_link", "trunk_link", "primary_link", "secondary_link", "tertiary_link") AND tags["fow"] != "SLIPROAD")
   OR (tags["junction"] != "roundabout" AND tags["highway"] = "unclassified" AND tags["fow"] != "UNDEFINED");
-- 34. Check if all forward oneways don't have backward maxspeed
SELECT COUNT(*) = 0
FROM way_sections
WHERE tags["oneway"] = "yes"
  AND (tags["maxspeed:backward"] IS NOT NULL OR tags["source:maxspeed:backward"] IS NOT NULL);
-- 35. Check if all backward oneways don't have forward maxspeed
SELECT COUNT(*) = 0
FROM way_sections
WHERE tags["oneway"] = "-1"
  AND (tags["maxspeed:forward"] IS NOT NULL OR tags["source:maxspeed:forward"] IS NOT NULL);
-- 36. Check if all maxspeed sources are in posted or derived
SELECT COUNT(*) = 0
FROM way_sections
WHERE tags["source:maxspeed"] NOT IN ("1", "2");
-- 37. Check if type:lanes count equal to turn:lanes count
SELECT COUNT(1) = 0
FROM way_sections
WHERE tags["turn:lanes"] IS NOT NULL
  AND tags["type:lanes"] IS NOT NULL
  AND length(tags["type:lanes"]) - length(replace(tags["type:lanes"], '|')) != length(tags["turn:lanes"]) - length(replace(tags["turn:lanes"], '|'));
-- 38. Check if type:lanes count equal to turn:lanes count
SELECT COUNT(1) = 0
FROM way_sections
WHERE tags["turn:lanes:forward"] IS NOT NULL
  AND tags["type:lanes:forward"] IS NOT NULL
  AND length(tags["type:lanes:forward"]) - length(replace(tags["type:lanes:forward"], '|')) != length(tags["turn:lanes:forward"]) - length(replace(tags["turn:lanes:forward"], '|'));
-- 39. Check if type:lanes count equal to turn:lanes count
SELECT COUNT(1) = 0
FROM way_sections
WHERE tags["turn:lanes:backward"] IS NOT NULL
  AND tags["type:lanes:backward"] IS NOT NULL
  AND length(tags["type:lanes:backward"]) - length(replace(tags["type:lanes:backward"], '|')) != length(tags["turn:lanes:backward"]) - length(replace(tags["turn:lanes:backward"], '|'));
-- 40. Check if type:lanes count equal to turn:lanes count
SELECT COUNT(1) = 0
FROM way_sections
WHERE tags["lanes:forward"] IS NULL
  AND tags["lanes:backward"] IS NULL
  AND tags["lane_cat"] != 1;
-- 41. Check if lane_cat exists if either lanes:forward or lanes:backward exists
SELECT COUNT(*) = 0
FROM way_sections
WHERE (tags["lanes:forward"] IS NOT NULL
    OR tags["lanes:backward"] IS NOT NULL)
  AND tags["lane_cat"] IS NULL;
-- 42. Check if all "lane_cat" set correctly
SELECT COUNT(1) = 0
FROM (
         SELECT GREATEST(IFNULL(INT(tags["lanes:forward"]), 0), IFNULL(INT(tags["lanes:backward"]), 0)) AS num_lanes, tags["lane_cat"] AS lane_cat
         FROM way_sections
         WHERE (tags["lanes:forward"] IS NOT NULL
             OR tags["lanes:backward"] IS NOT NULL)
     )
WHERE (num_lanes < 2 AND lane_cat != 1)
   OR (num_lanes = 2 AND lane_cat != 2)
   OR (num_lanes >= 3 AND lane_cat != 3);