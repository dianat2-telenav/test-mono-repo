--============--
--  Ngx Ways  --
--============--
-- 1. Tags required tags are present
select count(*) > 0 from ways
where tags["type"] = "carto_line";
select count(*) > 0 from ways
where tags["toll"] is not null;
select count(*) > 0 from ways
where tags["l1:left"] is not null;
select count(*) > 0 from ways
where tags["l2:left"] is not null;
select count(*) > 0 from ways
where tags["l3:left"] is not null;
select count(*) > 0 from ways
where tags["l1:right"] is not null;
select count(*) > 0 from ways
where tags["l2:right"] is not null;
select count(*) > 0 from ways
where tags["l3:right"] is not null;
-- 2. Invalid carto_line values are not present
select  count(*) = 0 from (
select tags from ways lateral view explode(tags) as key, value where key = "type" and value = "carto_line")
lateral view explode(tags) as key, value where key in ("rt", "rst", "sc", "functional_class");
-- 3. Tag "motorcar" is assigned to ferries only
select count(1) = 0 from ways
 where tags['motorcar'] is not null and tags['route'] <> 'ferry';
-- 4. Check if all "water" were removed
select count(1) = 0
from ways
where tags["water"] is not null;