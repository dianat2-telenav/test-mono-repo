package com.telenav.adapter;

import com.telenav.adapter.helper.FileWriter;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.unidb.RelationMemberRole;
import com.telenav.map.entity.*;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import com.telenav.relationenhancer.core.turnrestriction.common.RawRelationConstructor;
import com.telenav.relationenhancer.core.turnrestriction.common.bean.IdentifiedMember;
import com.telenav.relationenhancer.core.turnrestriction.common.bean.Member;
import com.telenav.relationenhancer.core.turnrestriction.common.bean.RestrictionInformation;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static com.telenav.adapter.component.fc.TagService.*;
import static com.telenav.adapter.helper.EntityProvider.*;
import static org.junit.Assert.*;

/**
 * @author liviuc
 */
public class AttributesIntegratorJobTest implements Serializable {

    // Temporary paths.
    private static final String TEMP_FOLDER = "src/test/resources/temp";
    private static final String INPUT_FOLDER = TEMP_FOLDER + "/input";
    private static final String WAYS_TEST_DATA = INPUT_FOLDER + "/ways";
    private static final String WAY_SECTIONS_TEST_DATA = INPUT_FOLDER + "/way_sections";
    private static final String NODES_TEST_DATA = INPUT_FOLDER + "/nodes";
    private static final String RELATIONS_TEST_DATA = INPUT_FOLDER + "/relations";
    private static final String OUTPUT_FOLDER = TEMP_FOLDER + "/output";
    private static final String WAYS_OUTPUT = OUTPUT_FOLDER + "/ways";
    private static final String WAY_SECTIONS_OUTPUT = OUTPUT_FOLDER + "/way_sections";
    private static final String NODES_OUTPUT = OUTPUT_FOLDER + "/nodes";
    private static final String RELATIONS_OUTPUT = OUTPUT_FOLDER + "/relations";
    // Sidefile paths.
    private static final String SIDEFILE_FOLDER = TEMP_FOLDER + "/sidefile";
    private static final String WAYS_SIDEFILE = SIDEFILE_FOLDER + "/ways";
    private static final String WAY_SECTIONS_SIDEFILE = SIDEFILE_FOLDER + "/way_sections";
    private static final String NODES_SIDEFILE = SIDEFILE_FOLDER + "/nodes";
    private static final String RELATIONS_SIDEFILE = SIDEFILE_FOLDER + "/relations";
    // Config paths.
    private static final String CONFIG_FOLDER = TEMP_FOLDER + "/config";
    private static final String WAYS_CONFIG = CONFIG_FOLDER + "/ways";
    private static final String WAY_SECTIONS_CONFIG = CONFIG_FOLDER + "/way_sections";
    private static final String NODES_CONFIG = CONFIG_FOLDER + "/nodes";
    private static final String RELATIONS_CONFIG = CONFIG_FOLDER + "/relations";
    // Entities ids.
    private static final List<Long> WAY_IDS = Arrays.asList(1L);
    private static final List<Long> NODE_IDS = Arrays.asList(1L);
    private static final List<Long> RELATION_IDS = Arrays.asList(1L);
    // Tags data.
    private static final List<String> WAY_NOT_ADDED_TAGS = Arrays.asList("do_not_add_way_tag");
    private static final List<String> WAY_TAGS_NEW = Arrays.asList("new_way_tag");
    private static final List<String> WAY_TAGS_TO_REMOVE = Arrays.asList("to_remove_way_without_adding_back");
    private static final List<String> WAY_TAGS_FROM_SIDEFILE = Arrays.asList("to_remove_way_and_add_back", "new_way_tag", "do_not_add_way_tag");
    private static final List<String> WAY_TAGS = Arrays.asList("to_remove_way_and_add_back", "to_remove_way_without_adding_back", "new_way_tag");
    private static final List<String> NODE_NOT_ADDED_TAGS = Arrays.asList("do_not_add_node_tag");
    private static final List<String> NODE_TAGS_NEW = Arrays.asList("new_node_tag");
    private static final List<String> NODE_TAGS_TO_REMOVE = Arrays.asList("to_remove_node_without_adding_back");
    private static final List<String> NODE_TAGS_FROM_SIDEFILE = Arrays.asList("to_remove_node_and_add_back", "new_node_tag", "do_not_add_node_tag");
    private static final List<String> NODE_TAGS = Arrays.asList("to_remove_node_and_add_back", "to_remove_node_without_adding_back", "new_node_tag");
    private static final List<String> RELATION_NOT_ADDED_TAGS = Arrays.asList("do_not_add_relation_tag");
    private static final List<String> RELATION_TAGS_NEW = Arrays.asList("new_relation_tag");
    private static final List<String> RELATION_TAGS_TO_REMOVE = Arrays.asList("to_remove_relation_without_adding_back");
    private static final List<String> RELATION_TAGS_FROM_SIDEFILE = Arrays.asList("to_remove_relation_and_add_back", "new_relation_tag", "do_not_add_relation_tag");
    private static final List<String> RELATION_TAGS = Arrays.asList("to_remove_relation_and_add_back", "to_remove_relation_without_adding_back", "new_relation_tag");

    private static JobInitializer jobInitializer;

    @BeforeClass
    public static void init() {
        jobInitializer = new JobInitializer.Builder()
                .appName("AttributesIntegratorJobTest")
                .runLocal(true)
                .build();
    }

    private static Map<String, String> tags(final TagAdjuster ta) {
        return ta.tags();
    }

    @Test
    public void testDefaultAttributeEnhancement() throws IOException {
        final JavaSparkContext sparkContext = jobInitializer.javaSparkContext();

        try {
            // Prepare test data.
            prepareTestData(WAYS_ENTITY_PROVIDER.rdd(WAY_IDS, WAY_TAGS, sparkContext),
                    NODES_ENTITY_PROVIDER.rdd(NODE_IDS, NODE_TAGS, sparkContext),
                    RAW_RELATIONS_ENTITY_PROVIDER.rdd(RELATION_IDS, RELATION_TAGS, sparkContext),
                    sparkContext);

            // Run job.
            AttributesIntegratorJob.main(args());

            // Test the output data.
            assertOutputData(sparkContext);

        } finally {
            deleteOutputFolder(TEMP_FOLDER);
        }
    }

    @Test
    public void testTurnRestrictionsEnhancement() throws IOException {
        // Spark context
        final JavaSparkContext sparkContext = jobInitializer.javaSparkContext();

        try {
            // Way sections
            final WaySection waySection1 = waySection(1, 1, 2);
            final WaySection waySection2 = waySection(2, 2, 3);
            final WaySection waySection3 = waySection(3, 3, 4);
            final WaySection waySection4 = waySection(4, 4, 5);
            final WaySection waySection5 = waySection(5, 5, 6);

            final JavaRDD<WaySection> inputWaySections =
                    sparkContext.parallelize(Arrays.asList(waySection1, waySection2, waySection3, waySection4, waySection5));
            RddWriter.WaySectionWriter.write(sparkContext, inputWaySections, WAY_SECTIONS_TEST_DATA);

            // Relations
            // Relations that should be kept:
            final RawRelation nonTurnRestriction =
                    new RawRelation(100L, Collections.emptyList(), Collections.emptyMap());
            final RawRelation noLeftTurnRelation = turnRestriction(1L,
                    1, 2,
                    1, 2, 3,
                    "no_left_turn");
            final RawRelation noRightTurnRelation = turnRestriction(2L,
                    3, 4,
                    3, 4, 5,
                    "no_right_turn");

            // Sidefile content
            final StringBuilder sidefileContentSb = new StringBuilder();
            // speed_limit,maxspeed,way_section_identifier,direction
            sidefileContentSb.append("no_left_turn,1_1_2_true:2_2_3_true\n");
            sidefileContentSb.append("no_right_turn,3_3_4_true:4_4_5_true\n");
            sidefileContentSb.append("no_u_turn,1_1_2_true:1_1_2_true\n");
            sidefileContentSb.append("no_through,4_4_5_true:5_5_6_true");
            final String sidefileContent = sidefileContentSb.toString();
            FileWriter.writeAttributesJobSidefile(sidefileContent, RELATIONS_SIDEFILE, sparkContext);

            // Config content
            final List<String> configTags = Arrays.asList("turn_restriction_detection");
            FileWriter.writeAttributesJobConfigFile(configTags, RELATIONS_CONFIG, sparkContext);

            RddWriter.RawRelationWriter.write(sparkContext,
                    sparkContext.parallelize(Arrays.asList(nonTurnRestriction, noLeftTurnRelation, noRightTurnRelation)),
                    RELATIONS_TEST_DATA);

            // Run job.
            AttributesIntegratorJob.main(relationsArgs());

            // Assertions
            final JavaRDD<RawRelation> actualRelations = readRelationsOutput(sparkContext).cache();

            // The initial 2 relations were deleted and replaced with 4 new relations resulting from the sidefile.
            assertEquals(5, actualRelations.count());

            actualRelations
                    .foreach(actualRelation -> {
                        final long actualRelationId = actualRelation.id();

                        // assert that initial relations are missing
                        assertFalse(1L == actualRelationId);
                        assertFalse(2L == actualRelationId);
                        if (actualRelationId == 100L) {
                            assertTrue(actualRelation.tags().isEmpty());
                            return;
                        }
                        assertTrue(actualRelationId != -1);

                        final String restriction = actualRelation.tags().get("restriction");
                        switch (restriction) {
                            case "no_u_turn":
                                assertMembers(Arrays.asList("from", "to", "via"),
                                        Arrays.asList("WAY", "WAY", "NODE"),
                                        Arrays.asList(4000000000001000L, 4000000000001000L, 2L),
                                        actualRelation.members());
                                break;
                            case "no_left_turn":
                                assertMembers(Arrays.asList("from", "to", "via"),
                                        Arrays.asList("WAY", "WAY", "NODE"),
                                        Arrays.asList(4000000000001000L, 4000000000002000L, 2L),
                                        actualRelation.members());
                                break;
                            case "no_right_turn":
                                assertMembers(Arrays.asList("from", "to", "via"),
                                        Arrays.asList("WAY", "WAY", "NODE"),
                                        Arrays.asList(4000000000003000L, 4000000000004000L, 4L),
                                        actualRelation.members());
                                break;
                            case "no_through":
                                assertMembers(Arrays.asList("from", "to", "via"),
                                        Arrays.asList("WAY", "WAY", "NODE"),
                                        Arrays.asList(4000000000004000L, 4000000000005000L, 5L),
                                        actualRelation.members());
                                break;
                            case "no_straight_on":
                                break;
                        }

                    });
        } finally {
            deleteOutputFolder(TEMP_FOLDER);
        }
    }

    @Test
    public void testSpeedLimitEnhancement() throws IOException {
        // Spark context
        final JavaSparkContext sparkContext = jobInitializer.javaSparkContext();

        try {
            // Way sections
            final WaySection waySection1 = waySection(1, 1, 2);
            final WaySection waySection2 = waySection(2, 2, 3, "maxspeed:forward", "10");
            final WaySection waySection3 = waySection(3, 3, 4, "maxspeed:backward", "50");
            final WaySection waySection4 = waySection(4, 4, 5, "maxspeed:backward", "50");

            final JavaRDD<WaySection> inputWaySections =
                    sparkContext.parallelize(Arrays.asList(waySection1, waySection2, waySection3, waySection4));
            RddWriter.WaySectionWriter.write(sparkContext, inputWaySections, WAY_SECTIONS_TEST_DATA);

            // Sidefile content
            final StringBuilder sidefileContentSb = new StringBuilder();
            // speed_limit,maxspeed,way_section_identifier,direction
            sidefileContentSb.append("speed_limit,50,1_1_2,false\n");
            sidefileContentSb.append("speed_limit,70,1_1_2,true\n");
            sidefileContentSb.append("speed_limit,60,2_2_3,true");
            final String sidefileContent = sidefileContentSb.toString();
            FileWriter.writeAttributesJobSidefile(sidefileContent, WAY_SECTIONS_SIDEFILE, sparkContext);

            // Config content
            final List<String> configTags = Arrays.asList("speed_limit_detection");
            FileWriter.writeAttributesJobConfigFile(configTags, WAY_SECTIONS_CONFIG, sparkContext);

            // Run job.
            AttributesIntegratorJob.main(waySectionsArgs());

            // Assertions
            readWaySectionsOutput(sparkContext)
                    .foreach(actualWaySection -> {
                        final long actualWayId = actualWaySection.strictIdentifier().wayId();
                        final Map<String, String> actualTags = actualWaySection.tags();
                        if (actualWayId == 1L) {
                            assertSpeedLimits("50",
                                    false,
                                    actualTags);
                            assertSpeedLimits("70",
                                    true,
                                    actualTags);
                        } else if (actualWayId == 2L) {
                            assertSpeedLimits("60",
                                    true,
                                    actualTags);
                        } else if (actualWayId == 3L) {
                            assertEquals(0, actualTags.size());
                        } else if (actualWayId == 4L) {
                            assertEquals(0, actualTags.size());
                        }
                    });
        } finally {
            deleteOutputFolder(TEMP_FOLDER);
        }
    }

    @Test
    public void testOnewayEnhancement() throws IOException {
        // Spark context
        final JavaSparkContext sparkContext = jobInitializer.javaSparkContext();

        try {
            // Way sections
            final WaySection waySection1 = waySection(1, 1, 2);
            final WaySection waySection2 = waySection(2, 2, 3);
            final WaySection waySection3 = waySection(3, 3, 4, "oneway", "yes");
            final WaySection waySection4 = waySection(4, 4, 5, "oneway", "-1");

            final JavaRDD<WaySection> inputWaySections =
                    sparkContext.parallelize(Arrays.asList(waySection1, waySection2, waySection3, waySection4));
            RddWriter.WaySectionWriter.write(sparkContext, inputWaySections, WAY_SECTIONS_TEST_DATA);

            // Sidefile content
            final StringBuilder sidefileContentSb = new StringBuilder();
            // speed_limit,maxspeed,way_section_identifier,direction
            sidefileContentSb.append("oneway,1_1_2,false\n");
            sidefileContentSb.append("oneway,2_2_3,true");
            final String sidefileContent = sidefileContentSb.toString();
            FileWriter.writeAttributesJobSidefile(sidefileContent, WAY_SECTIONS_SIDEFILE, sparkContext);

            // Config content
            final List<String> configTags = Arrays.asList("oneway_detection");
            FileWriter.writeAttributesJobConfigFile(configTags, WAY_SECTIONS_CONFIG, sparkContext);

            // Run job.
            AttributesIntegratorJob.main(waySectionsArgs());

            // Assertions
            readWaySectionsOutput(sparkContext)
                    .foreach(actualWaySection -> {
                        final long actualWayId = actualWaySection.strictIdentifier().wayId();
                        final Map<String, String> actualTags = actualWaySection.tags();
                        if (actualWayId == 1L) {
                            assertOneway("-1", actualTags);
                        } else if (actualWayId == 2L) {
                            assertOneway("yes", actualTags);
                        } else if (actualWayId == 3L) {
                            assertEquals(0, actualTags.size());
                        } else if (actualWayId == 4L) {
                            assertEquals(0, actualTags.size());
                        }
                    });
        } finally {
            deleteOutputFolder(TEMP_FOLDER);
        }
    }

    @Test
    public void testLanesEnhancement() throws IOException {
        // Spark context
        final JavaSparkContext sparkContext = jobInitializer.javaSparkContext();

        try {
            // Way sections
            // Lane integration cases
            // Oneway single enhancement
            final WaySection waySection1 = waySection(1, 1, 2,
                    LANES, "3",
                    "oneway", "yes");
            final WaySection waySection2 = waySection(2, 3, 4,
                    LANES, "3",
                    "oneway", "yes");
            final WaySection waySection3 = waySection(3, 4, 5,
                    LANES, "3",
                    DIVIDER_LANES, "dividerLanesValue",
                    TURN_LANES, "turnLanesValue",
                    TYPE_LANES, "typeLanesValue",
                    "oneway", "-1");
            final WaySection waySection4 = waySection(4, 5, 6,
                    LANES, "2",
                    DIVIDER_LANES, "dividerLanesValue",
                    TURN_LANES, "turnLanesValue",
                    TYPE_LANES, "typeLanesValue",
                    "oneway", "-1");
            // Non-oneway single enhancement
            // With turn lanes and type lanes
            final WaySection waySection5 = waySection(5, 6, 7,
                    LANES, "3",
                    DIVIDER_LANES, "dividerLanesValue",
                    TURN_LANES, "turnLanesValue",
                    TYPE_LANES, "typeLanesValue",
                    "oneway", "no");
            // Without turn lanes and without type lanes
            final WaySection waySection6 = waySection(6, 7, 8,
                    LANES, "3",
                    DIVIDER_LANES, "dividerLanesValue",
                    TURN_LANES, "turnLanesValue",
                    TYPE_LANES, "typeLanesValue",
                    "oneway", "np");
            // Non-oneway multiple enhancement
            // With turn lanes
            final WaySection waySection7 = waySection(7, 8, 9,
                    LANES, "3",
                    DIVIDER_LANES, "dividerLanesValue",
                    TURN_LANES, "turnLanesValue",
                    TYPE_LANES, "typeLanesValue",
                    "oneway", "np");
            // Without turn lanes and without type lanes
            final WaySection waySection8 = waySection(8, 9, 10,
                    LANES, "3",
                    DIVIDER_LANES, "dividerLanesValue",
                    TURN_LANES, "turnLanesValue",
                    TYPE_LANES, "typeLanesValue",
                    "oneway", "no");
            final WaySection waySection9 = waySection(9, 10, 11,
                    LANES, "3",
                    DIVIDER_LANES, "dividerLanesValue",
                    TURN_LANES, "turnLanesValue",
                    TYPE_LANES, "typeLanesValue",
                    "oneway", "no");
            final WaySection waySection10 = waySection(10, 11, 2,
                    LANES, "3",
                    DIVIDER_LANES, "dividerLanesValue",
                    TURN_LANES, "turnLanesValue",
                    TYPE_LANES, "typeLanesValue",
                    "oneway", "yes");
            final WaySection waySection11 = waySection(11, 11, 12,
                    LANES, "3",
                    DIVIDER_LANES, "dividerLanesValue",
                    TURN_LANES, "turnLanesValue",
                    TYPE_LANES, "typeLanesValue",
                    "oneway", "yes");
            final WaySection waySection12 = waySection(12, 12, 13,
                    LANES, "3",
                    DIVIDER_LANES, "dividerLanesValue",
                    TURN_LANES, "turnLanesValue",
                    TYPE_LANES, "typeLanesValue",
                    "oneway", "yes");
            final WaySection waySection13 = waySection(13, 13, 14,
                    LANES, "3",
                    DIVIDER_LANES, "dividerLanesValue",
                    TURN_LANES, "turnLanesValue",
                    TYPE_LANES, "typeLanesValue",
                    "oneway", "yes");

            final JavaRDD<WaySection> inputWaySections =
                    sparkContext.parallelize(Arrays.asList(
                            waySection1,
                            waySection2,
                            waySection3,
                            waySection4,
                            waySection5,
                            waySection6,
                            waySection7,
                            waySection8,
                            waySection9,
                            waySection10,
                            waySection11,
                            waySection12,
                            waySection13
                    ));
            RddWriter.WaySectionWriter.write(sparkContext, inputWaySections, WAY_SECTIONS_TEST_DATA);

            // Sidefile content
            final StringBuilder sidefileContentSb = new StringBuilder();
            // lanes,way_section_identifier,direction,divider:lanes,turn:lanes,type:lanes
            // Oneway single enhancement
            sidefileContentSb.append("lanes,1_1_2,true,3|1|1|3|2,4|0|0|0|64|64,1|1|1|1|2048|2048\n");
            sidefileContentSb.append("lanes,2_3_4,false,2,0|64,1|2048\n");
            sidefileContentSb.append("lanes,3_4_5,true,3|1|1|3|2,4|0|0|0|64|64,\n");
            sidefileContentSb.append("lanes,4_5_6,false,2,,1|2048\n");
            // Non-oneway single enhancement
            // With turn lanes and type lanes
            sidefileContentSb.append("lanes,5_6_7,true,2,0|64,1|2048\n");
            // Without turn lanes and without type lanes
            sidefileContentSb.append("lanes,6_7_8,true,2,,\n");
            // Non-oneway multiple enhancement
            // With turn lanes
            sidefileContentSb.append("lanes,7_8_9,true,2,0|0,0|65536\n");
            sidefileContentSb.append("lanes,7_8_9,false,2|0|1|3,0|0|0|0|0,0|65536|65536|65536|0\n");
            // Without turn lanes and without type lanes
            sidefileContentSb.append("lanes,8_9_10,true,1,,\n");
            sidefileContentSb.append("lanes,8_9_10,false,3,,\n");
            // With one turn lane and without type lanes
            sidefileContentSb.append("lanes,9_10_11,true,1,,\n");
            sidefileContentSb.append("lanes,9_10_11,false,3,64|0,\n");
            // No divider lanes
            sidefileContentSb.append("lanes,11_11_12,true,,,\n");
            sidefileContentSb.append("lanes,12_12_13,false,,64,2048\n");
            sidefileContentSb.append("lanes,13_13_14,true,,,2\n");
            sidefileContentSb.append("lanes,13_13_14,false,,,2");


            final String sidefileContent = sidefileContentSb.toString();
            FileWriter.writeAttributesJobSidefile(sidefileContent, WAY_SECTIONS_SIDEFILE, sparkContext);

            // Config content
            final List<String> configTags = Arrays.asList("lanes_detection");
            FileWriter.writeAttributesJobConfigFile(configTags, WAY_SECTIONS_CONFIG, sparkContext);

            // Run job.
            AttributesIntegratorJob.main(waySectionsArgs());

            // Assertions
            readWaySectionsOutput(sparkContext)
                    .foreach(actualWaySection -> {
                        final long actualWayId = actualWaySection.strictIdentifier().wayId();
                        final Map<String, String> actualTags = actualWaySection.tags();

                        if (actualWayId == 1L) {
                            // Oneway single enhancement
                            // sidefileContentSb.append("lanes,1_1_2,true,3|1|1|3|2,4|0|0|0|64|64,1|1|1|1|2048|2048\n");
                            assertLanesEnhancement("3|1|1|3|2",
                                    "4|0|0|0|64|64",
                                    "2048|1|1|1|2048|2048",
                                    6,
                                    6,
                                    null,
                                    3,
                                    actualTags);
                        } else if (actualWayId == 2L) {
                            // Oneway single enhancement
                            // sidefileContentSb.append("lanes,2_3_4,false,2,0|64,1|2048\n");
                            assertLanesEnhancement("2",
                                    "0|64",
                                    "1|2048",
                                    2,
                                    null,
                                    2,
                                    2,
                                    actualTags);
                        } else if (actualWayId == 3L) {
                            // Oneway single enhancement
                            // sidefileContentSb.append("lanes,3_4_5,true,3|1|1|3|2,4|0|0|0|64|64,\n");
                            assertLanesEnhancement("3|1|1|3|2",
                                    "4|0|0|0|64|64",
                                    "2048|1|1|1|2048|2048",
                                    6,
                                    6,
                                    null,
                                    3,
                                    actualTags);
                        } else if (actualWayId == 4L) {
                            // Oneway single enhancement
                            // sidefileContentSb.append("lanes,4_5_6,false,2,,1|2048\n");
                            assertLanesEnhancement("2",
                                    null,
                                    "1|2048",
                                    2,
                                    null,
                                    2,
                                    2,
                                    actualTags);
                        } else if (actualWayId == 5L) {
                            // Non-oneway single enhancement
                            // With turn lanes and type lanes
                            // sidefileContentSb.append("lanes,5_6_7,true,2,0|64,1|2048\n");
                            assertLanesEnhancement("2",
                                    "0|64|0",
                                    "1|2048|0",
                                    3,
                                    2,
                                    1,
                                    2,
                                    actualTags);
                        } else if (actualWayId == 6L) {
                            // Non-oneway single enhancement
                            // Without turn lanes and without type lanes
                            // sidefileContentSb.append("lanes,6_7_8,true,2,,\n");
                            assertLanesEnhancement("2",
                                    null,
                                    null,
                                    3,
                                    2,
                                    1,
                                    2,
                                    actualTags);
                        } else if (actualWayId == 7L) {
                            // Non-oneway multiple enhancement
                            // With turn lanes
                            // sidefileContentSb.append("lanes,7_8_9,true,2,0|0,0|65536\n");
                            // sidefileContentSb.append("lanes,7_8_9,false,2|0|1|3,0|0|0|0|0,0|65536|65536|65536|0\n");
                            assertLanesEnhancement("2|2|0|1|3",
                                    null, // null because all zero
                                    "0|65536|0|65536|65536|65536|0",
                                    7,
                                    2,
                                    5,
                                    3,
                                    actualTags);
                        } else if (actualWayId == 8L) {
                            // Non-oneway multiple enhancement
                            // Without turn lanes and without type lanes
                            // sidefileContentSb.append("lanes,8_9_10,true,1,,\n");
                            // sidefileContentSb.append("lanes,8_9_10,false,3,,\n");
                            assertLanesEnhancement("1|3",
                                    null,
                                    null,
                                    4,
                                    2,
                                    2,
                                    3,
                                    actualTags);
                        } else if (actualWayId == 9L) {
                            // Non-oneway multiple enhancement
                            // With one turn lane and without type lanes
                            // sidefileContentSb.append("lanes,9_10_11,true,1,,\n");
                            // sidefileContentSb.append("lanes,9_10_11,false,3,64|0,");
                            assertLanesEnhancement("1|3",
                                    "0|64|0", // 0|64|0
                                    "1|2048|1", // 1|2048|1
                                    4,
                                    2,
                                    2,
                                    3,
                                    actualTags);
                        } else if (actualWayId == 10L) {
                            assertLanesEnhancement(null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    actualTags);
                        } else if (actualWayId == 11L) {
                            assertLanesEnhancement(null,
                                    null,
                                    null,
                                    1,
                                    1,
                                    null,
                                    1,
                                    actualTags);
                        } else if (actualWayId == 12L) {
                            assertLanesEnhancement(null,
                                    "64",
                                    "2048",
                                    1,
                                    null,
                                    1,
                                    1,
                                    actualTags);
                        } else if (actualWayId == 13L) {
                            assertLanesEnhancement(null,
                                    null,
                                    "2|2",
                                    2,
                                    1,
                                    1,
                                    2,
                                    actualTags);
                        }
                        assertTrue(actualTags.containsKey("oneway"));
                    });
        } finally {
            deleteOutputFolder(TEMP_FOLDER);
        }
    }

    private JavaRDD<WaySection> readWaySectionsOutput(final JavaSparkContext sparkContext) {
        return RddReader.WaySectionReader.read(sparkContext, WAY_SECTIONS_OUTPUT);
    }

    private JavaRDD<RawRelation> readRelationsOutput(final JavaSparkContext sparkContext) {
        return RddReader.RawRelationReader.read(sparkContext, RELATIONS_OUTPUT);
    }

    private void assertTags(final List<Map<String, String>> actualTagsList,
                            final List<String> removedTags,
                            final List<String> enhancementTags,
                            final List<String> enhancementNewTags,
                            final List<String> notAddedTags) {
        for (final Map<String, String> actualTags : actualTagsList) {
            final Set<String> keySet = actualTags.keySet();
            assertTrue(keySet.containsAll(enhancementTags));
            assertTrue(keySet.containsAll(enhancementNewTags));
            removedTags.stream()
                    .forEach(removedTag -> assertFalse(keySet.contains(removedTag)));
            notAddedTags.stream()
                    .forEach(notAdded -> assertFalse(keySet.contains(notAdded)));
        }
    }

    private WaySection waySection(final long wayId,
                                  final long firstNodeId,
                                  final long lastNodeId,
                                  final String... tags) {
        final Node firstNode = new Node(firstNodeId, Latitude.MAXIMUM, Longitude.MAXIMUM);
        final Node lastNode = new Node(lastNodeId, Latitude.MAXIMUM, Longitude.MAXIMUM);
        final Map<String, String> tagsMap = new TreeMap<>();
        if (tags != null) {
            for (int i = 0; i < tags.length; i += 2) {
                tagsMap.put(tags[i], tags[i + 1]);
            }
        }
        return new WaySection(wayId, Arrays.asList(firstNode, lastNode), tagsMap);
    }

    private void assertOutputData(final JavaSparkContext sparkContext) {

        final JavaRDD<Node> nodes = RddReader.NodeReader.read(sparkContext, NODES_OUTPUT);

        final List<Map<String, String>> nodeTags = tags(nodes);
        assertTags(nodeTags,
                NODE_TAGS_TO_REMOVE,
                finalTagsFromSideFile(NODE_TAGS_FROM_SIDEFILE, NODE_NOT_ADDED_TAGS),
                NODE_TAGS_NEW,
                NODE_NOT_ADDED_TAGS);

        final JavaRDD<RawWay> ways = RddReader.RawWayReader.read(sparkContext, WAYS_OUTPUT);

        final List<Map<String, String>> wayTags = tags(ways);
        assertTags(wayTags,
                WAY_TAGS_TO_REMOVE,
                finalTagsFromSideFile(WAY_TAGS_FROM_SIDEFILE, WAY_NOT_ADDED_TAGS),
                WAY_TAGS_NEW,
                WAY_NOT_ADDED_TAGS);

        final JavaRDD<RawRelation> relations = RddReader.RawRelationReader.read(sparkContext, RELATIONS_OUTPUT);

        final List<Map<String, String>> relationTags = tags(relations);
        assertTags(relationTags,
                RELATION_TAGS_TO_REMOVE,
                finalTagsFromSideFile(RELATION_TAGS_FROM_SIDEFILE, RELATION_NOT_ADDED_TAGS),
                RELATION_TAGS_NEW,
                RELATION_NOT_ADDED_TAGS);
    }

    private List<String> finalTagsFromSideFile(final List<String> tagsFromSidefile,
                                               final List<String> notAddedTags) {
        return tagsFromSidefile.stream()
                .filter(tag -> !notAddedTags.contains(tag))
                .collect(Collectors.toList());
    }

    private List<Map<String, String>> tags(final JavaRDD tagAdjusters) {
        return (List<Map<String, String>>) tagAdjusters.collect()
                .stream()
                .map(val -> AttributesIntegratorJobTest.tags((TagAdjuster) val))
                .collect(Collectors.toList());
    }

    private RawRelation turnRestriction(final long relationId,
                                        final long fromWayId,
                                        final long toWayId,
                                        final long fromWayFirstNodeId,
                                        final long viaNodeId,
                                        final long toWaylastNodeId,
                                        final String turnType) {

        final RestrictionInformation restrictionInformation = RestrictionInformation.builder()
                .id(relationId)
                .numberOfMembers(2)
                .turnType(turnType)
                .forward(true)
                .source(TnSourceValue.TOPOLOGY.unwrap())
                .build();

        final WaySection fromWaySection = waySection(fromWayId, fromWayFirstNodeId, viaNodeId, null);
        final Member fromMember = new Member(fromWaySection.strictIdentifier(), RelationMemberRole.FROM_ROLE.unwrap());
        final IdentifiedMember fromIdentifiedMember = new IdentifiedMember(fromMember, fromWaySection);

        final WaySection toWaySection = waySection(toWayId, viaNodeId, toWaylastNodeId, null);
        final Member toMember = new Member(toWaySection.strictIdentifier(), RelationMemberRole.TO_ROLE.unwrap());
        final IdentifiedMember toIdentifiedMember = new IdentifiedMember(toMember, toWaySection);

        final List<IdentifiedMember> members = Arrays.asList(fromIdentifiedMember, toIdentifiedMember);

        return new RawRelationConstructor(restrictionInformation, members, null)
                .construct()
                .get();
    }

    private void prepareTestData(final JavaRDD<RawWay> ways,
                                 final JavaRDD<Node> nodes,
                                 final JavaRDD<RawRelation> relations,
                                 final JavaSparkContext javaSparkContext) {
        // Write entities.
        RddWriter.NodeWriter.write(javaSparkContext, nodes, NODES_TEST_DATA);
        RddWriter.RawRelationWriter.write(javaSparkContext, relations, RELATIONS_TEST_DATA);
        RddWriter.RawWaysWriter.write(javaSparkContext, ways, WAYS_TEST_DATA);

        // Write config files.
        FileWriter.writeAttributesJobConfigFile(WAY_TAGS, WAYS_CONFIG, javaSparkContext);
        FileWriter.writeAttributesJobConfigFile(NODE_TAGS, NODES_CONFIG, javaSparkContext);
        FileWriter.writeAttributesJobConfigFile(RELATION_TAGS, RELATIONS_CONFIG, javaSparkContext);

        // Write side files.
        FileWriter.writeAttributesJobSidefile(WAY_IDS, WAY_TAGS_FROM_SIDEFILE, WAYS_SIDEFILE, javaSparkContext);
        FileWriter.writeAttributesJobSidefile(NODE_IDS, NODE_TAGS_FROM_SIDEFILE, NODES_SIDEFILE, javaSparkContext);
        FileWriter.writeAttributesJobSidefile(RELATION_IDS, RELATION_TAGS_FROM_SIDEFILE, RELATIONS_SIDEFILE, javaSparkContext);
    }

    private void assertOneway(final String expectedValue,
                              final Map<String, String> actualTags) {
        assertEquals(expectedValue, actualTags.get("oneway"));
    }

    private void assertLanesEnhancement(final String expectedDividerLanes,
                                        final String expectedTurnLanes,
                                        final String expectedTypeLanes,
                                        final Integer expectedLanes,
                                        final Integer expectedLanesFw,
                                        final Integer expectedLanesBw,
                                        final Integer expectedLaneCat,
                                        final Map<String, String> actualTags) {

        if (!StringUtils.isEmpty(expectedDividerLanes)) {
            assertEquals(expectedDividerLanes, actualTags.get(DIVIDER_LANES));
        } else {
            assertFalse(actualTags.containsKey(DIVIDER_LANES));
        }

        if (!StringUtils.isEmpty(expectedTurnLanes)) {
            assertEquals(expectedTurnLanes, actualTags.get(TURN_LANES));
        } else {
            assertFalse(actualTags.containsKey(TURN_LANES));
        }

        if (!StringUtils.isEmpty(expectedTypeLanes)) {
            assertEquals(expectedTypeLanes, actualTags.get(TYPE_LANES));
        } else {
            assertFalse(actualTags.containsKey(TYPE_LANES));
        }

        if (expectedLanes != null) {
            assertEquals(expectedLanes.intValue(), Integer.parseInt(actualTags.get(LANES)));
        } else {
            assertFalse(actualTags.containsKey(LANES));
        }

        if (expectedLanesFw != null) {
            assertEquals(expectedLanesFw.intValue(), Integer.parseInt(actualTags.get(LANES_FORWARD)));
        } else {
            assertFalse(actualTags.containsKey(LANES_FORWARD));
        }

        if (expectedLanesFw != null) {
            assertEquals(expectedLanesFw.intValue(), Integer.parseInt(actualTags.get(LANES_FORWARD)));
        } else {
            assertFalse(actualTags.containsKey(LANES_FORWARD));
        }

        if (expectedLanesBw != null) {
            assertEquals(expectedLanesBw.intValue(), Integer.parseInt(actualTags.get(LANES_BACKWARD)));
        } else {
            assertFalse(actualTags.containsKey(LANES_BACKWARD));
        }

        if (expectedLaneCat != null) {
            assertEquals(expectedLaneCat.intValue(), Integer.parseInt(actualTags.get(LANE_CAT)));
        } else {
            assertFalse(actualTags.containsKey(LANE_CAT));
        }
    }

    private void assertSpeedLimits(final String expectedSpeedLimit,
                                   final boolean forward,
                                   final Map<String, String> actualTags) {
        if (forward) {
            assertEquals(expectedSpeedLimit, actualTags.get("maxspeed:forward"));
        } else {
            assertEquals(expectedSpeedLimit, actualTags.get("maxspeed:backward"));
        }
    }

    private void assertMembers(final List<String> expectedMemberRoles,
                               final List<String> expectedMemberTypes,
                               final List<Long> expectedMemberIds,
                               final List<RawRelation.Member> actualMembers) {
        final int actualMembersSize = actualMembers.size();
        assertEquals(expectedMemberTypes.size(), actualMembersSize);
        assertEquals(expectedMemberRoles.size(), actualMembersSize);
        assertEquals(expectedMemberIds.size(), actualMembersSize);

        for (int i = 0; i < actualMembersSize; i++) {
            final RawRelation.Member actualMember = actualMembers.get(i);
            switch (actualMember.role()) {
                case "from":
                    assertEquals(actualMember.id(), expectedMemberIds.get(0));
                    assertEquals(actualMember.type().toString(), expectedMemberTypes.get(0));
                    break;
                case "to":
                    assertEquals(actualMember.id(), expectedMemberIds.get(1));
                    assertEquals(actualMember.type().toString(), expectedMemberTypes.get(1));
                    break;
                case "via":
                    assertEquals(actualMember.id(), expectedMemberIds.get(2));
                    assertEquals(actualMember.type().toString(), expectedMemberTypes.get(2));
                    break;
            }
        }
    }

    private String[] args() {
        return new String[]{
                "ways_path=" + WAYS_TEST_DATA,
                "nodes_path=" + NODES_TEST_DATA,
                "relations_path=" + RELATIONS_TEST_DATA,
                "ways_sidefile_paths=" + WAYS_SIDEFILE,
                "nodes_sidefile_paths=" + NODES_SIDEFILE,
                "relations_sidefile_paths=" + RELATIONS_SIDEFILE,
                "ways_config_path=" + WAYS_CONFIG,
                "nodes_config_path=" + NODES_CONFIG,
                "relations_config_path=" + RELATIONS_CONFIG,
                "ways_output_path=" + WAYS_OUTPUT,
                "nodes_output_path=" + NODES_OUTPUT,
                "relations_output_path=" + RELATIONS_OUTPUT,
                "way_sections_broadcast_join=true",
                "ways_path_broadcast_join=true",
                "nodes_path_broadcast_join=true",
                "relations_path_broadcast_join=true"
        };
    }

    private String[] waySectionsArgs() {
        return new String[]{
                "way_sections_path=" + WAY_SECTIONS_TEST_DATA,
                "way_sections_sidefile_paths=" + WAY_SECTIONS_SIDEFILE,
                "way_sections_config_path=" + WAY_SECTIONS_CONFIG,
                "way_sections_output_path=" + WAY_SECTIONS_OUTPUT,
        };
    }

    private String[] relationsArgs() {
        return new String[]{
                "tr_way_sections_path=" + WAY_SECTIONS_TEST_DATA,
                "relations_path=" + RELATIONS_TEST_DATA,
                "relations_sidefile_paths=" + RELATIONS_SIDEFILE,
                "relations_config_path=" + RELATIONS_CONFIG,
                "relations_output_path=" + RELATIONS_OUTPUT,
        };
    }

    private void deleteOutputFolder(final String outputPath) throws IOException {
        final File outputFolder = new File(outputPath);
        FileUtils.cleanDirectory(outputFolder);
        FileUtils.deleteDirectory(outputFolder);
    }
}
