package com.telenav.adapter;

import com.telenav.adapter.utils.connectivity.geojson.GeoJson;
import com.telenav.adapter.utils.connectivity.service.ConnectivityCheckerService;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.telenav.adapter.component.fc.TagService.FC_TAG;
import static com.telenav.adapter.component.fc.TagService.ONEWAY_TAG;
import static org.junit.Assert.assertEquals;

/**
 * @author liviuc
 */
public class ConnectivityCheckerTest {

    private static final String TEMP_FOLDER = "src/test/resources/temp";
    private static final String CONNECTIVITY_CHECKER_TEST_DATA = TEMP_FOLDER + "/connectivity";

    private final Map<Long, Node> nodesById = new HashMap<>();
    private JavaSparkContext javaSparkContext;

    @Before
    public void init() {
        final SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.sql.parquet.binaryAsString", "true");
        sparkConf.setMaster("local[1]");
        sparkConf.setAppName("Test");

        this.javaSparkContext =
                JavaSparkContext.fromSparkContext(SparkContext.getOrCreate(sparkConf));
    }

    @After
    public void finish() {
        javaSparkContext.close();
    }

    @Test
    public void testGraphWithoutOneways() {
        final JavaRDD<WaySection> waySections =
                clepsidraGraph(javaSparkContext, "no", "no", "no", "no", "no", "no", "no");

        final JavaRDD<GeoJson> undirected = ConnectivityCheckerService.undirected(javaSparkContext, waySections).getGeojsons();
        final JavaRDD<GeoJson> directed = ConnectivityCheckerService.directed(javaSparkContext, waySections).getGeojsons();

        assertEquals(undirected.count(), 1);
        assertEquals(directed.count(), 1);
    }

    @Test
    public void testGraphWithOneways() {
        final JavaRDD<WaySection> waySections =
                clepsidraGraph(javaSparkContext, "yes", "yes", "no", "yes", "yes", "yes", "yes");

        final JavaRDD<GeoJson> undirected = ConnectivityCheckerService.undirected(javaSparkContext, waySections).getGeojsons();
        final JavaRDD<GeoJson> directed = ConnectivityCheckerService.directed(javaSparkContext, waySections).getGeojsons();

        assertEquals(undirected.count(), 1);
        assertEquals(directed.count(), 1);
    }

    /*
        1-----2
         \   /
           3
           |
           4
         /   \
        6-----5
     */
    private JavaRDD clepsidraGraph(final JavaSparkContext javaSparkContext,
                                   final String... onewayValues) {

        int onewayIdx = 0;

        final List<Node> nodes1 = Arrays.asList(
                node(1, 1, 1),
                node(2, 2, 2));

        final WaySection ws1 = waySection(1, 1, nodes1,
                tags(ONEWAY_TAG, onewayValues[onewayIdx++],
                        FC_TAG, "1", "tn__iso", "USA"));

        final List<Node> nodes2 = Arrays.asList(
                node(2, 2, 2),
                node(3, 3, 3));

        final WaySection ws2 = waySection(2, 1, nodes2,
                tags(ONEWAY_TAG, onewayValues[onewayIdx++],
                        FC_TAG, "1", "tn__iso", "USA"));

        final List<Node> nodes3 = Arrays.asList(
                node(3, 3, 3),
                node(4, 4, 4));

        final WaySection ws3 = waySection(3, 1, nodes3,
                tags(ONEWAY_TAG, onewayValues[onewayIdx++],
                        FC_TAG, "1", "tn__iso", "USA"));

        final List<Node> nodes4 = Arrays.asList(
                node(4, 4, 4),
                node(5, 5, 5));

        final WaySection ws4 = waySection(4, 1, nodes4,
                tags(ONEWAY_TAG, onewayValues[onewayIdx++],
                        FC_TAG, "1", "tn__iso", "USA"));

        final List<Node> nodes5 = Arrays.asList(
                node(5, 5, 5),
                node(6, 6, 6));

        final WaySection ws5 = waySection(5, 1, nodes5,
                tags(ONEWAY_TAG, onewayValues[onewayIdx++],
                        FC_TAG, "1", "tn__iso", "USA"));

        final List<Node> nodes6 = Arrays.asList(
                node(6, 6, 6),
                node(4, 4, 4));

        final WaySection ws6 = waySection(6, 1, nodes6,
                tags(ONEWAY_TAG, onewayValues[onewayIdx++],
                        FC_TAG, "1", "tn__iso", "USA"));

        final List<Node> nodes7 = Arrays.asList(
                node(3, 3, 3),
                node(1, 1, 1));

        final WaySection ws7 = waySection(7, 1, nodes7,
                tags(ONEWAY_TAG, onewayValues[onewayIdx++],
                        FC_TAG, "1", "tn__iso", "USA"));

        return javaSparkContext.parallelize(Arrays.asList(ws1, ws2, ws3, ws4, ws5, ws6, ws7));
    }

    private Node node(final long nodeId,
                      final double lat,
                      final double lon,
                      final String... keysAndValues) {

        if (nodesById.containsKey(nodeId)) {

            return nodesById.get(nodeId);

        }

        final Node node = new Node(nodeId, Latitude.degrees(lat), Longitude.degrees(lon), tags(keysAndValues));

        nodesById.put(nodeId, node);

        return node;
    }

    private Map<String, String> tags(final String... keysAndValues) {

        final Map<String, String> tags = new TreeMap<>();

        for (int i = 0; i < keysAndValues.length; i += 2) {

            tags.put(keysAndValues[i], keysAndValues[i + 1]);

        }

        return tags;
    }

    private WaySection waySection(final long wayId,
                                  final int sequenceNumber,
                                  final List<Node> nodes,
                                  final Map<String, String> tags) {
        return new WaySection(wayId, sequenceNumber, nodes, tags);
    }

    private void clean(final String outputhPath) {
        final File outputDir = new File(outputhPath);
        try {
            FileUtils.cleanDirectory(outputDir);
            FileUtils.deleteDirectory(outputDir);
        } catch (final IOException e) {
        }
    }
}
