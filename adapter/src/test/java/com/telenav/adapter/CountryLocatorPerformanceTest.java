package com.telenav.adapter;

import com.telenav.adapter.component.CountryLocatorHolder;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.fcd.locator.CountryLocator;
import com.telenav.map.entity.Node;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * The purpose of this test is to justify the usage of the CountryLocatorHolder class.
 * The test assesses that by using the CountryLocatorHolder which acts as a cache we achieve better performance
 * than by using a CountryLocator object.
 *
 * @author liviuc .
 */
public class CountryLocatorPerformanceTest {

    @Test
    @Ignore
    public void test() {

        final List<Double> coordinates = Arrays.asList(// San Francisco
                37.7749, -122.4194,
                // New York
                40.7128, -74.0060,
                // Miami
                25.7617, -80.1918,
                // Seattle
                47.6062, -122.3321
        );

        // Generate new coordinates
        final List<Double> newCoordinates = new LinkedList<>();
        for (int i = 0; i < coordinates.size() - 1; i += 2) {

            for (double coordinatesIncrement = 0.001; coordinatesIncrement < 1; coordinatesIncrement += 0.001) {

                double newLatitude = coordinates.get(i) + coordinatesIncrement;
                double newLongitude = coordinates.get(i + 1) + coordinatesIncrement;

                newCoordinates.add(newLatitude);
                newCoordinates.add(newLongitude);
            }
        }

        final List<Node> nodes = new LinkedList<>();
        for (int i = 0; i < newCoordinates.size() - 1; i += 2) {

            nodes.add(new Node(i, Latitude.degrees(newCoordinates.get(i)), Longitude.degrees(newCoordinates.get(i + 1))));
        }

        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Adapter Job - NGX nodes")
                .runLocal(Boolean.valueOf(true))
                .build();
        final JavaSparkContext javaSparkContext = jobInitializer.javaSparkContext();

        final JavaRDD<Node> nodesRdd = javaSparkContext.parallelize(nodes);

        final VoidFunction<Node> countryLocatorHolderFunc =
                node -> CountryLocatorHolder.getCountryForCoordinates(node.latitude().asDegrees(), node.longitude().asDegrees());

        CountryLocator countryLocator = null;
        try {
            countryLocator = new CountryLocator();
        } catch (IOException e) {
            e.printStackTrace();
        }
        final CountryLocator finalCountryLocator = countryLocator;


        final VoidFunction<Node> countryLocatorFunc =
                node -> finalCountryLocator.getCountryForCoordinates(node.latitude().asDegrees(), node.longitude().asDegrees());

        final long countryLocatorHolderFuncDuration = execute(countryLocatorHolderFunc, nodesRdd, 2);
        System.out.println("countryLocatorHolderFunc: " + countryLocatorHolderFuncDuration);
        final long countryLocatorFuncDuration = execute(countryLocatorFunc, nodesRdd, 2);
        System.out.println("countryLocatorFunc: " + countryLocatorFuncDuration);

        assertTrue(countryLocatorHolderFuncDuration * 10 < countryLocatorFuncDuration);
    }

    private long execute(final VoidFunction<Node> func,
                         final JavaRDD<Node> nodesRdd,
                         final int times) {

        final long t0 = System.currentTimeMillis();

        for (int i = 0; i < times; i++) {
            nodesRdd.foreach(func);
        }

        final long t1 = System.currentTimeMillis();

        return (t1 - t0) / 1000;
    }
}
