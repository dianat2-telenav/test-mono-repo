package com.telenav.adapter;

import com.telenav.adapter.component.fc.FcPromotionService;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Test;
import org.locationtech.jts.util.Assert;

import java.util.*;

import static com.telenav.adapter.component.fc.TagService.FC_TAG;
import static com.telenav.adapter.component.fc.TagService.HIGHWAY_TAG;

public class FcPromotionServiceTest {

    private final Map<Long, Node> nodesById = new HashMap<>();

    @Test
    public void testWhenOneWay() {

        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.sql.parquet.binaryAsString", "true");
        sparkConf.setMaster("local[1]");
        sparkConf.setAppName("Test");

        // way sections for way 607583435
        final List<Node> nodes607583435 = Arrays.asList(
                node(5757846037L, 64.0496581, -145.7382640),
                node(65897386L, 64.0457670, -145.7379384));

        final WaySection ws607583435 = waySection(607583435, 1, nodes607583435,
                tags(FC_TAG, "1", HIGHWAY_TAG, "trunk"));

        final JavaSparkContext javaSparkContext =
                JavaSparkContext.fromSparkContext(SparkContext.getOrCreate(sparkConf));

        // way sections for way 619281808_1
        final List<Node> nodes619281808_1 = Arrays.asList(
                node(5852284266L, 64.0400888, -145.7344005),
                node(5852286300L, 64.0408185, -145.7349248));

        final WaySection ws619281808_1 = waySection(619281808, 1, nodes619281808_1,
                tags(//FC_TAG, "1",
                        "oneway", "yes", HIGHWAY_TAG, "trunk"));

        // way sections for way 619281808_2
        final List<Node> nodes619281808_2 = Arrays.asList(
                node(5852286300L, 64.0408185, -145.7349248),
                node(5852286297L, 64.0415348, -145.7354047));

        final WaySection ws619281808_2 = waySection(619281808, 2, nodes619281808_2,
                tags(//FC_TAG, "1",
                        "oneway", "yes", HIGHWAY_TAG, "trunk"));

        // way sections for way 619281808_3
        final List<Node> nodes619281808_3 = Arrays.asList(
                node(5852286297L, 64.0415348, -145.7354047),
                node(5852286295L, 64.0424272, -145.7359495));

        final WaySection ws619281808_3 = waySection(619281808, 3, nodes619281808_3,
                tags(//FC_TAG, "1",
                        "oneway", "yes", HIGHWAY_TAG, "trunk"));

        // way sections for way 619281808_4
        final List<Node> nodes619281808_4 = Arrays.asList(
                node(5852286295L, 64.0424272, -145.7359495),
                node(5852286292L, 64.0430808, -145.7363318));

        final WaySection ws619281808_4 = waySection(619281808, 4, nodes619281808_4,
                tags(//FC_TAG, "1",
                        "oneway", "yes", HIGHWAY_TAG, "trunk"));

        // way sections for way 619281808_5
        final List<Node> nodes619281808_5 = Arrays.asList(
                node(5852286292L, 64.0430808, -145.7363318),
                node(5852286289L, 64.0437779, -145.7367319));

        final WaySection ws619281808_5 = waySection(619281808, 5, nodes619281808_5,
                tags(//FC_TAG, "1",
                        "oneway", "yes", HIGHWAY_TAG, "trunk"));

        // way sections for way 619281808_6
        final List<Node> nodes619281808_6 = Arrays.asList(
                node(5852286289L, 64.0437779, -145.7367319),
                node(5852286287L, 64.0444544, -145.7371206));

        final WaySection ws619281808_6 = waySection(619281808, 6, nodes619281808_6,
                tags(//FC_TAG, "1",
                        "oneway", "yes", HIGHWAY_TAG, "trunk"));

        // way sections for way 619281808_7
        final List<Node> nodes619281808_7 = Arrays.asList(
                node(5852286287L, 64.0444544, -145.7371206),
                node(5852286285L, 64.0455075, -145.7377246));

        final WaySection ws619281808_7 = waySection(619281808, 7, nodes619281808_7,
                tags(//FC_TAG, "1",
                        "oneway", "yes", HIGHWAY_TAG, "trunk"));

        // way sections for way 619281808_8
        final List<Node> nodes619281808_8 = Arrays.asList(
                node(5852286285L, 64.0455075, -145.7377246),
                node(65897386L, 64.0457670, -145.7379384));

        final WaySection ws619281808_8 = waySection(619281808, 8, nodes619281808_8,
                tags(//FC_TAG, "1",
                        "oneway", "yes", HIGHWAY_TAG, "trunk"));

        // way sections for way 619281811
        final List<Node> nodes619281811 = Arrays.asList(
                node(65897405L, 64.0392939, -145.7337725),
                node(5852284266L, 64.0400888, -145.7344005));

        final WaySection ws619281811 = waySection(619281811, 1, nodes619281811,
                tags("oneway", "yes", FC_TAG, "1", HIGHWAY_TAG, "trunk"));

        // way sections for way 959829600
        final List<Node> nodes959829600 = Arrays.asList(
                node(5852284263L, 64.0399238, -145.7344403),
                node(426686766L, 64.0392995, -145.7340435));

        final WaySection ws959829600 = waySection(959829600, 1, nodes959829600,
                tags("oneway", "yes", FC_TAG, "1", HIGHWAY_TAG, "trunk"));

        // way sections for way 619281812_1
        final List<Node> nodes619281812_1 = Arrays.asList(
                node(5852284261L, 64.0402605, -145.7346852),
                node(5852284265L, 64.0400698, -145.7345444));

        final WaySection ws619281812_1 = waySection(619281812, 1, nodes619281812_1,
                tags("oneway", "yes", HIGHWAY_TAG, "trunk", FC_TAG, "3"));

        // way sections for way 619281812_2
        final List<Node> nodes619281812_2 = Arrays.asList(
                node(5852284265L, 64.0400698, -145.7345444),
                node(426686766L, 64.0392995, -145.7340435));

        final WaySection ws619281812_2 = waySection(619281812, 2, nodes619281812_2,
                tags("oneway", "yes", HIGHWAY_TAG, "trunk", FC_TAG, "3"));

        // way sections for way 619281813_1
        final List<Node> nodes619281813_1 = Arrays.asList(
                node(65904100L, 64.0399879, -145.7351325),
                node(5852284265L, 64.0400698, -145.7345444));

        final WaySection ws619281813_1 = waySection(619281813, 1, nodes619281813_1,
                tags("oneway", "yes", HIGHWAY_TAG, "tertiary", FC_TAG, "3"));

        // way sections for way 619281813_2
        final List<Node> nodes619281813_2 = Arrays.asList(
                node(5852284265L, 64.0400698, -145.7345444),
                node(5852284266L, 64.0400888, -145.7344005));

        final WaySection ws619281813_2 = waySection(619281813, 2, nodes619281813_2,
                tags("oneway", "yes", HIGHWAY_TAG, "tertiary", FC_TAG, "3"));

        // way sections for way 619281813_3
        final List<Node> nodes619281813_3 = Arrays.asList(
                node(5852284266L, 64.0400888, -145.7344005),
                node(65891455L, 64.0401754, -145.7337110));

        final WaySection ws619281813_3 = waySection(619281813, 3, nodes619281813_3,
                tags("oneway", "yes", HIGHWAY_TAG, "tertiary", FC_TAG, "3"));

        // way sections for way 619281827_1
        final List<Node> nodes619281827_1 = Arrays.asList(
                node(65879719L, 64.0416097, -145.7348474),
                node(5852286297L, 64.0415348, -145.7354047));

        final WaySection ws619281827_1 = waySection(619281827, 1, nodes619281827_1,
                tags("oneway", "yes", HIGHWAY_TAG, "tertiary", FC_TAG, "3"));

        // way sections for way 619281827_2
        final List<Node> nodes619281827_2 = Arrays.asList(
                node(5852286297L, 64.0415348, -145.7354047),
                node(5852286298L, 64.0415168, -145.7355422));

        final WaySection ws619281827_2 = waySection(619281827, 2, nodes619281827_2,
                tags("oneway", "yes", HIGHWAY_TAG, "tertiary", FC_TAG, "3"));

        // way sections for way 619281827_3
        final List<Node> nodes619281827_3 = Arrays.asList(
                node(5852286298L, 64.0415168, -145.7355422),
                node(4735585906L, 64.0414509, -145.7359917));

        final WaySection ws619281827_3 = waySection(619281827, 3, nodes619281827_3,
                tags("oneway", "yes", HIGHWAY_TAG, "tertiary", FC_TAG, "3"));

        // way sections for way 619281826_1
        final List<Node> nodes619281826_1 = Arrays.asList(
                node(65879665L, 64.0424609, -145.7354077),
                node(5852286295L, 64.0424272, -145.7359495));

        final WaySection ws619281826_1 = waySection(619281826, 1, nodes619281826_1,
                tags("oneway", "yes", HIGHWAY_TAG, "tertiary", FC_TAG, "3"));

        // way sections for way 619281826_2
        final List<Node> nodes619281826_2 = Arrays.asList(
                node(5852286295L, 64.0424272, -145.7359495),
                node(5852286296L, 64.0424095, -145.7360982));

        final WaySection ws619281826_2 = waySection(619281826, 2, nodes619281826_2,
                tags("oneway", "yes", HIGHWAY_TAG, "tertiary", FC_TAG, "3"));

        // way sections for way 619281826_3
        final List<Node> nodes619281826_3 = Arrays.asList(
                node(5852286295L, 64.0424095, -145.7360982),
                node(4735585907L, 64.0423537, -145.7365556));

        final WaySection ws619281826_3 = waySection(619281826, 3, nodes619281826_3,
                tags("oneway", "yes", HIGHWAY_TAG, "tertiary", FC_TAG, "3"));

        final JavaRDD<WaySection> waySections = javaSparkContext.parallelize(Arrays.asList(
                ws619281808_1,
                ws619281808_2,
                ws619281808_3,
                ws619281808_4,
                ws619281808_5,
                ws619281808_6,
                ws619281808_7,
                ws619281808_8,
                ws619281811,
                ws607583435,
                ws959829600,
                ws619281812_1,
                ws619281812_2,
                ws619281813_1,
                ws619281813_2,
                ws619281813_3,
                ws619281827_1,
                ws619281827_2,
                ws619281827_3,
                ws619281826_1,
                ws619281826_2,
                ws619281826_3
        ));

        final Map<Long, String> expectedPromotionFcByWayId = new HashMap<>();
        expectedPromotionFcByWayId.put(619281808L, "1");
        expectedPromotionFcByWayId.put(959829600L, "1");

        final Map<Long, String> expectedUnchangedFcByWayId = new HashMap<>();
        expectedUnchangedFcByWayId.put(607583435L, "1");
        expectedUnchangedFcByWayId.put(619281811L, "1");
        expectedUnchangedFcByWayId.put(619281826L, "3");
        expectedUnchangedFcByWayId.put(619281827L, "3");
        expectedUnchangedFcByWayId.put(619281812L, "3");
        expectedUnchangedFcByWayId.put(619281813L, "3");

        final JavaRDD<WaySection> waySectionsWithPromotion = FcPromotionService.closeGaps2(waySections, javaSparkContext);

        //final List<WaySection> collect = waySectionsWithPromotion.collect();
        //int x = 1;

        waySectionsWithPromotion.foreach(waySection -> {
            final long wayId = waySection.strictIdentifier().wayId();
            Map<Long, String> fcByWayId = null;
            if (expectedPromotionFcByWayId.containsKey(wayId)) {
                fcByWayId = expectedPromotionFcByWayId;
            } else {
                fcByWayId = expectedUnchangedFcByWayId;
            }
            final String expectedFc = fcByWayId.get(wayId);
            final String actualFc = waySection.tags().get(FC_TAG);
            Assert.equals(expectedFc, actualFc);
        });
    }

    private Node node(final long nodeId,
                      final double latitude,
                      final double longitude,
                      final String... keysAndValues) {

        if (nodesById.containsKey(nodeId)) {

            return nodesById.get(nodeId);

        }

        final Node node = new Node(nodeId, Latitude.degrees(latitude), Longitude.degrees(longitude), tags(keysAndValues));

        nodesById.put(nodeId, node);

        return node;

    }

    private Node node(final long nodeId,
                      final String... keysAndValues) {

        if (nodesById.containsKey(nodeId)) {

            return nodesById.get(nodeId);

        }

        final Node node = new Node(nodeId, Latitude.MAXIMUM, Longitude.MAXIMUM, tags(keysAndValues));

        nodesById.put(nodeId, node);

        return node;

    }

    private Map<String, String> tags(final String... keysAndValues) {

        final Map<String, String> tags = new TreeMap<>();

        for (int i = 0; i < keysAndValues.length; i += 2) {

            tags.put(keysAndValues[i], keysAndValues[i + 1]);

        }

        return tags;
    }

    private WaySection waySection(final long wayId,
                                  final int sequenceNumber,
                                  final List<Node> nodes,
                                  final Map<String, String> tags) {
        return new WaySection(wayId, sequenceNumber, nodes, tags);
    }
}
