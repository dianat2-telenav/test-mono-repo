package com.telenav.adapter;

import com.telenav.adapter.component.NodeService;
import com.telenav.adapter.utils.RowUtils;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.RawRelation;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static com.telenav.adapter.utils.OsmKeys.ADMIN_LEVEL_TAG_KEY_IN_OSM;
import static com.telenav.adapter.utils.OsmKeys.PLACE_TAG_KEY_IN_OSM;
import static com.telenav.adapter.utils.UniDbKeys.CAT_ID_TAG_KEY_IN_UNIDB;
import static com.telenav.adapter.utils.UniDbKeys.PLACE_TAG_KEY_IN_UNIDB;
import static com.telenav.datapipelinecore.osm.OsmTagKey.ACCESS;
import static com.telenav.datapipelinecore.unidb.ApplicableToTagValue.applicableToValues;
import static com.telenav.datapipelinecore.unidb.UnidbTagKey.APPLICABLE_TO;
import static org.junit.Assert.*;

public class NgxNodesAdapterApplicationTest extends BaseTest {

    private static JobInitializer jobInitializer;

    @BeforeClass
    public static void initialize() {
        jobInitializer = new JobInitializer.Builder()
                .appName("Tests")
                .runLocal(true)
                .build();
    }

    @Test
    public void testEnhanceByApplicableToTag() {

        final Map<Long, Node> idToNode = getNodesWithTags(
                new HashMap<String, String>() {{
                    put(ACCESS.unwrap(), "yes");
                    put("bus", "no");
                }}
        );

        final Dataset<Row> nodeCityCenterRelations = nodeMembersDataset(idToNode);
        new NgxNodesAdapterApplication()
                .adapt(nodeCityCenterRelations)
                .collectAsList()
                .stream()
                // we have just one element
                .forEach(enhancedNode -> {
                    String applicableTo = RowUtils.extractTags(enhancedNode.getList(enhancedNode.fieldIndex("tags")))
                            .get(APPLICABLE_TO.unwrap());

                    final String expectedApplicableToValue = applicableToValues
                            .stream()
                            .filter(v -> !v.equals("bus"))
                            .collect(Collectors.joining(";"));
                    assertEquals(expectedApplicableToValue, applicableTo);
                });
    }

    @Test
    public void testEnhancePlace() {

        final Map<Long, Node> idToNode = getNodesWithTags(
                Collections.singletonMap(PLACE_TAG_KEY_IN_OSM, "city"),
                Collections.singletonMap(PLACE_TAG_KEY_IN_OSM, "neighborhood"),
                Collections.singletonMap(PLACE_TAG_KEY_IN_OSM, "hamlet")
        );

        final Dataset<Row> nodeCityCenterRelations = nodeMembersDataset(idToNode);
        new NgxNodesAdapterApplication()
                .adapt(nodeCityCenterRelations)
                .collectAsList()
                .stream()
                .forEach(enhancedNode -> {

                    final long nodeId = enhancedNode.getLong(0);

                    final Node initialNode = idToNode.get(nodeId);

                    final Map<String, String> enhancedNodeTags =
                            RowUtils.extractTags(enhancedNode.getList(enhancedNode.fieldIndex("tags")));

                    assertValuesBetweenKeys(initialNode,
                            PLACE_TAG_KEY_IN_OSM,
                            enhancedNodeTags,
                            PLACE_TAG_KEY_IN_UNIDB);
                });
    }

    @Test
    public void testEnhanceCatId() {

        final Map<Long, Node> idToNode = getNodesWithTags(
                Collections.singletonMap(PLACE_TAG_KEY_IN_OSM, "city"),
                Collections.singletonMap(PLACE_TAG_KEY_IN_OSM, "neighborhood"),
                Collections.singletonMap(PLACE_TAG_KEY_IN_OSM, "hamlet"),
                Collections.singletonMap(PLACE_TAG_KEY_IN_OSM, "county")
        );

        final Dataset<Row> nodeCityCenterRelations = nodeMembersDataset(idToNode);
        new NgxNodesAdapterApplication()
                .adapt(nodeCityCenterRelations)
                .collectAsList()
                .stream()
                .forEach(enhancedNode -> {

                    final Map<String, String> enahncedTags = RowUtils.extractTags(enhancedNode.getList(enhancedNode.fieldIndex("tags")));

                    final String catIdValue = enahncedTags.get(CAT_ID_TAG_KEY_IN_UNIDB);

                    final long id = enhancedNode.getLong(0);

                    final Node initialNode = idToNode.get(id);

                    final String placeTagValue = initialNode.tags().get(PLACE_TAG_KEY_IN_OSM);

                    if (placeTagValue.equals("city")) {

                        assertEquals(catIdValue, "4444");

                    } else if (placeTagValue.equals("neighborhood")) {

                        assertEquals(catIdValue, "9709");

                    } else if (placeTagValue.equals("hamlet")) {

                        assertEquals(catIdValue, "9998");

                    } else if (placeTagValue.equals("county")) {

                        assertFalse(getNodesWithTags().containsKey(CAT_ID_TAG_KEY_IN_UNIDB));

                    }
                });
    }

    /*@Test
    public void testEnhanceAddrStreet() {
        initialize();
        final Map<Long, Node> idToNode = getNodesWithTags(
                Collections.singletonMap(ADDRSTREET_TAG_KEY_IN_OSM, "Avenue des Champs-Élysées")
        );
        final JavaRDD<Tuple3<Node, Relation, Relation>> nodes = getNodesRdd(idToNode.values());

        final List<Node> adaptedNodes = new NgxNodesAdapterApplication()
                .adaptWithRelations(nodes, null)
                .collect();

        for (final Node enhancedNode : adaptedNodes) {
            assertValuesBetweenKeys(idToNode.get(enhancedNode.id()),
                    ADDRSTREET_TAG_KEY_IN_OSM,
                    enhancedNode,
                    STREET_NAME_TAG_KEY_IN_UNIDB);
        }
    }

    /*@Test
    public void testEnhanceAdminLevel() {
        initialize();
        final Map<Long, Node> idToNode = getNodesWithTags(
                Collections.singletonMap(ADMIN_LEVEL_TAG_KEY_IN_OSM, "2"),
                Collections.singletonMap(ADMIN_LEVEL_TAG_KEY_IN_OSM, "4"),
                Collections.singletonMap(ADMIN_LEVEL_TAG_KEY_IN_OSM, "6"),
                Collections.singletonMap(ADMIN_LEVEL_TAG_KEY_IN_OSM, "8"),
                Collections.singletonMap(ADMIN_LEVEL_TAG_KEY_IN_OSM, "9"),
                Collections.singletonMap(ADMIN_LEVEL_TAG_KEY_IN_OSM, "10"),
                Collections.singletonMap(ADMIN_LEVEL_TAG_KEY_IN_OSM, "-")

        );
        final JavaRDD<Tuple3<Node, Relation, Relation>> nodes = getNodesRdd(idToNode.values());

        final List<Node> adaptedNodes = new NgxNodesAdapterApplication()
                .adaptWithRelations(nodes, null)
                .collect();

        for (final Node enhancedNode : adaptedNodes) {
            final String adminLevel = enhancedNode.tags().get(ADMIN_LEVEL_TAG_KEY_IN_UNIDB);
            assertNotNull(adminLevel);
            final Node initialNode = idToNode.get(enhancedNode.id());
            assertEquals(ADMIN_LEVEL_OSM_TO_UNI_VALUES.get(initialNode.tags().get(ADMIN_LEVEL_TAG_KEY_IN_OSM)),
                    adminLevel);
        }
    }

    @Test
    public void testEnhanceIso() {
        initialize();
        final Map<Long, Node> idToNode = getNodesWithTags(
                Collections.singletonMap(IS_IN_STATE_CODE_TAG_KEY_IN_OSM, "FR")

        );
        final JavaRDD<Tuple3<Node, Relation, Relation>> nodes = getNodesRdd(idToNode.values());

        final List<Node> adaptedNodes = new NgxNodesAdapterApplication()
                .adaptWithRelations(nodes, null)
                .collect();

        for (final Node enhancedNode : adaptedNodes) {
            assertValuesBetweenKeys(idToNode.get(enhancedNode.id()),
                    IS_IN_STATE_CODE_TAG_KEY_IN_OSM,
                    enhancedNode,
                    ISO_TAG_KEY_IN_UNIDB);
        }
    }

    @Test
    public void testEnhanceCapital() {
        initialize();
        final Map<String, String>[] tagMaps = new Map[CAPITAL_OSM_ALLOWED_VALUES.size()];
        for (int idx = 0; idx < CAPITAL_OSM_ALLOWED_VALUES.size(); idx++) {
            tagMaps[idx] = (Collections.singletonMap(CAPITAL_TAG_KEY_IN_OSM, CAPITAL_OSM_ALLOWED_VALUES.get(idx)));
        }
        final Map<Long, Node> idToNode = getNodesWithTags(tagMaps);
        final JavaRDD<Tuple3<Node, Relation, Relation>> nodes = getNodesRdd(idToNode.values());

        final List<Node> adaptedNodes = new NgxNodesAdapterApplication()
                .adaptWithRelations(nodes, null)
                .collect();

        for (final Node enhancedNode : adaptedNodes) {
            if (idToNode.get(enhancedNode.id()).tags().get(CAPITAL_TAG_KEY_IN_OSM) != null) {
                assertEquals(enhancedNode.tags().get(CAPITAL_TAG_KEY_IN_UNIDB), "yes");
            }
        }
    }

    @Test
    public void testEnhanceCapitalOrder() {
        initialize();
        final Map<String, String>[] tagMaps = new Map[CAPITAL_OSM_ALLOWED_VALUES.size()];
        for (int idx = 0; idx < CAPITAL_OSM_ALLOWED_VALUES.size(); idx++) {
            tagMaps[idx] = (Collections.singletonMap(CAPITAL_TAG_KEY_IN_OSM, CAPITAL_OSM_ALLOWED_VALUES.get(idx)));
        }
        final Map<Long, Node> idToNode = getNodesWithTags(tagMaps);
        final JavaRDD<Tuple3<Node, Relation, Relation>> nodes = getNodesRdd(idToNode.values());

        final List<Node> adaptedNodes = new NgxNodesAdapterApplication()
                .adaptWithRelations(nodes, null)
                .collect();

        for (final Node enhancedNode : adaptedNodes) {
            final String capitalTagValueInOsm = idToNode.get(enhancedNode.id()).tags().get(CAPITAL_TAG_KEY_IN_OSM);
            if (capitalTagValueInOsm != null) {
                final String capitalOrder = ADMIN_LEVEL_UNIDB_VALUE_TO_CAPITAL_ORDER.get(capitalTagValueInOsm);
                if (capitalOrder != null) {
                    final List<String> capitalOrders = enhancedNode.tags()
                            .keySet()
                            .stream()
                            .filter(tagKey -> tagKey.contains("CAPITAL_ORDER"))
                            .collect(Collectors.toList());
                    if (!capitalOrders.isEmpty()) {
                        assertEquals(capitalOrders.size(), 1);
                        final String capitalOrderUniDbKey = capitalOrders.get(0);
                        assertEquals(enhancedNode.tags().get(capitalOrderUniDbKey), "yes");
                        assertEquals(ADMIN_LEVEL_UNIDB_VALUE_TO_CAPITAL_ORDER.get(capitalTagValueInOsm),
                                capitalOrderUniDbKey);
                    }
                }
            }
        }
    }*/

    private void assertValuesBetweenKeys(final Node initialNode,
                                         final String initialKey,
                                         final Map<String, String> enhancedTags,
                                         final String enhancedKey) {

        final String enhancedValue = enhancedTags.get(enhancedKey);

        assertNotNull(enhancedValue);

        final String initialValue = initialNode.tags().get(initialKey);

        assertEquals(initialValue, enhancedValue);
    }

    private Dataset<Row> nodeMembersDataset(final Map<Long, Node> nodeToId) {

        final JavaSparkContext javaSparkContext = jobInitializer.javaSparkContext();
        final SQLContext sqlContext = new SQLContext(javaSparkContext);

        final Dataset<Row> nodesDataset =
                nodeRddToDataSet(javaSparkContext.parallelize(new ArrayList<>(nodeToId.values())), sqlContext);

        final Dataset<Row> relationsDataset =
                relationRddToDataSet(javaSparkContext.parallelize(relations(nodeToId.keySet())), sqlContext);

        final Dataset<Row> nodeCityCenterRelations = RelationService.nodeCityCenterRelations(relationsDataset);

        return NodeService.joinNodesToCityCenterRelations(nodesDataset, nodeCityCenterRelations);
    }

    private List<RawRelation> relations(final Set<Long> nodeMemberIds) {
        final List<RawRelation> relationsList = new ArrayList<>();
        for (final Long nodeId : nodeMemberIds) {

            final long relationId = nodeId;

            final Map<String, String> relationTags = new TreeMap<>();
            /*
                We create a single relation with admin_level = 6 which defines that the relation corresponds to a city.
            */
            relationTags.put(ADMIN_LEVEL_TAG_KEY_IN_OSM, "6");

            final RawRelation.Member member = new RawRelation.Member(nodeId, RawRelation.MemberType.NODE, "admin_centre");

            final List<RawRelation.Member> members = Arrays.asList(member);

            final RawRelation relation = new RawRelation(relationId, members, relationTags);
            relationsList.add(relation);
            //memberRelations(nodeId);
        }
        return relationsList;
    }

    private Map<Long, Node> getNodesWithTags(final Map<String, String>... tagsMap) {

        int nodeId = 1;

        final Map<Long, Node> idToNode = new LinkedHashMap<>();

        for (final Map tags : tagsMap) {

            final Node node = node(nodeId++).withTags(tags);

            idToNode.put(node.id(), node);

        }
        return idToNode;
    }
}
