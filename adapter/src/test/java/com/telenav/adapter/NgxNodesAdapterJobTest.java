package com.telenav.adapter;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.map.entity.*;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static com.telenav.adapter.utils.OsmKeys.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author liviuc
 */
@RunWith(JUnitParamsRunner.class)
public class NgxNodesAdapterJobTest {

    // Temporary paths.
    private static final String TEMPORARY_PARQUET_FOLDER = "src/test/resources/temp";
    // Entities ids.
    private static final List<Long> WAY_SECTION_IDS = Arrays.asList(3L);
    private static final List<Long> WAY_IDS = Arrays.asList(9L);

    private static final Map<Long, List<Long>> WAY_SECTIONS_NODE_IDS = new HashMap<>();
    private static final Map<Long, List<Long>> WAYS_NODE_IDS = new HashMap<>();
    private static final List<Long> NODE_IDS = Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L);
    private static final List<Long> RELATION_IDS = Arrays.asList(11L, 22L, 33L, 44L, 55L, 66L);
    private static final String NODES_INPUT = TEMPORARY_PARQUET_FOLDER + "/NgxNodesAdapterJob/input/nodes";
    private static final String WAYS_INPUT = TEMPORARY_PARQUET_FOLDER + "/NgxNodesAdapterJob/input/ways";
    private static final String WAY_SECTIONS_INPUT = TEMPORARY_PARQUET_FOLDER + "/NgxNodesAdapterJob/input/waySections";
    private static final String RELATIONS_INPUT = TEMPORARY_PARQUET_FOLDER + "/NgxNodesAdapterJob/input/relations";
    private static final String OUTPUT = TEMPORARY_PARQUET_FOLDER + "/NgxNodesAdapterJob/output";
    private static Map<Long, Node> inputNodes = new HashMap<>();
    private static Map<Long, Node> enhancedNodes;

    static {
        WAY_SECTIONS_NODE_IDS.put(3L, Arrays.asList(4L, 6L));
        WAYS_NODE_IDS.put(9L, Arrays.asList(7L, 8L));
    }

    @BeforeClass
    public static void init() throws IOException {
        if (new File(TEMPORARY_PARQUET_FOLDER).exists()) {
            Files.walk(Paths.get(TEMPORARY_PARQUET_FOLDER))
                    .sorted(Comparator.reverseOrder())
                    .map(path -> path.toFile())
                    .forEach(File::delete);
        }
        if (!new File(TEMPORARY_PARQUET_FOLDER).mkdirs()) {
            throw new IOException(String.format("Temporary directory %s could not be created", TEMPORARY_PARQUET_FOLDER));
        }

        try (SparkSession sparkSession = new JobInitializer.Builder()
                .appName("AttributesIntegratorJobTest")
                .runLocal(true)
                .build().sparkSession()) {
            prepareTestData(nodesProvider(NODE_IDS),
                    waySectionsProvider(WAY_SECTION_IDS),
                    waysProvider(WAY_IDS),
                    relationsProvider(RELATION_IDS),
                    JavaSparkContext.fromSparkContext(sparkSession.sparkContext()));
        }

        NgxNodesAdapterJob.main(args());

        enhancedNodes = readParquetNodesToInMemoryMap();
    }

    @AfterClass // Comment this if for whatever reason there is a need for the generated parquet files
    public static void clean() throws IOException {
        Files.walk(Paths.get(TEMPORARY_PARQUET_FOLDER))
                .sorted(Comparator.reverseOrder())
                .map(path -> path.toFile())
                .forEach(File::delete);
    }

    private static Map<Long, Node> readParquetNodesToInMemoryMap() {
        try (SparkSession sparkSession = new JobInitializer.Builder()
                .appName("AttributesIntegratorJobTest")
                .runLocal(true)
                .build().sparkSession()) {
            final JavaRDD<Node> enhancedNodes = RddReader.NodeReader
                    .read(JavaSparkContext.fromSparkContext(sparkSession.sparkContext()),
                            OUTPUT + "/parquet");

            return enhancedNodes
                    .mapToPair(node -> new Tuple2<>(node.id(), node))
                    .collectAsMap();
        }
    }

    private static void prepareTestData(final EntityProvider<Node> nodesProvider,
                                        final EntityProvider<WaySection> waySectionsProvider,
                                        final EntityProvider<RawWay> waysProvider,
                                        final EntityProvider<RawRelation> relationsProvider,
                                        final JavaSparkContext javaSparkContext) {
        // Write entities.
        RddWriter.NodeWriter.write(javaSparkContext, nodesProvider.rdd(javaSparkContext), NODES_INPUT);

        RddWriter.WaySectionWriter.write(javaSparkContext, waySectionsProvider.rdd(javaSparkContext), WAY_SECTIONS_INPUT);

        RddWriter.RawWaysWriter.write(javaSparkContext, waysProvider.rdd(javaSparkContext), WAYS_INPUT);

        RddWriter.RawRelationWriter.write(javaSparkContext, relationsProvider.rdd(javaSparkContext), RELATIONS_INPUT);
    }

    private static String[] args() {

        return new String[]{
                "nodes_parquet=" + NODES_INPUT,
                "relations_parquet=" + RELATIONS_INPUT,
                "way_sections_parquet=" + WAY_SECTIONS_INPUT,
                "ways_parquet=" + WAYS_INPUT,

                "compute_stats=" + false,
                "saveAsText=" + false,
                "map_data_provided=" + false,

                "output_dir=" + OUTPUT,
                "run_local=" + true
        };
    }

    private static EntityProvider<Node> nodesProvider(final List<Long> ids) {
        return () -> {
            final List<Node> nodes = new ArrayList<>();

            final Node node4 =
                    new Node(4L, Latitude.MAXIMUM, Longitude.MAXIMUM);
            nodes.add(node4);
            inputNodes.put(node4.id(), node4);

            final Node node5 =
                    new Node(150959789L, Latitude.degrees(38.5810606), Longitude.degrees(-121.4938950))
                            .withTags(tags("name", "Sacramento",
                                    PLACE_TAG_KEY_IN_OSM, "neighborhood",
                                    ADMIN_LEVEL_TAG_KEY_IN_OSM, "6",
                                    CAPITAL_TAG_KEY_IN_OSM, "4",
                                    IS_IN_STATE_CODE_TAG_KEY_IN_OSM, "US",
                                    "iso", "USA"));
            nodes.add(node5);
            inputNodes.put(node5.id(), node5);

            final Node node6 =
                    new Node(6L, Latitude.MAXIMUM, Longitude.MAXIMUM);
            nodes.add(node6);
            inputNodes.put(node6.id(), node6);

            final Node node7 =
                    new Node(7L, Latitude.MAXIMUM, Longitude.MAXIMUM);
            nodes.add(node7);
            inputNodes.put(node7.id(), node7);

            final Node node8 =
                    new Node(8L, Latitude.MAXIMUM, Longitude.MAXIMUM);
            nodes.add(node8);
            inputNodes.put(node8.id(), node8);

            final Node node9 =
                    new Node(9L, Latitude.MAXIMUM, Longitude.MAXIMUM);
            nodes.add(node9);

            final Node node10 =
                    new Node(125027380L, Latitude.degrees(41.5910323), Longitude.degrees(-93.6046655))
                            .withTag("name", "Des Moines")
                            .withTag("name:en", "Des Moines")
                            .withTag("place", "city")
                            .withTag("capital", "4");
            nodes.add(node10);
            inputNodes.put(node10.id(), node10);

            final Node node11 =
                    new Node(153836071L, Latitude.degrees(40.3611643), Longitude.degrees(-83.7596557))
                            .withTag("name", "Bellefontaine")
                            .withTag("place", "town")
                            .withTag("capital", "6");
            nodes.add(node11);
            inputNodes.put(node11.id(), node11);

            return nodes;
        };
    }

    private static Map<String, String> tags(final String... tags) {
        final Map<String, String> map = new HashMap<>();
        for (int i = 0; i < tags.length; i += 2) {
            map.put(tags[i], tags[i + 1]);
        }
        return map;
    }

    private static EntityProvider<WaySection> waySectionsProvider(final List<Long> ids) {
        return () -> {
            final List<WaySection> waySections = new ArrayList<>();
            Iterator<Long> iterator = ids.iterator();
            Map<String, String> tags = new HashMap<>();
            Long wayId = iterator.next();
            final WaySection waySection1 = new WaySection(wayId, nodes(WAY_SECTIONS_NODE_IDS.get(wayId)), tags);
            waySections.add(waySection1);
            return waySections;
        };
    }

    private static EntityProvider<RawWay> waysProvider(final List<Long> ids) {
        return () -> {
            final List<RawWay> rawWays = new ArrayList<>();
            Iterator<Long> iterator = ids.iterator();
            Map<String, String> tags = new HashMap<>();
            Long wayId = iterator.next();
            final RawWay rawWay = new RawWay(wayId, WAYS_NODE_IDS.get(wayId), tags);
            rawWays.add(rawWay);
            return rawWays;
        };
    }

    private static EntityProvider<RawRelation> relationsProvider(final List<Long> ids) {
        return () -> {
            final List<RawRelation> relations = new ArrayList<>();
            List<RawRelation.Member> members = null;

            // Sacramento
            members = new ArrayList<>();
            members.add(new RawRelation.Member(150959789L, RawRelation.MemberType.NODE, "admin_centre"));
            final RawRelation sacramentoRelation = new RawRelation(6232940L, members,
                    tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "6",
                            "ad_level", "L5",
                            "name:eng", "Sacramento",
                            "place", "city",
                            "type", "multipolygon",
                            "iso", "USA",
                            "link_count", "83210",
                            "l1", "13712165",
                            "l2", "13712589",
                            "l3", "13717097",
                            "original_osm_admin_level", "7"// from mre
                    ));
            relations.add(sacramentoRelation);

            members = new ArrayList<>();
            members.add(new RawRelation.Member(150959789L, RawRelation.MemberType.NODE, "admin_centre"));
            final RawRelation californiaRelation = new RawRelation(165475L, members,
                    tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "4",
                            "name", "California",
                            "place", "city",
                            "type", "boundary"
                    ));
            relations.add(californiaRelation);

            members = new ArrayList<>();
            members.add(new RawRelation.Member(150959789L, RawRelation.MemberType.NODE, "admin_centre"));
            final RawRelation sacramentoCountyRelation = new RawRelation(396460L, members,
                    tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "4",
                            "name", "Sacramento County",
                            "place", "city",
                            "type", "multipolygon",
                            "ad_level", "L3",
                            "admin_level", "4",
                            "boundary", "administrative"
                    ));
            relations.add(sacramentoCountyRelation);

            // Des Moines
            members = new ArrayList<>();
            members.add(new RawRelation.Member(125027380L, RawRelation.MemberType.NODE, "admin_centre"));
            final RawRelation desMoinesRelation = new RawRelation(13723429L, members,
                    tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "6",
                            "ad_level", "L5",
                            "admin_order", "9",
                            "admin_type", "3110",
                            "name:eng", "Des Moines",
                            "place", "city",
                            "type", "multipolygon",
                            "osm_relation_id", "128653",
                            "iso", "USA",
                            "link_count", "59869",
                            "l1", "13712165",
                            "l2", "13712169",
                            "l3", "13715014",
                            "original_osm_admin_level", "7"// from mre
                    ));
            relations.add(desMoinesRelation);

            members = new ArrayList<>();
            members.add(new RawRelation.Member(125027380L, RawRelation.MemberType.NODE, "admin_centre"));
            final RawRelation iowaRelation = new RawRelation(128653L, members,
                    tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "4",
                            "name", "Iowa",
                            "place", "city",
                            "type", "boundary",
                            "is_in:country_code", "USA"
                    ));
            relations.add(iowaRelation);

            members = new ArrayList<>();
            members.add(new RawRelation.Member(125027380L, RawRelation.MemberType.NODE, "admin_centre"));
            final RawRelation polkCountyRelation = new RawRelation(1783069L, members,
                    tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "4",
                            "name", "Polk County",
                            "place", "city",
                            "type", "multypolygon",
                            "boundary", "administrative"
                    ));
            relations.add(polkCountyRelation);

            // Bellefontaine
            members = new ArrayList<>();
            members.add(new RawRelation.Member(153836071L, RawRelation.MemberType.NODE, "admin_centre"));
            final RawRelation bellefontaineRelation = new RawRelation(182398L, members,
                    tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "6",
                            "ad_level", "L5",
                            "name", "Polk County",
                            "place", "city",
                            "iso", "USA",
                            "type", "multypolygon",
                            "link_count", "2457",
                            "boundary", "administrative",
                            "l1", "13712165",
                            "l2", "13712233",
                            "l3", "13716889",
                            "original_osm_admin_level", "8"// from mre
                    ));
            relations.add(bellefontaineRelation);

            return relations;
        };
    }

    private static List<Node> nodes(final List<Long> nodeIds) {
        return nodeIds.stream()
                .map(id -> inputNodes.get(id))
                .collect(Collectors.toList());
    }

    @Test
    @Parameters({"150959789", "4", "6", "7", "8", "125027380", "153836071"})
    public void testEnhancementForNodeId(String nodeId) {
        assertNodeEnhancement(Integer.parseInt(nodeId));
    }

    private void assertNodeEnhancement(final int nodeId) {
        assertTrue(String.format("Node id %d not found", nodeId), enhancedNodes.containsKey(nodeId));
        final Node node = enhancedNodes.get(nodeId);
        if (nodeId == 150959789L) {
            // Sacramento
            assertNode(node, "type", "city_center",
                    "name", "Sacramento",
                    "name:eng", "Sacramento",
                    PLACE_TAG_KEY_IN_OSM, "neighborhood",
                    ADMIN_LEVEL_TAG_KEY_IN_OSM, "L4",
                    "capital_order1", "yes",
                    IS_IN_STATE_CODE_TAG_KEY_IN_OSM, "US",
                    "iso", "USA",
                    "cat_id", "9709",
                    "link_count", "83210",
                    "l1", "13712165",
                    "l2", "13712589",
                    "l3", "13717097");
        } else if (nodeId == 6) {
            // Node from non-navigable (raw) way
            assertNode(node, "type", "node");
        } else if (nodeId == 7) {
            // Node from non-navigable (raw) way
            assertNode(node, "type", "node");
        } else if (nodeId == 8) {
            // Node from non-navigable (raw) way
            assertNode(node, "type", "node");
        } else if (nodeId == 125027380L) {
            assertNode(node, "type", "city_center",
                    "admin_level", "L4",
                    "capital_order1", "yes",
                    "cat_id", "4444",
                    "iso", "USA",
                    "name", "Des Moines",
                    "name:eng", "Des Moines",
                    "place", "city",
                    "l1", "13712165",
                    "l2", "13712169",
                    "l3", "13715014",
                    "link_count", "59869");
        } else if (nodeId == 153836071L) {
            assertNode(node, "type", "city_center",
                    "admin_level", "L5",
                    "capital_order2", "yes",
                    "cat_id", "4444",
                    "iso", "USA",
                    "name", "Bellefontaine",
                    "name:eng", "Bellefontaine",
                    "place", "city",
                    "l1", "13712165",
                    "l2", "13712233",
                    "l3", "13716889",
                    "link_count", "2457");
        }
    }

    private void assertNode(final Node node,
                            final String... tags) {

        final int expectedTagsSize = tags.length / 2;
        final int actualTagsSize = node.tags().size();
        assertEquals(expectedTagsSize, actualTagsSize);

        for (int i = 0; i < tags.length; i += 2) {
            assertTrue(node.hasTag(tags[i], tags[i + 1]));
        }
    }

    @FunctionalInterface
    private interface EntityProvider<E extends StandardEntity & TagAdjuster<E>> {

        default JavaRDD<E> rdd(final JavaSparkContext sparkContext) {
            return sparkContext.parallelize(entities());
        }

        List<E> entities();
    }
}
