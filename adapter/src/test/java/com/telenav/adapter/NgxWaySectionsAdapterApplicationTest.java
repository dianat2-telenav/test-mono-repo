package com.telenav.adapter;

import com.telenav.adapter.bean.RoadSubType;
import com.telenav.adapter.bean.RoadType;
import com.telenav.adapter.component.extractors.Extractor;
import com.telenav.adapter.component.fc.TagService;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.apache.spark.api.java.JavaRDD;
import org.hamcrest.CoreMatchers;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static com.telenav.adapter.NgxWaySectionsAdapterApplication.CUSTOM_WAY_ID_TAG_KEY;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;


/**
 * @author adrianal
 */
public class NgxWaySectionsAdapterApplicationTest extends BaseTest {

    private static WaySection waySection;
    private final JobInitializer jobInitializer = new JobInitializer.Builder()
            .appName("Tests")
            .runLocal(true)
            .build();

    @BeforeClass
    public static void init() {
        final Node node1 = new Node(1, Latitude.degrees(24.32), Longitude.degrees(53.32));
        final Node node2 = new Node(2, Latitude.degrees(34.32), Longitude.degrees(12.32));
        waySection = new WaySection(3L, Arrays.asList(node1, node2), Collections.emptyMap()).withSequenceNumber(1);
    }

    @Test
    public void adaptAllTags() {
        final WaySection ws1 = waysection(1)
                .withTag(OsmTagKey.JUNCTION.unwrap(), "roundabout")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.ISA), "30")
                .withTag(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "eng")
                .withTag(OsmTagKey.NAME.unwrap(), "Main Street")
                .withTag(OsmTagKey.NAME.telenavPrefix(), "Main Str")
                .withTag(OsmTagKey.ALT_NAME.telenavPrefix(), "Alt Main Str")
                .withTag("ref", "FM 1385")
                .withTag(OsmTagKey.REF.telenavPrefix(), "US-101")
                .withTag(TnTagKey.TN_FUNCTIONAL_CLASS.unwrap(), "1")
                .withTag(TnTagKey.TN_DRIVING_SIDE.unwrap(), "R")
                .withTag(TnTagKey.TN_COUNTRY_ISO_CODE.unwrap(), "USA")
                .withTag(TnTagKey.TN_ADMIN_L1_LEFT.unwrap(), "1")
                .withTag(TnTagKey.TN_ADMIN_L1_RIGHT.unwrap(), "1")
                .withTag(TnTagKey.TN_ADMIN_L2_LEFT.unwrap(), "2")
                .withTag(TnTagKey.TN_ADMIN_L2_RIGHT.unwrap(), "2")
                .withTag(TnTagKey.TN_ADMIN_L3_LEFT.unwrap(), "3")
                .withTag(TnTagKey.TN_ADMIN_L3_RIGHT.unwrap(), "3")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "40")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "1")
                .withTag(TnTagKey.TN_SHIELD_ICON.unwrap(), "icon")
                .withTag(TnTagKey.TN_ORIGINAL_ID.unwrap(), "12")
                .withTag(TnTagKey.TN_FREE_FLOW_SPEED.unwrap(), "40")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "motorway")
                .withTag(OsmTagKey.FOOT.unwrap(), "permissive")
                .withTag(OsmTagKey.ROUTE.unwrap(), "shuttle_train")
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "yes")
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), "yes")
                .withTag(OsmTagKey.FRONTAGE_ROAD.unwrap(), "yes");

        JavaRDD<WaySection> waySections = jobInitializer.javaSparkContext().parallelize(Collections.singletonList(ws1));

        final List<WaySection> adaptedWaySections = new NgxWaySectionsAdapterApplication()
                .fcFunctionality(true)
                .adapt(waySections)
                .collect();

        assertThat(adaptedWaySections.size(), is(1));
        final Map<String, String> tags = adaptedWaySections.get(0).tags();
        assertThat(tags.get(TnTagKey.TN_SPEED_UNIT.withoutTelenavPrefix()), is("M"));
        assertThat(tags.get(OsmTagKey.MAXSPEED_FORWARD.unwrap()), is("30"));
        assertThat(tags.get(OsmTagKey.MAXSPEED.withSource()), is(TnSourceValue.DERIVED.unwrap()));
        assertNull(tags.get(OsmTagKey.MAXSPEED_BACKWARD.unwrap()));
        assertNull(tags.get(OsmTagKey.MAXSPEED_BACKWARD.withSource()));
        assertNull(tags.get(OsmTagKey.MAXSPEED_FORWARD.withSource()));

        assertThat(tags.get(OsmTagKey.JUNCTION.unwrap()), is("roundabout"));
        assertThat(tags.get(OsmTagKey.ONEWAY.unwrap()), is("yes"));

        assertThat(tags.get(TnTagKey.TN_LANGUAGE_CODE.withoutTelenavPrefix()), is("eng"));
        assertThat(tags.get(OsmTagKey.NAME.unwrap()), is("Main Str"));
        assertThat(tags.get(OsmTagKey.NAME.postFixed("eng")), is("Main Str"));
        assertThat(tags.get(OsmTagKey.ALT_NAME.postFixed("eng")), is("Alt Main Str"));
        assertThat(tags.get(OsmTagKey.REF.postFixed("eng")), is("US-101"));
        assertThat(TagService.ref(tags), is("FM 1385"));
        assertThat(tags.get("ref:eng:si"), is("icon"));

        assertThat(tags.get(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap()), is("4"));

        assertThat(tags.get(TnTagKey.TN_COUNTRY_ISO_CODE.withoutTelenavPrefix()), is("USA"));
        assertNull(tags.get("tn__iso"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L1_LEFT.withoutTelenavPrefix()), is("1"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L1_RIGHT.withoutTelenavPrefix()), is("1"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L2_LEFT.withoutTelenavPrefix()), is("2"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L2_RIGHT.withoutTelenavPrefix()), is("2"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L3_LEFT.withoutTelenavPrefix()), is("3"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L3_RIGHT.withoutTelenavPrefix()), is("3"));
        assertThat(tags.get(UnidbTagKey.DRIVING_SIDE.unwrap()), is("R"));

        assertThat(tags.get(TnTagKey.TN_ORIGINAL_ID.unwrap()), is("12"));
        assertThat(tags.get(OsmTagKey.HIGHWAY.unwrap()), is("motorway"));

        assertThat(tags.get(TnTagKey.TN_FREE_FLOW_SPEED.unwrap()), is("40"));

        assertNull(tags.get(OsmTagKey.FOOT.unwrap()));

        assertThat(tags.get(OsmTagKey.MOTORCAR.unwrap()), is("yes"));
        assertThat(tags.get(OsmTagKey.MOTORCYCLE.unwrap()), is("yes"));
        assertThat(tags.get(UnidbTagKey.RAIL_FERRY.unwrap()), is("yes"));
        assertThat(tags.get(RoadCategoryTagKey.ROAD_TYPE.unwrap()), is("11"));
        assertThat(tags.get(UnidbTagKey.APPLICABLE_TO.unwrap()), is("motorcar;foot;motorcycle"));
    }

    @Test
    public void addAll4Tags() {
        final WaySection ws1 = waySection
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK.unwrap())
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), DefaultTagValue.MILES.unwrap());


        JavaRDD<WaySection> waySections = jobInitializer.javaSparkContext().parallelize(Collections.singletonList(ws1));

        final WaySection adaptedWaySection = new NgxWaySectionsAdapterApplication()
                .adapt(waySections)
                .collect()
                .get(0);

        assertThat(adaptedWaySection.strictIdentifier().wayId(), CoreMatchers.is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), CoreMatchers.is((short) 1));
        assertTrue(adaptedWaySection.hasTag(RoadCategoryTagKey.ROAD_TYPE.unwrap(),
                RoadType.identifierFor(RoadType.HIGHWAY) + ""));
        assertTrue(adaptedWaySection.hasTag(RoadCategoryTagKey.ROAD_SUBTYPE.unwrap(),
                RoadSubType.identifierFor(RoadSubType.DEFAULT) + ""));
        assertTrue(adaptedWaySection.hasTag(RoadCategoryTagKey.SPEED_CATEGORY.unwrap(), "3"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK.unwrap()));
    }

    @Test
    public void addWithUrbanTags() {
        final long wayId = waySection.strictIdentifier().wayId();

        final WaySection ws1 = waySection
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap())
                .withTag(CUSTOM_WAY_ID_TAG_KEY, String.valueOf(wayId));

        final Map<Long, String> wayIdToAdminLevel = new HashMap<>();
        wayIdToAdminLevel.put(wayId, "7");

        final NgxWaySectionsAdapterApplication roadEnhancer =
                new NgxWaySectionsAdapterApplication(Collections.emptyMap(), Collections.emptyMap(), wayIdToAdminLevel);

        JavaRDD<WaySection> waySections = jobInitializer.javaSparkContext().parallelize(Collections.singletonList(ws1));

        final WaySection adaptedWaySection = roadEnhancer
                .adapt(waySections)
                .collect()
                .get(0);

        assertThat(adaptedWaySection.strictIdentifier().wayId(), CoreMatchers.is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), CoreMatchers.is((short) 1));
        assertTrue(adaptedWaySection.hasTag(RoadCategoryTagKey.ROAD_TYPE.unwrap(),
                RoadType.identifierFor(RoadType.FREEWAY_URBAN) + ""));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap()));
    }

    @Test
    public void adaptIsaSpeedLimits() {
        final WaySection waySection = waysection(3, 1)
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "K")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.ISA), "20");

        JavaRDD<WaySection> waySections = jobInitializer.javaSparkContext().parallelize(Collections.singletonList(waySection));

        final WaySection adaptedWaySection = new NgxWaySectionsAdapterApplication()
                .adapt(waySections)
                .collect()
                .get(0);

        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "20"));
        assertThat(adaptedWaySection.tags().get(OsmTagKey.MAXSPEED.withSource()), is(TnSourceValue.DERIVED.unwrap()));
        assertThat(adaptedWaySection.tags().get(TnTagKey.TN_SPEED_UNIT.withoutTelenavPrefix()), is("K"));
    }

    @Test
    public void cleanSpeedLimits() {
        final WaySection ws1 = waySection
                .withTag("tn__osm_maxspeed", "")
                .withTag("tn__isa_maxspeed:forward", "")
                .withTag("tn__isa_maxspeed:backward", "")
                .withTag("tn__osm_maxspeed:forward", "")
                .withTag("tn__osm_maxspeed:backward", "")
                .withTag("tn__osm_maxspeed:conditional", "")
                .withTag("source:maxspeed:forward", "")
                .withTag("source:maxspeed:backward", "")
                .withTag("source:maxspeed:forward", "2")
                .withTag("source:maxspeed:backward", "2")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "residential_link");

        JavaRDD<WaySection> waySections = jobInitializer.javaSparkContext().parallelize(Collections.singletonList(ws1));

        final WaySection adaptedWaySection = new NgxWaySectionsAdapterApplication()
                .adapt(waySections)
                .collect()
                .get(0);

        final Map<String, String> tags = adaptedWaySection.tags();
        assertNull(tags.get("tn__osm_maxspeed"));
        assertNull(tags.get("tn__isa_maxspeed:forward"));
        assertNull(tags.get("tn__isa_maxspeed:backward"));
        assertNull(tags.get("tn__osm_maxspeed:forward"));
        assertNull(tags.get("tn__osm_maxspeed:backward"));
        assertNull(tags.get("tn__osm_maxspeed:conditional"));
        assertNull(tags.get("source:maxspeed:forward"));
        assertNull(tags.get("source:maxspeed:backward"));
        assertThat(tags.get("source:maxspeed"), is("2"));
        assertThat(tags.get(OsmTagKey.HIGHWAY.unwrap()), is("residential"));
    }

    @Test
    public void simpleTest() {
        final WaySection ws1 = waySection
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "path")
                .withTag(OsmTagKey.FOOT.unwrap(), "yes");

        JavaRDD<WaySection> waySections = jobInitializer.javaSparkContext().parallelize(Collections.singletonList(ws1));

        final WaySection adaptedWaySection = new NgxWaySectionsAdapterApplication()
                .adapt(waySections)
                .collect()
                .get(0);

        final Map<String, String> tags = adaptedWaySection.tags();
        System.out.println(tags);
    }

    @Test
    public void testRouteFerry() {
        final WaySection ws1 = waySection
                .withTag(OsmTagKey.ROUTE.unwrap(), "ferry");

        JavaRDD<WaySection> waySections = jobInitializer.javaSparkContext().parallelize(Collections.singletonList(ws1));

        final WaySection adaptedWaySection = new NgxWaySectionsAdapterApplication()
                .adapt(waySections)
                .collect()
                .get(0);

        final Map<String, String> tags = adaptedWaySection.tags();

        assertThat(tags.get(OsmTagKey.ROUTE.unwrap()), is("ferry"));
    }

    @Test
    // route=ferry remains route=ferry
    public void testShipFerry() {
        // given
        final WaySection ws = NgxWaySectionsAdapterApplicationTest.waySection
                .withTag(OsmTagKey.ROUTE.unwrap(), Extractor.FERRY_VALUE)
                .withTag(TnTagKey.TN_DRIVING_SIDE.unwrap(), "R")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag("timezone:left", "030")
                .withTag("timezone:right", "030")
                .withTag(TnTagKey.TN_ADMIN_L1_LEFT.unwrap(), "1")
                .withTag(TnTagKey.TN_ADMIN_L1_RIGHT.unwrap(), "1")
                .withTag(TnTagKey.TN_ADMIN_L2_LEFT.unwrap(), "2")
                .withTag(TnTagKey.TN_ADMIN_L2_RIGHT.unwrap(), "2");

        // when
        JavaRDD<WaySection> waySections = jobInitializer.javaSparkContext().parallelize(Collections.singletonList(ws));
        List<WaySection> actualWaySections = new NgxWaySectionsAdapterApplication()
                .adapt(waySections)
                .collect();

        // then
        final WaySection adaptedWaySection = actualWaySections.get(0);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(tags.get(OsmTagKey.ROUTE.unwrap()), is(Extractor.FERRY_VALUE));
        assertThat(tags.get(UnidbTagKey.DRIVING_SIDE.unwrap()), is("R"));
        assertThat(tags.get("timezone:left"), is("030"));
        assertThat(tags.get("timezone:right"), is("030"));
        assertThat(tags.get(TnTagKey.TN_SPEED_UNIT.withoutTelenavPrefix()), is("M"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L1_LEFT.withoutTelenavPrefix()), is("1"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L1_RIGHT.withoutTelenavPrefix()), is("1"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L2_LEFT.withoutTelenavPrefix()), is("2"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L2_RIGHT.withoutTelenavPrefix()), is("2"));
        assertThat(tags.get(RoadCategoryTagKey.ROAD_TYPE.unwrap()), is("10"));
        assertThat(tags.get(RoadCategoryTagKey.ROAD_SUBTYPE.unwrap()), is("1"));
        assertThat(tags.get(RoadCategoryTagKey.SPEED_CATEGORY.unwrap()), is("14"));
    }

    @Test
    // route=shuttle_train becomes rail_ferry=yes (without route tag)
    public void testRailFerry() {
        // given
        final WaySection ws = NgxWaySectionsAdapterApplicationTest.waySection
                .withTag(OsmTagKey.ROUTE.unwrap(), Extractor.SHUTTLE_TRAIN_VALUE)
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "yes")
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), "yes")
                .withTag(TnTagKey.TN_DRIVING_SIDE.unwrap(), "R")
                .withTag("timezone:left", "030")
                .withTag("timezone:right", "030")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(TnTagKey.TN_ADMIN_L1_LEFT.unwrap(), "1")
                .withTag(TnTagKey.TN_ADMIN_L1_RIGHT.unwrap(), "1")
                .withTag(TnTagKey.TN_ADMIN_L2_LEFT.unwrap(), "2")
                .withTag(TnTagKey.TN_ADMIN_L2_RIGHT.unwrap(), "2");

        // when
        JavaRDD<WaySection> waySections = jobInitializer.javaSparkContext().parallelize(Collections.singletonList(ws));
        List<WaySection> actualWaySections = new NgxWaySectionsAdapterApplication()
                .adapt(waySections)
                .collect();

        // then
        final WaySection adaptedWaySection = actualWaySections.get(0);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertFalse(tags.containsKey(OsmTagKey.ROUTE.unwrap()));
        assertThat(tags.get(UnidbTagKey.RAIL_FERRY.unwrap()), is("yes"));
        assertThat(tags.get(UnidbTagKey.DRIVING_SIDE.unwrap()), is("R"));
        assertThat(tags.get("timezone:left"), is("030"));
        assertThat(tags.get("timezone:right"), is("030"));
        assertThat(tags.get(TnTagKey.TN_SPEED_UNIT.withoutTelenavPrefix()), is("M"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L1_LEFT.withoutTelenavPrefix()), is("1"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L1_RIGHT.withoutTelenavPrefix()), is("1"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L2_LEFT.withoutTelenavPrefix()), is("2"));
        assertThat(tags.get(TnTagKey.TN_ADMIN_L2_RIGHT.withoutTelenavPrefix()), is("2"));
        assertThat(tags.get(RoadCategoryTagKey.ROAD_TYPE.unwrap()), is("11"));
        assertThat(tags.get(RoadCategoryTagKey.ROAD_SUBTYPE.unwrap()), is("1"));
        assertThat(tags.get(RoadCategoryTagKey.SPEED_CATEGORY.unwrap()), is("7"));
    }

}