package com.telenav.adapter;

import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.RawRelation;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.apache.commons.io.FileUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static com.telenav.adapter.component.fc.TagService.*;
import static org.junit.Assert.assertTrue;

public class TestRoadCategoryRule
        extends BaseTest
        implements Serializable {

    private static final String TEMP_FOLDER = "src/test/resources/temp";
    private static final String WAY_SECTIONS_TEST_DATA = TEMP_FOLDER + "/way-sections";
    private static final String RAW_RELATIONS_TEST_DATA = TEMP_FOLDER + "/raw-relations";
    private static final String JOB_OUTPUT_FOLDER = TEMP_FOLDER + "/output";

    private final List<Long> toAssertForFc1 = new LinkedList<>();

    private final List<Long> toAssertForFc2 = new LinkedList<>();

    private final List<Long> toAssertForFc3 = new LinkedList<>();

    private final List<Long> toAssertForFc4 = new LinkedList<>();

    private final List<Long> toAssertForFc5 = new LinkedList<>();

    @Test
    @Ignore
    public void test() throws IOException {

        try {

            final JobInitializer jobInitializer = new JobInitializer.Builder()
                    .appName("FcLevelTest")
                    .runLocal(true)
                    .build();
            final JavaSparkContext sparkContext = jobInitializer.javaSparkContext();

        /*
            Prepare test data.

            We write data in the format that is ingested by the job.
            We do it like this, at the tests runtime, although it takes more time,
            in order to avoid the case when we serialize test data and then run into
            deserialization issues because classes got changed.
         */
            prepareTestData(sparkContext);

            final String[] args = new String[]{
                    "relations_parquet=" + RAW_RELATIONS_TEST_DATA,
                    "way_sections_parquet=" + WAY_SECTIONS_TEST_DATA,
                    "run_local=true",
                    "output_dir=" + JOB_OUTPUT_FOLDER
            };

            NgxWaySectionsAdapterJob.main(args);

            final JavaRDD<WaySection> waySections =
                    RddReader.WaySectionReader.read(sparkContext, JOB_OUTPUT_FOLDER + "/way_sections/parquet")
                            .cache();
            assertWaySections(waySections);

        } finally {
            /*
                Clean.
            */
            deleteOutputFolder(TEMP_FOLDER);
        }
    }

    private void assertWaySections(final JavaRDD<WaySection> waySections) {

        withFc(waySections, toAssertForFc1, "1");

        withFc(waySections, toAssertForFc2, "2");

        withFc(waySections, toAssertForFc3, "3");

        withFc(waySections, toAssertForFc4, "4");

        withFc(waySections, toAssertForFc5, "5");
    }

    private void withFc(final JavaRDD<WaySection> waySections,
                        final List<Long> toAssertFor,
                        final String fc) {

        final List<Long> withFc = hasFc(waySections, fc);

        assertTrue(withFc.containsAll(toAssertFor));
        assertTrue(toAssertFor.containsAll(withFc));
    }

    private List<Long> hasFc(final JavaRDD<WaySection> waySections,
                             final String fc) {
        return waySections.map(waySection -> {

            final long wayId = wayId(waySection);
            if (waySection.hasTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), fc)) {
                return wayId;
            }

            return null;
        }).filter(Objects::nonNull)
                .collect();
    }

    private long wayId(final WaySection waySection) {
        return waySection.strictIdentifier().wayId();
    }

    private void prepareTestData(final JavaSparkContext sparkContext) {

        writeRawRelations(sparkContext);

        writeWaySections(sparkContext);
    }

    private void writeWaySections(final JavaSparkContext sparkContext) {

        final Node mockNode = new Node(0L, Latitude.degrees(45.0), Longitude.degrees(45.0));
        final List<Node> nodes = Arrays.asList(mockNode);

         /*
            FC 1 roads
         */
        // i10Az members:
        // Tucson-Benson Highway
        final WaySection tucsonBensonHighway =
                new WaySection(458503090L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "USA")
                        .withTag(REF_TAG, "I 10");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5"); // we mock this value for test purpose to validate the functionality
        toAssertForFc1.add(458503090L);

        // i35Ks members:
        // Kansas Turnpike
        final WaySection kansasTurnpike =
                new WaySection(51245674L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "USA")
                        .withTag(REF_TAG, "I 35");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(51245674L);

        // Highway 2 (BC) (Alberta to Dawson Creek) members:
        // Dawson Creek-Tupper Highway
        final WaySection dawsonCreekTupperHighway =
                new WaySection(43442589L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "2");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(43442589L);

        // Alaska Highway members:
        // Alaska Highway
        final WaySection alaskaHighway =
                new WaySection(50623466L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "97");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(50623466L);

        // Trans Canada Highway (ON):
        // Highway 17
        final WaySection highway17 =
                new WaySection(127743547L, nodes, Collections.emptyMap())
                        .withTag("iso", "CAN")
                        .withTag(HIGHWAY_TAG, "trunk")
                        .withTag(NAME_TAG, "Highway 17")
                        .withTag("name:eng", "Highway 17")
                        .withTag(REF_TAG, "17")
                        .withTag("alt_name:eng", "Trans-Canada Hwy, l2")
                        .withTag("nat_name:en", "Trans-Canada Highway")
                        .withTag("nat_name", "Trans-Canada Highway")
                        .withTag("nat_name:fr", "Route Transcanadienne");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "1");
        toAssertForFc1.add(127743547L);

        // Highway 43
        final WaySection highway43 =
                new WaySection(478987865L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "43");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(478987865L);

        // Highway 2
        final WaySection highway2 =
                new WaySection(27593150L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "2");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(27593150L);

        // Highway 2
        final WaySection autorouteFélixLeclerc =
                new WaySection(20944935L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "40");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(20944935L);

        // I 94;US 52
        final WaySection i94Us52 =
                new WaySection(42330937L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "USA")
                        .withTag(REF_TAG, "I 94;US 52");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(42330937L);

        // I 85
        final WaySection way591441520 =
                new WaySection(591441520, nodes, Collections.emptyMap())
                        .withTag("iso", "USA")
                        .withTag(REF_TAG, "I 85;US 15;US 70")
                        .withTag(HIGHWAY_TAG, "motorway");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        // FC 1 because it has ref info that determines that!
        toAssertForFc1.add(591441520L);

        // Sunshine Skyway Bridge
        final WaySection sunshineSkywayBridge =
                new WaySection(39418630L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "USA")
                        .withTag(REF_TAG, "I 94;US 52");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(39418630L);

        // Loop Road
        final WaySection loopRoad =
                new WaySection(39418630L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "USA")
                        .withTag(REF_TAG, "ITS Connector");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(39418630L);

        // Carretera Torreón-Saltillo
        final WaySection carreteraTorreonSaltillo =
                new WaySection(805306363L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "MEX")
                        .withTag(REF_TAG, "MEX 40");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(805306363L);

        // Carretera Transpeninsular
        final WaySection carreteraTranspeninsular =
                new WaySection(242772783, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "MEX")
                        .withTag(REF_TAG, "MEX 1")
                        .withTag(HIGHWAY_TAG, "trunk");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(242772783L);

        // Pembina Highway
        final WaySection pembinaHighway =
                new WaySection(943422689, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "PTH 75;Route 42")
                        .withTag(HIGHWAY_TAG, "trunk")
                        .withTag("name", "Pembina Highway");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(943422689L);

        // Ambasador Bridge (Connecting Detroit to Canada)
        final WaySection mn61 =
                new WaySection(167817460L, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "USA")
                        .withTag(HIGHWAY_TAG, "trunk")
                        .withTag(REF_TAG, "MN 61");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(167817460L);

        // Huron Street (Canada)
        final WaySection huronStreet =
                new WaySection(35225397L, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "CAN")
                        .withTag(HIGHWAY_TAG, "primary");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(35225397L);

        // Sault Ste. Marie International Bridge
        final WaySection steMarieInternationalBridge =
                new WaySection(17365421L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "trunk")
                        .withTag("iso", "USA");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(17365421L);

        // Queen's Street East
        final WaySection queenStreetEast =
                new WaySection(148015251L, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "CAN")
                        .withTag(HIGHWAY_TAG, "secondary");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(148015251L);

        // International Crossing
        final WaySection internationalCrossing =
                new WaySection(596497692L, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "CAN")
                        .withTag(HIGHWAY_TAG, "secondary")
                        .withTag("name", "International Crossing");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(596497692L);

        // A 15
        final WaySection a15 =
                new WaySection(567458989L, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "15")
                        .withTag(HIGHWAY_TAG, "motorway");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(567458989L);

        // New Brunswick Route 95
        final WaySection route95NewBrunswick =
                new WaySection(89782227, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "95");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(89782227L);

        // Highway 2 (north of Morinville)
        final WaySection highway2Alberta =
                new WaySection(14986732, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "2");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(14986732L);

        // Highway 3 (AB, Crowsnest Highway)
        final WaySection crowsnestHighwayAb =
                new WaySection(624673153, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "CAN")
                        .withTag(HIGHWAY_TAG, "trunk")
                        .withTag("name", "Crowsnest Highway");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(624673153L);

        // Vancouver – Blaine Highway
        final WaySection blaineHighway =
                new WaySection(177665314, nodes, Collections.emptyMap())
                        .withTag("iso", "USA")
                        .withTag(HIGHWAY_TAG, "trunk")
                        .withTag("name", "Vancouver – Blaine Highway");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(177665314L);

        // Canada - US Vancouver border
        final WaySection way456709133 =
                new WaySection(456709133, nodes, Collections.emptyMap())
                        .withTag("iso", "USA")
                        .withTag(HIGHWAY_TAG, "trunk");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc1.add(456709133L);

        /*
            FC 2 roads
         */
        // COAH 82
        final WaySection coah82 =
                new WaySection(723173795, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "MEX")
                        .withTag(REF_TAG, "COAH 82");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(723173795L);

        final WaySection carreteraLeonSantaRosaCiudadManuelDoblado =
                new WaySection(748879625, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "MEX")
                        .withTag(REF_TAG, "GTO 87-0");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(748879625L);

        // Provincial Trunk Highway 6
        final WaySection highway6 =
                new WaySection(291964708, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "PTH 6");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(291964708L);

        // Route Jacques-Cartier
        final WaySection routeJacquesCartier =
                new WaySection(33048012, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "138");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(33048012L);

        // The Viking Trail
        final WaySection theVikingTrail =
                new WaySection(418678415, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "430");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(418678415L);

        // Highway 101
        final WaySection highway101 =
                new WaySection(242772783, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "NS 101;NS 1");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(242772783L);

        // Avenida Tlacote
        final WaySection avenidaTlacote =
                new WaySection(359436892, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "MEX")
                        .withTag(REF_TAG, "QRO 11");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(359436892L);

        // Newfoundland and Labrador Route 100
        final WaySection route100Newfoundland =
                new WaySection(147585725, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "100");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(147585725L);

        // MacKenzie Highway
        final WaySection macKenzieHighway =
                new WaySection(265156315, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "2");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(265156315L);

        // Yellowknife Highway
        final WaySection yellowknifeHighway =
                new WaySection(144053103, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "3");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(144053103L);

        // Dempster Highway Yukon
        final WaySection dempsterHighwayYukon =
                new WaySection(259320215, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "5");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(259320215L);

        // Carolina Bays Parkway
        final WaySection carolinaBaysParkway =
                new WaySection(89182965, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "USA")
                        .withTag(REF_TAG, "5");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(89182965L);

        // Ambasador Bridge (Connecting Detroit to Canada)
        final WaySection ambassadorBridge =
                new WaySection(4898838, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "USA")
                        .withTag(HIGHWAY_TAG, "trunk")
                        .withTag("name", "Ambassador Bridge");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc2.add(4898838L);

        // Interstate I-169
        final WaySection interstateI169 =
                new WaySection(104203153, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "USA")
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag(REF_TAG, "I 169;TX 550 Toll");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        // We exceptionally consider this as a fc 2 road.
        toAssertForFc2.add(104203153L);

        // Interstate I 69C
        final WaySection interstateI69c =
                new WaySection(272034186, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "USA")
                        .withTag(HIGHWAY_TAG, "motorway")
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5")
                        .withTag(REF_TAG, "I 69C;US 281");
        // We exceptionally consider this as a fc 2 road.
        toAssertForFc2.add(272034186L);

        // Interstate I 2
        final WaySection interstateI2 =
                new WaySection(12180886, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "USA")
                        .withTag(HIGHWAY_TAG, "motorway")
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5")
                        .withTag(REF_TAG, "I 2; US 83");
        // We exceptionally consider this as a fc 2 road.
        toAssertForFc2.add(12180886L);

        // Interstate I 69E
        final WaySection interstateI69E =
                new WaySection(163681524, nodes, Collections.emptyMap())
                        .withTag("tn__iso", "USA")
                        .withTag(HIGHWAY_TAG, "motorway")
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5")
                        .withTag(REF_TAG, "I 69E;US 77");
        // We exceptionally consider this as a fc 2 road.
        toAssertForFc2.add(163681524L);
        /*
            FC 3 roads
         */
        // California State Highway
        final WaySection californiaHighway198 =
                new WaySection(183131874, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "USA")
                        .withTag(REF_TAG, "CA 198");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(183131874L);

        // Calzada Ermita Iztapalapa
        final WaySection calzadaErmitaIztapalapa =
                new WaySection(38527577, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "MEX")
                        .withTag(REF_TAG, "EJE 8 SUR");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(38527577L);

        // Route 101 Quebec
        final WaySection route101 =
                new WaySection(186967468, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "101");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(186967468L);

        // Highway 10X Alberta
        final WaySection highway10xAlberta =
                new WaySection(63674497, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "10X");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(63674497L);

        // New Brunswick Route 100
        final WaySection route100NewBrunswick =
                new WaySection(384610284, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "100");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(384610284L);

        // Newfoundland and Labrador Route 101
        final WaySection route101Newfoundland =
                new WaySection(39438451, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "101");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(39438451L);

        // Inuvik-Tuktoyaktuk Highway
        final WaySection inuvikTuktoyaktukHighway =
                new WaySection(50426944, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "10");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(50426944L);

        // Highway 7A Ontario
        final WaySection highway7aOntario =
                new WaySection(480203208, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "7A");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(480203208L);

        // Route 20 Prince Edwards Islands
        final WaySection route20PrinceEdwardsIslands =
                new WaySection(181379672, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "20");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(181379672L);

        // Tagish Road Yukon
        final WaySection tagishRoadYukon =
                new WaySection(906628419, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "8");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(906628419L);

        //  Northern Woods & Water Route Saskatchewan
        final WaySection northernWoodsAndWaterRouteSaskatchewan =
                new WaySection(258953018, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "55");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(258953018L);

        //  Route 175 Quebec
        final WaySection route185Quebec =
                new WaySection(33219729, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "191");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(33219729L);

        //  PTH 52 Manitoba
        final WaySection pth52Manitoba =
                new WaySection(241737913, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "PTH 52");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(241737913L);

        // Highway 631 Ontario
        final WaySection highway631Ontario =
                new WaySection(35125652, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "631");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(35125652L);

        // Highway 622 Ontario
        final WaySection highway622Ontario =
                new WaySection(36750447, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "622");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(36750447L);

        // Highway 600 Ontario
        final WaySection highway600Ontario =
                new WaySection(36757608, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "600");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc3.add(36757608L);

        // Alabama country road
        final WaySection bearCreekRoad =
                new WaySection(8600735, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "USA")
                        .withTag("tiger:cfcc", "A31:A41");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc4.add(8600735L);

        // Hydaburg Highway
        final WaySection hydaburgHighway =
                new WaySection(755422370, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "USA")
                        .withTag(REF_TAG, "AK 913")
                        .withTag("tiger:cfcc", "A31");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        //toAssertForFc3.add(755422370L);

        /*
            FC 4 roads
         */
        // Wood Islands Road
        final WaySection woodIslandsRoad =
                new WaySection(208162738L, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "315");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc4.add(208162738L);

        // Manitoba Provincial Road 213
        final WaySection provincialRoad213 =
                new WaySection(479006300, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "PR 213");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc4.add(479006300L);

        // New Brunswick Route 205
        final WaySection route205NewBrunswick =
                new WaySection(119324416, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "205");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc4.add(119324416L);

        // Route 303 Nova Scotia
        final WaySection route303NovaScotia =
                new WaySection(760776451, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "303");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc4.add(760776451L);

        // Highway 600 Saskatchewan
        final WaySection highway600Saskatchewan =
                new WaySection(93985358, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "600");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc4.add(93985358L);

        //  Route 175 Quebec
        final WaySection route202Quebec =
                new WaySection(48153992, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "202");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc4.add(48153992L);

        // PR 210 Manitoba
        final WaySection pr210Manitoba =
                new WaySection(106215856, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "PR 210");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc4.add(106215856L);

        // Highway 62 British Columbia
        final WaySection highway62 =
                new WaySection(43309932, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "62");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc4.add(43309932L);

        /*
            FC 5 roads
         */
        // Colorado forest road
        final WaySection coloradoForestRoad =
                new WaySection(10378175, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "USA")
                        .withTag("tiger:cfcc", "A41; A51");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc5.add(10378175L);

        // Way Near Columbus OH
        /*
            We don't add this one the an asserttion list, ast it is a link but not connected to any non-link
            - i.e. won't get enhanced.
         */
        final WaySection wayAroundColumbus =
                new WaySection(21348474L, nodes, Collections.emptyMap())
                        .withTag("iso", "USA")
                        .withTag(HIGHWAY_TAG, "motorway_link") //here
                        .withTag("tiger:cfcc", "A63");

       /*
            We don't add this one the an asserttion list, ast it is a link but not connected to any non-link
            - i.e. won't get enhanced.
         */
        final WaySection hope = new WaySection(41263061, nodes, Collections.emptyMap())
                .withTag("iso", "CAN")
                .withTag(HIGHWAY_TAG, "motorway_link");

        //  Newfoundland and Labrador Route 10-30
        final WaySection route10dash30Newfoundland =
                new WaySection(832852775, nodes, Collections.emptyMap())
                        .withTag(HIGHWAY_TAG, "motorway")
                        .withTag("tn__iso", "CAN")
                        .withTag(REF_TAG, "10-30");
//                        .withTag(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "5");
        toAssertForFc5.add(832852775L);

        final List<WaySection> waySectionsList =
                Arrays.asList(
                        hope,
                        /*
                            FC 1 roads
                         */
                        // USA:
                        sunshineSkywayBridge,
                        tucsonBensonHighway,
                        kansasTurnpike,
                        i94Us52,
                        loopRoad,
                        way456709133,
                        way591441520,
                        // Canada:
                        highway17,
                        highway43,
                        highway2,
                        autorouteFélixLeclerc,
                        dawsonCreekTupperHighway,
                        alaskaHighway,
                        pembinaHighway,
                        steMarieInternationalBridge,
                        queenStreetEast,
                        a15,
                        blaineHighway,
                        // Mexico:
                        carreteraTorreonSaltillo,
                        carreteraTranspeninsular,
                        coah82,
                        carreteraLeonSantaRosaCiudadManuelDoblado,
                        avenidaTlacote,
                        //ambassadorBridge,
                        mn61,
                        internationalCrossing,
                        /*
                            FC 2 roads
                         */
                        // USA:
                        carolinaBaysParkway,
                        interstateI169,
                        interstateI69c,
                        interstateI2,
                        interstateI69E,
                        ambassadorBridge,
                        // Canada:
                        highway2Alberta,
                        highway6,
                        routeJacquesCartier,
                        theVikingTrail,
                        highway101,
                        macKenzieHighway,
                        yellowknifeHighway,
                        route100Newfoundland,
                        route95NewBrunswick,
                        crowsnestHighwayAb,
                        /*
                            FC 3 roads
                         */
                        // Canada:
                        route101,
                        highway10xAlberta,
                        highway62,
                        route100NewBrunswick,
                        route205NewBrunswick,
                        route101Newfoundland,
                        inuvikTuktoyaktukHighway,
                        highway7aOntario,
                        highway600Ontario,
                        highway631Ontario,
                        route303NovaScotia,
                        route20PrinceEdwardsIslands,
                        dempsterHighwayYukon,
                        tagishRoadYukon,
                        northernWoodsAndWaterRouteSaskatchewan,
                        route185Quebec,
                        pth52Manitoba,
                        highway622Ontario,
                        huronStreet,
                        // Mexico:
                        calzadaErmitaIztapalapa,
                        /*
                            FC 4 roads
                         */
                        // USA:
                        californiaHighway198,
                        // Canada:
                        woodIslandsRoad,
                        provincialRoad213,
                        highway600Saskatchewan,
                        route202Quebec,
                        pr210Manitoba,
                        route10dash30Newfoundland, // cannot assign proper fc to it => fc 5
                        /*
                            FC 5 roads
                         */
                        // USA:
                        coloradoForestRoad,
                        bearCreekRoad,
                        wayAroundColumbus
                );

        final JavaRDD<WaySection> waySections =
                sparkContext.parallelize(waySectionsList);

        RddWriter.WaySectionWriter.write(sparkContext, waySections, WAY_SECTIONS_TEST_DATA);
    }

    private void writeRawRelations(final JavaSparkContext sparkContext) {

        final List<RawRelation.Member> i10AzMembers = wayMembers(458503090L);

        final List<RawRelation.Member> ks35Members = wayMembers(51245674L);

        final List<RawRelation.Member> highway2Members = wayMembers(43442589L);

        final List<RawRelation.Member> alaskaHighwayMembers = wayMembers(50623466L);

        final List<RawRelation.Member> transCanadaOnMembers = wayMembers(127743547L);

        final List<RawRelation.Member> highway43Members = wayMembers(478987865L);

        final List<RawRelation.Member> nsTradeCorridorMembers = wayMembers(27593150L);

        final List<RawRelation.Member> transCanadaQcMembers = wayMembers(20944935L);

        final List<RawRelation.Member> i94Us52Members = wayMembers(42330937L);

        final List<RawRelation.Member> sunshineSkywayBridgeMembers = wayMembers(39418630L);

        final List<RawRelation.Member> caPe315Members = wayMembers(208162738L);

        final List<RawRelation.Member> highway2AlbertaMembers = wayMembers(14986732);

        final List<RawRelation.Member> highway6ManitobaMembers = wayMembers(291964708);

        final List<RawRelation.Member> routeJacquesCartierMembers = wayMembers(33048012);

        final List<RawRelation.Member> greatNorthernHighwayMembers = wayMembers(418678415);

        final List<RawRelation.Member> highway101Members = wayMembers(242772783);

        final List<RawRelation.Member> route101Members = wayMembers(186967468);

        final List<RawRelation.Member> highway10xMembers = wayMembers(63674497);

        final List<RawRelation.Member> highway62Members = wayMembers(43309932);

        final List<RawRelation.Member> provincialRoad213Members = wayMembers(479006300);

        final List<RawRelation.Member> route95NewBrunswickMembers = wayMembers(89782227);

        final List<RawRelation.Member> route100NewBrunswickMembers = wayMembers(384610284);

        final List<RawRelation.Member> route205NewBrunswickMembers = wayMembers(119324416);

        final List<RawRelation.Member> route100NewfoundlandMembers = wayMembers(147585725);

        final List<RawRelation.Member> route101NewfoundlandMembers = wayMembers(39438451);

        final List<RawRelation.Member> macKenzieHighwayMembers = wayMembers(265156315);

        final List<RawRelation.Member> yellowKnifeHighwayMembers = wayMembers(144053103);

        final List<RawRelation.Member> inuvikTuktoyaktukHighwayMembers = wayMembers(50426944);

        final List<RawRelation.Member> highway7aOntarioMembers = wayMembers(480203208);

        final List<RawRelation.Member> highway600OntarioMembers = wayMembers(36757608);

        final List<RawRelation.Member> route303NovaScotiaMembers = wayMembers(760776451);

        final List<RawRelation.Member> route20PrinceEdwardsIslandsMembers = wayMembers(181379672);

        final List<RawRelation.Member> dempsterHighwayMembers = wayMembers(259320215);

        final List<RawRelation.Member> tagishRoadYukonMembers = wayMembers(906628419);

        final List<RawRelation.Member> northernWoodsAndWaterRouteSaskatchewanMembers = wayMembers(258953018);

        final List<RawRelation.Member> highway600SaskatchewanMembers = wayMembers(93985358);

        final List<RawRelation.Member> route191QuebecMembers = wayMembers(33219729);

        final List<RawRelation.Member> route202QuebecMembers = wayMembers(48153992);

        final List<RawRelation.Member> pth52ManitobaMembers = wayMembers(241737913);

        final List<RawRelation.Member> pr210ManitobaMembers = wayMembers(106215856);

        final List<RawRelation.Member> highway622OntarioMembers = wayMembers(36750447);

        final List<RawRelation.Member> highway631OntarioMembers = wayMembers(35125652);

        final List<RawRelation.Member> pembinaHighwayMembers = wayMembers(943422689);

        final List<RawRelation.Member> nhsHighwayPriorityCorridor18Members = wayMembers(4898838, 104203153);

        final List<RawRelation.Member> constitutionalRoute1Members = wayMembers(167817460);

        final List<RawRelation.Member> steMarieInternationalBridgeMembers = wayMembers(17365421);

        final List<RawRelation.Member> fourRiversideMembers = wayMembers(148015251);

        final List<RawRelation.Member> a15Members = wayMembers(567458989);

        final List<RawRelation.Member> carolinaBaysParkwayMembers = wayMembers(89182965);

        final List<RawRelation.Member> crowsnestHighwayAbMembers = wayMembers(624673153);

        final List<RawRelation.Member> californiaStateHighway198Members = wayMembers(183131874);

        final List<RawRelation.Member> vancouverBlaineHighwayMembers = wayMembers(177665314);

        final List<RawRelation.Member> i5Members = wayMembers(456709133);

        final List<RawRelation.Member> us15Members = wayMembers(591441520);

        final List<RawRelation.Member> interstateI2Members = wayMembers(12180886);

        final List<RawRelation.Member> interstateI69CMembers = wayMembers(272034186);

        final List<RawRelation.Member> interstateI69EMembers = wayMembers(163681524);

        // I-10 (AZ)
        final RawRelation i10Az =
                new RawRelation(3332621L, i10AzMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:I")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation _80Az =
                new RawRelation(9230611L, i10AzMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:US")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation formerUs80 =
                new RawRelation(9133248L, i10AzMembers, Collections.emptyMap())
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation historicUs80MarshStationLoop =
                new RawRelation(4682079L, i10AzMembers, Collections.emptyMap())
                        .withTag("route", "road")
                        .withTag("type", "route");

        // I 35 (KS southbound)
        final RawRelation i35KsSouthbound =
                new RawRelation(2335812, ks35Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:I")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation kansasTurnpike =
                new RawRelation(228590, ks35Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:KS")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Highway 2 (BC) (Alberta to Dawson Creek)
        final RawRelation highway2 =
                new RawRelation(326933, highway2Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:BC")
                        .withTag(REF_TAG, "2")
                        .withTag(NAME_TAG, "Highway 2 (BC) (Alberta to Dawson Creek)")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Alaska Highway
        final RawRelation alaskaHighway =
                new RawRelation(326897, alaskaHighwayMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "Alaska Highway")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation highway97 =
                new RawRelation(7166559, alaskaHighwayMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "Highway 97 (BC)")
                        .withTag(REF_TAG, "97")
                        .withTag(NETWORK_TAG, "CA:BC")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Trans Canada Highway (ON)
        final RawRelation transCanadaOn =
                new RawRelation(11074490, transCanadaOnMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "Trans Canada Highway (ON) (Hwy 17, Hwy 11 to MB)")
                        .withTag(NETWORK_TAG, "CA:transcanada:namedRoute")
                        .withTag(REF_TAG, "CA:transcanada:namedRoute")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Trans Canada Highway (ON)
        final RawRelation transCanadaOnManitobaBorder =
                new RawRelation(3739620, transCanadaOnMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "Ontario Highway 17 (Manitoba border to Dinorwic)")
                        .withTag(NETWORK_TAG, "CA:ON:primary")
                        .withTag(REF_TAG, "17")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation highway17 =
                new RawRelation(3739620, transCanadaOnMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "Ontario Highway 17 (Manitoba border to Dinorwic)")
                        .withTag(NETWORK_TAG, "CA:ON:primary")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Highway 43
        final RawRelation highway43 =
                new RawRelation(326931, highway43Members, Collections.emptyMap())
                        .withTag(NAME_TAG, "Highway 43")
                        .withTag(REF_TAG, "43")
                        .withTag(NETWORK_TAG, "CA:AB:primary")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation nsTradeCorridor =
                new RawRelation(8897238, nsTradeCorridorMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "N-S Trade Corridor (South)")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Autoroute Felix-Leclerc
        final RawRelation autoroute40 =
                new RawRelation(407970, transCanadaQcMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "Autoroute 40")
                        .withTag(NETWORK_TAG, "CA:QC:A")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation transCanadaHighwayQc =
                new RawRelation(396229, transCanadaQcMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "Route Transcanadienne (QC)")
                        .withTag(NETWORK_TAG, "CA:transcanada:namedRoute")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // I 94;US 52
        final RawRelation us52 =
                new RawRelation(2309414, i94Us52Members, Collections.emptyMap())
                        .withTag(NAME_TAG, "US 52 (ND)")
                        .withTag(REF_TAG, "52")
                        .withTag(NETWORK_TAG, "US:US")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation i94 =
                new RawRelation(1858376, i94Us52Members, Collections.emptyMap())
                        .withTag(NAME_TAG, "I 94 (ND)")
                        .withTag(REF_TAG, "94")
                        .withTag(NETWORK_TAG, "US:I")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Sunshine Skyway Bridge
        final RawRelation fl93 =
                new RawRelation(1856422, sunshineSkywayBridgeMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "FL 93")
                        .withTag(NETWORK_TAG, "US:FL")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation fl55 =
                new RawRelation(1723537, sunshineSkywayBridgeMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "FL 55")
                        .withTag(NETWORK_TAG, "US:FL")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation us19 =
                new RawRelation(377631, sunshineSkywayBridgeMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "US 19 (FL)")
                        .withTag(NETWORK_TAG, "US:US")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation i275 =
                new RawRelation(1858376, sunshineSkywayBridgeMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "I 275 (FL)")
                        .withTag(NETWORK_TAG, "US:I")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final RawRelation sunshineSkywayBridge =
                new RawRelation(9883206, sunshineSkywayBridgeMembers, Collections.emptyMap())
                        .withTag("type", "bridge");

        final RawRelation caPe315 =
                new RawRelation(5953434, caPe315Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:PE")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Highway 2 (north of Morinville)
        final RawRelation hjighway2 =
                new RawRelation(8716057, highway2AlbertaMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:AB:primary")
                        .withTag(REF_TAG, "2")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Provincial Trunk Highway 6
        final RawRelation highway6 =
                new RawRelation(4575537, highway6ManitobaMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:MB:PTH")
                        .withTag(REF_TAG, "6")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Route Jacques-Cartier
        final RawRelation routeJacquesCartier =
                new RawRelation(395635, routeJacquesCartierMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:QC:R")
                        .withTag(REF_TAG, "138")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Great Northern Highway
        final RawRelation greatNorthernHighway =
                new RawRelation(5384079, greatNorthernHighwayMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:NL:R")
                        .withTag(REF_TAG, "430")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Highway 101
        final RawRelation highway101 =
                new RawRelation(1732795, highway101Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:NS:H")
                        .withTag(REF_TAG, "430")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Route 101
        final RawRelation route101 =
                new RawRelation(411484, route101Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:QC:R")
                        .withTag(REF_TAG, "101")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Highway 10x Alberta
        final RawRelation highway10x =
                new RawRelation(8687225, highway10xMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:AB:primary")
                        .withTag(REF_TAG, "10X")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Highway 62 British Columbia
        final RawRelation highway62 =
                new RawRelation(8746198, highway62Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:BC")
                        .withTag(REF_TAG, "62")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Manitoba Provincial Road 213
        final RawRelation provincialRoad213 =
                new RawRelation(4578850, provincialRoad213Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:MB:PR")
                        .withTag(REF_TAG, "213")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Route 95 New Brunswick
        final RawRelation route95NewBrunswick =
                new RawRelation(1727836, route95NewBrunswickMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:NB:primary")
                        .withTag(REF_TAG, "95")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Route 100 New Brunswick
        final RawRelation route100NewBrunswick =
                new RawRelation(5721039, route100NewBrunswickMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:NB:primary")
                        .withTag(REF_TAG, "100")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Route 205 New Brunswick
        final RawRelation route205NewBrunswick =
                new RawRelation(5721067, route205NewBrunswickMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:NB:tertiary")
                        .withTag(REF_TAG, "205")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Route 100 Newfoundland and Labrador
        final RawRelation route100Newfoundland =
                new RawRelation(5380644, route100NewfoundlandMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:NL:R")
                        .withTag(REF_TAG, "5380644")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Route 101 Newfoundland and Labrador
        final RawRelation route101Newfoundland =
                new RawRelation(5380645, route101NewfoundlandMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:NL:R")
                        .withTag(REF_TAG, "5380644")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // MacKenzie Highway
        final RawRelation macKenzieHighway =
                new RawRelation(5384504, macKenzieHighwayMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:NT")
                        .withTag(REF_TAG, "2")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Yellowknife Highway
        final RawRelation yellowKnifeHighway =
                new RawRelation(5384544, yellowKnifeHighwayMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:NT")
                        .withTag(REF_TAG, "3")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Inuvik-Tuktoyaktuk Highway
        final RawRelation inuvikTuktoyaktukHighway =
                new RawRelation(7675728, inuvikTuktoyaktukHighwayMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:NT")
                        .withTag(REF_TAG, "10")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Highway 7A Ontario
        final RawRelation highway7aOntario =
                new RawRelation(7675728, highway7aOntarioMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:ON:primary")
                        .withTag(REF_TAG, "7A")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Highway 600 Ontario
        final RawRelation highway600Ontario =
                new RawRelation(4124166, highway600OntarioMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:ON:secondary")
                        .withTag(REF_TAG, "600")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Nova Scotia 303
        final RawRelation route303NovaScotia =
                new RawRelation(5382244, route303NovaScotiaMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:NS:R")
                        .withTag(REF_TAG, "303")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Route 20 Prince Edwards Islands
        final RawRelation route20PrinceEdwardsIslands =
                new RawRelation(5918127, route20PrinceEdwardsIslandsMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:PE")
                        .withTag(REF_TAG, "20")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Dempster Highway Yukon
        final RawRelation dempsterHighway =
                new RawRelation(5386003, dempsterHighwayMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:YT")
                        .withTag(REF_TAG, "5")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Tagish Road Yukon
        final RawRelation tagishRoadYukon =
                new RawRelation(5386017, tagishRoadYukonMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:YT")
                        .withTag(REF_TAG, "8")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Northern Woods & Water Route Saskatchewan
        final RawRelation northernWoodsAndWaterRouteSaskatchewan =
                new RawRelation(8741762, northernWoodsAndWaterRouteSaskatchewanMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:SK:primary")
                        .withTag(REF_TAG, "55")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Highway 600 Saskatchewan
        final RawRelation highway600Saskatchewan =
                new RawRelation(8681420, highway600SaskatchewanMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:SK:secondary")
                        .withTag(REF_TAG, "600")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Route 191 Quebec
        final RawRelation route191Quebec =
                new RawRelation(406703, route191QuebecMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:QC:R")
                        .withTag(REF_TAG, "191")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Route 202 Quebec
        final RawRelation route202Quebec =
                new RawRelation(4565141, route202QuebecMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:QC:R")
                        .withTag(REF_TAG, "202")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // PTH 52 Manitoba
        final RawRelation pth52Manitoba =
                new RawRelation(4578529, pth52ManitobaMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:MB:PTH")
                        .withTag(REF_TAG, "52")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // PR 210 Manitoba
        final RawRelation pr210Manitoba =
                new RawRelation(4578844, pr210ManitobaMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:MB:PR")
                        .withTag(REF_TAG, "210")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Highway 622 Ontario
        final RawRelation highway622Ontario =
                new RawRelation(4124275, highway622OntarioMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:ON:secondary")
                        .withTag(REF_TAG, "622")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Highway 631 Ontario
        final RawRelation highway631Ontario =
                new RawRelation(4124315, highway631OntarioMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:ON:secondary")
                        .withTag(REF_TAG, "631")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Pembina Highway
        final RawRelation pembinaHighway =
                new RawRelation(1726910, pembinaHighwayMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:MB:PTH")
                        .withTag(NAME_TAG, "PTH 75 (North)")
                        .withTag(REF_TAG, "75")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // NHS High Priority Corridor 18
        final RawRelation nhsHighwayPriorityCorridor18 =
                new RawRelation(1319864, nhsHighwayPriorityCorridor18Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:NHS High Priority Corridors")
                        .withTag(NAME_TAG, "NHS High Priority Corridor 18")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Constitutional Route 1 (Minnesota)
        final RawRelation constitutionalRoute1 =
                new RawRelation(2538984, constitutionalRoute1Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:MN:legislative")
                        .withTag(NAME_TAG, "Constitutional Route 1")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Sault Ste. Marie International Bridge
        final RawRelation steMarieInternationalBridge =
                new RawRelation(9857186, steMarieInternationalBridgeMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "Sault Ste. Marie International Bridge")
                        .withTag("route", "road")
                        .withTag("type", "bridge");

        // 4 Riverside
        final RawRelation fourRiverside =
                new RawRelation(6779490, fourRiversideMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "4 Riverside")
                        .withTag("route", "bus")
                        .withTag("type", "route");

        // A 15
        final RawRelation a15 =
                new RawRelation(1731597, a15Members, Collections.emptyMap())
                        .withTag(NAME_TAG, "A 15")
                        .withTag(REF_TAG, "15")
                        .withTag(NETWORK_TAG, "CA:QC:A")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Carolina Bays Parkway
        final RawRelation carolinaBaysParkway =
                new RawRelation(10382641, carolinaBaysParkwayMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "I 74 (SC - future)")
                        .withTag(NETWORK_TAG, "US:I")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Carolina Bays Parkway
        final RawRelation crowsnestHighwayAb =
                new RawRelation(8245009, crowsnestHighwayAbMembers, Collections.emptyMap())
                        .withTag(NAME_TAG, "Highway 3 (AB, Crowsnest Highway)")
                        .withTag(NETWORK_TAG, "CA:AB:primary")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // California State Highway 198
        final RawRelation californiaStateHighway198 =
                new RawRelation(330676, californiaStateHighway198Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:CA")
                        .withTag(REF_TAG, "198")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Vancouver Blaine Highway
        final RawRelation vancouverBlaineHighway =
                new RawRelation(330676, vancouverBlaineHighwayMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "CA:BC")
                        .withTag(REF_TAG, "99")
                        .withTag(NAME_TAG, "Highway 99 (BC) (South)")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Vancouver Blaine Highway
        final RawRelation i5 =
                new RawRelation(2332808, i5Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:I")
                        .withTag(REF_TAG, "5")
                        .withTag(NAME_TAG, "I 5 (WA) (South)")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // US 15
        final RawRelation us15 =
                new RawRelation(2301104, us15Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:US")
                        .withTag(REF_TAG, "15")
                        .withTag(NAME_TAG, "US 15 (NC)")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Interstate I 2
        final RawRelation i2 =
                new RawRelation(3103759, interstateI2Members, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:I")
                        .withTag(REF_TAG, "2")
                        .withTag(NAME_TAG, "Interstate 2")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Interstate I 69C
        final RawRelation i69C =
                new RawRelation(3103788, interstateI69CMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:I")
                        .withTag(REF_TAG, "69C")
                        .withTag(NAME_TAG, "Interstate 69C")
                        .withTag("route", "road")
                        .withTag("type", "route");

        // Interstate I 69E
        final RawRelation i69e =
                new RawRelation(3103756, interstateI69EMembers, Collections.emptyMap())
                        .withTag(NETWORK_TAG, "US:I")
                        .withTag(REF_TAG, "69E")
                        .withTag(NAME_TAG, "Interstate 69E")
                        .withTag("route", "road")
                        .withTag("type", "route");

        final List<RawRelation> relationsList = Arrays.asList(
                // Tucson-Benson Highway is member of multiple relations
                i10Az,
                _80Az,
                formerUs80,
                historicUs80MarshStationLoop,
                // Kansas Turnpike
                i35KsSouthbound,
                kansasTurnpike,
                // Highway 2 (BC) (Alberta to Dawson Creek)
                highway2,
                // Alaska Highway
                alaskaHighway, //ok
                highway97, // ok
                // Trans Canada Highway (ON)
                transCanadaOn, // ok
                transCanadaOnManitobaBorder,
                highway17, //ok
                highway43,
                nsTradeCorridor,
                autoroute40,
                transCanadaHighwayQc,
                us52,
                i94,
                // Sunshine Skyway Bridge
                fl93,
                fl55,
                us19,
                i275,
                sunshineSkywayBridge,
                // Prince Edwards Islands Highway 315
                caPe315,
                // Highway 2 (north of Morinville)
                hjighway2,
                // Highway 6
                highway6,
                // Route Jacques-Cartier
                routeJacquesCartier,
                // Great Northern Highway
                greatNorthernHighway,
                // Highway 101
                highway101,
                // Route 101
                route101,
                // Highway 10x Alberta
                highway10x,
                // Highway 62 British Columbia
                highway62,
                // Manitoba Provincial Road 213
                provincialRoad213,
                // Route 95 New Brunswick
                route95NewBrunswick,
                // Route 100 New Brunswick
                route100NewBrunswick,
                // Route 205 New Brunswick
                route205NewBrunswick,
                // Route 100 Newfoundland
                route100Newfoundland,
                // Route 100 Newfoundland
                route101Newfoundland,
                // MacKenzie Highway
                macKenzieHighway,
                // Yellowknife Highway
                yellowKnifeHighway,
                // Inuvik0Tuktoyaktuk Highway
                inuvikTuktoyaktukHighway,
                // Highway 7A Ontario
                highway7aOntario,
                // Highway 600 Ontario
                highway600Ontario,
                // Route 303 Nova Scotia
                route303NovaScotia,
                // Route 20 Prince Edwards Islands
                route20PrinceEdwardsIslands,
                // Dempster Highway
                dempsterHighway,
                // Tagish Road
                tagishRoadYukon,
                // Northern Woods And Water Route Saskatchewan
                northernWoodsAndWaterRouteSaskatchewan,
                // Highway 600 Saskatchewan
                highway600Saskatchewan,
                // Route 175 Quebec
                route191Quebec,
                // Route 202 Quebec
                route202Quebec,
                // PTH 52 Manitoba
                pth52Manitoba,
                // PR 210 Manitoba
                pr210Manitoba,
                // Highway 622 Ontario
                highway622Ontario,
                // Highway 631 Ontario
                highway631Ontario,
                pembinaHighway,
                nhsHighwayPriorityCorridor18,
                constitutionalRoute1,
                steMarieInternationalBridge,
                fourRiverside,
                a15,
                carolinaBaysParkway,
                crowsnestHighwayAb,
                californiaStateHighway198,
                vancouverBlaineHighway,
                i5,
                us15,
                i2,
                i69C
        );

        final JavaRDD<RawRelation> relations = sparkContext.parallelize(relationsList);

        RddWriter.RawRelationWriter.write(sparkContext, relations, RAW_RELATIONS_TEST_DATA);
    }

    private List<RawRelation.Member> wayMembers(final long... wayIds) {
        return Arrays.stream(wayIds)
                .mapToObj(wayId -> new RawRelation.Member(wayId, RawRelation.MemberType.WAY, ""))
                .collect(Collectors.toList());
    }

    private void deleteOutputFolder(final String outputPath) throws IOException {
        final File outputFolder = new File(outputPath);
        FileUtils.cleanDirectory(outputFolder);
        FileUtils.deleteDirectory(outputFolder);
    }
}
