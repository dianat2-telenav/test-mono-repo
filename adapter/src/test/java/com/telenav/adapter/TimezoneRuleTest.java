package com.telenav.adapter;

import com.telenav.adapter.component.extractors.TimezoneExtractor;
import com.telenav.adapter.rules.TimezoneTagRule;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Unit tests for {@link com.telenav.adapter.rules.TimezoneTagRule}.
 *
 * @author dianat2
 */
public class TimezoneRuleTest extends BaseTest {

    private TimezoneTagRule<WaySection> waySectionTimezoneRule;
    private TimezoneTagRule<RawWay> wayTimezoneRule;


    @Before
    public void initRule() {
        waySectionTimezoneRule = new TimezoneTagRule<>(new TimezoneExtractor());
        wayTimezoneRule = new TimezoneTagRule<>(new TimezoneExtractor());
    }

    @Test
    public void waySectionWithNoTag() {
        // Given
        final WaySection waySection = waysection(3, 1);

        // When
        final WaySection adaptedWaySection = waySectionTimezoneRule.apply(waySection);

        // Then
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void waySectionWithTimezoneTag() {
        // Given
        final WaySection waySection = waysection(3, 1)
                .withTag(TnTagKey.TN_TIMEZONE.unwrap(), "UTC");

        // When
        final WaySection adaptedWaySection = waySectionTimezoneRule.apply(waySection);

        // Then
        assertEquals(3, adaptedWaySection.strictIdentifier().wayId());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(1, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection
                .hasTag(TnTagKey.TN_TIMEZONE.withoutTelenavPrefix(), "UTC"));
    }

    @Test
    public void wayWithNoTag() {
        // Given
        final RawWay way = rawWay(1);

        // When
        final RawWay adaptedWay = wayTimezoneRule.apply(way);

        // Then
        assertEquals(way, adaptedWay);
    }

    @Test
    public void wayWithTimezoneTag() {
        // Given
        final int expectedWayId = 1;
        final RawWay way = rawWay(expectedWayId)
                .withTag(TnTagKey.TN_TIMEZONE.unwrap(), "UTC");

        // When
        final RawWay adaptedWay = wayTimezoneRule.apply(way);

        // Then
        assertEquals(expectedWayId, adaptedWay.id());
        assertEquals(way.nodeIds(), adaptedWay.nodeIds());
        assertEquals(1, adaptedWay.tags().size());
        assertTrue(adaptedWay.hasTag(TnTagKey.TN_TIMEZONE.withoutTelenavPrefix(), "UTC"));
    }
}