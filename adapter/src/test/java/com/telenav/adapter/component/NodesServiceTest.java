package com.telenav.adapter.component;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.utils.UniDbKeys;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.writer.converter.RowToNodeConverter;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.telenav.datapipelinecore.unidb.RoadCategoryTagKey.FUNCTIONAL_CLASS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NodesServiceTest extends BaseTest {

    private JavaSparkContext javaSparkContext;

    @Before
    public void init() {
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.sql.parquet.binaryAsString", "true");
        sparkConf.setMaster("local[1]");
        sparkConf.setAppName("Test");

        this.javaSparkContext = JavaSparkContext.fromSparkContext(SparkContext.getOrCreate(sparkConf));
    }

    @Test
    public void joinNodesFromWaysWithNodesAddTypeTagAndNavnTag() {
        Node node0 = node(0)
                .withTag("type", "city_center");
        // Way section nodes:
        Node node1 = node(1);
        Node node2 = node(2);
        Node node3 = node(3);
        Node node4 = node(4);
        Node node5 = node(5);
        Node node6 = node(6);
        Node node7 = node(7);
        Node node8 = node(8);
        Node node9 = node(9);
        Node node10 = node(10);
        Node node11 = node(11);
        // Ways nodes:
        Node node12 = node(12).withTag(UniDbKeys.TYPE_TAG, "city_center");
        Node node13 = node(13);
        Node node14 = node(14);
        Node node15 = node(15);
        Node node16 = node(16);

        // Way sections
        final WaySection waySection1 = waysection(2, 1)
                .withTags(tags(OsmTagKey.HIGHWAY.unwrap(), "service", FUNCTIONAL_CLASS.unwrap(), "1"))
                .replaceNodes(Arrays.asList(node1, node2, node3, node4, node8));

        final WaySection waySection2 = waysection(4, 1)
                .withTags(tags(OsmTagKey.HIGHWAY.unwrap(), "primary"))
                .replaceNodes(Arrays.asList(node5, node6, node3, node7));

        final WaySection waySection3 = waysection(5, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "motorway")
                .replaceNodes(Arrays.asList(node7, node6, node4, node3, node5));

        final SQLContext sqlContext = new SQLContext(javaSparkContext);

        // Way Sections
        final List<WaySection> waySectionsList = Arrays.asList(waySection1, waySection2, waySection3);
        final Dataset<Row> waySections =
                waySections(waySectionsList, sqlContext);

        // Ways
        final RawWay way1 = rawWay(6, Arrays.asList(node12.id(), node13.id(), node14.id()))
                .withTags(tags(FUNCTIONAL_CLASS.unwrap(), "5"));

        final RawWay way2 = rawWay(7, Arrays.asList(node15.id(), node16.id()));

        // Ways
        final List<RawWay> wayList = Arrays.asList(way1, way2);
        final Dataset<Row> ways =
                rawWays(wayList, sqlContext);

        // Nodes
        final List<Node> nodesList =
                Arrays.asList(node0,
                        node1, node2, node3, node4, node5, node6, node7, node8, node9, node10, node11,
                        node12, node13, node14, node15, node16);
        final Dataset<Row> nodes =
                nodes(nodesList, sqlContext);

        final NodeService target = new NodeService(sqlContext.sparkSession());

        final Dataset<Row> adaptedNodesDataset =
                target.enhanceNodesFromWaysSections(nodes,
                        waySections,
                        ways).cache();

        // The following nodes are expected to gen enhanced with shape_point tags.
        node1 = node1.withTag(UniDbKeys.TYPE_TAG, "node")
                .withTag(UniDbKeys.NAVN_TAG, "y");
        node2 = node2.withTag(UniDbKeys.TYPE_TAG, "shape_point")
                .withTag(UniDbKeys.NAVN_TAG, "y");
        node3 = node3.withTag(UniDbKeys.TYPE_TAG, "shape_point")
                .withTag(UniDbKeys.NAVN_TAG, "y");
        node4 = node4.withTag(UniDbKeys.TYPE_TAG, "shape_point")
                .withTag(UniDbKeys.NAVN_TAG, "y");
        node8 = node8.withTag(UniDbKeys.TYPE_TAG, "node")
                .withTag(UniDbKeys.NAVN_TAG, "y");

        node5 = node5.withTag(UniDbKeys.TYPE_TAG, "node");
        node6 = node6.withTag(UniDbKeys.TYPE_TAG, "shape_point");
        node7 = node7.withTag(UniDbKeys.TYPE_TAG, "node");

        node12 = node12//.withTag(UniDbKeys.TYPE_TAG, "city_center")
                .withTag(UniDbKeys.NAVN_TAG, "y");
        node13 = node13.withTag(UniDbKeys.TYPE_TAG, "shape_point")
                .withTag(UniDbKeys.NAVN_TAG, "y");
        node14 = node14.withTag(UniDbKeys.TYPE_TAG, "node")
                .withTag(UniDbKeys.NAVN_TAG, "y");

        node15 = node15.withTag(UniDbKeys.TYPE_TAG, "node");
        node16 = node16.withTag(UniDbKeys.TYPE_TAG, "node");

        final List<Node> expectedNodes =
                Arrays.asList(node0,
                        node1, node2, node3, node4, node5, node6, node7, node8, node9, node10, node11,
                        node12, node13, node14, node15, node16);

        final RowToNodeConverter converter = new RowToNodeConverter();

        final List<Node> actualNodes = adaptedNodesDataset.collectAsList()
                .stream()
                .map(converter::apply)
                .collect(Collectors.toList());

        for (int i = 0; i < 20; i++) {

            Node a = null;
            for (Node actualNode : actualNodes) {
                if (actualNode.id() == i) {
                    a = actualNode;
                }
            }
            Node e = null;
            for (Node expectedNode : expectedNodes) {
                if (expectedNode.id() == i) {
                    e = expectedNode;
                }
            }
            if (a == null && e == null) {
                continue;
            }
            assertEquals(a, e);
        }

        // Assertions.
        assertEquals(expectedNodes.size(), actualNodes.size());
        assertTrue(expectedNodes.containsAll(actualNodes));
        assertTrue(actualNodes.containsAll(expectedNodes));
    }

    private Dataset<Row> nodes(final List<Node> nodesList,
                               final SQLContext sqlContext) {
        final JavaRDD<Node> nodesRdd = javaSparkContext.parallelize(nodesList);
        return nodeRddToDataSet(nodesRdd, sqlContext);
    }

    private Dataset<Row> waySections(final List<WaySection> waySectionsList,
                                     final SQLContext sqlContext) {
        final JavaRDD<WaySection> waySectionsRdd = javaSparkContext.parallelize(waySectionsList);
        return waySectionRddToDataSet(waySectionsRdd, sqlContext);
    }

    private Dataset<Row> rawWays(final List<RawWay> rawWaysList,
                                 final SQLContext sqlContext) {
        final JavaRDD<RawWay> rawWaysRdd = javaSparkContext.parallelize(rawWaysList);
        return rawWayRddToDataSet(rawWaysRdd, sqlContext);
    }

    private Map<String, String> tags(final String... keyValuePairs) {
        final Map<String, String> tags = new HashMap<>();
        for (int i = 0; i < keyValuePairs.length; i += 2) {
            tags.put(keyValuePairs[i], keyValuePairs[i + 1]);
        }
        return tags;
    }
}
