package com.telenav.adapter.component.extractors;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmTagKey.ACCESS;
import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;


/**
 * Unit tests for {@link AccessExtractor}.
 */
public class AccessExtractorTest {

    private final AccessExtractor extractor = new AccessExtractor();

    @Test
    public void tagsWithoutAccessTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put("tag1", "yes");
        tags.put("tag2", "yes");
        final Optional<String> accessWithoutValue = extractor.extract(tags);
        assertFalse(accessWithoutValue.isPresent());
    }

    @Test
    public void tagsWithNoValueAccessTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put("tag1", "yes");
        tags.put(ACCESS.unwrap(), "no");
        final Optional<String> accessWithNoValue = extractor.extract(tags);
        assertTrue(accessWithNoValue.isPresent());
        assertEquals("no", accessWithNoValue.get());
    }

    @Test
    public void tagsWithYesValueAccessTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put("tag1", "yes");
        tags.put(ACCESS.unwrap(), "yes");
        final Optional<String> accessWithNoValue = extractor.extract(tags);
        assertTrue(accessWithNoValue.isPresent());
        assertEquals("yes", accessWithNoValue.get());
    }
}