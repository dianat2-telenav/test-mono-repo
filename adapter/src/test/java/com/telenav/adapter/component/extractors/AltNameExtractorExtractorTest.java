package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/**
 * @author roxanal
 */
public class AltNameExtractorExtractorTest extends BaseExtractorTest {

    private final TnAltNameExtractor extractor = new TnAltNameExtractor();

    @Test
    public void testAltNameTag() {
        final Optional<String> altNameTag = extractor.extract(altNameTags("Detroit-Toledo"));
        assertThat(altNameTag.get(), is("Detroit-Toledo"));
    }

    @Test
    public void testWithoutAltNameTag() {
        final Optional<String> altNameTag = extractor.extract(highwayWayTags((OsmHighwayValue.MOTORWAY.unwrap())));
        assertThat("There is should be no alt_name tag", altNameTag, is(Optional.empty()));
    }
}