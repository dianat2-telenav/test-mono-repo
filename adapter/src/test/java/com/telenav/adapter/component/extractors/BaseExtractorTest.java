package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.osm.OsmRelationTagKey;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * @author roxanal
 */
public class BaseExtractorTest {

    public static Map<String, String> highwayWayTags(final String highwayValue) {
        return Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), highwayValue);
    }

    public static Map<String, String> altNameTags(final String tagValue) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.ALT_NAME.telenavPrefix(), tagValue);
        tags.put(OsmTagKey.ALT_NAME.unwrap(), tagValue);
        return tags;
    }

    public static Map<String, String> nameTags(final String tagValue) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.NAME.telenavPrefix(), tagValue);
        tags.put(OsmTagKey.NAME.unwrap(), tagValue);
        return tags;
    }

    public static Map<String, String> refTags(final String tagValue) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.REF.telenavPrefix(), tagValue);
        tags.put(OsmTagKey.REF.telenavPrefix() + "_1", tagValue);
        tags.put(OsmTagKey.REF.telenavPrefix() + "_2", tagValue);
        tags.put(OsmTagKey.REF.unwrap(), tagValue);
        return tags;
    }

    public static Map<String, String> oneRefTag(final String tagValue) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.REF.telenavPrefix(), tagValue);
        return tags;
    }

    public static Map<String, String> roundaboutTags(final String tagValue) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.JUNCTION.unwrap(), tagValue);
        return tags;
    }

    public static Map<String, String> laneTags(final String laneValue) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.LANES.unwrap(), laneValue);
        return tags;
    }

    public static Map<String, String> junctionRefTags(final String laneValue) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.JUNCTION_REF.unwrap(), laneValue);
        return tags;
    }

    public static Map<String, String> typeTags(final String typeValue) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmRelationTagKey.TYPE.unwrap(), typeValue);
        return tags;
    }
}