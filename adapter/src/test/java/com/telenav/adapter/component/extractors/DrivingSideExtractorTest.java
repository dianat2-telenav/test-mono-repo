package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import org.junit.Test;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;


/**
 * Unit tests for {@link DrivingSideExtractor}.
 *
 * @author ioanao
 */
public class DrivingSideExtractorTest {

    private final DrivingSideExtractor extractor = new DrivingSideExtractor();

    @Test
    public void emptyCollectionOfTags() {
        final Map<String, String> tags = Collections.emptyMap();
        final Optional<String> drivingSide = extractor.extract(tags);

        assertFalse(drivingSide.isPresent());
    }

    @Test
    public void tagsWithoutDrivingSideTag() {
        final Map<String, String> tags = Collections.singletonMap("oneway", "true");
        final Optional<String> drivingSide = extractor.extract(tags);

        assertFalse(drivingSide.isPresent());
    }

    @Test
    public void tagsWithDrivingSide() {
        final Map<String, String> tags = new HashMap<>();
        tags.put("oneway", "true");
        tags.put(TnTagKey.TN_DRIVING_SIDE.unwrap(), DefaultTagValue.RIGHT_DRIVING_SIDE.unwrap());
        final Optional<String> drivingSide = extractor.extract(tags);

        assertEquals(drivingSide.get(), DefaultTagValue.RIGHT_DRIVING_SIDE.unwrap());
    }
}