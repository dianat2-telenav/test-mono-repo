package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


/**
 * @author dianat2
 */
public class DstExtractorTest {

    private final DstExtractor extractor = new DstExtractor();

    @Test
    public void extract_entityHasDst_dstValue() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        // dst=1 (first Sunday of October 0:00 - second Sunday of April 0:00)
        tags.put(TnTagKey.TN_DST.unwrap(), "1");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertTrue(result.isPresent());
        assertThat(result.get(), is("1"));
    }

    @Test
    public void extract_entityHasNoDst_optionalEmpty() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), "motorway");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertFalse(result.isPresent());
    }
}