package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmRelationTagKey;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.adapter.bean.SpeedCategory;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * @author petrum
 */
public class HighwayBasedSpeedCategoryExtractorTest {

    private final HighwayBasedSpeedCategoryExtractor extractor = new HighwayBasedSpeedCategoryExtractor();

    @Test
    public void motorway() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_101_130));
    }

    @Test
    public void motorwayLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY_LINK.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_51_70));
    }

    @Test
    public void trunk() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_91_100));
    }

    @Test
    public void trunkLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK_LINK.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_51_70));
    }

    @Test
    public void primary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.PRIMARY.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_71_90));
    }

    @Test
    public void primaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.PRIMARY_LINK.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void secondary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SECONDARY.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_51_70));
    }

    @Test
    public void secondaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SECONDARY_LINK.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void residential() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void residentialLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void service() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SERVICE.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_11_30));
    }

    @Test
    public void tertiary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TERTIARY.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void tertiaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TERTIARY_LINK.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void unclassified() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.UNCLASSIFIED.unwrap());
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void shipFerry() {
        final Map<String, String> tags = Collections.singletonMap(OsmRelationTagKey.ROUTE.unwrap(), "ferry");
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_11_30));
    }

    @Test
    public void railFerry() {
        final Map<String, String> tags = Collections.singletonMap(OsmRelationTagKey.ROUTE.unwrap(), "shuttle_train");
        final Optional<SpeedCategory> sc = extractor.extract(tags);
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_51_70));
    }

    @Test
    public void speedCategory14() {
        final String[] values = { "road", "track", "undefined", "unknown" };
        Arrays.stream(values).forEach(value -> {
            final Map<String, String> tags = Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), value);
            final Optional<SpeedCategory> sc = extractor.extract(tags);
            assertTrue("Speed Category cannot be null", sc.isPresent());
            assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_11_30));
        });
    }

    @Test
    public void speedCategory7() {
        final String[] values =
                { "footway", "pedestrian", "steps", "path", "bridleway", "construction", "cycleway", "path",
                        "bus_guideway", "living_street", "private" };
        Arrays.stream(values).forEach(value -> {
            final Map<String, String> tags = Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), value);
            final Optional<SpeedCategory> sc = extractor.extract(tags);
            assertTrue("Speed Category cannot be null", sc.isPresent());
            assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_LESS_THAN_11));
        });
    }

    @Test
    public void defaultValue() {
        final Optional<SpeedCategory> sc =
                extractor.extract(Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), "emergency_bay"));
        assertTrue("Speed Category cannot be null", sc.isPresent());
        assertThat(sc.get(), is(SpeedCategory.SPEED_CAT_LESS_THAN_11));
    }
}
