package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/**
 * @author adrianal
 */
public class HighwayExtractorTest {

    private final HighwayExtractor extractor = new HighwayExtractor();

    @Test
    public void extractHighwayValue() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), "primary");
        assertThat(extractor.extract(tags).get(), is("primary"));
    }

    @Test
    public void extractWhenNoHighwayValueIsPresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.ONEWAY.unwrap(), "yes");
        assertThat("There should be no lane tag!", extractor.extract(tags), is(Optional.empty()));
    }
}