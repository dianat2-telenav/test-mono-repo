package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * Unit tests for {@link MaxSpeedRtExtractor}.
 *
 * @author petrum
 */
public class MaxSpeedRtExtractorTest {

    @Test
    public void noMaxSpeedTagIsPresent() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        final Optional<Map<String, String>> maxSpeedTags = new MaxSpeedRtExtractor().extract(tags);

        assertFalse(maxSpeedTags.isPresent());
    }

    @Test
    public void maxSpeedTagIsPresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.MAXSPEED.unwrap(), "20");
        final Optional<Map<String, String>> maxSpeedTags = new MaxSpeedRtExtractor().extract(tags);

        assertTrue(maxSpeedTags.isPresent());
        assertThat(maxSpeedTags.get().size(), is(1));
        assertThat(maxSpeedTags.get(), hasEntry(OsmTagKey.MAXSPEED.unwrap(), "20"));
    }

    @Test
    public void maxSpeedAndMaxSpeedForwardTagArePresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.MAXSPEED.unwrap(), "20");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "30");
        final Optional<Map<String, String>> maxSpeedTags = new MaxSpeedRtExtractor().extract(tags);

        assertTrue(maxSpeedTags.isPresent());
        assertThat(maxSpeedTags.get().size(), is(1));
        assertThat(maxSpeedTags.get(), hasEntry(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "30"));
    }

    @Test
    public void maxSpeedAndMaxSpeedBackwardTagArePresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.MAXSPEED.unwrap(), "20");
        tags.put(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "30");
        final Optional<Map<String, String>> maxSpeedTags = new MaxSpeedRtExtractor().extract(tags);

        assertTrue(maxSpeedTags.isPresent());
        assertThat(maxSpeedTags.get().size(), is(1));
        assertThat(maxSpeedTags.get(), hasEntry(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "30"));
    }

    @Test
    public void maxSpeedAndMaxSpeedBackwardAndMaxSpeedForwardTagArePresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.MAXSPEED.unwrap(), "20");
        tags.put(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "30");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "40");
        final Optional<Map<String, String>> maxSpeedTags = new MaxSpeedRtExtractor().extract(tags);

        assertTrue(maxSpeedTags.isPresent());
        assertThat(maxSpeedTags.get().size(), is(2));
        assertThat(maxSpeedTags.get(), hasEntry(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "30"));
        assertThat(maxSpeedTags.get(), hasEntry(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "40"));
    }
}
