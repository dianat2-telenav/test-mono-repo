package com.telenav.adapter.component.extractors;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmTagKey.MOTORCAR;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;


/**
 * Unit tests for {@link MotorCarExtractor}.
 *
 * @author catalinm
 */
public class MotorCarExtractorTest {

    private final MotorCarExtractor extractor = new MotorCarExtractor();

    @Test
    public void tagsWithoutMotorCarTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put("tag1", "yes");
        tags.put("tag2", "yes");
        final Optional<String> motorCarTag = extractor.extract(tags);
        assertFalse(motorCarTag.isPresent());
    }

    @Test
    public void tagsWithMotorCarTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put("tag1", "yes");
        tags.put(MOTORCAR.unwrap(), "no");
        final Optional<String> motorCarTag = extractor.extract(tags);
        assertTrue(motorCarTag.isPresent());
    }
}