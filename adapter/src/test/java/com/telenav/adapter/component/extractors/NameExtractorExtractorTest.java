package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/**
 * @author roxanal
 */
public class NameExtractorExtractorTest extends BaseExtractorTest {

    private final TnNameExtractor extractor = new TnNameExtractor();

    @Test
    public void testNameTag() {
        final Optional<String> nameTag = extractor.extract(nameTags("Park Dr"));
        assertThat(nameTag.get(), is("Park Dr"));
    }

    @Test
    public void testWithoutNameTag() {
        final Optional<String> nameTag = extractor.extract(highwayWayTags((OsmHighwayValue.MOTORWAY.unwrap())));
        assertThat("There should be no name tag", nameTag, is(Optional.empty()));
    }
}