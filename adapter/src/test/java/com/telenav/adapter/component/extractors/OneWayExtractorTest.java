package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;


/**
 * @author roxanal
 */
public class OneWayExtractorTest {

    private final OneWayExtractor extractor = new OneWayExtractor();

    @Test
    public void tagsWithOsmOneWayTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.ONEWAY.unwrap(), "1");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "30");
        final Optional<Map<String, String>> oneWayValues = extractor.extract(tags);

        assertTrue(oneWayValues.isPresent());
        assertEquals(1, oneWayValues.get().size());
        assertEquals("1", oneWayValues.get().get(OsmTagKey.ONEWAY.unwrap()));
    }

    @Test
    public void tagsWithOsmOneWayTagAndProbeTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.ONEWAY.unwrap(), "1");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "30");
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "-1");
        final Optional<Map<String, String>> oneWayValues = extractor.extract(tags);

        assertTrue(oneWayValues.isPresent());
        assertEquals(2, oneWayValues.get().size());
        assertEquals("1", oneWayValues.get().get(OsmTagKey.ONEWAY.unwrap()));
        assertEquals("-1", oneWayValues.get().get(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES)));
    }

    @Test
    public void tagsWithOsmOneWayTagProbeTagAndImageTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.ONEWAY.unwrap(), "1");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "30");
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "-1");
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "-1");
        final Optional<Map<String, String>> oneWayValues = extractor.extract(tags);

        assertTrue(oneWayValues.isPresent());
        assertEquals(3, oneWayValues.get().size());
        assertEquals("1", oneWayValues.get().get(OsmTagKey.ONEWAY.unwrap()));
        assertEquals("-1", oneWayValues.get().get(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES)));
        assertEquals("-1", oneWayValues.get().get(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES)));
    }

    @Test
    public void tagsWithDetectionsTags() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "30");
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "-1");
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "-1");
        final Optional<Map<String, String>> oneWayValues = extractor.extract(tags);

        assertTrue(oneWayValues.isPresent());
        assertEquals(2, oneWayValues.get().size());
        assertEquals("-1", oneWayValues.get().get(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES)));
        assertEquals("-1", oneWayValues.get().get(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES)));
    }

    @Test
    public void tagsWithoutAnyOneWayRelatedTags() {
        final Optional<Map<String, String>> oneWayValues =
                extractor.extract(Collections.singletonMap("highway", "motorway"));
        assertFalse(oneWayValues.isPresent());
    }
}