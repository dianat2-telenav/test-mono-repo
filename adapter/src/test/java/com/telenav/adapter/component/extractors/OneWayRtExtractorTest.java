package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * Unit tests for {@link OneWayRtExtractor}.
 *
 * @author petrum
 */
public class OneWayRtExtractorTest {

    @Test
    public void noOneWayTagIsPresent() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        final Optional<String> oneway = new OneWayRtExtractor().extract(tags);

        assertFalse(oneway.isPresent());
    }

    @Test
    public void osmOneWayTagIsPresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.ONEWAY.unwrap(), "yes");
        final Optional<String> oneway = new OneWayRtExtractor().extract(tags);

        assertTrue(oneway.isPresent());
        assertThat(oneway.get(), is("yes"));
    }

    @Test
    public void probesOneWayTagIsPresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "1");
        final Optional<String> oneway = new OneWayRtExtractor().extract(tags);

        assertTrue(oneway.isPresent());
        assertThat(oneway.get(), is("1"));
    }

    @Test
    public void probesAndOsmOneWayTagArePresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "1");
        tags.put(OsmTagKey.ONEWAY.unwrap(), "-1");
        final Optional<String> oneway = new OneWayRtExtractor().extract(tags);

        assertTrue(oneway.isPresent());
        assertThat(oneway.get(), is("-1"));
    }

    @Test
    public void imagesOneWayTagIsPresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "1");
        final Optional<String> oneway = new OneWayRtExtractor().extract(tags);

        assertTrue(oneway.isPresent());
        assertThat(oneway.get(), is("1"));
    }

    @Test
    public void imagesAndOsmOneWayTagArePresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "1");
        tags.put(OsmTagKey.ONEWAY.unwrap(), "-1");
        final Optional<String> oneway = new OneWayRtExtractor().extract(tags);

        assertTrue(oneway.isPresent());
        assertThat(oneway.get(), is("1"));
    }

    @Test
    public void imagesAndProbesOneWayTagArePresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "1");
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "-1");
        final Optional<String> oneway = new OneWayRtExtractor().extract(tags);

        assertTrue(oneway.isPresent());
        assertThat(oneway.get(), is("-1"));
    }

    @Test
    public void imagesProbesAndOsmOneWayTagArePresent() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "1");
        tags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "-1");
        tags.put(OsmTagKey.ONEWAY.unwrap(), "yes");
        final Optional<String> oneway = new OneWayRtExtractor().extract(tags);

        assertTrue(oneway.isPresent());
        assertThat(oneway.get(), is("-1"));
    }
}
