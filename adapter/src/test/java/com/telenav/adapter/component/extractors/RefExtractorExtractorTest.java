package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import org.junit.Test;

import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/**
 * @author roxanal
 */
public class RefExtractorExtractorTest extends BaseExtractorTest {

    private final RefExtractor extractor = new RefExtractor();

    @Test
    public void testRefTag() {
        final Optional<Map<String, String>> refTags = extractor.extract(refTags("MI-85 N"));
        assertThat(refTags.get().size(), is(3));
        assertThat(refTags.get().get(OsmTagKey.REF.telenavPrefix()), is("MI-85 N"));
        assertThat(refTags.get().get(OsmTagKey.REF.telenavPrefix() + "_1"), is("MI-85 N"));
        assertThat(refTags.get().get(OsmTagKey.REF.telenavPrefix() + "_2"), is("MI-85 N"));
    }

    @Test
    public void testWithOneRefTag() {
        final Optional<Map<String, String>> refTags = extractor.extract(oneRefTag("MI-85 N"));
        assertThat(refTags.get().size(), is(1));
        assertThat(refTags.get().get(OsmTagKey.REF.telenavPrefix()), is("MI-85 N"));
    }

    @Test
    public void testWithoutRefTag() {
        final Optional<Map<String, String>> refTag =
                extractor.extract(highwayWayTags((OsmHighwayValue.MOTORWAY.unwrap())));
        assertThat("There should be no ref tag", refTag, is(Optional.empty()));
    }
}