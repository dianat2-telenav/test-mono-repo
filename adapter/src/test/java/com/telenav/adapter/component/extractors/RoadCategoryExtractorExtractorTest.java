package com.telenav.adapter.component.extractors;

import com.telenav.adapter.bean.Tag;
import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/**
 * @author roxanal
 */
public class RoadCategoryExtractorExtractorTest extends BaseExtractorTest {

    private final RoadCategoryExtractor extractor = new RoadCategoryExtractor();

    @Test
    public void testRoadTypeTag() {
        final Optional<List<Tag>> roadTypeTags = extractor.extract(buildTagMap(TN_ROAD_TYPE.unwrap(), "1"));
        assertThat(roadTypeTags.get().size(), is(1));
        assertThat(roadTypeTags.get().get(0).key(), is(TN_ROAD_TYPE.unwrap()));
        assertThat(roadTypeTags.get().get(0).value(), is("1"));
    }

    @Test
    public void testRoadSubtypeTag() {
        final Optional<List<Tag>> roadSubtypeTag = extractor.extract(buildTagMap(TN_ROAD_SUBTYPE.unwrap(), "7"));
        assertThat(roadSubtypeTag.get().size(), is(1));
        assertThat(roadSubtypeTag.get().get(0).key(), is(TN_ROAD_SUBTYPE.unwrap()));
        assertThat(roadSubtypeTag.get().get(0).value(), is("7"));
    }

    @Test
    public void testFunctionalClassTag() {
        final Optional<List<Tag>> functionalClassTag =
                extractor.extract(buildTagMap(TN_FUNCTIONAL_CLASS.unwrap(), "3"));
        assertThat(functionalClassTag.get().size(), is(1));
        assertThat(functionalClassTag.get().get(0).key(), is(TN_FUNCTIONAL_CLASS.unwrap()));
        assertThat(functionalClassTag.get().get(0).value(), is("3"));
    }

    @Test
    public void testUpgradedFunctionalClassTag() {
        final Optional<List<Tag>> functionalClassTag =
                extractor.extract(buildTagMap(TN_FUNCTIONAL_CLASS_UPGRADE.unwrap(), "5"));
        assertThat(functionalClassTag.get().size(), is(1));
        assertThat(functionalClassTag.get().get(0).key(), is(TN_FUNCTIONAL_CLASS_UPGRADE.unwrap()));
        assertThat(functionalClassTag.get().get(0).value(), is("5"));
    }

    @Test
    public void testSpeedCategoryTag() {
        final Optional<List<Tag>> speedCategoryTag = extractor.extract(buildTagMap(TN_SPEED_CATEGORY.unwrap(), "15"));
        assertThat(speedCategoryTag.get().size(), is(1));
        assertThat(speedCategoryTag.get().get(0).key(), is(TN_SPEED_CATEGORY.unwrap()));
        assertThat(speedCategoryTag.get().get(0).value(), is("15"));
    }

    @Test
    public void testRampTag() {
        final Optional<List<Tag>> speedCategoryTag = extractor.extract(buildTagMap(TN_RAMP.unwrap(), "Y"));
        assertThat(speedCategoryTag.get().size(), is(1));
        assertThat(speedCategoryTag.get().get(0).key(), is(TN_RAMP.unwrap()));
        assertThat(speedCategoryTag.get().get(0).value(), is("Y"));
    }

    @Test
    public void testIntersectionLinkTag() {
        final Optional<List<Tag>> speedCategoryTag =
                extractor.extract(buildTagMap(TN_INTERSECTION_LINK.unwrap(), "someValue"));
        assertThat(speedCategoryTag.get().size(), is(1));
        assertThat(speedCategoryTag.get().get(0).key(), is(TN_INTERSECTION_LINK.unwrap()));
        assertThat(speedCategoryTag.get().get(0).value(), is("someValue"));
    }

    @Test
    public void testConnectionRoadTag() {
        final Optional<List<Tag>> speedCategoryTag =
                extractor.extract(buildTagMap(TN_CONNECTION_ROAD.unwrap(), "someValue"));
        assertThat(speedCategoryTag.get().size(), is(1));
        assertThat(speedCategoryTag.get().get(0).key(), is(TN_CONNECTION_ROAD.unwrap()));
        assertThat(speedCategoryTag.get().get(0).value(), is("someValue"));
    }

    @Test
    public void testDualWayTag() {
        final Optional<List<Tag>> speedCategoryTag = extractor.extract(buildTagMap(TN_DUAL_WAY.unwrap(), "someValue"));
        assertThat(speedCategoryTag.get().size(), is(1));
        assertThat(speedCategoryTag.get().get(0).key(), is(TN_DUAL_WAY.unwrap()));
        assertThat(speedCategoryTag.get().get(0).value(), is("someValue"));
    }

    @Test
    public void testWithNoFCTags() {
        final Optional<List<Tag>> fcTag = extractor.extract(highwayWayTags((OsmHighwayValue.MOTORWAY.unwrap())));
        assertThat("There should be no name tag", fcTag, is(Optional.empty()));
    }

    private Map<String, String> buildTagMap(final String tagKey, final String tagValue) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(tagKey, tagValue);
        return tags;
    }
}