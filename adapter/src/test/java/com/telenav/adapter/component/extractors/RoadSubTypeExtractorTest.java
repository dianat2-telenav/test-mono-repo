package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.osm.OsmRelationTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.adapter.bean.RoadSubType;
import org.junit.Test;

import java.util.*;

import static com.telenav.datapipelinecore.osm.OsmHighwayValue.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * @author petrum
 */
public class RoadSubTypeExtractorTest {

    private final RoadSubTypeExtractor extractor = new RoadSubTypeExtractor();

    @Test
    public void motorway() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void motorwayOneWayYes() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap());
        tags.put(OsmTagKey.ONEWAY.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void motorwayOneWayNegative1() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap());
        tags.put(OsmTagKey.ONEWAY.unwrap(), "-1");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void motorwayOneWayNo() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap());
        tags.put(OsmTagKey.ONEWAY.unwrap(), "no");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void motorwayLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY_LINK.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.RAMP));
    }

    @Test
    public void trunk() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void trunkOneWayYes() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK.unwrap());
        tags.put(OsmTagKey.ONEWAY.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void trunkOneWayNegative1() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK.unwrap());
        tags.put(OsmTagKey.ONEWAY.unwrap(), "-1");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void trunkOneWayNo() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK.unwrap());
        tags.put(OsmTagKey.ONEWAY.unwrap(), "no");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void trunkLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK_LINK.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void primary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.PRIMARY.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void primaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.PRIMARY_LINK.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void secondary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SECONDARY.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void secondaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SECONDARY_LINK.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void residential() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void motorwayAndTunnel() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap());
        tags.put(OsmTagKey.TUNNEL.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.TUNNEL));
    }

    @Test
    public void motorwayOneWayAndTunnel() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap());
        tags.put(OsmTagKey.TUNNEL.unwrap(), "yes");
        tags.put(OsmTagKey.ONEWAY.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.TUNNEL));
    }

    @Test
    public void motorwayOneWayAndBridge() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap());
        tags.put(OsmTagKey.BRIDGE.unwrap(), "yes");
        tags.put(OsmTagKey.ONEWAY.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.BRIDGE));
    }

    @Test
    public void residentialAndBridge() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), RESIDENTIAL.unwrap());
        tags.put(OsmTagKey.BRIDGE.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.BRIDGE));
    }

    @Test
    public void residentialLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL_LINK.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void service() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SERVICE.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.SERVICE));
    }

    @Test
    public void tertiary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TERTIARY.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void tertiaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TERTIARY_LINK.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void ferry() {
        final Optional<RoadSubType> rst =
                extractor.extract(Collections.singletonMap(OsmRelationTagKey.ROUTE.unwrap(), "ferry"));
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.DEFAULT));
    }

    @Test
    public void roundabout() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), PRIMARY.unwrap());
        tags.put(OsmTagKey.JUNCTION.unwrap(), "roundabout");
        tags.put(OsmTagKey.ONEWAY.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.ROUNDABOUT));
    }

    @Test
    public void connectionRoadService() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), SERVICE.unwrap());
        tags.put(TnTagKey.TN_CONNECTION_ROAD.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.SERVICE));
    }

    @Test
    public void connectionRoadBridge() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), PRIMARY_LINK.unwrap());
        tags.put(TnTagKey.TN_CONNECTION_ROAD.unwrap(), "yes");
        tags.put(OsmTagKey.BRIDGE.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void residentialAndConnection() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), RESIDENTIAL.unwrap());
        tags.put(TnTagKey.TN_CONNECTION_ROAD.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void linkAndRamp() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK_LINK.unwrap());
        tags.put(TnTagKey.TN_RAMP.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.RAMP));
    }

    @Test
    public void serviceAndRamp() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), SERVICE.unwrap());
        tags.put(TnTagKey.TN_RAMP.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.RAMP));
    }

    @Test
    public void intersectionLinkAndService() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), SERVICE.unwrap());
        tags.put(TnTagKey.TN_INTERSECTION_LINK.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.INTERSECTION_LINK));
    }

    @Test
    public void linkAndBridge() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK_LINK.unwrap());
        tags.put(OsmTagKey.BRIDGE.unwrap(), "yes");
        tags.put(OsmTagKey.ONEWAY.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void connectionLinkTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), SECONDARY_LINK.unwrap());
        tags.put(TnTagKey.TN_CONNECTION_ROAD.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void intersectionLink() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), RESIDENTIAL.unwrap());
        tags.put(TnTagKey.TN_INTERSECTION_LINK.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.INTERSECTION_LINK));
    }


    @Test
    public void intersectionLinkAndTrunk() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK.unwrap());
        tags.put(TnTagKey.TN_INTERSECTION_LINK.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.INTERSECTION_LINK));
    }

    @Test
    public void intersectionLinkAndPrimary() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), PRIMARY.unwrap());
        tags.put(TnTagKey.TN_INTERSECTION_LINK.unwrap(), "yes");
        tags.put(OsmTagKey.ONEWAY.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.INTERSECTION_LINK));
    }


    @Test
    public void connectionRoadAndTrunk() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK.unwrap());
        tags.put(TnTagKey.TN_CONNECTION_ROAD.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void connectionRoadAndRamp() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK.unwrap());
        tags.put(TnTagKey.TN_CONNECTION_ROAD.unwrap(), "yes");
        tags.put(TnTagKey.TN_RAMP.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.RAMP));
    }

    @Test
    public void connectionRoadAndBridge() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK.unwrap());
        tags.put(TnTagKey.TN_CONNECTION_ROAD.unwrap(), "yes");
        tags.put(OsmTagKey.BRIDGE.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void intersectionLinkAndRamp() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK.unwrap());
        tags.put(TnTagKey.TN_INTERSECTION_LINK.unwrap(), "yes");
        tags.put(TnTagKey.TN_RAMP.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.RAMP));
    }

    @Test
    public void intersectionLinkAndBridge() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK.unwrap());
        tags.put(TnTagKey.TN_INTERSECTION_LINK.unwrap(), "yes");
        tags.put("bridge", "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.INTERSECTION_LINK));
    }

    @Test
    public void intersectionLinkAndConnection() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), RESIDENTIAL.unwrap());
        tags.put(TnTagKey.TN_INTERSECTION_LINK.unwrap(), "yes");
        tags.put(TnTagKey.TN_CONNECTION_ROAD.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.INTERSECTION_LINK));
    }

    @Test
    public void highwayNotInTXDList() {
        final Optional<RoadSubType> rst =
                extractor.extract(Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), "emergency_bay"));
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
    }

    @Test
    public void restOfHighwayValues() {
        final String[] values =
                { "road", "track", "unclassified", "undefined", "unknown", "living_street", "private", "footway",
                        "pedestrian", "steps", "bridleway", "construction", "cycleway", "path", "bus_guideway" };
        Arrays.stream(values).forEach(value -> {
            final Optional<RoadSubType> rst =
                    extractor.extract(Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), value));
            assertTrue("Road Sub Type cannot be null", rst.isPresent());
            assertThat(rst.get(), is(RoadSubType.DEFAULT));
        });
    }

    @Test
    public void highwayTagIsMissing() {
        final Map<String, String> tags = new HashMap<>();
        tags.put("destination", "The Moon");
        tags.put("oneway", "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type should be null", rst.isPresent());
    }

    @Test
    public void dualWayTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), RESIDENTIAL.unwrap());
        tags.put(TnTagKey.TN_DUAL_WAY.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.MAIN_ROAD));
    }

    @Test
    public void dualWayTagAndIntersectionLink() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), RESIDENTIAL.unwrap());
        tags.put(TnTagKey.TN_DUAL_WAY.unwrap(), "yes");
        tags.put(TnTagKey.TN_INTERSECTION_LINK.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.INTERSECTION_LINK));
    }

    @Test
    public void dualWayTagAndMainRoad() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), RESIDENTIAL.unwrap());
        tags.put(TnTagKey.TN_DUAL_WAY.unwrap(), "yes");
        tags.put(OsmTagKey.HIGHWAY.unwrap(), PRIMARY.unwrap());
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.MAIN_ROAD));
    }

    @Test
    public void prioritySorting() {
        final List<RoadSubType> subTypes =
                Arrays.asList(RoadSubType.MAIN_ROAD, RoadSubType.MAIN_ROAD, RoadSubType.CONNECTING_ROAD);
        final RoadSubTypeExtractor roadSubTypeExtractor = new RoadSubTypeExtractor();
        final RoadSubType roadSubType = roadSubTypeExtractor.highestPriority(subTypes);
        assertThat(roadSubType, is(RoadSubType.CONNECTING_ROAD));
    }

    @Test
    public void bridgeAndTrunk() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), TRUNK.unwrap());
        tags.put(OsmTagKey.BRIDGE.unwrap(), "yes");
        final Optional<RoadSubType> rst = extractor.extract(tags);
        assertTrue("Road Sub Type cannot be null", rst.isPresent());
        assertThat(rst.get(), is(RoadSubType.BRIDGE));
    }
}
