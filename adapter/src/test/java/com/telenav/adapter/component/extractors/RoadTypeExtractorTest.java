package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmRelationTagKey;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import com.telenav.adapter.bean.RoadType;
import com.telenav.adapter.rules.RoadTypeRule;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;


/**
 * @author petrum
 */
@RunWith(JUnitParamsRunner.class)
public class RoadTypeExtractorTest {

    private final RoadTypeExtractor extractor = new RoadTypeExtractor();

    @Test
    public void motorway() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.FREEWAY));
    }

    @Test
    public void motorwayLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY_LINK.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.FREEWAY));
    }

    @Test
    public void trunk() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.HIGHWAY));
    }

    @Test
    public void trunkLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK_LINK.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.HIGHWAY));
    }

    @Test
    public void primary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.PRIMARY.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.THROUGHWAY));
    }

    @Test
    public void primaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.PRIMARY_LINK.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.THROUGHWAY));
    }

    //TODO trunk link, primary link with bifurcation, road angle etc

    @Test
    public void secondary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SECONDARY.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.THROUGHWAY));
    }

    @Test
    public void secondaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SECONDARY_LINK.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.THROUGHWAY));
    }

    @Test
    public void residential() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.LOCAL));
    }

    @Test
    public void residentialLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL_LINK.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.LOCAL));
    }

    @Test
    public void service() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SERVICE.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.VERY_LOW_SPEED));
    }

    @Test
    public void tertiary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TERTIARY.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.LOCAL));
    }

    @Test
    public void tertiaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TERTIARY_LINK.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.LOCAL));
    }

    @Test
    public void unclassified() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.UNCLASSIFIED.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.LOCAL));
    }

    @Test
    public void ferry() {
        final Optional<RoadType> rt =
                extractor.extract(Collections.singletonMap(OsmRelationTagKey.ROUTE.unwrap(), "ferry"));
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.FERRY));
    }

    @Test
    public void highwayNotInTXDList() {
        final Optional<RoadType> rt =
                extractor.extract(Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), "emergency_bay"));
        assertTrue("Road Type should be null", rt.isPresent());
    }

    @Test
    public void roadType6() {
        final String[] values = {"road", "track", "undefined", "unknown"};
        Arrays.stream(values).forEach(value -> {
            final Optional<RoadType> rt =
                    extractor.extract(Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), value));
            assertTrue("Road Type cannot be null", rt.isPresent());
            assertThat(rt.get(), is(RoadType.VERY_LOW_SPEED));
        });
    }

    @Test
    public void roadType7() {
        final String[] values = {"living_street", "private"};
        Arrays.stream(values).forEach(value -> {
            final Optional<RoadType> rt =
                    extractor.extract(Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), value));
            assertTrue("Road Type cannot be null", rt.isPresent());
            assertThat(rt.get(), is(RoadType.PRIVATE_ROAD));
        });
    }

    @Test
    public void roadType8() {
        final String[] values = {"footway", "pedestrian", "steps", "path"};
        Arrays.stream(values).forEach(value -> {
            final Optional<RoadType> rt =
                    extractor.extract(Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), value));
            assertTrue("Road Type cannot be null", rt.isPresent());
            assertThat(rt.get(), is(RoadType.WALKWAY));
        });
    }

    @Test
    public void roadType9() {
        final String[] values = {"bridleway", "construction"};
        Arrays.stream(values).forEach(value -> {
            final Optional<RoadType> rt =
                    extractor.extract(Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), value));
            assertTrue("Road Type cannot be null", rt.isPresent());
            assertThat(rt.get(), is(RoadType.NON_NAVIGABLE));
        });
    }

    @Test
    public void busGuideway() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.BUS_GUIDEWAY.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.PUBLIC_VEHICLE_ONLY));
    }

    @Test
    public void residentialAccessPrivate() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        tags.put(OsmTagKey.ACCESS.unwrap(), "private");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.LOCAL));
    }

    @Test
    public void residentialAccessNo() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        tags.put(OsmTagKey.ACCESS.unwrap(), "no");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.NON_NAVIGABLE));
    }

    @Test
    public void highwayTagIsMissing() {
        final Optional<RoadType> rt = extractor.extract(Collections.singletonMap("destination", "The Moon"));
        assertTrue("Road Type should be null", rt.isPresent());
    }

    @Test
    public void speedUnder30KmPerHour() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        tags.put(OsmTagKey.MAXSPEED.unwrap(), "29");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.VERY_LOW_SPEED));
    }

    @Test
    public void speedUnder30MilesPerHour() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        tags.put(OsmTagKey.MAXSPEED.unwrap(), "18 mph");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.VERY_LOW_SPEED));
    }

    @Test
    public void speedOver30KmPerHour() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        tags.put(OsmTagKey.MAXSPEED.unwrap(), "50");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.LOCAL));
    }

    @Test
    public void speedOver30MilesPerHour() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        tags.put(OsmTagKey.MAXSPEED.unwrap(), "30 mph");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.LOCAL));
    }

    @Test
    public void speedLimitWithKphString() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        tags.put(OsmTagKey.MAXSPEED.unwrap(), "18 kph");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.VERY_LOW_SPEED));
    }

    @Test
    public void invalidSpeedLimit() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        tags.put(OsmTagKey.MAXSPEED.unwrap(), "78m ph");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
    }

    @Test
    public void detectSpeedIfMaxspeedKMH() {
        final RoadTypeExtractor roadTypeExtractor = new RoadTypeExtractor();
        assertThat(30, is(roadTypeExtractor.detectSpeed("30")));
    }

    @Test
    public void detectSpeedIfMaxSpeedMPH() {
        final RoadTypeExtractor roadTypeExtractor = new RoadTypeExtractor();
        assertThat(28, is(roadTypeExtractor.detectSpeed("18 mph")));
    }

    @Test
    @Parameters({"residential", "tertiary", "unclassified", "tertiary_link", "residential_link"})
    public void isLocal(final String highwayValue) {
        final RoadTypeExtractor roadTypeExtractor = new RoadTypeExtractor();
        assertThat(true, is(roadTypeExtractor.isLocalRoad(highwayValue)));
    }

    @Test
    public void isNotLocalRoad() {
        final RoadTypeExtractor roadTypeExtractor = new RoadTypeExtractor();
        assertThat(false, is(roadTypeExtractor.isLocalRoad("primary")));
    }

    @Test
    public void cyclewayAndRst() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.CYCLEWAY.unwrap());
        tags.put(TnTagKey.TN_ROAD_SUBTYPE.unwrap(), "1");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.NON_NAVIGABLE));
    }

    @Test
    public void privateAndWalkWay() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.FOOTWAY.unwrap());
        tags.put(OsmTagKey.ACCESS.unwrap(), "private");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.WALKWAY));
    }

    @Test
    public void accessNoAndFreeway() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(OsmTagKey.ACCESS.unwrap(), "no");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.NON_NAVIGABLE));
    }

    @Test
    public void nonNavigableAndWalkWay() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.FOOTWAY.unwrap());
        tags.put(OsmTagKey.ACCESS.unwrap(), "no");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertTrue("Road Type cannot be null", rt.isPresent());
        assertThat(rt.get(), is(RoadType.NON_NAVIGABLE));
    }

    @Test
    @Parameters({"city", "town", "município"})
    public void urbanHighwayWithBorderType(final String borderTypeValue) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put("border_type", "município");
        final Optional<RoadType> rt = extractor.extract(tags);
        assertThat(rt.get(), is(RoadType.FREEWAY_URBAN));
    }

    @Test
    @Parameters({"7", "8", "9", "10", "11", "12"})
    public void urbanAdminLevels(final String urbanAdminLevel) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put("admin_level", urbanAdminLevel);
        final Optional<RoadType> rt = extractor.extract(tags);
        assertThat(rt.get(), is(RoadType.FREEWAY_URBAN));
    }

    @Test
    @Parameters({"township", "village"})
    public void nonUrbanHighwayWithBorderType(final String borderTypeValue) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put("border_type", borderTypeValue);
        final Optional<RoadType> rt = extractor.extract(tags);
        assertFalse(rt.get().equals(RoadType.FREEWAY_URBAN));
    }

    @Test
    public void nonUrbanHighwayWithoutBorderType() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        final Optional<RoadType> rt = extractor.extract(tags);
        assertFalse(rt.get().equals(RoadType.FREEWAY_URBAN));
    }

    @Test
    @Parameters({"1", "2", "3", "4", "5", "6"})
    public void nonUrbanAdminLevels(final String nonUrbanAdminLevel) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put("admin_level", nonUrbanAdminLevel);
        final Optional<RoadType> rt = extractor.extract(tags);
        assertFalse(rt.get().equals(RoadType.FREEWAY_URBAN));
    }

    private final RoadTypeRule roadTypeRule = new RoadTypeRule(new RoadTypeExtractor());

    private final List<Node> nodes =
            Collections.singletonList(new Node(1L, Latitude.degrees(21.1234), Longitude.degrees(34.1234)));
    WaySection waySection = new WaySection(1, 1, nodes, Collections.emptyMap());

    @Test
    public void roadType11CaseWhenIsRt11() {
        WaySection adaptedWaySection = roadTypeRule.apply(waySection.withTag(OsmRelationTagKey.ROUTE.unwrap(), "shuttle_train")
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "yes")
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), "yes"))
                .withTag("key1", "value1");

        final Map<String, String> tags = adaptedWaySection.tags();

        assertEquals(tags.size(), 5);

        assertThat(tags.get(OsmRelationTagKey.ROUTE.unwrap()), is("shuttle_train"));
        assertThat(tags.get(OsmTagKey.MOTORCAR.unwrap()), is("yes"));
        assertThat(tags.get(OsmTagKey.MOTORCYCLE.unwrap()), is("yes"));
        assertThat(tags.get(RoadCategoryTagKey.ROAD_TYPE.unwrap()), is("11"));
    }

    @Test
    public void roadType11CaseWhenIsNotRt11() {
        WaySection adaptedWaySection = roadTypeRule.apply(waySection.withTag(OsmRelationTagKey.ROUTE.unwrap(), "shuttle_train")
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "yes")
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), "no"))
                .withTag("key1", "value1");

        final Map<String, String> tags = adaptedWaySection.tags();

        assertEquals(tags.size(), 5);

        assertThat(tags.get(OsmRelationTagKey.ROUTE.unwrap()), is("shuttle_train"));
        assertThat(tags.get(OsmTagKey.MOTORCAR.unwrap()), is("yes"));
        assertThat(tags.get(OsmTagKey.MOTORCYCLE.unwrap()), is("no"));
        assertNull(tags.get(TnTagKey.TN_ROAD_TYPE.unwrap()));
    }

    @Test
    public void roadType11CaseWhenIsRt10WithMotorcarAndMotorcycle() {
        WaySection adaptedWaySection = roadTypeRule.apply(waySection.withTag(OsmRelationTagKey.ROUTE.unwrap(), "ferry")
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "yes")
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), "yes"));

        final Map<String, String> tags = adaptedWaySection.tags();

        assertEquals(tags.size(), 4);

        assertThat(tags.get(OsmRelationTagKey.ROUTE.unwrap()), is("ferry"));
        assertThat(tags.get(OsmTagKey.MOTORCAR.unwrap()), is("yes"));
        assertThat(tags.get(OsmTagKey.MOTORCYCLE.unwrap()), is("yes"));
        assertThat(tags.get(RoadCategoryTagKey.ROAD_TYPE.unwrap()), is("10"));
    }

    @Test
    public void roadType11CaseWithRouteValueWrongAndMotorcarAndMotorcycle() {
        WaySection adaptedWaySection = roadTypeRule.apply(waySection.withTag(OsmRelationTagKey.ROUTE.unwrap(), "roadValue")
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "yes")
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), "yes"));

        final Map<String, String> tags = adaptedWaySection.tags();

        assertEquals(tags.size(), 4);

        assertThat(tags.get(OsmRelationTagKey.ROUTE.unwrap()), is("roadValue"));
        assertThat(tags.get(OsmTagKey.MOTORCAR.unwrap()), is("yes"));
        assertThat(tags.get(OsmTagKey.MOTORCYCLE.unwrap()), is("yes"));
    }

    @Test
    public void roadType11CaseWhenIsNotRt11WithoutMotorcarAndMotorcycle() {
        WaySection adaptedWaySection = roadTypeRule.apply(waySection.withTag(OsmRelationTagKey.ROUTE.unwrap(), "shuttle_train")
                .withTag("key1", "value1"));
        final Map<String, String> tags = adaptedWaySection.tags();

        assertEquals(tags.size(), 3);

        assertThat(tags.get(OsmRelationTagKey.ROUTE.unwrap()), is("shuttle_train"));
        assertNull(tags.get(OsmTagKey.MOTORCAR.unwrap()));
        assertNull(tags.get(OsmTagKey.MOTORCYCLE.unwrap()));
        assertNull(tags.get(TnTagKey.TN_ROAD_TYPE.unwrap()));
    }

    @Test
    public void roadType11CaseWhenTagRouteDoesNotExist() {
        WaySection adaptedWaySection = roadTypeRule.apply(waySection.withTag("key1", "value1")
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "yes")
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), "yes")
                .withTag("key2", "value2"));
        final Map<String, String> tags = adaptedWaySection.tags();

        assertEquals(tags.size(), 5);

        assertNull(tags.get(OsmRelationTagKey.ROUTE.unwrap()));
        assertThat(tags.get(OsmTagKey.MOTORCAR.unwrap()), is("yes"));
        assertThat(tags.get(OsmTagKey.MOTORCYCLE.unwrap()), is("yes"));
        assertNull(tags.get(TnTagKey.TN_ROAD_TYPE.unwrap()));
    }
}