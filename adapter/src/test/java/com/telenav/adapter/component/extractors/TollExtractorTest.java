package com.telenav.adapter.component.extractors;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmTagKey.TOLL;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;


/**
 * Unit tests for {@link TollExtractor}.
 *
 * @author catalinm
 */
public class TollExtractorTest {

    private final TollExtractor extractor = new TollExtractor();

    @Test
    public void tagsWithoutTollTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put("tag1", "yes");
        tags.put("tag2", "yes");
        final Optional<String> tollCostWithNoValue = extractor.extract(tags);

        assertFalse(tollCostWithNoValue.isPresent());
    }

    @Test
    public void tagsWithNoValueTollTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put("tag1", "yes");
        tags.put(TOLL.unwrap(), "no");
        final Optional<String> tollCostWithNoValue = extractor.extract(tags);

        assertTrue(tollCostWithNoValue.isPresent());
    }

    @Test
    public void tagsWithYesValueTollTag() {
        final Map<String, String> tags = new HashMap<>();
        tags.put("tag1", "yes");
        tags.put(TOLL.unwrap(), "yes");
        final Optional<String> tollCostWithNoValue = extractor.extract(tags);

        assertTrue(tollCostWithNoValue.isPresent());
    }
}