package com.telenav.adapter.component.extractors;


import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * @author irinap
 */
public class TypeExtractorTest extends BaseExtractorTest {
    private TypeExtractor typeExtractor;

    @Before
    public void initExtractor() {
        typeExtractor = new TypeExtractor();
    }

    @Test
    public void testExtractNoTypeTag() {
        final Map<String, String> tags = new HashMap<>();
        final Optional<String> extractedValue = typeExtractor.extract(tags);
        assertFalse(extractedValue.isPresent());
    }

    @Test
    public void testExtractTypeTag() {
        final Map<String, String> tags = typeTags("tag_value");
        final Optional<String> extractedValue = typeExtractor.extract(tags);
        assertTrue(extractedValue.isPresent());
        assertEquals(extractedValue.get(), "tag_value");
    }
}
