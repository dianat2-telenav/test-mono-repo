package com.telenav.adapter.component.extractors.adminTags;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


/**
 * @author dianat2
 */
public class AdminL1LeftExtractorTest {

    private final AdminL1LeftExtractor extractor = new AdminL1LeftExtractor();

    @Test
    public void extract_entityHasAdminL1Left_adminL1LeftValue() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        tags.put(TnTagKey.TN_ADMIN_L1_LEFT.unwrap(), "11111");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertTrue(result.isPresent());
        assertThat(result.get(), is("11111"));
    }

    @Test
    public void extract_entityHasNoAdminL1Left_optionalEmpty() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), "motorway");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertFalse(result.isPresent());
    }
}