package com.telenav.adapter.component.extractors.adminTags;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


/**
 * @author adrianal
 */
public class AdminL1NameExtractorTest {

    private final AdminL1NameExtractor extractor = new AdminL1NameExtractor();

    @Test
    public void extract_entityHasAdminL1Name_adminL1NameValue() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        tags.put(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), "Jakarta Raya");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertTrue(result.isPresent());
        assertThat(result.get(), is("Jakarta Raya"));
    }

    @Test
    public void extract_entityHasAdminL1Name_optionalEmpty() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), "motorway");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertFalse(result.isPresent());
    }
}