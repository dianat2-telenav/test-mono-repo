package com.telenav.adapter.component.extractors.adminTags;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


/**
 * @author dianat2
 */
public class AdminL2RightExtractorTest {

    private final AdminL2RightExtractor extractor = new AdminL2RightExtractor();

    @Test
    public void extract_entityHasAdminL2Right_adminL2RightValue() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        tags.put(TnTagKey.TN_ADMIN_L2_RIGHT.unwrap(), "11111");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertTrue(result.isPresent());
        assertThat(result.get(), is("11111"));
    }

    @Test
    public void extract_entityHasNoAdminL2Right_optionalEmpty() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), "motorway");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertFalse(result.isPresent());
    }
}