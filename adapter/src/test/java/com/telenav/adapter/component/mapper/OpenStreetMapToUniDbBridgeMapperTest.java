package com.telenav.adapter.component.mapper;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * @author Andrei Puf
 */
@RunWith(JUnitParamsRunner.class)
public class OpenStreetMapToUniDbBridgeMapperTest extends BaseTest {
    OpenStreetMapToUniDbBridgeMapper openStreetMapToUniDbBridgeMapper;

    @Before
    public void init() {
        openStreetMapToUniDbBridgeMapper = new OpenStreetMapToUniDbBridgeMapper();
    }

    @Test
    @Parameters({"Sourceboardwalk","covered", "skybridge", "overpass", "movable", "truss", "trestle", "aqueduct",
    "yes", "viaduct", "boardwalk", "cantilever", "simple_brunnel", "suspension", "true", "bent", "pontoon", "footbridge",
    "pier", "log_bridge", "bridge", "causeway"})
    public void mapToYes(String tagValue) {
        final Optional<UnidbTagValue> uniDbTagValue = openStreetMapToUniDbBridgeMapper.map(tagValue);
        assertTrue("Bridge tag value not mapped to UniDb tag value",
                uniDbTagValue.isPresent());
        assertEquals("Yes bridge tag value not mapped to public UniDb tag value",
                UnidbTagValue.YES,
                uniDbTagValue.get());
    }

    @Test
    @Parameters({"pedestrian", "low_water_crossing", "proposed", "no", "random_value"})
    public void doNotMap(String tagValue) {
        final Optional<UnidbTagValue> uniDbTagValue = openStreetMapToUniDbBridgeMapper.map(tagValue);
        assertFalse("Bridge tag value mapped to UniDb tag value",
                uniDbTagValue.isPresent());
    }
}
