package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.FootExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Unit tests for {@link AccessPedsRule}
 *
 * @author petrum
 */
@RunWith(JUnitParamsRunner.class)
public class AccessPedsRuleTest extends BaseTest {

    private static final WaySection WAY_SECTION = waysection(1)
            .withTag(OsmTagKey.HIGHWAY.unwrap(), "motorway")
            .withTag(OsmTagKey.NAME.unwrap(), "Main Str");
    private static final RawWay RAW_WAY = rawWay(1)
            .withTag(OsmTagKey.HIGHWAY.unwrap(), "motorway")
            .withTag(OsmTagKey.NAME.unwrap(), "Main Str");

    private static final AccessPedsRule<WaySection> WAY_SECTION_ACCESS_PEDS_RULE =
            new AccessPedsRule<>(new FootExtractor());
    private static final AccessPedsRule<RawWay> RAW_WAY_ACCESS_PEDS_RULE =
            new AccessPedsRule<>(new FootExtractor());

    @Test
    public void withoutTagFoot() {
        final WaySection adaptedWaySections = WAY_SECTION_ACCESS_PEDS_RULE.apply(WAY_SECTION);
        final RawWay adaptedRawWay = RAW_WAY_ACCESS_PEDS_RULE.apply(RAW_WAY);

        assertEquals(adaptedWaySections.tags().size(), 2);
        assertNull(adaptedWaySections.tags().get(OsmTagKey.FOOT.unwrap()));

        assertEquals(adaptedRawWay.tags().size(), 2);
        assertNull(adaptedRawWay.tags().get(OsmTagKey.FOOT.unwrap()));
    }

    @Test
    @Parameters({
            "yes",
            "designated",
            "permit",
            "yes;␣designated",
            "oui",
            "si",
            "passable",
            "allowed",
    })
    public void withTagFootAndValueYes(final String footValue) {
        final WaySection adaptedWaySection = WAY_SECTION_ACCESS_PEDS_RULE.apply(WAY_SECTION.withTag(OsmTagKey.FOOT.unwrap(),
                footValue));
        final RawWay adaptedRawWay = RAW_WAY_ACCESS_PEDS_RULE.apply(RAW_WAY.withTag(OsmTagKey.FOOT.unwrap(),
                footValue));

        testOutput(adaptedWaySection.tags(), "yes");
        testOutput(adaptedRawWay.tags(), "yes");
    }

    @Test
    @Parameters({
            "no",
            "private",
            "unknown",
            "undefined",
            "military",
            "unclassified",
            "impassable",
            "hunting",
    })
    public void withTagFootAndValueNo(final String footValue) {
        final WaySection adaptedWaySection = WAY_SECTION_ACCESS_PEDS_RULE.apply(WAY_SECTION.withTag(OsmTagKey.FOOT.unwrap(),
                footValue));
        final RawWay adaptedRawWay = RAW_WAY_ACCESS_PEDS_RULE.apply(RAW_WAY.withTag(OsmTagKey.FOOT.unwrap(),
                footValue));

        testOutput(adaptedWaySection.tags(), "no");
        testOutput(adaptedRawWay.tags(), "no");
    }

    private void testOutput(final Map<String, String> tags, final String value) {
        assertEquals(tags.size(), 3);
        assertThat(tags.get(OsmTagKey.FOOT.unwrap()), is(value));
    }
}