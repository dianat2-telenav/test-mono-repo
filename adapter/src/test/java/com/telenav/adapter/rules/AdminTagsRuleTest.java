package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.adminTags.*;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author dianat2
 */
public class AdminTagsRuleTest extends BaseTest {

    private AdminTagsRule<WaySection> adminTagRule = new AdminTagsRule<>(new AdminL1LeftExtractor(),
            new AdminL1RightExtractor(), new AdminL2LeftExtractor(), new AdminL2RightExtractor(),
            new AdminL3LeftExtractor(), new AdminL3RightExtractor());

    public AdminTagsRuleTest() throws IOException {
    }

    @Test
    public void adaptAdminTags_allAdminTags_updatedAdminTags() {
        // given
        final WaySection waySection = waysection(3, 1)
                .withTag(TnTagKey.TN_ADMIN_L1_LEFT.unwrap(), "1")
                .withTag(TnTagKey.TN_ADMIN_L1_RIGHT.unwrap(), "1")
                .withTag(TnTagKey.TN_ADMIN_L2_LEFT.unwrap(), "2")
                .withTag(TnTagKey.TN_ADMIN_L2_RIGHT.unwrap(), "2")
                .withTag(TnTagKey.TN_ADMIN_L3_LEFT.unwrap(), "3")
                .withTag(TnTagKey.TN_ADMIN_L3_RIGHT.unwrap(), "3");

        Map<String, String> expectedTags = new HashMap<>();
        expectedTags.put(TnTagKey.TN_ADMIN_L1_LEFT.withoutTelenavPrefix(), "1");
        expectedTags.put(TnTagKey.TN_ADMIN_L1_RIGHT.withoutTelenavPrefix(), "1");
        expectedTags.put(TnTagKey.TN_ADMIN_L2_LEFT.withoutTelenavPrefix(), "2");
        expectedTags.put(TnTagKey.TN_ADMIN_L2_RIGHT.withoutTelenavPrefix(), "2");
        expectedTags.put(TnTagKey.TN_ADMIN_L3_LEFT.withoutTelenavPrefix(), "3");
        expectedTags.put(TnTagKey.TN_ADMIN_L3_RIGHT.withoutTelenavPrefix(), "3");

        // when
        final WaySection result = adminTagRule.apply(waySection);

        // then
        assertThat(result.strictIdentifier()).isEqualTo(waySection.strictIdentifier());
        assertThat(result.nodes()).containsExactlyInAnyOrderElementsOf(waySection.nodes());
        assertThat(result.tags().size()).isEqualTo(expectedTags.size());
        assertThat(result.tags()).containsAllEntriesOf(expectedTags);
    }

}
