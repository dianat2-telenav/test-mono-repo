package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.TnAltNameExtractor;
import com.telenav.adapter.component.extractors.LanguageExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


/**
 * @author roxanal
 */
public class AltNameRuleTest extends BaseTest {

    private AltNameRule altNameRule;


    @Before
    public void initRule() {
        altNameRule = new AltNameRule(new TnAltNameExtractor(), new LanguageExtractor());
    }

    @Test
    public void addAltNameTagPrefixedWithLanguage() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.ALT_NAME.telenavPrefix(), "Detroit-Toledo")
                .withTag(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "eng")
                .withTag(OsmTagKey.ALT_NAME.unwrap(), "Detroit-Toledo");
        final WaySection adaptedAltNameSection = altNameRule.apply(waySection);
        assertThat(adaptedAltNameSection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedAltNameSection.sequenceNumber(), is((short) 1));
        assertThat(adaptedAltNameSection.tags().values(), hasSize(4));
        assertThat(adaptedAltNameSection.tags(), hasEntry(OsmTagKey.ALT_NAME.postFixed("eng"), "Detroit-Toledo"));
        assertThat(adaptedAltNameSection.tags(), hasEntry(OsmTagKey.ALT_NAME.telenavPrefix(), "Detroit-Toledo"));
        assertThat(adaptedAltNameSection.tags(), hasEntry(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "eng"));
        assertThat(adaptedAltNameSection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "yes"));
    }

    @Test
    public void removeNameTagWhenLanguageIsMissing() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.ALT_NAME.telenavPrefix(), "Detroit-Toledo")
                .withTag(OsmTagKey.ALT_NAME.unwrap(), "Detroit-Toledo");
        final WaySection adaptedAltNameSection = altNameRule.apply(waySection);
        assertEquals(waySection.withoutTag(OsmTagKey.ALT_NAME.unwrap()), adaptedAltNameSection);
    }

    @Test
    public void removeNameTagWhenTnTagIsMissing() {
        final WaySection waySection = waysection(3, 1)
                .withTag(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "eng")
                .withTag(OsmTagKey.ALT_NAME.unwrap(), "Detroit-Toledo");
        final WaySection adaptedAltNameSection = altNameRule.apply(waySection);
        assertEquals(waySection.withoutTag(OsmTagKey.ALT_NAME.unwrap()), adaptedAltNameSection);
    }

    @Test
    public void doNotAddAltNameTagWhenTnAltNameTagIsMissing() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "eng");
        final WaySection adaptedAltNameSection = altNameRule.apply(waySection);
        assertEquals(waySection, adaptedAltNameSection);
    }
}