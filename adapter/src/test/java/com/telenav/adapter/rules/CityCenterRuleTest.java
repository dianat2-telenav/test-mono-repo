package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.RelationService;
import com.telenav.adapter.component.NodeService;
import com.telenav.adapter.component.repository.ResourceRepository;
import com.telenav.adapter.entity.LangCodeBag;
import com.telenav.adapter.entity.dto.MemberRelation;
import com.telenav.adapter.rules.node.CityCenterRule;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.datapipelinecore.bean.Member;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.RawRelation;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.*;

import static com.telenav.adapter.utils.OsmInfo.*;
import static com.telenav.adapter.utils.OsmKeys.ADMIN_LEVEL_TAG_KEY_IN_OSM;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class CityCenterRuleTest extends BaseTest {

    private static JavaSparkContext sparkContext;
    private final List<LangCodeBag> LANG_CODES;

    public CityCenterRuleTest() throws IOException {
        this.LANG_CODES = ResourceRepository.getLangCode();
    }

    @BeforeClass
    public static void init() {
        sparkContext = getSparkContext();
    }

    private static JavaSparkContext getSparkContext() {
        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Adapter Job - NGX nodes")
                .runLocal(Boolean.valueOf(true))
                .build();
        return jobInitializer.javaSparkContext();
    }

    private CityCenterRule initCityCenterRule(final Node node,
                                              final List<MemberRelation> memberRelations) {

        //boundary relations involving the admin_centre node
        return new CityCenterRule(node.id(),
                node.latitude().asDegrees(),
                node.longitude().asDegrees(),
                memberRelations);
    }

    @Test
    public void testPlaceTag() {
        final String PLACE = "place";
        final String CAT_ID_TAG_KEY_IN_UNIDB = "cat_id";
        final Map<String, String> osmToUnidbPlaceValues =
                tags("neighborhood", "neighborhood",
                        "suburb", "neighborhood",
                        "city", "city",
                        "town", "city",
                        "square", "city",
                        "city_block", "city",
                        "quarter", "city",
                        "hamlet", "hamlet",
                        "locality", "hamlet",
                        "village", "hamlet",
                        "isolated_dwelling", "hamlet",
                        "islet", "hamlet",
                        "island", "hamlet",
                        "farm", "hamlet"
                );
        final Map<String, String> uniTbPlaceTagToCatIds =
                tags("neighborhood", "9709",
                        "city", "4444",
                        "hamlet", "9998");

        for (final Map.Entry<String, String> entry : osmToUnidbPlaceValues.entrySet()) {

            final Map<String, String> nodeTags = tags(PLACE, entry.getKey());
            final Node node = new Node(1, Latitude.degrees(30.2672), Longitude.degrees(-97.7431)).withTags(nodeTags);

            final Map<String, String> enhancedTags = runCityCenter(node, 112309L, null);

            final String uniDbPlaceValue = entry.getValue();
            assertEquals(enhancedTags.get(PLACE), uniDbPlaceValue);
            final String catId = uniTbPlaceTagToCatIds.get(uniDbPlaceValue);
            assertEquals(enhancedTags.get(CAT_ID_TAG_KEY_IN_UNIDB), catId);
        }
    }

    @Test
    public void testNameTagWithExistingLangCode() {
        final String frenchName = "Québec";
        final Map<String, String> tags =
                tags("name", frenchName,
                        "name:fr", frenchName,
                        "name:en", "Quebec",
                        "place", "city");
        //Quebec City, Quebec, CA => lang code is fr
        final Node node = new Node(30915641, Latitude.degrees(46.8259601), Longitude.degrees(-71.2352226))
                .withTags(tags);
        final Map<String, String> enhancedTags = runCityCenter(node, 2319206L, null);
        assertTrue(enhancedTags.containsKey("name"));
        assertTrue(enhancedTags.containsKey("name:en"));
        assertTrue(enhancedTags.containsKey("name:fr"));
        assertEquals(frenchName, enhancedTags.get("name:fr"));
    }

    @Test
    public void testAdminLevel() {
        final Map<String, String> nodeTags = tags("name", "Québec", "place", "city");
        final Node node = new Node(30915641, Latitude.degrees(46.8259601), Longitude.degrees(-71.2352226))
                .withTags(nodeTags);

        final String l1RelationId = "123";
        final String l2RelationId = "234";
        final String l3RelationId = "345";

        final Map<String, String> memberRelationTags =
                tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "6",
                        "l1", l1RelationId,
                        "l2", l2RelationId,
                        "l3", l3RelationId);

        final Map<String, String> enhancedTags = runCityCenter(node, 1L, memberRelationTags);

        assertEquals(enhancedTags.get("l1"), l1RelationId);
        assertEquals(enhancedTags.get("l2"), l2RelationId);
        assertEquals(enhancedTags.get("l3"), l3RelationId);
    }

    @Test
    @Ignore
    @Parameters({
            // Germany
            "Berlin" + "|" + "yes" + "|" + "4" + "|" + "capital" + "|" + "240109189" + "|" + "62422" + "|" + "52.5170365" + "|" + "13.3888599" + "|" + "51477" + "|" + "2",
            "Munich" + "|" + "4" + "|" + "4" + "|" + "capital_order1" + "|" + "1700534808" + "|" + "62428" + "|" + "48.1371079" + "|" + "11.5753822" + "|" + "2145268" + "|" + "4",
            // France
            "Paris" + "|" + "yes" + "|" + "8" + "|" + "capital" + "|" + "17807753" + "|" + "71525" + "|" + "48.8566969" + "|" + "2.3514616" + "|" + "2202162" + "|" + "2",
            "Marseille" + "|" + "4" + "|" + "8" + "|" + "capital_order1" + "|" + "26761400" + "|" + "76469" + "|" + "43.2961743" + "|" + "5.3699525" + "|" + "8654" + "|" + "4",
            "Versailles" + "|" + "6" + "|" + "8" + "|" + "capital_order2" + "|" + "26692491" + "|" + "30295" + "|" + "48.8035403" + "|" + " 2.1266886" + "|" + "7457" + "|" + "6",
    })
    public void testCapitalOrderEurope(final String... params) {
        final String name = params[0];
        final String capital = params[1];
        final String cityRelationAdminLevel = params[2];
        final String expectedCapitalOrder = params[3];
        final String nodeId = params[4];
        final String cityRelationId = params[5];
        final String latitudeDegrees = params[6];
        final String longitudeDegrees = params[7];
        final String regionRelationId = params[8];
        final String regionAdminLevel = params[9];

        final Map<String, String> tags = tags("name", name, "place", "city", "capital", capital);
        final Node city = node(Integer.parseInt(nodeId), Double.parseDouble(latitudeDegrees), Double.parseDouble(longitudeDegrees), tags);

        final Map<String, String> cityRelationTags = tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, cityRelationAdminLevel);
        final Member cityRelationMember = new Member(city.id(), OSM_LABEL_MEMBER_ROLE, NODE_MEMBER_TYPE);
        final MemberRelation cityRelation = new MemberRelation(Long.parseLong(cityRelationId), cityRelationMember, cityRelationTags);

        final List<MemberRelation> relations = new ArrayList<>();
        relations.add(cityRelation);

        final Map<String, String> regionRelationTags = tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, regionAdminLevel);

        final Member regionRelationMember = new Member(city.id(), OSM_ADMIN_CENTER_MEMBER_ROLE, NODE_MEMBER_TYPE);

        final MemberRelation regionRelation = new MemberRelation(Long.parseLong(regionRelationId), regionRelationMember, regionRelationTags);
        relations.add(regionRelation);

        final Map<String, String> enhancedTags = runCityCenter(city, relations);

        assertEquals(enhancedTags.get(expectedCapitalOrder), "yes");
    }

    @Test
    @Ignore
    @Parameters({
            // USA
            "Eureka" + "|" + "6" + "|" + "8" + "|" + "capital_order8" + "|" + "141034768" + "|" + "7931953" + "|" + "40.8020712" + "|" + "-124.1636729"
    })
    public void testCapitalOrderNorthAmericaWithoutRegionData(final String... params) {
        final String name = params[0];
        final String capital = params[1];
        final String cityRelationAdminLevel = params[2];
        final String expectedCapitalOrder = params[3];
        final String nodeId = params[4];
        final String cityRelationId = params[5];
        final String latitudeDegrees = params[6];
        final String longitudeDegrees = params[7];

        final Map<String, String> tags = tags("name", name, "place", "city", "capital", capital);
        final Node city = node(Integer.parseInt(nodeId), Double.parseDouble(latitudeDegrees), Double.parseDouble(longitudeDegrees), tags);

        final Map<String, String> cityRelationTags = tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, cityRelationAdminLevel);
        final Member cityRelationMember = new Member(city.id(), OSM_LABEL_MEMBER_ROLE, NODE_MEMBER_TYPE);
        final MemberRelation cityRelation = new MemberRelation(Long.parseLong(cityRelationId), cityRelationMember, cityRelationTags);

        final List<MemberRelation> relations = Arrays.asList(cityRelation);

        final Map<String, String> enhancedTags = runCityCenter(city, relations);

        assertEquals(enhancedTags.get(expectedCapitalOrder), "yes");
    }

    @Test
    @Ignore
    @Parameters({
            // USA
            "Sacramento" + "|" + "4" + "|" + "2" + "|" + "capital_order1" + "|" + "150959789" + "|" + "6232940" + "|" + "38.5810606" + "|" + "-121.4938950" + "|" + "165475" + "|" + "2",
            // Canada
            "Ottawa" + "|" + "yes" + "|" + "2" + "|" + "capital" + "|" + "18886011" + "|" + "4136816" + "|" + "45.4211060" + "|" + "-75.6903080" + "|" + "1428125" + "|" + "2",
            // Mexico
            "Ciudad de México" + "|" + "yes" + "|" + "2" + "|" + "capital" + "|" + "62270270" + "|" + "1376330" + "|" + "19.4326296" + "|" + "-99.1331785" + "|" + "114686" + "|" + "2"
    })
    public void testCapitalOrderNorthAmericaWithRegionData(final String... params) {
        final String name = params[0];
        final String capital = params[1];
        final String cityRelationAdminLevel = params[2];
        final String expectedCapitalOrder = params[3];
        final String nodeId = params[4];
        final String cityRelationId = params[5];
        final String latitudeDegrees = params[6];
        final String longitudeDegrees = params[7];
        final String regionRelationId = params[8];
        final String regionAdminLevel = params[9];

        final Map<String, String> tags = tags("name", name, "place", "city", "capital", capital);
        final Node city = node(Integer.parseInt(nodeId), Double.parseDouble(latitudeDegrees), Double.parseDouble(longitudeDegrees), tags);

        final Map<String, String> cityRelationTags = tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, cityRelationAdminLevel);
        final Member cityRelationMember = new Member(city.id(), OSM_LABEL_MEMBER_ROLE, NODE_MEMBER_TYPE);
        final MemberRelation cityRelation = new MemberRelation(Long.parseLong(cityRelationId), cityRelationMember, cityRelationTags);

        final List<MemberRelation> relations = new ArrayList<>();
        relations.add(cityRelation);

        final Map<String, String> regionRelationTags = tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, regionAdminLevel);

        final Member regionRelationMember = new Member(city.id(), OSM_ADMIN_CENTER_MEMBER_ROLE, NODE_MEMBER_TYPE);

        final MemberRelation regionRelation = new MemberRelation(Long.parseLong(regionRelationId), regionRelationMember, regionRelationTags);
        relations.add(regionRelation);

        final Map<String, String> enhancedTags = runCityCenter(city, relations);

        assertEquals(enhancedTags.get(expectedCapitalOrder), "yes");
    }

    private Map<String, String> runCityCenter(final Node node,
                                              final List<MemberRelation> memberRelations) {

        final CityCenterRule cityCenterRule = initCityCenterRule(node, memberRelations);

        return cityCenterRule.apply(node.tags());
    }

    private Map<String, String> runCityCenter(final Node node,
                                              final long relationId,
                                              final Map<String, String> memberRelationTags) {

        final List<MemberRelation> memberRelations = singletonMemberRelationList(node.id(), relationId, memberRelationTags);

        return runCityCenter(node, memberRelations);
    }

    private List<MemberRelation> singletonMemberRelationList(final long nodeId,
                                                             final long relationId,
                                                             Map<String, String> memberRelationTags) {
        final Member member = new Member(nodeId, OSM_ADMIN_CENTER_MEMBER_ROLE, NODE_MEMBER_TYPE);

        if (memberRelationTags == null) {
            memberRelationTags = tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "6");
        } else {
            memberRelationTags.put(ADMIN_LEVEL_TAG_KEY_IN_OSM, "6");
        }

        final MemberRelation memberRelation = new MemberRelation(relationId, member, memberRelationTags);

        return Arrays.asList(memberRelation);
    }

    private Node node(final int id,
                      final double lat,
                      final double lon,
                      final Map<String, String> tags) {
        return new Node(id, Latitude.degrees(lat), Longitude.degrees(lon))
                .withTags(tags);
    }

    private Map<String, String> tags(final String... keyAndValues) {
        final Map<String, String> tags = new HashMap<>();
        for (int i = 0; i < keyAndValues.length; i += 2) {
            tags.put(keyAndValues[i], keyAndValues[i + 1]);
        }
        return tags;
    }

    private Dataset<Row> nodeMembersDataset(final JavaRDD<Node> nodesRdd,
                                            final JavaRDD<RawRelation> relationsRdd) {

        final SQLContext sqlContext = new SQLContext(sparkContext);

        final Dataset<Row> nodesDataset =
                nodeRddToDataSet(nodesRdd, sqlContext);

        final Dataset<Row> relationsDataset =
                relationRddToDataSet(relationsRdd, sqlContext);

        final Dataset<Row> nodeCityCenterRelations = RelationService.nodeCityCenterRelations(relationsDataset);

        return NodeService.joinNodesToCityCenterRelations(nodesDataset, nodeCityCenterRelations);
    }
}
