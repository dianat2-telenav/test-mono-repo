package com.telenav.adapter.rules;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;


public class CleanFootRuleTest extends BaseTest {

    private final CleanFootRule rule = new CleanFootRule();

    @Test
    public void testNonFoot() {
        WaySection ws = waysection(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap())
                .withTag(OsmTagKey.HOV.unwrap(), "yes");
        final WaySection adapted = rule.apply(ws);
        assertNotNull(adapted);
        assertEquals(2, adapted.tags().size());
        assertEquals("motorway", adapted.tags().get("highway"));
        assertEquals("yes", adapted.tags().get("hov"));
    }

    @Test
    public void testNonMotorway() {
        WaySection ws = waysection(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SECONDARY.unwrap())
                .withTag(OsmTagKey.FOOT.unwrap(), "yes");
        final WaySection adapted = rule.apply(ws);
        assertNotNull(adapted);
        assertEquals(2, adapted.tags().size());
        assertEquals("secondary", adapted.tags().get("highway"));
        assertEquals("yes", adapted.tags().get("foot"));
    }

    @Test
    public void testMotorway() {
        WaySection ws = waysection(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap())
                .withTag(OsmTagKey.FOOT.unwrap(), "yes");
        final WaySection adapted = rule.apply(ws);
        assertNotNull(adapted);
        assertEquals(1, adapted.tags().size());
        assertEquals("motorway", adapted.tags().get("highway"));
    }

}
