package com.telenav.adapter.rules;

import static org.junit.Assert.assertEquals;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.RawWay;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * Unit tests for {@link CleanNavigableTagRule}.
 *
 * @author odrab
 */
public class CleanNavigableTagRuleTest extends BaseTest {
    private static RawWay wayForCleanup;


    @BeforeClass
    public static void init() {
        wayForCleanup = rawWay(1)
            .withTag(OsmTagKey.MAXSPEED.unwrap(), "")
            .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "")
            .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "")
            .withTag(OsmTagKey.MINSPEED.unwrap(), "")
            .withTag(OsmTagKey.MINSPEED_BACKWARD.unwrap(), "")
            .withTag(OsmTagKey.MINSPEED_FORWARD.unwrap(), "")
            .withTag(OsmTagKey.MAXSPEED_CONDITIONAL.unwrap(), "")
            .withTag(OsmTagKey.LANES.unwrap(), "")
            .withTag(OsmTagKey.LANES_BACKWARD.unwrap(), "")
            .withTag(OsmTagKey.LANES_FORWARD.unwrap(), "")
            .withTag(OsmTagKey.HOV.unwrap(), "")
            .withTag("source:maxspeed", "")
            .withTag("tagToRemain", "");
    }

    @Test
    public void waySectionWithoutTags() {
        RawWay way = rawWay(1)
            .withTag("tagToRemain", "");
        final RawWay cleanedWay = new CleanNavigableTagRule().apply(wayForCleanup);
        assertEquals(way, cleanedWay);
    }
}