package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.RouteExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.RawWay;
import org.junit.Test;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

/**
 * @author maksymf
 */
public class CleanRouteRuleTest extends BaseTest {

    private final CleanRouteRule<RawWay> riverFixerRule = new CleanRouteRule<>(new RouteExtractor());

    @Test
    public void wayWithRouteFerry(){
        final RawWay way = rawWay(1)
                .withTag(OsmTagKey.ROUTE.unwrap(),"ferry");

        final RawWay adaptedWay = riverFixerRule.apply(way);

        assertThat(adaptedWay.tags().values(), hasSize(1));
        assertTrue(adaptedWay.hasTag(OsmTagKey.ROUTE.unwrap(), "ferry"));
    }

    @Test
    public void wayWithoutRouteFerry(){
        final RawWay way = rawWay(1)
                .withTag(OsmTagKey.ROUTE.unwrap(),"sddsfksdf");

        final RawWay adaptedWay = riverFixerRule.apply(way);

        assertThat(adaptedWay.tags().values(), hasSize(0));
        assertFalse(adaptedWay.tags().containsKey(OsmTagKey.ROUTE.unwrap()));
    }
}
