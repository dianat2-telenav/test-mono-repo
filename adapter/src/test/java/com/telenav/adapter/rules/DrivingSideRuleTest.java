package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.DrivingSideExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Unit tests for {@link DrivingSideRule}.
 *
 * @author ioanao
 */
public class DrivingSideRuleTest extends BaseTest {

    private DrivingSideRule drivingSideRule;


    @Before
    public void initRule() {
        drivingSideRule = new DrivingSideRule(new DrivingSideExtractor());
    }

    @Test
    public void waySectionWithNoTag() {
        // Given
        final WaySection waySection = waysection(3, 1);

        // When
        final WaySection adaptedWaySection = drivingSideRule.apply(waySection);

        // Then
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void waySectionWithNoDrivingSideTag() {
        // Given
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "yes");

        // When
        final WaySection adaptedWaySection = drivingSideRule.apply(waySection);

        // Then
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void waySectionWithDrivingSideTag() {
        // Given
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(TnTagKey.TN_DRIVING_SIDE.unwrap(), DefaultTagValue.RIGHT_DRIVING_SIDE.unwrap());

        // When
        final WaySection adaptedWaySection = drivingSideRule.apply(waySection);

        // Then
        assertEquals(3, adaptedWaySection.strictIdentifier().wayId());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(3, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection
                .hasTag(UnidbTagKey.DRIVING_SIDE.unwrap(), DefaultTagValue.RIGHT_DRIVING_SIDE.unwrap()));
        assertTrue(adaptedWaySection
                .hasTag(TnTagKey.TN_DRIVING_SIDE.unwrap(), DefaultTagValue.RIGHT_DRIVING_SIDE.unwrap()));
        assertTrue(adaptedWaySection.hasTag("oneway", "yes"));
    }

}