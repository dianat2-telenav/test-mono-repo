package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.DstExtractor;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


/**
 * Unit tests for {@link DstTagRule}.
 *
 * @author dianat2
 */
public class DstTagRuleTest extends BaseTest {

    private DstTagRule dstTagRule;

    @Before
    public void initRule() {
        dstTagRule = new DstTagRule(new DstExtractor());
    }

    @Test
    public void waySectionWithoutDstTag() {
        // Given
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes");

        // When
        final WaySection adaptedWaySection = dstTagRule.apply(waySection);

        // Then
        assertEquals(waySection, adaptedWaySection);
        assertNull(adaptedWaySection.tags().get(TnTagKey.TN_DST.unwrap()));
    }

    @Test
    public void waySectionWithYesValueForMotorCarTag() {
        // Given
        final WaySection waySection = waysection(3, 1).withTag(TnTagKey.TN_DST.unwrap(), "1");

        // When
        final WaySection adaptedWaySection = dstTagRule.apply(waySection);

        // Then
        assertEquals(adaptedWaySection.tags().get(TnTagKey.TN_DST.withoutTelenavPrefix()), "1");
    }

}