package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.bean.Fow;
import com.telenav.adapter.component.extractors.HighwayExtractor;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.telenav.adapter.rules.FormOfWayRule.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author liviuc
 */
@RunWith(JUnitParamsRunner.class)
public class FormOfWayRuleTest extends BaseTest {

    private FormOfWayRule rule;

    @Before
    public void initRule() {
        rule = new FormOfWayRule(new HighwayExtractor());
    }

    @Test
    public void adaptFow_unclassifiedWay_undefinedFow() {
        // given
        final WaySection waySection = waysection(3, 1)
                .withTag(OSM_HIGHWAY_KEY, OSM_UNCLASSIFIED_HIGHWAY_VALUE);

        // when
        final WaySection adaptedWaySection = rule.apply(waySection);

        // then
        assertThat(adaptedWaySection.strictIdentifier().wayId()).isEqualTo(waySection.strictIdentifier().wayId());
        assertThat(adaptedWaySection.tags().values()).hasSize(waySection.tags().values().size() + 1);
        assertThat(adaptedWaySection.tags().entrySet()).containsAll(waySection.tags().entrySet());
        assertThat(adaptedWaySection.tags()).containsKey(FOW_TAG_KEY);
        assertThat(adaptedWaySection.tags().get(FOW_TAG_KEY)).isEqualTo(Fow.UNDEFINED.name());
    }

    @Test
    public void adaptFow_motorwayWay_motorwayFow() {
        // given
        final WaySection waySection = waysection(3, 1)
                .withTag(OSM_HIGHWAY_KEY, OSM_MOTORWAY_HIGHWAY_VALUE);

        // when
        final WaySection adaptedWaySection = rule.apply(waySection);

        // then
        assertThat(adaptedWaySection.strictIdentifier().wayId()).isEqualTo(waySection.strictIdentifier().wayId());
        assertThat(adaptedWaySection.tags().values()).hasSize(waySection.tags().values().size() + 1);
        assertThat(adaptedWaySection.tags().entrySet()).containsAll(waySection.tags().entrySet());
        assertThat(adaptedWaySection.tags()).containsKey(FOW_TAG_KEY);
        assertThat(adaptedWaySection.tags().get(FOW_TAG_KEY)).isEqualTo(Fow.MOTORWAY.name());
    }

    @Test
    public void adaptFow_roundaboutWay_roundaboutFow() {
        // given
        final WaySection waySection = waysection(1, 1)
                .withTag(OSM_HIGHWAY_KEY, "freeway")
                .withTag(OSM_JUNCTION_KEY, OSM_ROUNDABOUT_JUNCTION_VALUE);

        // when
        final WaySection adaptedWaySection = rule.apply(waySection);

        // then
        assertThat(adaptedWaySection.strictIdentifier().wayId()).isEqualTo(waySection.strictIdentifier().wayId());
        assertThat(adaptedWaySection.tags().values()).hasSize(waySection.tags().values().size() + 1);
        assertThat(adaptedWaySection.tags().entrySet()).containsAll(waySection.tags().entrySet());
        assertThat(adaptedWaySection.tags()).containsKey(FOW_TAG_KEY);
        assertThat(adaptedWaySection.tags().get(FOW_TAG_KEY)).isEqualTo(Fow.ROUNDABOUT.name());
    }

    @Test
    public void adaptFow_undefinedHighway_roundaboutFow() {
        // given
        final WaySection waySection = waysection(1, 1)
                .withTag(OSM_HIGHWAY_KEY, "undefined")
                .withTag(OSM_JUNCTION_KEY, OSM_ROUNDABOUT_JUNCTION_VALUE);

        // when
        final WaySection adaptedWaySection = rule.apply(waySection);

        // then
        assertThat(adaptedWaySection.strictIdentifier().wayId()).isEqualTo(waySection.strictIdentifier().wayId());
        assertThat(adaptedWaySection.tags().values()).hasSize(waySection.tags().values().size() + 1);
        assertThat(adaptedWaySection.tags().entrySet()).containsAll(waySection.tags().entrySet());
        assertThat(adaptedWaySection.tags()).containsKey(FOW_TAG_KEY);
        assertThat(adaptedWaySection.tags().get(FOW_TAG_KEY)).isEqualTo(Fow.ROUNDABOUT.name());
    }

    @Test
    public void adaptFow_motorwayHighway_roundaboutFow() {
        // given
        final WaySection waySection = waysection(1, 1)
                .withTag(OSM_HIGHWAY_KEY, "motorway")
                .withTag(OSM_JUNCTION_KEY, OSM_ROUNDABOUT_JUNCTION_VALUE);

        // when
        final WaySection adaptedWaySection = rule.apply(waySection);

        // then
        assertThat(adaptedWaySection.strictIdentifier().wayId()).isEqualTo(waySection.strictIdentifier().wayId());
        assertThat(adaptedWaySection.tags().values()).hasSize(waySection.tags().values().size() + 1);
        assertThat(adaptedWaySection.tags().entrySet()).containsAll(waySection.tags().entrySet());
        assertThat(adaptedWaySection.tags()).containsKey(FOW_TAG_KEY);
        assertThat(adaptedWaySection.tags().get(FOW_TAG_KEY)).isEqualTo(Fow.ROUNDABOUT.name());
    }

    @Test
    public void adaptFow_turningCircleCulDeSac_roundaboutFow() {
        // given
        final Node node1 = new Node(1L, Latitude.MINIMUM, Longitude.MINIMUM);
        final Node node2 = new Node(2L, Latitude.MINIMUM, Longitude.MINIMUM);
        final Node node3 = new Node(3L, Latitude.MAXIMUM, Longitude.MAXIMUM)
                .withTag(OSM_HIGHWAY_KEY, "turning_circle");
        final WaySection waySection = waysection(1, 1, node1, node2, node3);

        // when
        final WaySection adaptedWaySection = rule.apply(waySection);

        // then
        assertThat(adaptedWaySection.strictIdentifier().wayId()).isEqualTo(waySection.strictIdentifier().wayId());
        assertThat(adaptedWaySection.tags().values()).hasSize(waySection.tags().values().size() + 1);
        assertThat(adaptedWaySection.tags().entrySet()).containsAll(waySection.tags().entrySet());
        assertThat(adaptedWaySection.tags()).containsKey(FOW_TAG_KEY);
        assertThat(adaptedWaySection.tags().get(FOW_TAG_KEY)).isEqualTo(Fow.ROUNDABOUT.name());
    }

    @Test
    public void adaptFow_turningCircleCulDeSacNotTheLastNode_NoRoundaboutFow() {
        // given
        final Node node1 = new Node(1L, Latitude.MINIMUM, Longitude.MINIMUM);
        final Node node2 = new Node(3L, Latitude.MAXIMUM, Longitude.MAXIMUM)
                .withTag(OSM_HIGHWAY_KEY, "turning_circle");
        final Node node3 = new Node(2L, Latitude.MINIMUM, Longitude.MINIMUM);
        final WaySection waySection = waysection(1, 1, node1, node2, node3);

        // when
        final WaySection adaptedWaySection = rule.apply(waySection);

        // then
        assertThat(adaptedWaySection.strictIdentifier().wayId()).isEqualTo(waySection.strictIdentifier().wayId());
        assertThat(adaptedWaySection.tags().values()).hasSize(waySection.tags().values().size());
        assertThat(adaptedWaySection.tags().entrySet()).containsAll(waySection.tags().entrySet());
        assertThat(adaptedWaySection.tags()).doesNotContainKey(FOW_TAG_KEY);
    }

    @Test
    public void adaptFow_motorwayLink_NoRoundaboutFow() {
        // given
        final Node node1 = new Node(1L, Latitude.MINIMUM, Longitude.MINIMUM);
        final Node node2 = new Node(3L, Latitude.MAXIMUM, Longitude.MAXIMUM)
                .withTag(OSM_HIGHWAY_KEY, "turning_circle");
        final Node node3 = new Node(2L, Latitude.MINIMUM, Longitude.MINIMUM);
        final WaySection waySection = waysection(1, 1, node1, node2, node3);

        // when
        final WaySection adaptedWaySection = rule.apply(waySection);

        // then
        assertThat(adaptedWaySection.strictIdentifier().wayId()).isEqualTo(waySection.strictIdentifier().wayId());
        assertThat(adaptedWaySection.tags().values()).hasSize(waySection.tags().values().size());
        assertThat(adaptedWaySection.tags().entrySet()).containsAll(waySection.tags().entrySet());
        assertThat(adaptedWaySection.tags()).doesNotContainKey(FOW_TAG_KEY);
    }

    @Test
    @Parameters({"motorway_link, SLIPROAD",
            "trunk_link, SLIPROAD",
            "primary_link, SLIPROAD",
            "secondary_link, SLIPROAD",
            "tertiary_link, SLIPROAD"
    })
    public void adaptFow_motorwayLink_roundaboutFow(String highwayValue, String expectedFow) {
        // given
        final WaySection waySection = waysection(1, 1)
                .withTag(OSM_HIGHWAY_KEY, highwayValue);

        // when
        final WaySection adaptedWaySection = rule.apply(waySection);

        // then
        assertThat(adaptedWaySection.strictIdentifier().wayId()).isEqualTo(waySection.strictIdentifier().wayId());
        assertThat(adaptedWaySection.tags().entrySet()).hasSize(waySection.tags().entrySet().size() + 1);
        assertThat(adaptedWaySection.tags().values()).contains(highwayValue);
        assertThat(adaptedWaySection.tags().keySet()).contains(FOW_TAG_KEY);
        assertThat(adaptedWaySection.tags()).containsValue(expectedFow);
    }

    @Test
    public void adaptFow_nothing_NoFow() {
        // given
        final WaySection waySection = waysection(1, 1);

        // when
        final WaySection adaptedWaySection = rule.apply(waySection);

        // then
        assertThat(adaptedWaySection.strictIdentifier().wayId()).isEqualTo(waySection.strictIdentifier().wayId());
        assertThat(adaptedWaySection.tags().entrySet()).containsAll(waySection.tags().entrySet());
        assertThat(adaptedWaySection.tags()).doesNotContainKey(FOW_TAG_KEY);
    }

}
