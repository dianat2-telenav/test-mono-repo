package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.FrontageExtractor;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;


/**
 * Unit tests for {@link FrontageRule}.
 */
@RunWith(JUnitParamsRunner.class)
public class FrontageRuleTest extends BaseTest {
    private FrontageRule frontageRule;
    private final String UNIDB_FRONTAGE_TAG_KEY = "frontage";
    private final String OSM_FRONTAGE_ROAD_TAG_KEY = "frontage_road";

    @Before
    public void initRule() {
        frontageRule = new FrontageRule(new FrontageExtractor());
    }

    @Test
    public void waySectionWithoutFrontageTag() {
        final WaySection waySection = waysection(3, 1)
                .withTag("tag1", "yes")
                .withTag("tag2", "yes");
        final WaySection adaptedWaySection = frontageRule.apply(waySection);
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void waySectionWithNoValueFrontageTag() {
        final WaySection waySection = waysection(3, 1)
                .withTag("tag1", "yes")
                .withTag(OSM_FRONTAGE_ROAD_TAG_KEY, "no");
        final WaySection adaptedWaySection = frontageRule.apply(waySection);
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void waySectionWithYesValueFrontageTagAndRt5() {
        final WaySection waySection = waysection(3, 1)
                .withTag("tag1", "yes")
                .withTag("rt", "5")
                .withTag(OSM_FRONTAGE_ROAD_TAG_KEY, "yes");
        final WaySection adaptedWaySection = frontageRule.apply(waySection);
        assertTrue(adaptedWaySection.hasKey(UNIDB_FRONTAGE_TAG_KEY));
    }
}