package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HeightRemoverRuleTest extends BaseTest {

    private static final HeightRemoverRule heightRemoverRule = new HeightRemoverRule();

    @Test
    public void testCorrectRemoving() {
        // given
        final WaySection waySection = waysection(1)
                .withTag("height", "1.2");

        // when
        WaySection result = heightRemoverRule.apply(waySection);

        // then
        assertThat(result).isNotNull();
        assertThat(result.tags().size()).isEqualTo(0);
    }

    @Test
    public void testNotRemovingAnythingElse() {
        // given
        final WaySection waySection = waysection(2)
                .withTag("highway", "motorway")
                .withTag("height", "1.2");

        // when
        WaySection result = heightRemoverRule.apply(waySection);

        // then
        assertThat(result).isNotNull();
        assertThat(result.tags().size()).isEqualTo(1);
    }
}