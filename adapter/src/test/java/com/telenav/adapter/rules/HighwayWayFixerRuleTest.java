package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.HighwayExtractor;
import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.osm.OsmTagValue;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link HighwayWayFixerRule}
 *
 * @author dianat2
 */
@RunWith(JUnitParamsRunner.class)
public class HighwayWayFixerRuleTest extends BaseTest {

    private static final HighwayWayFixerRule RAW_WAY_HIGHWAY_RULE = new HighwayWayFixerRule(new HighwayExtractor());

    @Test
    @Parameters({
            "escape",
            "busway",
    })
    public void applyRule_supportedHighwayTags_entityWithAccessPrivateAndWithoutHighwayTag(final String givenValue) {
        // Given
        final RawWay rawWay = rawWay(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), givenValue);

        // When
        final RawWay adaptedRawWay = RAW_WAY_HIGHWAY_RULE.apply(rawWay);

        // Then
        assertEquals(1, adaptedRawWay.tags().size());
        assertFalse(adaptedRawWay.hasKey(OsmTagKey.HIGHWAY.unwrap()));
        assertTrue(adaptedRawWay.hasTag(OsmTagKey.ACCESS.unwrap(), OsmHighwayValue.PRIVATE.unwrap()));
    }

    @Test
    public void applyRule_unsupportedHighwayTag_entityWithoutHighwayTag(){
        // Given
        final RawWay rawWay = rawWay(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());

        // When
        final RawWay adaptedRawWay = RAW_WAY_HIGHWAY_RULE.apply(rawWay);

        // Then
        assertEquals(0, adaptedRawWay.tags().size());
        assertFalse(adaptedRawWay.hasKey(OsmTagKey.HIGHWAY.unwrap()));
    }
}