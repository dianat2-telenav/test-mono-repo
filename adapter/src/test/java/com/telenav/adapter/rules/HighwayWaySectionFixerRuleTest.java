package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.HighwayExtractor;
import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link HighwayWaySectionFixerRule}
 *
 * @author petrum
 */
@RunWith(JUnitParamsRunner.class)
public class HighwayWaySectionFixerRuleTest extends BaseTest {

    private static final HighwayWaySectionFixerRule WAY_SECTION_HIGHWAY_RULE =
            new HighwayWaySectionFixerRule(new HighwayExtractor());

    @Test
    @Parameters({
            "motorway",
            "motorway_link",
            "trunk",
            "trunk_link",
            "primary",
            "primary_link",
            "secondary",
            "secondary_link",
            "tertiary",
            "tertiary_link",
            "residential",
            "service",
            "pedestrian",
            "track",
            "raceway"
    })
    public void applyRule_entityWithSameUniDbCorrespondent_entityUpdated(final String givenValue) {
        // Given
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), givenValue);

        // When
        final WaySection adaptedWaySection = WAY_SECTION_HIGHWAY_RULE.apply(waySection);

        // Then
        assertEquals(1, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), givenValue));
    }

    @Test
    @Parameters({
            "unclassified, tertiary",
            "residential_link, residential",
            "living_street, residential",
            "footway, pedestrian",
            "path, pedestrian",
            "motorway, motorway"
    })
    public void applyRule_entityWithDifferentUniDbCorrespondent_entityUpdated(final String givenValue,
                                                                              final String expectedValue) {
        // Given
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), givenValue);

        // When
        final WaySection adaptedWaySection = WAY_SECTION_HIGHWAY_RULE.apply(waySection);

        // Then
        assertEquals(1, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), expectedValue));
    }

    @Test
    @Parameters({
            "bus_guideway",
            "private"
    })
    public void applyRule_entityWithTagsCorrespondingToUnsectionedWays_entityNotUpdated(final String givenValue) {
        // Given
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), givenValue);

        // When
        final WaySection adaptedWaySection = WAY_SECTION_HIGHWAY_RULE.apply(waySection);

        // Then
        assertEquals(2, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SERVICE.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ACCESS.unwrap(), OsmHighwayValue.PRIVATE.unwrap()));
    }

    @Test
    public void applyRule_entityWithBridlewayHighwayTag_entityUpdated() {
        // Given
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.BRIDLEWAY.unwrap());

        // When
        final WaySection adaptedWaySection = WAY_SECTION_HIGHWAY_RULE.apply(waySection);

        // Then
        assertEquals(2, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRACK.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ACCESS.unwrap(), OsmHighwayValue.PRIVATE.unwrap()));
    }

    @Test
    public void applyRule_entityWithNoHighwayTag_entityNotUpdated(){
        // Given
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.ACCESS.unwrap(), "public");

        // When
        final WaySection adaptedWaySection = WAY_SECTION_HIGHWAY_RULE.apply(waySection);

        // Then
        assertEquals(1, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ACCESS.unwrap(), "public"));
    }

    //TODO: to be removed
    @Test
    @Parameters({
            "bus_guideway",
            "private",
    })
    public void applyRule_entityWithHighwayToBeRemoved_entityUpdated(final String givenValue) {
        // Given
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), givenValue);

        // When
        final WaySection adaptedWaySection = WAY_SECTION_HIGHWAY_RULE.apply(waySection);

        // Then
        assertEquals(2, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SERVICE.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ACCESS.unwrap(), OsmHighwayValue.PRIVATE.unwrap()));
    }

    @Test
    @Parameters({
            "road, pedestrian",
            "steps, pedestrian",
            "cycleway, pedestrian",
            "construction, track",
            "unknown, track",
            "undefined, track"
    })
    public void applyRule_entityWithHighwayToBeRemoved_entityWithPedestrianHighway(final String givenValue,
                                                                                   final String expectedValue) {
        // Given
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), givenValue);

        // When
        final WaySection adaptedWaySection = WAY_SECTION_HIGHWAY_RULE.apply(waySection);

        // Then
        assertEquals(1, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), expectedValue));
    }
}