package com.telenav.adapter.rules;

import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.HovExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;


/**
 * Unit tests for {@link HovRule}.
 *
 * @author odrab
 */
@RunWith(JUnitParamsRunner.class)
public class HovRuleTest extends BaseTest {

    @Test
    @Parameters({
        "yes." + "yes",
        "designated." + "designated",
        "no." + "no"
    })
    public void mapHovTag(final String parameter) {

        final String[] parameters = parameter.split("\\.");
        final String originalHov = parameters[0];
        final String exceptedHov = parameters[1];

        WaySection waySection = waysection(3, 1).withTag(OsmTagKey.HOV.unwrap(), originalHov);
        WaySection adaptedWaySection = new HovRule(new HovExtractor()).apply(waySection);
        assertThat(adaptedWaySection.tags().values(), hasSize(1));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.HOV.unwrap(), exceptedHov));
    }
}