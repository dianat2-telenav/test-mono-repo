package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.OneWayExtractor;
import com.telenav.adapter.component.extractors.TurnLaneExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.telenav.adapter.component.fc.TagService.*;
import static com.telenav.adapter.rules.LaneTypeRule.*;
import static com.telenav.adapter.utils.turnLanes.OsmToUniDbTurnLanesEnhancer.TURN_LANES_BACKWARD;
import static com.telenav.adapter.utils.turnLanes.OsmToUniDbTurnLanesEnhancer.TURN_LANES_FORWARD;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

/**
 * @author lcira
 */
@RunWith(JUnitParamsRunner.class)
public class LaneTypeRuleTest extends BaseTest {

    private static final String TURN_LANE_VALUE = "2048";
    private static final String HOV_LANE_VALUE = "2";
    private static final String CENTRE_TURN_LANE_VALUE = "4096";
    private static final String BICYCLE_LANE_VALUE = "65536";
    private static final String REVERSIBLE_LANE_VALUE = "4";
    private LaneTypeRule laneTypeRule;
    private TurnLaneRule<WaySection> turnLaneRule;
    private Map<String, String> osmLaneTypesToValues;

    private final List<String> osmLaneTypes = Arrays.asList(HOV_LANES_OSM_KEY,
            BICYCLE_LANES_OSM_KEY,
            REVERSIBLE_LANES_OSM_KEY);

    @Before
    public void initRule() {
        laneTypeRule = new LaneTypeRule();
        turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);
        osmLaneTypesToValues = new HashMap();
        osmLaneTypesToValues.put(HOV_LANES_OSM_KEY, HOV_LANE_VALUE);
        osmLaneTypesToValues.put(CENTRE_TURN_LANE_OSM_KEY, CENTRE_TURN_LANE_VALUE);
        osmLaneTypesToValues.put(BICYCLE_LANES_OSM_KEY, BICYCLE_LANE_VALUE);
        osmLaneTypesToValues.put(REVERSIBLE_LANES_OSM_KEY, REVERSIBLE_LANE_VALUE);
        osmLaneTypesToValues.put(OsmTagKey.TURN_LANES.unwrap(), TURN_LANE_VALUE);
    }

    @Test
    public void testRealCase() {
        WaySection waySection = waysection(496032370, 1)
                .withTag("highway", "secondary")
                .withTag(LANES, "2")
                .withTag("maxspeed", "35 mph")
                .withTag("name", "Scott Boulevard")
                .withTag("oneway", "yes")
                .withTag("surface", "asphalt")
                .withTag("tiger:county", "Santa Clara, CA")
                .withTag("tiger:name_base", "Scott")
                .withTag("tiger:name_type", "\tBlvd")
                .withTag("turn:lanes:forward", "left||");
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertFalse(tags.containsKey(CORRUPTED_WAY_SECTION_KEY));
        assertFalse(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
    }

    @Test
    public void testRealCase2() {
        WaySection waySection = waysection(592924210, 1)
                .withTag("change:lanes", "no|no|not_left|yes|yes")
                .withTag("cycleway", "lane")
                .withTag("highway", "primary")
                .withTag(LANES, "5")
                .withTag("name", "Berryessa Road")
                .withTag("oneway", "yes")
                .withTag("tiger:county", "Santa Clara, CA")
                .withTag("tiger:name_base", "Berryessa")
                .withTag("tiger:name_type", "Rd")
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "reverse;left|left|||");
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertFalse(tags.containsKey(CORRUPTED_WAY_SECTION_KEY));
        assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
    }

    @Test
    public void testRealCase3() {
        WaySection waySection = waysection(761336065, 1)
                .withTag("oneway", "yes")
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "left|left|through|trough");//typo
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertFalse(tags.containsKey(CORRUPTED_WAY_SECTION_KEY));
        assertFalse(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
    }

    /*
        test that regular lanes are correctly mapped
     */
    @Test
    public void testRealCase4() {
        WaySection waySection = waysection(417326002, 1)
                .withTag("highway", "secondary")
                .withTag("name", "Shell Boulevard")
                .withTag("oneway", "yes")
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "left||");
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
        assertEquals(tags.get(UNI_DB_TYPE_LANES_KEY), "1|1|2048");
    }

    /*
        test that lane types for turn lanes are correctly mapped
     */
    @Test
    public void testRealCase5() {
        WaySection waySection = waysection(185382496, 1)
                .withTag("highway", "primary")
                .withTag(LANES, "5")
                .withTag("name", "Mission Boulevard")
                .withTag("oneway", "yes")
                .withTag("name", "Mission Boulevard")
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "through|through|through|through|right");
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
        assertEquals(tags.get(UNI_DB_TYPE_LANES_KEY), "2048|2048|2048|2048|2048");
    }

    @Test
    public void testRealCase6() {
        WaySection waySection = waysection(689711229, 1)
                .withTag("highway", "residential")
                .withTag(LANES, "2")
                .withTag("name", "B Street")
                .withTag("oneway", "yes")
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "left;through|through");
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
        assertEquals(tags.get(UNI_DB_TYPE_LANES_KEY), "2048|2048");
    }

    @Test
    public void testRealCase7() {
        WaySection waySection = waysection(617930741, 1)
                .withTag("highway", "residential")
                .withTag(LANES, "2")
                .withTag("name", "B Street")
                .withTag("oneway", "yes")
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "through|through");
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
        assertEquals(tags.get(UNI_DB_TYPE_LANES_KEY), "2048|2048");
    }

    /*
        test that type lanes is correctly mapped
     */
    @Test
    public void testRealCase8() {
        WaySection waySection = waysection(13143668, 1)
                .withTag("highway", "motorway_link")
                .withTag(LANES, "3")
                .withTag("name", "B Street")
                .withTag("oneway", "yes")
                .withTag(HOV_LANES_OSM_KEY, "designated|||")
                .withTag("hov", "lane");
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
        assertEquals(tags.get(UNI_DB_TYPE_LANES_KEY), "1|1|1|2");
    }

    @Test
    public void testRealCase9() {
        WaySection waySection = waysection(355568006, 1)
                .withTag("highway", "primary")
                .withTag(LANES, "5")
                .withTag("name", "El Camino Real")
                .withTag("oneway", "yes")
                .withTag("hgv", "designated")
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "reverse;left|left|||");
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
        assertEquals(tags.get(UNI_DB_TYPE_LANES_KEY), "1|1|1|2048|2048");
    }

    @Test
    public void testRealCase10() {
        WaySection waySection = waysection(28414349, 1)
                .withTag("highway", "motorway")
                .withTag(LANES, "5")
                .withTag("name", "El Camino Real")
                .withTag("oneway", "yes")
                .withTag("hov", "lane")
                .withTag(HOV_LANES_OSM_KEY, "designated|||")
                .withTag(LANES, "4")
                .withTag(LANES, "4");
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
        assertEquals(tags.get(UNI_DB_TYPE_LANES_KEY), "1|1|1|2");
    }

    /*
        test that 'none' is correctly mapped
     */
    @Test
    public void testRealCase11() {
        WaySection waySection = waysection(417326002, 1)
                .withTag("highway", "secondary")
                .withTag("name", "Shell Boulevard")
                .withTag("oneway", "yes")
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "left|none|none");
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
        assertEquals(tags.get(UNI_DB_TYPE_LANES_KEY), "1|1|2048");
    }

    @Test
    public void testRealCase12() {
        WaySection waySection = waysection(417318849, 1)
                .withTag("highway", "secondary")
                .withTag("name", "East Hillsdale Boulevard")
                .withTag("oneway", "yes")
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "left|none|none|right");
        waySection = turnLaneRule.apply(waySection);
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
        assertEquals(tags.get(UNI_DB_TYPE_LANES_KEY), "2048|1|1|2048");
    }

    /*
        Test for all values pf lane type values (yes, no, designated)and for combinations as well,
        correlated with osm lane types
     */
    @Test
    public void testWithTurnLanes() {
        final String osmLaneTypeValuesCombinedWithNoValues = "no|no|no|no";
        final String osmLaneTypeValuesCombinedWithYesValues = "yes|yes|yes|yes";
        final String osmLaneTypeValuesCombinedWithDesignatedValues = "designated|designated|designated|designated";
        final String osmLaneTypeValuesCombinedWithAllValues = "designated|no|yes|designated";
        final List<String> osmLaneTypeValuesCombinedList = Arrays.asList(osmLaneTypeValuesCombinedWithNoValues,
                osmLaneTypeValuesCombinedWithYesValues,
                osmLaneTypeValuesCombinedWithDesignatedValues,
                osmLaneTypeValuesCombinedWithAllValues
        );
        for (final String osmLaneTypeValuesCombined : osmLaneTypeValuesCombinedList) {
            for (final String osmLaneType : osmLaneTypes) {
                WaySection waySection = waysection(3, 1)
                        .withTag(osmLaneType, osmLaneTypeValuesCombined)
                        .withTag(OsmTagKey.TURN_LANES.unwrap(), "left|through|right")
                        .withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
                waySection = turnLaneRule.apply(waySection);
                final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
                final Map<String, String> tags = adaptedWaySection.tags();
                assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
                assertEquals("2048|2048|2048", tags.get(UNI_DB_TYPE_LANES_KEY));
            }
        }
    }

    @Test
    public void testWithNoTurnLanes() {
        final String osmLaneTypeValuesCombinedWithNoValues = "no|no|no|no";
        final String osmLaneTypeValuesCombinedWithYesValues = "yes|yes|yes|yes";
        final String osmLaneTypeValuesCombinedWithDesignatedValues = "designated|designated|designated|designated";
        final String osmLaneTypeValuesCombinedWithAllValues = "designated|designated|no|yes";
        final List<String> osmLaneTypeValuesCombinedList = Arrays.asList(
                osmLaneTypeValuesCombinedWithNoValues,
                osmLaneTypeValuesCombinedWithYesValues,
                osmLaneTypeValuesCombinedWithDesignatedValues,
                osmLaneTypeValuesCombinedWithAllValues
        );
        for (final String osmLaneTypeValuesCombined : osmLaneTypeValuesCombinedList) {
            for (final String osmLaneType : osmLaneTypes) {
                WaySection waySection = waysection(3, 1)
                        .withTag(osmLaneType, osmLaneTypeValuesCombined);
                waySection = turnLaneRule.apply(waySection);
                final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
                final Map<String, String> tags = adaptedWaySection.tags();
                assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
                final String laneValue = osmLaneTypesToValues.get(osmLaneType);
                final String[] split = osmLaneTypeValuesCombined.split("\\|");
                final String expectedValue = getValueOrOne(split[3], laneValue) + "|" +
                        getValueOrOne(split[2], laneValue) + "|" +
                        getValueOrOne(split[1], laneValue) + "|" +
                        getValueOrOne(split[0], laneValue);
                final String actualValue = tags.get(UNI_DB_TYPE_LANES_KEY);
                assertEquals(expectedValue, actualValue);
            }
        }
    }

    @Test
    public void testWithTurnLanesForward() {
        final String osmLaneTypeValuesCombinedWithNoValues = "no|no|no";
        final String osmLaneTypeValuesCombinedWithYesValues = "yes|yes|yes";
        final String osmLaneTypeValuesCombinedWithDesignatedValues = "designated|designated|designated";
        final String osmLaneTypeValuesCombinedWithAllValues = "designated|no|yes";
        final List<String> osmLaneTypeValuesCombinedList = Arrays.asList(osmLaneTypeValuesCombinedWithNoValues,
                osmLaneTypeValuesCombinedWithYesValues,
                osmLaneTypeValuesCombinedWithDesignatedValues,
                osmLaneTypeValuesCombinedWithAllValues
        );
        for (final String osmLaneTypeValuesCombined : osmLaneTypeValuesCombinedList) {
            for (final String osmLaneType : osmLaneTypes) {
                WaySection waySection = waysection(3, 1)
                        .withTag(osmLaneType, osmLaneTypeValuesCombined)
                        .withTag(TURN_LANES_FORWARD, "left|through|right");
                waySection = turnLaneRule.apply(waySection);
                final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
                final Map<String, String> tags = adaptedWaySection.tags();
                assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
                assertEquals("2048|2048|2048", tags.get(UNI_DB_TYPE_LANES_KEY));
            }
        }
    }

    @Test
    public void testWithTurnLanesBackward() {
        final String osmLaneTypeValuesCombinedWithNoValues = "no|no|no|no";
        final String osmLaneTypeValuesCombinedWithYesValues = "yes|yes|yes|yes";
        final String osmLaneTypeValuesCombinedWithDesignatedValues = "designated|designated|designated|designated";
        final String osmLaneTypeValuesCombinedWithAllValues = "designated|no|yes|designated";
        final List<String> osmLaneTypeValuesCombinedList = Arrays.asList(osmLaneTypeValuesCombinedWithNoValues,
                osmLaneTypeValuesCombinedWithYesValues,
                osmLaneTypeValuesCombinedWithDesignatedValues,
                osmLaneTypeValuesCombinedWithAllValues
        );
        for (final String osmLaneTypeValuesCombined : osmLaneTypeValuesCombinedList) {
            for (final String osmLaneType : osmLaneTypes) {
                WaySection waySection = waysection(3, 1)
                        .withTag(osmLaneType, osmLaneTypeValuesCombined)
                        .withTag(TURN_LANES_BACKWARD, "left|through|right");
                waySection = turnLaneRule.apply(waySection);
                final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
                final Map<String, String> tags = adaptedWaySection.tags();
                assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
                assertEquals("2048|2048|2048", tags.get(UNI_DB_TYPE_LANES_KEY));
            }
        }
    }

    @Test
    public void testWithBothForwardAndBackwardLanes() {
        WaySection waySection = waysection(3, 1)
                .withTag("hov:lanes:forward", "designated|yes")
                .withTag("hov:lanes:backward", "yes|yes|designated");
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertTrue(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
        assertEquals("1|2|1|1|2", tags.get(UNI_DB_TYPE_LANES_KEY));
    }

    @Test
    public void testTurnLanesWithCorruptedWaySection() {
        WaySection waySection = waysection(398023701, 1)
                .withTag(LANES, "5")
                .withTag("lanes:both_ways", "\tl")
                .withTag(LANES_BACKWARD, "2")
                .withTag(LANES_FORWARD, "3")
                .withTag(CORRUPTED_WAY_SECTION_KEY, "yes");
        final WaySection adaptedWaySection = laneTypeRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertFalse(tags.containsKey(UNI_DB_TYPE_LANES_KEY));
        assertFalse(tags.containsKey(CORRUPTED_WAY_SECTION_KEY));
        assertThat(tags.values(), hasSize(4));
    }

    private String getValueOrOne(final String s, String laneValue) {
        return ("designated".equals(s)) ? laneValue : "1";
    }

    @Test
    @Parameters({
            "0\\|64\\|65\\|64\\|0, 2048\\|2048\\|2048, 1\\|2048\\|2048\\|2048\\|1",
            "0\\|64\\|0\\|4\\|0, 2048\\|2048, 1\\|2048\\|1\\|2048\\|1",
            "0\\|0\\|1, 2048, 1\\|1\\|2048",
            "0\\|64\\|64\\|0\\|0, 1\\|2048\\|2048, 1\\|2048\\|2048\\|1\\|1",
            "4\\|1\\|64\\|0\\|0, 2048\\|2048\\|2048, 2048\\|2048\\|2048\\|1\\|1"

    })
    public void testTurnLaneWithTurnLanes(String turnLanesValue, String typeLanesValue, String typeLanesValueExpected) {
        WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.TURN_LANES.unwrap(), turnLanesValue)
                .withTag(TYPE_LANES, typeLanesValue);

        WaySection adaptedWaySection = laneTypeRule.apply(waySection);

        Map<String, String> adaptedWaySectionTags = adaptedWaySection.tags();
        assertEquals(typeLanesValueExpected, adaptedWaySectionTags.get(TYPE_LANES));
        assertEquals(turnLanesValue, adaptedWaySectionTags.get(OsmTagKey.TURN_LANES.unwrap()));

    }
}