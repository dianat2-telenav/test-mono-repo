package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmSourceTagKey;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import static com.telenav.datapipelinecore.osm.OsmHighwayValue.MOTORWAY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * @author roxanal
 */
public class ManualOneWayRuleTest extends BaseTest {

    @Test
    public void manualEditAndOneWayValuePresent() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "1")
                .withTag(OsmSourceTagKey.SOURCE.telenavPrefix() + ":" + OsmTagKey.ONEWAY.unwrap(),
                        TnSourceValue.MANUAL.unwrap());
        final WaySection adaptedOneWaySection = new ManualOneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(2, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "1"));
        assertTrue(
                adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.MANUAL), "1"));
    }

    @Test
    public void onlyManualEditValuePresent() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmSourceTagKey.SOURCE.telenavPrefix() + ":" + OsmTagKey.ONEWAY.unwrap(),
                        TnSourceValue.MANUAL.unwrap());
        final WaySection adaptedOneWaySection = new ManualOneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(1, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.MANUAL),
                TnSourceValue.MISSING.unwrap()));
    }

    @Test
    public void onlyOneWayValuePresent() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "1");
        final WaySection adaptedOneWaySection = new ManualOneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(1, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "1"));
    }

    @Test
    public void manualEditAndOneWayValueMissing() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap());
        final WaySection adaptedOneWaySection = new ManualOneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(1, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
    }
}