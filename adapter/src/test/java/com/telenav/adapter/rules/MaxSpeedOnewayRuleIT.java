package com.telenav.adapter.rules;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;
import java.util.Map;
import org.hamcrest.core.Is;
import org.junit.Test;


/**
 * @author olehd
 */
public class MaxSpeedOnewayRuleIT extends BaseTest {

    @Test
    public void onewayForward() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "20")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "30");

        final WaySection adaptedWaySection = new MaxSpeedOnewayRule<WaySection>().apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(tags.get(OsmTagKey.MAXSPEED_FORWARD.unwrap()), Is.is("20"));
        assertNull(tags.get(OsmTagKey.MAXSPEED_BACKWARD.unwrap()));
    }

    @Test
    public void onewayBackward() {
        final WaySection waySection = waysection(3,  1)
            .withTag(OsmTagKey.ONEWAY.unwrap(), "-1")
            .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "20")
            .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "30");

        final WaySection adaptedWaySection = new MaxSpeedOnewayRule<WaySection>().apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertNull(tags.get(OsmTagKey.MAXSPEED_FORWARD.unwrap()));
        assertThat(tags.get(OsmTagKey.MAXSPEED_BACKWARD.unwrap()), Is.is("30"));
    }
}