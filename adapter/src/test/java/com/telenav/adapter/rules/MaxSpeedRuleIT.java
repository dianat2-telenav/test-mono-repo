package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * @author roxanal
 */
public class MaxSpeedRuleIT extends BaseTest {

    @Test
    public void doesNotChangeAnythingInCaseOfNonExistingMaxSpeedTags() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.ONEWAY.unwrap(), "1");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void addMaxSpeedFromOsmForForwardDirection() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "22")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(5));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.archivedKeyName(), "22"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
    }

    @Test
    public void addMaxSpeedFromImageForForwardDirection() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "22")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES), "30")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(6));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "30"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.IMAGES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20"));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.archivedKeyName(), "22"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
    }

    @Test
    public void addMaxSpeedFromImageAndProbeForForwardDirection() {
        final WaySection waySection = waysection(3,  1)
            .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "22")
            .withTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20")
            .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES), "30")
            .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.PROBES), "40")
            .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
            .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(7));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "30"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.IMAGES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20"));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.archivedKeyName(), "22"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
    }

    @Test
    public void addMaxSpeedFromManualForBackwardDirection() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.DELETED), "20");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(5));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.DELETED.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "20"));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20"));
    }

    @Test
    public void addMaxSpeedFromOsmForBackwardDirection() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(5));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
    }

    @Test
    public void addMaxSpeedFromImageForBackwardDirection() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "22")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "22")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "30")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(6));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "30"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.IMAGES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "22"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "22"));
    }

    @Test
    public void addMaxSpeedFromImageAndProbesForBackwardDirection() {
        final WaySection waySection = waysection(3,  1)
            .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "22")
            .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "22")
            .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "30")
            .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.PROBES), "40")
            .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
            .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(7));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "30"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.IMAGES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "22"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "22"));
    }

    @Test
    public void addMaxSpeedFromOsmForBackwardAndForwardDirection() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "22 mph")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "22")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "23 mph")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "23")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(8));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "22"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "23"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "22 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.archivedKeyName(), "23 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
    }

    @Test
    public void addMaxSpeedFromImagesForBackwardAndForwardDirection() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "22 mph")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "22")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "50")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "23")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "23")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES), "60")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(10));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "50"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.IMAGES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "60"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.IMAGES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "22 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "22 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "23"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "22"));
    }

    @Test
    public void addMaxSpeedFromImagesForBackwardAndFromOsmForForward() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20 mph")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "50")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "23")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "60")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(9));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "50"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.IMAGES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "60"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "20 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.archivedKeyName(), "23"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20"));
    }

    @Test
    public void addMaxSpeedFromImagesForBackwardAndMaxSpeedConditional() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20 mph")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "50")
                .withTag(OsmTagKey.MAXSPEED_CONDITIONAL.unwrap(), "23 mph")
                .withTag(OsmTagKey.MAXSPEED_CONDITIONAL.telenavPrefix(), "23")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(9));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "50"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.IMAGES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "20 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_CONDITIONAL.archivedKeyName(), "23 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_CONDITIONAL.unwrap(), "23"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_CONDITIONAL.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20"));
    }

    @Test
    public void removeMaxSpeedTagsIfSpeedUnitIsMissing() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "22 mph")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "1")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "23")
                .withTag(OsmTagKey.MAXSPEED_CONDITIONAL.unwrap(), "24")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(5));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "1"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "22 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_CONDITIONAL.archivedKeyName(), "24"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.archivedKeyName(), "23"));
    }

    @Test
    public void addOnlyArchivedMaxSpeedConditionalTagWhenTheTagValueIsWrong() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_CONDITIONAL.unwrap(), "24mp")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(2));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_CONDITIONAL.archivedKeyName(), "24mp"));
    }

    @Test
    public void addMaxSpeedFromManualAndOsmForForwardDirection() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "20")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.MANUAL), "20");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(6));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.archivedKeyName(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.MANUAL.unwrap()));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
    }

    @Test
    public void addMaxSpeedFromManualForBackwardAndMaxSpeedConditional() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20 mph")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.MANUAL), "20 mph")
                .withTag(OsmTagKey.MAXSPEED_CONDITIONAL.unwrap(), "23 mph")
                .withTag(OsmTagKey.MAXSPEED_CONDITIONAL.telenavPrefix(), "23")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50; 8");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(9));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.MANUAL.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "20 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_CONDITIONAL.archivedKeyName(), "23 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_CONDITIONAL.unwrap(), "23"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_CONDITIONAL.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "50; 8"));
    }

    @Test
    public void addMaxSpeedFromManualForForwardDirectionAndImagesForBackwardDirection() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "20")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "20")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.MANUAL), "20");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(8));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.MANUAL.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.IMAGES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20"));
    }

    @Test
    public void addMaxSpeedFromManualForBackwardDirectionAndImagesForForwardDirection() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "20")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES), "20")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.MANUAL), "20");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(8));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.MANUAL.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.IMAGES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20"));
    }

    @Test
    public void addMaxSpeedFromManualForForwardDirectionAndImagesForForwardDirection() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "20")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES), "20")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.MANUAL), "20");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(6));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.MANUAL.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES), "20"));
    }

    @Test
    public void addMaxSpeedFromManualForBackwardDirectionAndImagesForBackwardDirection() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "20")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "20")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.MANUAL), "20");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(6));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.archivedKeyName(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.MANUAL.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "20"));
    }

    @Test
    public void osmMaxSpeedTagIsMissing() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "20")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag("tn__" + TnSourceValue.MANUAL.unwrap() + "_" + OsmTagKey.MAXSPEED.unwrap(), "missing");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(4));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection
                .hasTag("tn__" + TnSourceValue.MANUAL.unwrap() + "_" + OsmTagKey.MAXSPEED.unwrap(), "missing"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.IMAGES.unwrap()));
    }

    @Test
    public void addMaxSpeedFromProbesAndIsaForForwardDirection() {
        final WaySection waySection = waysection(3,  1)
            .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
            .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.PROBES), "20")
            .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.ISA), "25");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(4));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.PROBES.unwrap()));
    }

    @Test
    public void addMaxSpeedFromIsaForForwardDirection() {
        final WaySection waySection = waysection(3,  1)
            .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
            .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.ISA), "20");

        final WaySection adaptedWaySection = new MaxSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "20"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.ISA.unwrap()));
    }


}