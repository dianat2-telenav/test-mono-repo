package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.fc.TagService;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author olehd
 */
@RunWith(JUnitParamsRunner.class)
public class MaxSpeedSourceRuleIT extends BaseTest {

    @Test
    public void maxSpeedIsaSource() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.ISA.unwrap());
        final WaySection adaptedWaySection = new MaxSpeedSourceRule<WaySection>().apply(waySection);
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.withSource(), TnSourceValue.DERIVED.unwrap()));
    }

    @Test
    public void maxSpeedProbesSource() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.PROBES.unwrap());
        final WaySection adaptedWaySection = new MaxSpeedSourceRule<WaySection>().apply(waySection);
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.withSource(), TnSourceValue.DERIVED.unwrap()));
    }

    @Test
    public void maxSpeedImagesSource() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.IMAGES.unwrap());
        final WaySection adaptedWaySection = new MaxSpeedSourceRule<WaySection>().apply(waySection);
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.withSource(), TnSourceValue.POSTED.unwrap()));
    }

    @Test
    public void maxSpeedOsmSource() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.OSM.unwrap());
        final WaySection adaptedWaySection = new MaxSpeedSourceRule<WaySection>().apply(waySection);
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.withSource(), TnSourceValue.POSTED.unwrap()));
    }

    @Test
    @Parameters({
            "USA",
            "MEX",
            "CAN",
    })
    public void maxSpeedSourceWaySectionInRegion(final String region) {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.OSM.unwrap())
                .withTag(TagService.TN_ISO_TAG, region);
        final WaySection adaptedWaySection = new MaxSpeedSourceRule<WaySection>().apply(waySection);
        final WaySection testWaySection = waysection(3, 1)
                .withTag(OsmTagKey.MAXSPEED.withSource(), TnSourceValue.POSTED.unwrap())
                .withTag(TagService.TN_ISO_TAG, region);
        assertEquals(adaptedWaySection, testWaySection);
    }

    @Test
    public void maxSpeedSourceWaySectionWithCoordinatesOfNAWithoutIsoTag(){
        Node node = new Node(1, Latitude.degrees(24.679516900000003), Longitude.degrees(-81.2340119));
        Node node1 = new Node(2, Latitude.degrees(24.678117800000003), Longitude.degrees(-81.236253));
        final WaySection waySection = waysection(3,1,node,node1)
                .withTag(OsmTagKey.MAXSPEED.withSource(),"FDTO maximum speed limit");
        final WaySection testWaySection = waysection(3,1,node,node1)
                .withTag(OsmTagKey.MAXSPEED.withSource(), TnSourceValue.POSTED.unwrap());

        final WaySection adaptedWaySection = new MaxSpeedSourceRule<WaySection>().apply(waySection);

        assertEquals(adaptedWaySection,testWaySection);
    }

}