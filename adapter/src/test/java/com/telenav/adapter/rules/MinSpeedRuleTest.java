package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.osm.OsmPostFix;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;


/**
 * Unit tests for {@link MinSpeedRule}.
 *
 * @author ioanao
 */
public class MinSpeedRuleTest {

    private static WaySection basicWaySection;


    @BeforeClass
    public static void init() {
        final Node node1 = new Node(1, Latitude.degrees(24.32), Longitude.degrees(53.32));
        basicWaySection =
                new WaySection(3L, Collections.singletonList(node1), Collections.emptyMap()).withSequenceNumber(1);
    }

    @Test
    public void doesNotChangeAnythingInCaseOfNonExistingMinSpeedTags() {
        final WaySection waySection = basicWaySection.withTag(OsmTagKey.ONEWAY.unwrap(), "1");

        final WaySection adaptedWaySection = new MinSpeedRule().apply(waySection);
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void addMinSpeedTags() {
        final WaySection waySection = basicWaySection.withTag(OsmTagKey.MINSPEED_FORWARD.unwrap(), "22 mph")
                .withTag(OsmTagKey.MINSPEED_FORWARD.telenavPrefix(), "22")
                .withTag(OsmTagKey.MINSPEED_BACKWARD.unwrap(), "23 mph")
                .withTag(OsmTagKey.MINSPEED_BACKWARD.telenavPrefix(), "23")
                .withTag(OsmTagKey.MINSPEED.unwrap(), "24 mph")
                .withTag(OsmTagKey.MINSPEED.telenavPrefix(), "24")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M");

        final WaySection adaptedWaySection = new MinSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(10));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_FORWARD.unwrap(), "22"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_FORWARD.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED.unwrap(), "24"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_BACKWARD.unwrap(), "23"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_BACKWARD.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_FORWARD.archivedKeyName(), "22 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_BACKWARD.archivedKeyName(), "23 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED.archivedKeyName(), "24 mph"));
    }

    @Test
    public void addOnlyArchivedMinSpeedTags() {
        final WaySection waySection = basicWaySection.withTag(OsmTagKey.MINSPEED_FORWARD.unwrap(), "22 m")
                .withTag(OsmTagKey.MINSPEED_BACKWARD.unwrap(), "23 m").withTag(OsmTagKey.MINSPEED.unwrap(), "24 m")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M");

        final WaySection adaptedWaySection = new MinSpeedRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(4));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_FORWARD.archivedKeyName(), "22 m"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_BACKWARD.archivedKeyName(), "23 m"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED.archivedKeyName(), "24 m"));
    }
}