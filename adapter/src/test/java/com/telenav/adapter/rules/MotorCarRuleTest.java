package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.Way;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.telenav.datapipelinecore.osm.OsmTagKey.*;
import static org.junit.Assert.assertEquals;


/**
 * Unit tests for {@link MotorCarRule}.
 *
 * @author catalinm
 */
public class MotorCarRuleTest extends BaseTest {

    private MotorCarRule<WaySection> motorCarRuleWaySection;

    @Before
    public void initRule() {
        motorCarRuleWaySection = new MotorCarRule<>();
    }

    @Test
    public void waySectionWithYesValueForMotorCarTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // nothing is done if motorcar tag already exists and has 'yes' value
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void waySectionWithNoValueForMotorCarTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // nothing is done if motorcar tag already exists and has 'no' value
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void waySectionWithDesignatedValueForMotorCarTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "designated");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithPermissiveValueForMotorCarTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "permissive");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndUnclassifiedValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "unclassified");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "unclassified");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndMotorwayValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "motorway");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "motorway");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndMotorwayLinkValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "motorway_link");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "motorway_link");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndTrunkValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "trunk");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "trunk");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndTrunkLinkValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "trunk_link");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "trunk_link");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndPrimaryValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "primary");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "primary");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndPrimaryLinkValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "primary_link");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "primary_link");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndSecondaryValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "secondary");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "secondary");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndSecondaryLinkValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "secondary_link");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "secondary_link");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndTertiaryValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "tertiary");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "tertiary");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndTertiaryLinkValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "tertiary_link");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "tertiary_link");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndResidentialValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "residential");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "residential");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndServiceValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "service");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "service");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "yes"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndOtherValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown")
                .withTag(HIGHWAY.unwrap(), "other");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "no")
                .withTag(HIGHWAY.unwrap(), "other");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "no"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithUnknownValueForMotorCarTagAndNoValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "unknown");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "no"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithOtherValueForMotorCarTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "other");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // replace the existing tag value with "no"
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndUnclassifiedValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "unclassified");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "unclassified");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndMotorwayValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "motorway");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "motorway");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndMotorwayLinkValueForHighWayTag() {
        final WaySection waySection =
                waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "motorway_link");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "motorway_link");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndTrunkValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "trunk");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "trunk");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndTrunkLinkValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "trunk_link");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "trunk_link");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndPrimaryValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "primary");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "primary");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndPrimaryLinkValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "primary_link");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "primary_link");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndSecondaryValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "secondary");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "secondary");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndSecondaryLinkValueForHighWayTag() {
        final WaySection waySection =
                waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "secondary_link");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "secondary_link");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndTertiaryValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "tertiary");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "tertiary");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndTertiaryLinkValueForHighWayTag() {
        final WaySection waySection =
                waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "tertiary_link");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "tertiary_link");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndResidentialValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "residential");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "residential");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndServiceValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "service");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "yes")
                .withTag(HIGHWAY.unwrap(), "service");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "yes" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndOtherValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(HIGHWAY.unwrap(), "other");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "no")
                .withTag(HIGHWAY.unwrap(), "other");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "no" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndNoValueForHighWayTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add a motorcar tag with "no" value
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndNoValueForHighWayTagAndMotorVehicleTagYesWithRouteTagFerry() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTOR_VEHICLE.unwrap(),"yes").withTag(ROUTE.unwrap(),"ferry");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTOR_VEHICLE.unwrap(),"yes")
                .withTag(MOTORCAR.unwrap(), "yes").withTag(ROUTE.unwrap(),"ferry");;
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add motorcar tag according with motor_vehicle tag ("yes")
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithoutMotorCarTagAndNoValueForHighWayTagAndMotorVehicleTagNoWithRouteTagFerry() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTOR_VEHICLE.unwrap(),"no").withTag(ROUTE.unwrap(),"ferry");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTOR_VEHICLE.unwrap(),"no")
                .withTag(MOTORCAR.unwrap(), "no").withTag(ROUTE.unwrap(),"ferry");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add motorcar tag according with motor_vehicle tag ("no")
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithMotorCarTagYesAndMotorVehicleTagYesWithRouteTagFerry() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTOR_VEHICLE.unwrap(),"yes").withTag(ROUTE.unwrap(),"ferry").withTag(MOTORCAR.unwrap(),"no");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTOR_VEHICLE.unwrap(),"yes")
                .withTag(MOTORCAR.unwrap(), "yes").withTag(ROUTE.unwrap(),"ferry");;
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add motorcar tag according with motor_vehicle tag ("yes")
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithMotorCarTagYesAndMotorVehicleTagNoWithRouteTagFerry() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTOR_VEHICLE.unwrap(),"no").withTag(ROUTE.unwrap(),"ferry").withTag(MOTORCAR.unwrap(),"yes");
        final WaySection testSection = waysection(3, 1).withTag("tag1", "yes").withTag(MOTOR_VEHICLE.unwrap(),"no")
                .withTag(MOTORCAR.unwrap(), "no").withTag(ROUTE.unwrap(),"ferry");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add motorcar tag according with motor_vehicle tag ("no")
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithNoAccessAndWithoutMotorcarTag() {
        final WaySection waySection = waysection(3, 1).withTag(ACCESS.unwrap(), "no");
        final WaySection testSection = waysection(3, 1).withTag(ACCESS.unwrap(), "no").withTag(MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add "no" motorcar tag because it is not allowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithNoAccessAndWithMotorcarTag() {
        final WaySection waySection = waysection(3, 1).withTag(ACCESS.unwrap(), "no").withTag(MOTORCAR.unwrap(), "yes");
        final WaySection testSection = waysection(3, 1).withTag(ACCESS.unwrap(), "no").withTag(MOTORCAR.unwrap(), "yes");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // do nothing because motorcar is allowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithAgriculturalAccessAndWithoutMotorcarTag() {
        final WaySection waySection = waysection(3, 1).withTag(ACCESS.unwrap(), "agricultural");
        final WaySection testSection = waysection(3, 1).withTag(ACCESS.unwrap(), "agricultural").withTag(MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add "no" motorcar tag because it is not allowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithAgriculturalAccessAndWithMotorcarYesTag() {
        final WaySection waySection = waysection(3, 1).withTag(ACCESS.unwrap(), "agricultural").withTag(MOTORCAR.unwrap(), "yes");
        final WaySection testSection = waysection(3, 1).withTag(ACCESS.unwrap(), "agricultural").withTag(MOTORCAR.unwrap(), "yes");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // do nothing because motorcar is allowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithAgriculturalAccessAndWithMotorcarNoTag() {
        final WaySection waySection = waysection(3, 1).withTag(ACCESS.unwrap(), "agricultural").withTag(MOTORCAR.unwrap(), "no");
        final WaySection testSection = waysection(3, 1).withTag(ACCESS.unwrap(), "agricultural").withTag(MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // do nothing because motorcar is disallowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithForestryAccessAndWithoutMotorcarTag() {
        final WaySection waySection = waysection(3, 1).withTag(ACCESS.unwrap(), "forestry");
        final WaySection testSection = waysection(3, 1).withTag(ACCESS.unwrap(), "forestry").withTag(MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add "no" motorcar tag because it is not allowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithForestryAccessAndWithMotorcarYesTag() {
        final WaySection waySection = waysection(3, 1).withTag(ACCESS.unwrap(), "forestry").withTag(MOTORCAR.unwrap(), "yes");
        final WaySection testSection = waysection(3, 1).withTag(ACCESS.unwrap(), "forestry").withTag(MOTORCAR.unwrap(), "yes");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // do nothing because motorcar is allowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithForestryAccessAndWithMotorcarNoTag() {
        final WaySection waySection = waysection(3, 1).withTag(ACCESS.unwrap(), "forestry").withTag(MOTORCAR.unwrap(), "no");
        final WaySection testSection = waysection(3, 1).withTag(ACCESS.unwrap(), "forestry").withTag(MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // do nothing because motorcar is disallowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithEmergencyAccessAndWithoutMotorcarTag() {
        final WaySection waySection = waysection(3, 1).withTag(ACCESS.unwrap(), "emergency");
        final WaySection testSection = waysection(3, 1).withTag(ACCESS.unwrap(), "emergency").withTag(MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // add "no" motorcar tag because it is not allowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithEmergencyAccessAndWithMotorcarYesTag() {
        final WaySection waySection = waysection(3, 1).withTag(ACCESS.unwrap(), "emergency").withTag(MOTORCAR.unwrap(), "yes");
        final WaySection testSection = waysection(3, 1).withTag(ACCESS.unwrap(), "emergency").withTag(MOTORCAR.unwrap(), "yes");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // do nothing because motorcar is allowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void waySectionWithEmergencyAccessAndWithMotorcarNoTag() {
        final WaySection waySection = waysection(3, 1).withTag(ACCESS.unwrap(), "emergency").withTag(MOTORCAR.unwrap(), "no");
        final WaySection testSection = waysection(3, 1).withTag(ACCESS.unwrap(), "emergency").withTag(MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // do nothing because motorcar is disallowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }

    @Test
    public void wayFilterPermissiveToYes() {
        MotorCarRule<RawWay> motorCarRule = new MotorCarRule<>();
        List<Long> nodesIds = new ArrayList<>();
        nodesIds.add(1L);
        nodesIds.add(2L);
        final RawWay way = rawWay(3,nodesIds).withTag(MOTORCAR.unwrap(), "permissive");
        final RawWay testWay = rawWay(3,nodesIds);
        final RawWay adaptedWay = motorCarRule.apply(way);
        // do nothing because motorcar is disallowed explicitly
        assertEquals(testWay, adaptedWay);
    }
    @Test
    public void testMotorcarPrivate() {
        final WaySection waySection = waysection(3, 1)
                .withTag(MOTOR_VEHICLE.unwrap(),"private")
                .withTag(ROUTE.unwrap(), "ferry");
        final WaySection testSection = waysection(3, 1).withTag(MOTORCAR.unwrap(), "no").withTag(MOTOR_VEHICLE.unwrap(),"private")
                .withTag(ROUTE.unwrap(), "ferry");;
        final WaySection adaptedWaySection = motorCarRuleWaySection.apply(waySection);
        // do nothing because motorcar is disallowed explicitly
        assertEquals(testSection, adaptedWaySection);
    }
}