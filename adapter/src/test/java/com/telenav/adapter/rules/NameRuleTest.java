package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static com.telenav.datapipelinecore.osm.OsmHighwayValue.MOTORWAY;
import static org.assertj.core.api.Assertions.assertThat;


/**
 * Unit tests for {@link NameRule}
 *
 * @author roxanal
 */
public class NameRuleTest extends BaseTest {

    private NameRule<WaySection> waySectionNameRule;
    private NameRule<RawWay> rawWayNameRule;

    @Before
    public void initRule() {
        waySectionNameRule = new NameRule<>();
        rawWayNameRule = new NameRule<>();
    }

    @Test
    public void doNotAddNameTagPrefixedWithLanguageWhenLanguageTagIsMissing_waySection() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.NAME.telenavPrefix(), "Park Dr");
        final WaySection adaptedNameSection = waySectionNameRule.apply(waySection);
        assertThat(waySection).isEqualTo(adaptedNameSection);
    }

    @Test
    public void addNameTagPrefixedWithLanguage_waySection() {
        // Given
        final WaySection completeWaySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.NAME.unwrap(), "Park Drive")
                .withTag(OsmTagKey.NAME.telenavPrefix(), "Park Dr")
                .withTag(OsmTagKey.ALT_NAME.telenavPrefix(), "Detroit-Toledo")
                .withTag(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "eng");

        // When
        final WaySection adaptedNameSection = waySectionNameRule.apply(completeWaySection);

        // Then
        Map<String, String> adaptedTags = adaptedNameSection.tags();

        assertThat(adaptedNameSection.strictIdentifier().wayId()).isEqualTo(3);
        assertThat(adaptedTags).hasSize(completeWaySection.tags().size() + 1);
        assertThat(adaptedTags).containsEntry("name:eng", "Park Dr");
        assertThat(adaptedTags).containsEntry("name", "Park Dr");
        assertThat(adaptedTags).containsEntry("tn__name", "Park Dr");
        assertThat(adaptedTags).containsEntry("tn__alt_name", "Detroit-Toledo");
        assertThat(adaptedTags).containsEntry("highway", "motorway");
        assertThat(adaptedTags).containsEntry("tn__language_code", "eng");
    }

    @Test
    public void doNotAddNameTagPrefixedWithLanguageWhenLanguageTagIsMissing_rawWay() {
        final RawWay rawWay = rawWay(3).withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.NAME.telenavPrefix(), "Park Dr");
        final RawWay adaptedRawWay = rawWayNameRule.apply(rawWay);
        assertThat(rawWay).isEqualTo(adaptedRawWay);
    }

    @Test
    public void addNameTagPrefixedWithLanguage_rawWay() {
        // Given
        final RawWay rawWay = rawWay(3)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.NAME.unwrap(), "Park Drive")
                .withTag(OsmTagKey.NAME.telenavPrefix(), "Park Dr")
                .withTag(OsmTagKey.ALT_NAME.telenavPrefix(), "Detroit-Toledo")
                .withTag(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "eng");

        // When
        final RawWay adaptedRawWay = rawWayNameRule.apply(rawWay);

        // Then
        Map<String, String> adaptedTags = adaptedRawWay.tags();

        assertThat(adaptedRawWay.id()).isEqualTo(rawWay.id());
        assertThat(adaptedTags).hasSize(rawWay.tags().size() + 1);
        assertThat(adaptedTags).containsEntry("name:eng", "Park Dr");
        assertThat(adaptedTags).containsEntry("name", "Park Dr");
        assertThat(adaptedTags).containsEntry("tn__name", "Park Dr");
        assertThat(adaptedTags).containsEntry("tn__alt_name", "Detroit-Toledo");
        assertThat(adaptedTags).containsEntry("highway", "motorway");
        assertThat(adaptedTags).containsEntry("tn__language_code", "eng");
    }
}