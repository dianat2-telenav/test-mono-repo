package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author dianat2
 */
public class OneWayProbesValueRuleTest extends BaseTest {

    private OneWayProbesValueRule oneWayProbesValueRule = new OneWayProbesValueRule();

    @Test
    public void adaptOneWayProbeValue_hasForwardValue_valueIsYesInsteadOf1() {
        // given
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), DefaultTagValue.FORWARD_ONEWAY.unwrap());

        Map<String, String> expectedTags = new HashMap<>();
        expectedTags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "yes");

        // when
        final WaySection result = oneWayProbesValueRule.apply(waySection);

        // then
        assertThat(result.strictIdentifier()).isEqualTo(waySection.strictIdentifier());
        assertThat(result.nodes()).containsExactlyInAnyOrderElementsOf(waySection.nodes());
        assertThat(result.tags().size()).isEqualTo(expectedTags.size());
        assertThat(result.tags()).containsAllEntriesOf(expectedTags);
    }

    @Test
    public void adaptOneWayProbesValue_hasBackwardValue_valueIsYesInsteadOfMinus1() {
        // given
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), DefaultTagValue.BACKWARD_ONEWAY.unwrap());

        Map<String, String> expectedTags = new HashMap<>();
        expectedTags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "-1");

        // when
        final WaySection result = oneWayProbesValueRule.apply(waySection);

        // then
        assertThat(result.strictIdentifier()).isEqualTo(waySection.strictIdentifier());
        assertThat(result.nodes()).containsExactlyInAnyOrderElementsOf(waySection.nodes());
        assertThat(result.tags().size()).isEqualTo(expectedTags.size());
        assertThat(result.tags()).containsAllEntriesOf(expectedTags);
    }

    @Test
    public void adaptOneWayProbesValue_notForwardNorBackward_doNothing() {
        // given
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "yes");

        Map<String, String> expectedTags = new HashMap<>();
        expectedTags.put(OsmTagKey.MOTORCAR.unwrap(), "yes");

        // when
        final WaySection result = oneWayProbesValueRule.apply(waySection);

        // then
        assertThat(result.strictIdentifier()).isEqualTo(waySection.strictIdentifier());
        assertThat(result.nodes()).containsExactlyInAnyOrderElementsOf(waySection.nodes());
        assertThat(result.tags().size()).isEqualTo(expectedTags.size());
        assertThat(result.tags()).containsAllEntriesOf(expectedTags);
    }

}