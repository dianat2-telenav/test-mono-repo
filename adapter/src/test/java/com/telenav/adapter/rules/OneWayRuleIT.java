package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import static com.telenav.datapipelinecore.osm.OsmHighwayValue.MOTORWAY;
import static org.junit.Assert.*;


/**
 * Unit tests for {@link OneWayRule}.
 *
 * @author roxanal
 */
public class OneWayRuleIT extends BaseTest {

    @Test
    public void waySectionWithoutOneWayTag() {
        final WaySection waySection = waysection(1, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap());
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(waySection, adaptedOneWaySection);
    }

    @Test
    public void waySectionWithOsmOneWayTagYesValue() {
        WaySection waySection = waysection(3, 1);
        waySection = waySection.withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap());
        waySection = waySection.withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(2, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "yes"));

    }

    @Test
    public void waySectionWithOsmOneWayTagOneValue() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.unwrap(), "1");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(1, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
        // one_way tag is removed
        assertFalse(adaptedOneWaySection.hasKey(OsmTagKey.ONEWAY.unwrap()));
        assertFalse(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.withSource(), TnSourceValue.OSM.unwrap()));
        assertFalse(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.archivedKeyName(), "1"));
    }

    @Test
    public void waySectionWithOsmOneWayTagAndProbeTag() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "-1");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(2, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "-1"));
    }

    @Test
    public void waySectionWithProbeTag() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "-1");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(2, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "-1"));
    }

    @Test
    public void waySectionWithProbeAndImageTag() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "-1")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "yes");
        final WaySection adaptedOneWaySection =
                new OneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(3, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "yes"));
    }

    @Test
    public void waySectionWithImageTag() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "-1");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(2, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "-1"));
    }

    @Test
    public void waySectionWithImageTagAndOsmTagKey() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "-1");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(2, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "-1"));
    }

    @Test
    public void waySectionWithImageTagOsmTagKeyAndProbeTag() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "-1")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "1");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(3, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "-1"));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "1"));
    }

    @Test
    public void waySectionWithManualAndOsmTagKey() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.MANUAL), "yes");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(3, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "yes"));
    }

    @Test
    public void waySectionWithManualAndOsmAndImageTag() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "-1")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.MANUAL), "yes");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(3, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "-1"));
    }

    @Test
    public void waySectionWithManualAndOsmAndImageAndProbesTag() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "yes")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "yes")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.MANUAL), "yes");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(4, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "yes"));
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "yes"));
    }

    @Test
    public void waySectionWithOneWayTag() {
        final WaySection waySection = waysection(1, 1)
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(1, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "yes"));
    }

    @Test
    public void oneWayTagWithIllegalValue() {
        final WaySection waySection = waysection(1, 1)
                .withTag(OsmTagKey.ONEWAY.unwrap(), "alternative");
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(0, adaptedOneWaySection.tags().size());
        assertFalse(adaptedOneWaySection.hasKey(OsmTagKey.ONEWAY.unwrap()));
    }

    @Test
    public void noOneWayTag() {
        final WaySection waySection = waysection(1, 1);
        final WaySection adaptedOneWaySection = new OneWayRule().apply(waySection);

        assertEquals(0, adaptedOneWaySection.tags().size());
        assertFalse(adaptedOneWaySection.hasKey(OsmTagKey.ONEWAY.unwrap()));
    }

}