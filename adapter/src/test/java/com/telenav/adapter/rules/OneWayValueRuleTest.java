package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * @author roxanal
 */
public class OneWayValueRuleTest extends BaseTest {

    @Test
    public void adaptOneWayValue() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
        final WaySection adaptedOneWaySection = new OneWayValueRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(1, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "1"));
    }

    @Test
    public void doNotAdaptOneWayValue() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "-1");
        final WaySection adaptedOneWaySection = new OneWayValueRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(1, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "-1"));
    }

    @Test
    public void doNotAdaptOneWayNullValue() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.MOTORCAR.unwrap(), "yes");
        final WaySection adaptedOneWaySection = new OneWayValueRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedOneWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedOneWaySection.nodes());
        assertEquals(1, adaptedOneWaySection.tags().size());
        assertTrue(adaptedOneWaySection.hasTag(OsmTagKey.MOTORCAR.unwrap(), "yes"));
    }
}