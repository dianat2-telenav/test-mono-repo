package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.MotorCarExtractor;
import com.telenav.adapter.component.extractors.MotorCycleExtractor;
import com.telenav.adapter.component.extractors.RouteExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Unit tests for {@link RailFerryRule}
 *
 * @author petrum
 */
@RunWith(JUnitParamsRunner.class)
public class RailFerryRuleTest extends BaseTest {
    private static final RailFerryRule<WaySection> RAIL_FERRY_RULE = new RailFerryRule<>(new RouteExtractor(),
            new MotorCarExtractor(), new MotorCycleExtractor());

    private static final WaySection WAY_SECTION = waysection(1)
            .withTag(OsmTagKey.HIGHWAY.unwrap(), "motorway")
            .withTag(OsmTagKey.NAME.unwrap(), "Main Str");

    @Test
    public void simpleRailFerryDetected() {
        WaySection waySection = addSpecificRailFerryTags(waysection(1), "shuttle_train", "yes", "yes");

        WaySection adaptedWaySection = RAIL_FERRY_RULE.apply(waySection);

        assertNull(adaptedWaySection.tags().get(OsmTagKey.ROUTE.unwrap()));
        assertThat(adaptedWaySection.tags().get(OsmTagKey.MOTORCAR.unwrap()), is("yes"));
        assertThat(adaptedWaySection.tags().get(OsmTagKey.MOTORCYCLE.unwrap()), is("yes"));
        assertThat(adaptedWaySection.tags().get(UnidbTagKey.RAIL_FERRY.unwrap()), is("yes"));
//        assertThat(adaptedWaySection.tags().get(RoadCategoryTagKey.ROAD_TYPE.unwrap()), is("11"));
    }

    @Test
    public void railFerryDetectedWithOtherTags() {
        WaySection waySection = addSpecificRailFerryTags(WAY_SECTION, "shuttle_train", "yes", "yes");

        WaySection adaptedWaySection = RAIL_FERRY_RULE.apply(waySection);

        assertNull(adaptedWaySection.tags().get(OsmTagKey.ROUTE.unwrap()));
        assertThat(adaptedWaySection.tags().get(OsmTagKey.MOTORCAR.unwrap()), is("yes"));
        assertThat(adaptedWaySection.tags().get(OsmTagKey.MOTORCYCLE.unwrap()), is("yes"));
        assertThat(adaptedWaySection.tags().get(UnidbTagKey.RAIL_FERRY.unwrap()), is("yes"));
//        assertThat(adaptedWaySection.tags().get(RoadCategoryTagKey.ROAD_TYPE.unwrap()), is("11"));
    }

    @Test
    public void simpleNoRailFerryDetected() {
        WaySection waySection = addSpecificRailFerryTags(waysection(1), "ferry", "yes", "yes");

        WaySection adaptedWaySection = RAIL_FERRY_RULE.apply(waySection);

        testSpecificRailFerryTags(adaptedWaySection, "ferry", "yes", "yes", 3);
    }

    @Test
    public void noRailFerryDetectedWithOtherTags() {
        WaySection waySection = addSpecificRailFerryTags(WAY_SECTION, "route", "yes", "yes");

        WaySection adaptedWaySection = RAIL_FERRY_RULE.apply(waySection);

        testSpecificRailFerryTags(adaptedWaySection, "route", "yes", "yes", 5);
    }

    @Test
    @Parameters({
            "no, yes",
            "yes, no",
            "no, no",
    })
    public void differentValuesForTagsDifferentValuesForMotorcarAndMotorcycle(final String valueMotorcar,
                                                                              final String valueMotorcycle) {
        WaySection waySection = addSpecificRailFerryTags(WAY_SECTION, "shuttle_train", valueMotorcar, valueMotorcycle);
        WaySection adaptedWaySection = RAIL_FERRY_RULE.apply(waySection);
        testSpecificRailFerryTags(adaptedWaySection, "shuttle_train", valueMotorcar, valueMotorcycle, 5);
    }

    @Test
    public void routeTagIsNull() {
        WaySection waySection = WAY_SECTION.withTag("key1", "value1").withTag("key2", "value2");

        WaySection adaptedWaySection = RAIL_FERRY_RULE.apply(waySection);

        assertEquals(adaptedWaySection.tags().size(), 4);
        assertNull(adaptedWaySection.tags().get(OsmTagKey.ROUTE.unwrap()));
        assertNull(adaptedWaySection.tags().get(OsmTagKey.MOTORCAR.unwrap()));
        assertNull(adaptedWaySection.tags().get(OsmTagKey.MOTORCYCLE.unwrap()));
    }

    private WaySection addSpecificRailFerryTags(final WaySection waySection, final String valueForRouteTage,
                                                final String valueForMotorcarTag, final String valueForMotorcycleTag) {
        return waySection.withTag(OsmTagKey.ROUTE.unwrap(), valueForRouteTage)
                .withTag(OsmTagKey.MOTORCAR.unwrap(), valueForMotorcarTag)
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), valueForMotorcycleTag);
    }

    private void testSpecificRailFerryTags(final WaySection waySection, final String valueForRouteTage,
                                           final String valueForMotorcarTag, final String valueForMotorcycleTag,
                                           final int waySectionLength) {
        assertEquals(waySection.tags().size(), waySectionLength);
        assertThat(waySection.tags().get(OsmTagKey.ROUTE.unwrap()), is(valueForRouteTage));
        assertThat(waySection.tags().get(OsmTagKey.MOTORCAR.unwrap()), is(valueForMotorcarTag));
        assertThat(waySection.tags().get(OsmTagKey.MOTORCYCLE.unwrap()), is(valueForMotorcycleTag));
    }
}
