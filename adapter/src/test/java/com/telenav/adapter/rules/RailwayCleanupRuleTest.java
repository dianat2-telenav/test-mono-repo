package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import static com.telenav.datapipelinecore.unidb.RoadCategoryTagKey.*;
import static com.telenav.datapipelinecore.unidb.UnidbTagValue.*;
import static com.telenav.datapipelinecore.unidb.UnidbTagKey.RAILWAY;
import static com.telenav.datapipelinecore.unidb.UnidbTagKey.TYPE;
import static org.junit.Assert.*;

/**
 * Unit tests for {@link RailwayCleanupRule}
 *
 * @author irinap
 */
public class RailwayCleanupRuleTest extends BaseTest {
    private RailwayCleanupRule<WaySection> waySectionRule;
    private RailwayCleanupRule<RawWay> rawWayRule;
    private final String DUMMY_VALUE = "tag_value";

    @Before
    public void initRule() {
        waySectionRule = new RailwayCleanupRule<>();
        rawWayRule = new RailwayCleanupRule<>();
    }

    /* test on way sections */

    @Test
    public void testWaySectionNoRailwayTag() {
        final WaySection waySection = waysection(0, 0);
        final WaySection adaptedWaySection = waySectionRule.apply(waySection);
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void testWaySectionWithRailwayTag() {
        final WaySection waySection = waysection(0, 0).withTag(RAILWAY.unwrap(), DUMMY_VALUE);
        final WaySection adaptedWaySection = waySectionRule.apply(waySection);
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void testWaySectionWithRailwayWithRoadCategoryTagsNoCartoLine() {
        final WaySection waySection = waysection(0, 0)
                .withTag(RAILWAY.unwrap(), DUMMY_VALUE)
                .withTag(ROAD_TYPE.unwrap(), DUMMY_VALUE)
                .withTag(ROAD_SUBTYPE.unwrap(), DUMMY_VALUE)
                .withTag(FUNCTIONAL_CLASS.unwrap(), DUMMY_VALUE)
                .withTag(SPEED_CATEGORY.unwrap(), DUMMY_VALUE);
        final WaySection adaptedWaySection = waySectionRule.apply(waySection);
        assertEquals(waySection, adaptedWaySection);
        assertTrue(adaptedWaySection.hasTag(ROAD_TYPE.unwrap(), DUMMY_VALUE));
        assertTrue(adaptedWaySection.hasTag(ROAD_SUBTYPE.unwrap(), DUMMY_VALUE));
        assertTrue(adaptedWaySection.hasTag(SPEED_CATEGORY.unwrap(), DUMMY_VALUE));
        assertTrue(adaptedWaySection.hasTag(FUNCTIONAL_CLASS.unwrap(), DUMMY_VALUE));
    }

    @Test
    public void testWaySectionWithRailwayWithRoadCategoryTagsWithCartoLine() {
        final WaySection waySection = waysection(0, 0)
                .withTag(RAILWAY.unwrap(), RAIL.unwrap())
                .withTag(TYPE.unwrap(), CARTO_LINE.unwrap())
                .withTag(ROAD_TYPE.unwrap(), DUMMY_VALUE)
                .withTag(ROAD_SUBTYPE.unwrap(), DUMMY_VALUE)
                .withTag(FUNCTIONAL_CLASS.unwrap(), DUMMY_VALUE)
                .withTag(SPEED_CATEGORY.unwrap(), DUMMY_VALUE);
        final WaySection adaptedWaySection = waySectionRule.apply(waySection);
        assertNotEquals(waySection, adaptedWaySection);
        assertFalse(adaptedWaySection.hasTag(ROAD_TYPE.unwrap(), DUMMY_VALUE));
        assertFalse(adaptedWaySection.hasTag(ROAD_SUBTYPE.unwrap(), DUMMY_VALUE));
        assertFalse(adaptedWaySection.hasTag(SPEED_CATEGORY.unwrap(), DUMMY_VALUE));
        assertFalse(adaptedWaySection.hasTag(FUNCTIONAL_CLASS.unwrap(), DUMMY_VALUE));
    }

    /* test on raw ways */

    @Test
    public void testRawWayNoRailwayTag() {
        final RawWay rawWay = rawWay(0);
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertEquals(rawWay, adaptedRawWay);
    }

    @Test
    public void testRawWayWithRailwayTag() {
        final RawWay rawWay = rawWay(0).withTag(RAILWAY.unwrap(), DUMMY_VALUE);
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertEquals(rawWay, adaptedRawWay);
    }

    @Test
    public void testRawWayWithRailwayWithRoadCategoryTagsNoCartoLine() {
        final RawWay rawWay = rawWay(0)
                .withTag(RAILWAY.unwrap(), DUMMY_VALUE)
                .withTag(ROAD_TYPE.unwrap(), DUMMY_VALUE)
                .withTag(ROAD_SUBTYPE.unwrap(), DUMMY_VALUE)
                .withTag(FUNCTIONAL_CLASS.unwrap(), DUMMY_VALUE)
                .withTag(SPEED_CATEGORY.unwrap(), DUMMY_VALUE);
        final RawWay adaptedRawWay  = rawWayRule.apply(rawWay);
        assertEquals(rawWay, adaptedRawWay);
        assertTrue(adaptedRawWay.hasTag(ROAD_TYPE.unwrap(), DUMMY_VALUE));
        assertTrue(adaptedRawWay.hasTag(ROAD_SUBTYPE.unwrap(), DUMMY_VALUE));
        assertTrue(adaptedRawWay.hasTag(SPEED_CATEGORY.unwrap(), DUMMY_VALUE));
        assertTrue(adaptedRawWay.hasTag(FUNCTIONAL_CLASS.unwrap(), DUMMY_VALUE));
    }

    @Test
    public void testRawWayWithRailwayWithRoadCategoryTagsWithCartoLine() {
        final RawWay rawWay = rawWay(0)
                .withTag(RAILWAY.unwrap(), RAIL.unwrap())
                .withTag(TYPE.unwrap(), CARTO_LINE.unwrap())
                .withTag(ROAD_TYPE.unwrap(), DUMMY_VALUE)
                .withTag(ROAD_SUBTYPE.unwrap(), DUMMY_VALUE)
                .withTag(FUNCTIONAL_CLASS.unwrap(), DUMMY_VALUE)
                .withTag(SPEED_CATEGORY.unwrap(), DUMMY_VALUE);
        final RawWay adaptedRawWay  = rawWayRule.apply(rawWay);
        assertNotEquals(rawWay, adaptedRawWay);
        assertFalse(adaptedRawWay.hasTag(ROAD_TYPE.unwrap(), DUMMY_VALUE));
        assertFalse(adaptedRawWay.hasTag(ROAD_SUBTYPE.unwrap(), DUMMY_VALUE));
        assertFalse(adaptedRawWay.hasTag(SPEED_CATEGORY.unwrap(), DUMMY_VALUE));
        assertFalse(adaptedRawWay.hasTag(FUNCTIONAL_CLASS.unwrap(), DUMMY_VALUE));
    }
}
