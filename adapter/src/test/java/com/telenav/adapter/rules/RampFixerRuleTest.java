package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

@RunWith(JUnitParamsRunner.class)
public class RampFixerRuleTest {
    private List<Node> nodes = Arrays.asList(
            new Node(1, Latitude.MAXIMUM, Longitude.MAXIMUM, null),
            new Node(2, Latitude.MAXIMUM, Longitude.MAXIMUM, null));

    @Test
    @Parameters({
            "wheelchair=yes",
            "separate",
            "incline",
            "1",
            "down",
            "wheelchair",
            "up",
            "yes",
            "seperate",
            "bicycle=yes",
            "bicycle"})
    public void testValidTunnelValues(String ramp) {
        RampFixerRule rule = new RampFixerRule();
        WaySection ws = new WaySection(1, 1, nodes, null).withTag(RampFixerRule.RAMP_TAG_KEY, ramp);
        WaySection result = rule.apply(ws);
        Assert.assertTrue("Failed case: " + ramp, result.hasTag(RampFixerRule.RAMP_TAG_KEY, "Y"));
    }

    @Test
    @Parameters({"No", "no"})
    public void testNoTunnelValues(String ramp) {
        RampFixerRule rule = new RampFixerRule();
        WaySection ws = new WaySection(1, 1, nodes, null).withTag(RampFixerRule.RAMP_TAG_KEY, ramp);
        WaySection result = rule.apply(ws);
        Assert.assertTrue("Failed case: " + ramp, result.hasTag(RampFixerRule.RAMP_TAG_KEY, "N"));
    }
}
