package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.LanguageExtractor;
import com.telenav.adapter.component.extractors.RefExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import static com.telenav.datapipelinecore.osm.OsmTagKey.*;
import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_LANGUAGE_CODE;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;


/**
 * @author roxanal
 */
public class RefRuleTest extends BaseTest {

    private RefRule refRule;


    @Before
    public void initRule() {
        refRule = new RefRule(new RefExtractor(), new LanguageExtractor());
    }

    @Test
    public void waySectionWithNoRef() {
        final WaySection waySection = waysection(3,  1);
        final WaySection adaptedRefSection = refRule.apply(waySection);

        assertEquals(adaptedRefSection, waySection);
    }

    @Test
    public void waySectionWithOnlyOsmRef() {
        final WaySection waySection = waysection(3,  1)
                .withTag(REF.unwrap(), "MI-85 N")
                .withTag(ONEWAY.unwrap(), "true");
        final WaySection adaptedRefSection = refRule.apply(waySection);

        assertThat(adaptedRefSection.strictIdentifier(), is(waySection.strictIdentifier()));
        assertThat(adaptedRefSection.nodes(), is(waySection.nodes()));
        assertThat(adaptedRefSection.tags().values(), hasSize(2));
        assertTrue(adaptedRefSection.hasTag(ONEWAY.unwrap(), "true"));
    }

    @Test
    public void waySectionWithRelationRef() {
        final WaySection waySection = waysection(3,  1)
                .withTag(ONEWAY.unwrap(), "true")
                .withTag(OsmTagKey.REF.telenavPrefix(), "MI-85 S")
                .withTag(TN_LANGUAGE_CODE.unwrap(), "eng");
        final WaySection adaptedRefSection = refRule.apply(waySection);

        assertThat(adaptedRefSection.strictIdentifier(), is(waySection.strictIdentifier()));
        assertThat(adaptedRefSection.nodes(), is(waySection.nodes()));
        assertThat(adaptedRefSection.tags().values(), hasSize(4));
        assertTrue(adaptedRefSection.hasTag(ONEWAY.unwrap(), "true"));
        assertTrue(adaptedRefSection.hasTag(REF.postFixed("eng"), "MI-85 S"));
        assertTrue(adaptedRefSection.hasTag(OsmTagKey.REF.telenavPrefix(), "MI-85 S"));
        assertTrue(adaptedRefSection.hasTag(TN_LANGUAGE_CODE.unwrap(), "eng"));
    }

    @Test
    public void waySectionWithOsmRefAndRelationRefs() {
        final WaySection waySection = waysection(3,  1)
                .withTag(ONEWAY.unwrap(), "true")
                .withTag(REF.unwrap(), "CA-77 N")
                .withTag(OsmTagKey.REF.telenavPrefix(), "MI-85 S")
                .withTag(OsmTagKey.REF.telenavPrefix() + "_1", "CA-78 N")
                .withTag(TN_LANGUAGE_CODE.unwrap(), "eng");
        final WaySection adaptedRefSection = refRule.apply(waySection);

        assertThat(adaptedRefSection.strictIdentifier(), is(waySection.strictIdentifier()));
        assertThat(adaptedRefSection.nodes(), is(waySection.nodes()));
        assertThat(adaptedRefSection.tags().values(), hasSize(7));
        assertTrue(adaptedRefSection.hasTag(ONEWAY.unwrap(), "true"));
        assertTrue(adaptedRefSection.hasTag(REF.postFixed("eng"), "MI-85 S"));
        assertTrue(adaptedRefSection.hasTag(OsmTagKey.REF.telenavPrefix(), "MI-85 S"));
        assertTrue(adaptedRefSection.hasTag(REF.postFixed((short) 1, "eng"), "CA-78 N"));
        assertTrue(adaptedRefSection.hasTag(OsmTagKey.REF.telenavPostFixed((short) 1), "CA-78 N"));
        assertTrue(adaptedRefSection.hasTag(TN_LANGUAGE_CODE.unwrap(), "eng"));
    }

    @Test
    public void waySectionNoLanguage() {
        final WaySection waySection = waysection(3,  1)
                .withTag(REF.unwrap(), "MI 85 N")
                .withTag(OsmTagKey.REF.telenavPrefix(), "MI-85 N")
                .withTag(ONEWAY.unwrap(), "true");
        final WaySection adaptedRefSection = refRule.apply(waySection);

        assertThat(adaptedRefSection.strictIdentifier(), is(waySection.strictIdentifier()));
        assertThat(adaptedRefSection.nodes(), is(waySection.nodes()));
        assertThat(adaptedRefSection.tags().values(), hasSize(3));
        assertTrue(adaptedRefSection.hasTag(ONEWAY.unwrap(), "true"));
        assertTrue(adaptedRefSection.hasTag(OsmTagKey.REF.telenavPrefix(), "MI-85 N"));
    }
}