package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.RoadCategoryExtractor;
import com.telenav.adapter.component.extractors.RoadFunctionalClassExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;

import static com.telenav.adapter.component.fc.TagService.HIGHWAY_TAG;
import static com.telenav.datapipelinecore.osm.OsmHighwayValue.MOTORWAY;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;


/**
 * @author roxanal
 */
@RunWith(JUnitParamsRunner.class)
public class RoadCategoryRuleTest extends BaseTest {

    private RoadCategoryRule rule;


    @Before
    public void initRule() {
        rule = new RoadCategoryRule(new RoadFunctionalClassExtractor(), Collections.emptyMap());
    }

    @Test
    public void testRoadCategoryAdapterFc() {
        final WaySection waySection =
                waysection(3, 1).withTag(TnTagKey.TN_FUNCTIONAL_CLASS.unwrap(), "1")
                        .withTag(HIGHWAY_TAG, "motorway");
        final WaySection adaptedWaySection = rule.apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertThat(adaptedWaySection.tags(), hasEntry(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "1"));
        assertThat(adaptedWaySection.tags(), hasEntry(TnTagKey.TN_FUNCTIONAL_CLASS.unwrap(), "1"));
    }

    @Test
    @Parameters({
            "tn__ramp, Y",
            "tn__intersection_link, value",
            "tn__connection_road, value",
            "tn__dual_way, value"
    })
    public void testFunctionalClassAdapterWithoutSpecificTags(final String tagKey,
                                                              final String tagValue){
        final WaySection waySection = waysection(3, 1).withTag(tagKey, tagValue);
        final WaySection adaptedWaySection = rule.apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(1));
        assertThat(adaptedWaySection.tags(), hasEntry(tagKey, tagValue));
    }

    @Test
    public void testForCompleteWaySection() {
        final Node node = node(1).withTag(OsmTagKey.HIGHWAY.unwrap(), "motorway_junction");
        final WaySection waySection = waysection(3, 1, node)
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.NAME.telenavPrefix(), "Park Dr")
                .withTag(OsmTagKey.ALT_NAME.telenavPrefix(), "Detroit-Toledo")
                .withTag(OsmTagKey.REF.telenavPrefix(), "MI-85 N")
                .withTag(OsmTagKey.JUNCTION.unwrap(), "roundabout")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50 mph")
                .withTag(OsmTagKey.LANES.unwrap(), "2")
                .withTag(OsmTagKey.JUNCTION.unwrap() + ":ref", "1A");

        final WaySection adaptedWaySection = rule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(10));
        assertThat(adaptedWaySection.tags(), hasEntry(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(), "1"));
    }
}