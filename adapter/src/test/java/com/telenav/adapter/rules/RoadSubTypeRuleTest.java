package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import com.telenav.adapter.bean.RoadSubType;
import com.telenav.adapter.component.extractors.RoadSubTypeExtractor;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmTagKey.HIGHWAY;
import static com.telenav.datapipelinecore.osm.OsmTagKey.MAXSPEED;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


/**
 * @author petrum
 */
@RunWith(MockitoJUnitRunner.class)
public class RoadSubTypeRuleTest {

    private static WaySection waySection;
    private static Node node1 = new Node(1, Latitude.degrees(24.32), Longitude.degrees(53.32));
    private static Node node2 = new Node(2, Latitude.degrees(34.32), Longitude.degrees(12.32));
    @Mock
    private RoadSubTypeExtractor extractor;

    @BeforeClass
    public static void init() {
        waySection = new WaySection(3L, Arrays.asList(node1, node2), Collections.emptyMap()).withSequenceNumber(1);
    }

    @Test
    public void addRSTTag() {
        Mockito.when(extractor.extract(waySection.tags())).thenReturn(Optional.of(RoadSubType.MAIN_ROAD));

        final RoadSubTypeRule withRST = new RoadSubTypeRule(extractor);
        final WaySection waySectionWithRST = withRST.apply(waySection);

        Mockito.verify(extractor).extract(waySection.tags());
        assertThat(waySectionWithRST.strictIdentifier().wayId(), is(3L));
        assertThat(waySectionWithRST.sequenceNumber(), is((short) 1));
        assertThat(waySectionWithRST.tags().values(), hasSize(1));
        assertThat(waySectionWithRST.tags(), hasEntry(RoadCategoryTagKey.ROAD_SUBTYPE.unwrap(),
                RoadSubType.identifierFor(RoadSubType.MAIN_ROAD) + ""));
    }

    @Test
    public void noRSTTag() {
        Mockito.when(extractor.extract(waySection.tags())).thenReturn(Optional.empty());

        final RoadSubTypeRule withRST = new RoadSubTypeRule(extractor);
        final WaySection waySectionWithRST = withRST.apply(waySection);

        Mockito.verify(extractor).extract(waySection.tags());
        assertThat(waySectionWithRST, is(waySection));
    }

    @Test
    public void realCases() {
        final RoadSubTypeExtractor roadSubTypeExtractor = new RoadSubTypeExtractor();
        final RoadSubTypeRule withRST = new RoadSubTypeRule(roadSubTypeExtractor);

        //https://www.openstreetmap.org/way/178081711
        final WaySection clineAve = new WaySection(178081711L,
                Arrays.asList(node1, node2),
                Collections.emptyMap())
                .withTag(HIGHWAY.unwrap(), "motorway")
                .withTag("rst", "2");
        final WaySection clineAveRT = withRST.apply(clineAve);
        assertEquals(clineAveRT.tags().get(RoadCategoryTagKey.ROAD_SUBTYPE.unwrap()), "2");

        //https://www.openstreetmap.org/way/24221335
        final WaySection jArthurYoungerFreeway = new WaySection(24221335L,
                Arrays.asList(node1, node2),
                Collections.emptyMap())
                .withTag(HIGHWAY.unwrap(), "motorway")
                .withTag("rst", "2");;
        final WaySection jArthurYoungerFreewayRt = withRST.apply(jArthurYoungerFreeway);
        assertEquals(jArthurYoungerFreewayRt.tags().get(RoadCategoryTagKey.ROAD_SUBTYPE.unwrap()), "2");

        //https://www.openstreetmap.org/way/68325061
        final WaySection twelfthAve = new WaySection(68325061L,
                Arrays.asList(node1, node2),
                Collections.emptyMap())
                .withTag(HIGHWAY.unwrap(), "primary")
                .withTag(MAXSPEED.unwrap(), "30 mph")
                .withTag("rst", "2");;
        final WaySection twelfthAveRt = withRST.apply(twelfthAve);
        assertEquals(twelfthAveRt.tags().get(RoadCategoryTagKey.ROAD_SUBTYPE.unwrap()), "2");
    }
}
