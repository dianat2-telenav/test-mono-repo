package com.telenav.adapter.rules;

import com.telenav.adapter.bean.RoadType;
import com.telenav.adapter.component.extractors.RoadTypeExtractor;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static com.telenav.adapter.NgxWaySectionsAdapterApplication.CUSTOM_WAY_ID_TAG_KEY;
import static com.telenav.datapipelinecore.osm.OsmTagKey.HIGHWAY;
import static com.telenav.datapipelinecore.osm.OsmTagKey.MAXSPEED;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


/**
 * @author petrum
 */
@RunWith(MockitoJUnitRunner.class)
public class RoadTypeRuleTest {
    private static WaySection waySection;
    private static Node node1 = new Node(1, Latitude.degrees(24.32), Longitude.degrees(53.32));
    private static Node node2 = new Node(2, Latitude.degrees(34.32), Longitude.degrees(12.32));
    @Mock
    private RoadTypeExtractor extractor;

    @BeforeClass
    public static void init() {
        waySection = new WaySection(3L, Arrays.asList(node1, node2), Collections.emptyMap()).withSequenceNumber(1);
    }

    @Test
    public void addRTTag() {
        Mockito.when(extractor.extract(waySection.tags())).thenReturn(Optional.of(RoadType.HIGHWAY));

        final RoadTypeRule withRT = new RoadTypeRule(extractor);
        final WaySection waySectionWithRT = withRT.apply(waySection);

        Mockito.verify(extractor).extract(waySection.tags());
        assertThat(waySectionWithRT.strictIdentifier().wayId(), is(3L));
        assertThat(waySectionWithRT.sequenceNumber(), is((short) 1));
        assertThat(waySectionWithRT.tags().values(), hasSize(1));
        assertThat(waySectionWithRT.tags(), hasEntry(RoadCategoryTagKey.ROAD_TYPE.unwrap(),
                RoadType.identifierFor(RoadType.HIGHWAY) + ""));
    }

    @Test
    public void noRTTag() {
        Mockito.when(extractor.extract(waySection.tags())).thenReturn(Optional.empty());

        final RoadTypeRule withRT = new RoadTypeRule(extractor);
        final WaySection waySectionWithRT = withRT.apply(waySection);

        Mockito.verify(extractor).extract(waySection.tags());
        assertThat(waySectionWithRT, is(waySection));
    }

    @Test
    public void realCases() {
        /*
            in a real case scenarion, when running the job, these ids correspond to urban area admin levels
         */
        final Long wayId178081711 = 178081711L;
        final Long wayId24221335 = 24221335L;
        final Map<Long, String> wayIdToAdminLevel = new HashMap();
        wayIdToAdminLevel.put(wayId178081711, "7");
        wayIdToAdminLevel.put(wayId24221335, "11");

        final RoadTypeExtractor extractor = new RoadTypeExtractor();
        extractor.setBroadcastedWayIdToAdminLevel(wayIdToAdminLevel);

        final RoadTypeRule withRT = new RoadTypeRule(extractor);

        //https://www.openstreetmap.org/way/178081711
        final WaySection clineAve = new WaySection(wayId178081711,
                Arrays.asList(node1, node2),
                Collections.emptyMap())
                .withTag(HIGHWAY.unwrap(), "motorway")
                .withTag(CUSTOM_WAY_ID_TAG_KEY, String.valueOf(wayId178081711));
        final WaySection clineAveRT = withRT.apply(clineAve);
        assertEquals(clineAveRT.tags().get(RoadCategoryTagKey.ROAD_TYPE.unwrap()), "1");

        //https://www.openstreetmap.org/way/24221335
        final WaySection jArthurYoungerFreeway = new WaySection(wayId24221335,
                Arrays.asList(node1, node2),
                Collections.emptyMap())
                .withTag(HIGHWAY.unwrap(), "motorway")
                .withTag(CUSTOM_WAY_ID_TAG_KEY, String.valueOf(wayId24221335));
        final WaySection jArthurYoungerFreewayRt = withRT.apply(jArthurYoungerFreeway);
        assertEquals(jArthurYoungerFreewayRt.tags().get(RoadCategoryTagKey.ROAD_TYPE.unwrap()), "1");

        //https://www.openstreetmap.org/way/68325061
        final WaySection twelfthAve = new WaySection(68325061L,
                Arrays.asList(node1, node2),
                Collections.emptyMap())
                .withTag(HIGHWAY.unwrap(), "primary")
                .withTag(MAXSPEED.unwrap(), "30 mph");
        final WaySection twelfthAveRt = withRT.apply(twelfthAve);
        assertEquals(twelfthAveRt.tags().get(RoadCategoryTagKey.ROAD_TYPE.unwrap()), "4");
    }
}
