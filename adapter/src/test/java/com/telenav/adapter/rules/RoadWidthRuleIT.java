package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.RoadWidthExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * @author roxanal
 */
public class RoadWidthRuleIT extends BaseTest {

    private final RoadWidthExtractor extractor = new RoadWidthExtractor();

    @Test
    public void roadWidthFromOsm() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.WIDTH.unwrap(), "5.2");
        final WaySection adaptedWaySection = new RoadWidthRule(extractor).apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(3, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.WIDTH.unwrap(), "5.2"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.WIDTH.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.WIDTH.archivedKeyName(), "5.2"));
    }

    @Test
    public void roadWidthFromImages() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.WIDTH.withSource(TnSourceValue.IMAGES), "5.2");
        final WaySection adaptedWaySection = new RoadWidthRule(extractor).apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(2, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.WIDTH.unwrap(), "5.2"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.WIDTH.withSource(), TnSourceValue.IMAGES.unwrap()));
    }

    @Test
    public void roadWidthFromOsmAndImages() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.WIDTH.unwrap(), "5")
                .withTag(OsmTagKey.WIDTH.withSource(TnSourceValue.IMAGES), "5.2");
        final WaySection adaptedWaySection = new RoadWidthRule(extractor).apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(4, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.WIDTH.unwrap(), "5.2"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.WIDTH.withSource(), TnSourceValue.IMAGES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.WIDTH.archivedKeyName(), "5"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.WIDTH.withSource(TnSourceValue.OSM), "5"));
    }
}