package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.RoundaboutExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import static com.telenav.datapipelinecore.osm.OsmHighwayValue.MOTORWAY;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;


/**
 * @author roxanal
 */
public class RoundaboutRuleTest extends BaseTest {

    private RoundaboutRule rule;


    @Before
    public void initRule() {
        rule = new RoundaboutRule(new RoundaboutExtractor());
    }

    @Test
    public void testRoundaboutAdapter() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.JUNCTION.unwrap(), "roundabout");
        final WaySection adaptedRoundaboutSection = rule.apply(waySection);

        assertThat(adaptedRoundaboutSection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedRoundaboutSection.sequenceNumber(), is((short) 1));
        assertThat(adaptedRoundaboutSection.tags().values(), hasSize(2));
        assertThat(adaptedRoundaboutSection.tags(), hasEntry("junction", "roundabout"));
        assertThat(adaptedRoundaboutSection.tags(), hasEntry("oneway", "yes"));
    }

    @Test
    public void testForCompleteWaySection() {
        final Node node = node(1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "motorway_junction");
        final WaySection waySection = waysection(3, 1, node)
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap())
                .withTag(OsmTagKey.NAME.telenavPrefix(), "Park Dr")
                .withTag(OsmTagKey.ALT_NAME.telenavPrefix(), "Detroit-Toledo")
                .withTag(OsmTagKey.REF.telenavPrefix(), "MI-85 N")
                .withTag(TnTagKey.TN_FUNCTIONAL_CLASS_UPGRADE.unwrap(), "4")
                .withTag(OsmTagKey.JUNCTION.unwrap(), "roundabout")
                .withTag(TnTagKey.TN_ROAD_TYPE.unwrap(), "1")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50 mph")
                .withTag(OsmTagKey.LANES.unwrap(), "2")
                .withTag(OsmTagKey.JUNCTION.unwrap() + ":ref", "1A")
                .withTag(TnTagKey.TN_FUNCTIONAL_CLASS.unwrap(), "2");
        final WaySection adaptedRoundaboutSection = rule.apply(waySection);

        assertThat(adaptedRoundaboutSection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedRoundaboutSection.sequenceNumber(), is((short) 1));
        assertThat(adaptedRoundaboutSection.tags().values(), hasSize(12));
        assertThat(adaptedRoundaboutSection.tags(), hasEntry("junction", "roundabout"));
        assertThat(adaptedRoundaboutSection.tags(), hasEntry("oneway", "yes"));
        assertThat(adaptedRoundaboutSection.tags(), hasEntry("highway", "motorway"));
    }
}