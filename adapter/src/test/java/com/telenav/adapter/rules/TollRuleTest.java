package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.TollExtractor;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import static com.telenav.datapipelinecore.osm.OsmTagKey.TOLL;
import static org.junit.Assert.assertEquals;


/**
 * Unit tests for {@link TollRule}.
 *
 * @author catalinm
 */
public class TollRuleTest extends BaseTest {

    private TollRule<WaySection> tollRuleWaySection;
    private TollRule<RawWay> tollRuleWay;

    @Before
    public void initRule() {
        tollRuleWaySection = new TollRule<>(new TollExtractor());
        tollRuleWay = new TollRule<>(new TollExtractor());
    }

    @Test
    public void waySectionWithoutTollTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag("tag2", "yes");
        final WaySection adaptedWaySection = tollRuleWaySection.apply(waySection);
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void waySectionWithNoValueTollTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(TOLL.unwrap(), "no");
        final WaySection adaptedWaySection = tollRuleWaySection.apply(waySection);
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void waySectionWithYesValueTollTag() {
        final WaySection waySection = waysection(3, 1).withTag("tag1", "yes").withTag(TOLL.unwrap(), "yes");
        final WaySection adaptedWaySection = tollRuleWaySection.apply(waySection);
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void wayWithoutTollTag() {
        final RawWay way = rawWay(1).withTag("tag1", "yes").withTag("tag2", "yes");
        final RawWay adaptedWay = tollRuleWay.apply(way);
        assertEquals(way, adaptedWay);
    }

    @Test
    public void wayWithNoValueTollTag() {
        final RawWay way = rawWay(1).withTag("tag1", "yes").withTag(TOLL.unwrap(), "no");
        final RawWay adaptedWay = tollRuleWay.apply(way);
        assertEquals(way, adaptedWay);
    }

    @Test
    public void wayWithYesValueTollTag() {
        final RawWay way = rawWay(1).withTag("tag1", "yes").withTag(TOLL.unwrap(), "yes");
        final RawWay adaptedWay = tollRuleWay.apply(way);
        assertEquals(way, adaptedWay);
    }
}