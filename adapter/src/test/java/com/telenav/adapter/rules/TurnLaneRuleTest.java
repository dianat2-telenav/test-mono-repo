package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.bean.DrivingSide;
import com.telenav.adapter.component.extractors.OneWayExtractor;
import com.telenav.adapter.component.extractors.TurnLaneExtractor;
import com.telenav.adapter.exception.CorruptWaySectionException;
import com.telenav.adapter.utils.turnLanes.OsmToUniDbTurnLanesGeneralEnhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.junit.Test;

import java.util.*;

import static com.telenav.adapter.component.fc.TagService.*;
import static com.telenav.adapter.rules.LaneTypeRule.CORRUPTED_WAY_SECTION_KEY;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;


/**
 * @author adrianal
 */
public class TurnLaneRuleTest extends BaseTest {

    @Test
    public void splitTurnLaneCase1() {
        String turnLanes = "left|left|right";
        List<String> parts = new OsmToUniDbTurnLanesGeneralEnhancer().laneParts(turnLanes);
        assertThat(parts.size(), is(3));
        assertThat(parts.get(0), is("left"));
        assertThat(parts.get(1), is("left"));
        assertThat(parts.get(2), is("right"));
    }

    @Test
    public void splitTurnLaneCase2() {
        String turnLanes = "left|left|right|";
        List<String> parts = new OsmToUniDbTurnLanesGeneralEnhancer().laneParts(turnLanes);
        assertThat(parts.size(), is(4));
        assertThat(parts.get(0), is("left"));
        assertThat(parts.get(1), is("left"));
        assertThat(parts.get(2), is("right"));
        assertThat(parts.get(3), is(""));
    }

    @Test
    public void splitTurnLaneCase3() {
        String turnLanes = "left|left||";
        List<String> parts = new OsmToUniDbTurnLanesGeneralEnhancer().laneParts(turnLanes);
        assertThat(parts.size(), is(4));
        assertThat(parts.get(0), is("left"));
        assertThat(parts.get(1), is("left"));
        assertThat(parts.get(2), is(""));
        assertThat(parts.get(3), is(""));
    }

    @Test
    public void splitTurnLaneCase4() {
        String turnLanes = "|left|left|right";
        List<String> parts = new OsmToUniDbTurnLanesGeneralEnhancer().laneParts(turnLanes);
        assertThat(parts.size(), is(4));
        assertThat(parts.get(0), is(""));
        assertThat(parts.get(1), is("left"));
        assertThat(parts.get(2), is("left"));
        assertThat(parts.get(3), is("right"));
    }

    @Test
    public void splitTurnLaneCase5() {
        String turnLanes = "||left|left";
        List<String> parts = new OsmToUniDbTurnLanesGeneralEnhancer().laneParts(turnLanes);
        assertThat(parts.size(), is(4));
        assertThat(parts.get(0), is(""));
        assertThat(parts.get(1), is(""));
        assertThat(parts.get(2), is("left"));
        assertThat(parts.get(3), is("left"));
    }

    @Test
    public void convertTurnLanesMoreValuesPresent() {
        LinkedList<String> turnLanes = new LinkedList<>(Arrays.asList("left", "through", "none", "", "right"));
        List<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(5));
        assertThat(integers.get(0), is(64));
        assertThat(integers.get(1), is(1));
        assertThat(integers.get(2), is(0));
        assertThat(integers.get(3), is(0));
        assertThat(integers.get(4), is(4));
    }

    @Test
    public void convertNoneValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList("none"));
        List<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(1));
        assertThat(integers.get(0), is(0));
    }

    @Test
    public void convertEmptyValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList(""));
        List<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(1));
        assertThat(integers.get(0), is(0));
    }

    @Test
    public void convertThroughValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList("through"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(1));
        assertThat(integers.get(0), is(1));
    }

    @Test
    public void convertRightValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList("right"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(1));
        assertThat(integers.get(0), is(4));
    }

    @Test
    public void convertSlightRightValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList("slight_right"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(1));
        assertThat(integers.get(0), is(2));
    }


    @Test
    public void convertSharpRightValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList("sharp_right"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(1));
        assertThat(integers.get(0), is(8));
    }

    @Test
    public void convertLeftValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList("left"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(1));
        assertThat(integers.get(0), is(64));
    }

    @Test
    public void convertSlightLeftValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList("slight_left"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(1));
        assertThat(integers.get(0), is(128));
    }

    @Test
    public void convertSharpLeftValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList("sharp_left"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(1));
        assertThat(integers.get(0), is(32));
    }

    @Test
    public void convertMergeToLeftValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList("merge_to_left"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(1));
        assertThat(integers.get(0), is(512));
    }

    @Test
    public void convertMergeToRightValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList("merge_to_right"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(1));
        assertThat(integers.get(0), is(2048));
    }

    @Test(expected = CorruptWaySectionException.class)
    public void convertInvalidValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Collections.singletonList("invalid"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
    }

    @Test(expected = CorruptWaySectionException.class)
    public void convertValidWithInvalidValue() {
        LinkedList<String> turnLanes = new LinkedList<>(Arrays.asList("right", "invalid", "left"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
    }

    @Test
    public void convertCombinedRightTurnLanes() {
        LinkedList<String> turnLanes = new LinkedList<>(Arrays.asList("left", "right;through", "right"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(3));
        assertThat(integers.get(0), is(64));
        assertThat(integers.get(1), is(5));
        assertThat(integers.get(2), is(4));
    }

    @Test
    public void convertCombinedLeftTurnLanes() {
        LinkedList<String> turnLanes = new LinkedList<>(Arrays.asList("left", "through;left", "right"));
        LinkedList<Integer> integers = new OsmToUniDbTurnLanesGeneralEnhancer().convertTurnLanes(turnLanes, DrivingSide.RIGHT);
        assertThat(integers.size(), is(3));
        assertThat(integers.get(0), is(64));
        assertThat(integers.get(1), is(65));
        assertThat(integers.get(2), is(4));
    }

    @Test
    public void compactEmptyList() {
        LinkedList<Integer> turnLanes = new LinkedList<>(Collections.emptyList());
        String compacted = new OsmToUniDbTurnLanesGeneralEnhancer().compact(turnLanes);
        assertThat(compacted, is(""));
    }

    @Test
    public void compactTurnLaneList() {
        LinkedList<Integer> turnLanes = new LinkedList<>(Arrays.asList(64, 1, 1, 0, 4));
        String compacted = new OsmToUniDbTurnLanesGeneralEnhancer().compact(turnLanes);
        assertThat(compacted, is("4|0|1|1|64"));
    }

    @Test
    public void testConvertTurnLaneWhenOneway() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "left|through|through;right|right|")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.TURN_LANES.unwrap(), "0|4|5|1|64"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "yes"));
    }

    @Test
    public void testConvertTurnLaneWhenOnewayAndEmptyLanes() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "left|through||right||")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.TURN_LANES.unwrap(), "0|0|4|0|1|64"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "yes"));
    }

    /*@Test
    public void testConvertTurnLaneWhenNotOneway() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "left|through|through;right|right|")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "no");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(2));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.TURN_LANES.unwrap(),
                "left|through|through;right|right|"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "no"));
    }*/

    @Test
    public void testConvertTurnLaneWhenOnewayAndInvalidTurnLane() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "left|invalid|through;right|right|")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "-1");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        assertThat(adaptedWaySection.tags().containsKey(CORRUPTED_WAY_SECTION_KEY), is(true));
    }

    @Test
    public void testConvertTurnLaneWhenOnewayAndThreeTurnLaneTypes() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "through;left|left|through;slight_right;right|right")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "-1");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.TURN_LANES.unwrap(),
                "4|7|64|65"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "-1"));
    }

    /*
        test for 'reverse' lanes (real case scenarios)
    */
    @Test
    public void testPacificCoastHighway() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        final WaySection waySection = waysection(397545066, 1)
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "reverse|none|none|none|none|right")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(397545066L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.TURN_LANES.unwrap(),
                "4|0|0|0|0|16"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "yes"));
    }

    @Test
    public void testCountyHighway18() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        final WaySection waySection = waysection(402453607, 1)
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "reverse|||||slight_right;right")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(402453607L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.TURN_LANES.unwrap(),
                "6|0|0|0|0|16"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "yes"));
    }

    @Test
    public void testVistaSorrentoParkway() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        final WaySection waySection = waysection(436413991, 1)
                .withTag(OsmTagKey.TURN_LANES.unwrap(), "reverse|||right")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(436413991L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.TURN_LANES.unwrap(),
                "4|0|0|16"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "yes"));
    }

    @Test
    public void testLondonBridge() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        final WaySection waySection =
                new WaySection(436413991,
                        1,
                        Arrays.asList(new Node[]{new Node(1, Latitude.degrees(51.5072616), Longitude.degrees(-0.0878600), new HashMap<>())}),
                        null)
                        .withTag(OsmTagKey.TURN_LANES.unwrap(), "reverse|||reverse;right")
                        .withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(436413991L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertEquals(adaptedWaySection.tags().get(OsmTagKey.TURN_LANES.unwrap()), "20|0|0|16");
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "yes"));
    }

    @Test
    public void testWithReverseLanes1() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(38740640, 1)
                .withTag("highway", "secondary")
                .withTag("hgv", "designated")
                .withTag("oneway", "yes")
                .withTag("name", "Olympic Parkway")
                .withTag(TURN_LANES, "left;reverse||||right");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(38740640L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        //assertThat(tags.values(), hasSize(6));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "4|0|0|0|80"));
    }

    @Test
    public void testWithReverseLanes2() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(436250899, 1)
                .withTag("highway", "primary")
                .withTag(LANES, "5")
                .withTag("oneway", "yes")
                .withTag("name", "Rosecrans Street")
                .withTag(TURN_LANES, "left;reverse|left|||");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(436250899L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(6));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "0|0|0|64|80"));
    }

    @Test
    public void testWithReverseLanes3() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(291527959, 1)
                .withTag("highway", "trunk")
                .withTag(LANES, "5")
                .withTag("oneway", "yes")
                .withTag("name", "Antonio Parkway")
                .withTag(TURN_LANES, "left;reverse|left|none|none|none");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(291527959L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(6));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "0|0|0|64|80"));
    }

    @Test
    public void testTurnLanesOneWay() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(1, 1)
                .withTag("oneway", "yes")
                .withTag(TURN_LANES, "|||right");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(1L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(3));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "4|0|0|0"));
    }

    @Test
    public void testTurnLanesOneWay2() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(1, 1)
                .withTag("oneway", "yes")
                .withTag(TURN_LANES, "left|left;through|through|through");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(1L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(3));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "1|1|65|64"));
    }

    @Test
    public void testTurnLanesOneWay3() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(1, 1)
                .withTag("oneway", "yes")
                .withTag(TURN_LANES, "left|left|through;right|slight_right;right|right");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(1L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(3));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "4|6|5|64|64"));
    }

    @Test
    public void testTurnLanesOneWay4() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(1, 1)
                .withTag("oneway", "yes")
                .withTag(TURN_LANES, "|||right");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(1L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(3));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "4|0|0|0"));
    }

    @Test
    public void testTurnLanesOneWay5() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(1, 1)
                .withTag("oneway", "yes")
                .withTag(TURN_LANES, "left|through|through;slight_right;right");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(1L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(3));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "7|1|64"));
    }

    @Test
    public void testTurnLanesOneWay6() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(1, 1)
                .withTag("oneway", "yes")
                .withTag(TURN_LANES, "left|left|none|none");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(1L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(3));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "0|0|64|64"));
    }

    @Test
    public void testTurnLanesBidirectional() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(1, 1)
                .withTag("turn:lanes:forward", "left|left;through|through;right")
                .withTag(LANES_BACKWARD, "2");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(1L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(4));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "5|65|64|0|0"));
    }

    @Test
    public void testTurnLanesBidirectional2() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(1, 1)
                .withTag("turn:lanes:backward", "left|left||")
                .withTag(LANES_FORWARD, "2");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(1L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(4));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "0|0|64|64|0|0"));
    }

    @Test
    public void testTurnLanesBidirectional3() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(1, 1)
                .withTag("turn:lanes:forward", "left||")
                .withTag("turn:lanes:backward", "||right");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(1L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(5));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "0|0|64|0|0|4"));
    }

    @Test
    public void testTurnLanesWithBothWays() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(1, 1)
                .withTag("turn:lanes:forward", "||right")
                .withTag(LANES_BACKWARD, "2")
                .withTag("lanes:both_ways", "1")
                .withTag("turn:lanes:both_ways", "left");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(1L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(tags.values(), hasSize(7));
        assertThat(tags, hasEntry(OsmTagKey.TURN_LANES.unwrap(), "4|0|0|64|0|0"));
    }

    @Test
    public void testTurnLanesWithCorruptedWaySection() {
        final TurnLaneRule<WaySection> turnLaneRule = new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), null);

        WaySection waySection = waysection(398023701, 1)
                .withTag(LANES, "5")
                .withTag("lanes:both_ways", "\tl")
                .withTag(LANES_BACKWARD, "2")
                .withTag(LANES_FORWARD, "3");
        final WaySection adaptedWaySection = turnLaneRule.apply(waySection);
        final Map<String, String> tags = adaptedWaySection.tags();
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(398023701L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertTrue(tags.containsKey(CORRUPTED_WAY_SECTION_KEY));
    }
}