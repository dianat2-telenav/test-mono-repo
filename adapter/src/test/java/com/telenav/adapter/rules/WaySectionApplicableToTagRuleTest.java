package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.AccessExtractor;
import com.telenav.adapter.component.extractors.MotorCarExtractor;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.stream.Collectors;

import static com.telenav.datapipelinecore.osm.OsmTagKey.ACCESS;
import static com.telenav.datapipelinecore.osm.OsmTagKey.EXCEPT;
import static com.telenav.datapipelinecore.unidb.ApplicableToTagValue.*;
import static com.telenav.datapipelinecore.unidb.UnidbTagKey.APPLICABLE_TO;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class WaySectionApplicableToTagRuleTest extends BaseTest {
    WaySectionApplicableToTagRule applicableToTagRule;

    @Before
    public void init() {
        applicableToTagRule = new WaySectionApplicableToTagRule(new AccessExtractor(), new MotorCarExtractor());
    }

    @Test
    @Parameters({"tourist_bus, access_through_traffic;delivery;emergency;foot;hov;motorcar;motorcycle;taxi;truck",
                 "coach, access_through_traffic;delivery;emergency;foot;hov;motorcar;motorcycle;taxi;truck"})
    public void vehicleAdditionTagNoOnWay(final String tagVehicle, final String expected) {
        final WaySection waySection = waysection(1,  1)
                .withTag(tagVehicle, "no");
        final WaySection enhancedWay = applicableToTagRule.apply(waySection);
        assertTrue(enhancedWay.hasKey(APPLICABLE_TO.unwrap()));
        assertEquals(expected, enhancedWay.tags().get(APPLICABLE_TO.unwrap()));
    }

    @Test
    @Parameters({"tourist_bus, bus", "coach, bus", "foot, foot"})
    public void vehicleAdditionTagYesOnRawWay(final String tagVehicle, final String expected) {
        final WaySection waySection = waysection(1,  1)
                .withTag(tagVehicle, "yes");
        WaySection enhancedWay = applicableToTagRule.apply(waySection);
        assertTrue(enhancedWay.hasKey(APPLICABLE_TO.unwrap()));
        assertEquals(expected, enhancedWay.tags().get(APPLICABLE_TO.unwrap()));
    }

    @Test
    @Parameters({"hgv, truck",
                 "motorcar, motorcar",
                 "bus, bus",
                 "motorcycle, motorcycle",
                 "motor_vehicle,  access_through_traffic;bus;delivery;emergency;hov;motorcar;motorcycle;taxi;truck"})
    public void restrictionTagOnWay(final String tagVehicle, final String expected) {
        final WaySection waySection = waysection(1,  1)
                .withTag("restriction:" + tagVehicle, "no_right_turn");
        final WaySection enhancedWay = applicableToTagRule.apply(waySection);
        assertTrue(enhancedWay.hasKey(APPLICABLE_TO.unwrap()));
        assertEquals(expected, enhancedWay.tags().get(APPLICABLE_TO.unwrap()));
    }

    @Test
    @Parameters({"psv, access_through_traffic;delivery;emergency;foot;hov;motorcar;motorcycle;truck",
                 "hgv, access_through_traffic;bus;delivery;emergency;foot;hov;motorcar;motorcycle;taxi",
                 "motorcar, access_through_traffic;bus;delivery;emergency;foot;hov;motorcycle;taxi;truck",
                 "emergency, access_through_traffic;bus;delivery;foot;hov;motorcar;motorcycle;taxi;truck",})
    public void exceptTagOnWay(String tagVehicle, String expected) {
        final WaySection waySection = waysection(1,  1)
                .withTag(EXCEPT.unwrap() , tagVehicle);
        final WaySection enhancedWay = applicableToTagRule.apply(waySection);
        assertTrue(enhancedWay.hasKey(APPLICABLE_TO.unwrap()));
        assertEquals(expected, enhancedWay.tags().get(APPLICABLE_TO.unwrap()));
    }

    @Test
    @Parameters({"yes", "designated", "permissive"})
    public void accessYesWayExist(final String yes) {
        final WaySection waySection = waysection(1,  1)
                .withTag(ACCESS.unwrap(), yes);
        final WaySection enhancedWay = applicableToTagRule.apply(waySection);
        assertTrue(enhancedWay.hasKey(APPLICABLE_TO.unwrap()));
        assertEquals(ACCESS_THROUGH_TRAFFIC.unwrap(), enhancedWay.tags().get(APPLICABLE_TO.unwrap()));
    }

    @Test
    @Parameters({"motorcar", "bus", "taxi", "hov", "foot", "truck", "delivery", "emergency", "motorcycle"})
    public void accessYesVehicleNoValue(final String vehicle) {
        final WaySection waySection = waysection(1,  1)
                .withTag(ACCESS.unwrap(), "yes")
                .withTag(vehicle, "no");
        final WaySection enhancedWay = applicableToTagRule.apply(waySection);
        assertTrue(enhancedWay.hasKey(APPLICABLE_TO.unwrap()));
        assertThat(enhancedWay.tags().get(APPLICABLE_TO.unwrap()),
                not(isIn(applicableToValues
                        .stream()
                        .filter(v -> !v.equals(vehicle))
                        .collect(Collectors.toList()))));
    }

    @Test
    public void accessYesVehiclesNoValue() {
        final WaySection waySection = waysection(1,  1)
                .withTag(ACCESS.unwrap(), "yes")
                .withTag("motorcar", "no")
                .withTag("bus", "no")
                .withTag("hgv", "no");
        final WaySection enhancedWay = applicableToTagRule.apply(waySection);
        assertTrue(enhancedWay.hasKey(APPLICABLE_TO.unwrap()));
        assertEquals("access_through_traffic;delivery;emergency;foot;hov;motorcycle;taxi",
                enhancedWay.tags().get(APPLICABLE_TO.unwrap()));
    }

    @Test
    @Parameters({"bus", "taxi", "hov", "foot", "truck", "delivery", "emergency", "motorcycle"})
    public void accessNoVehicleYesValue(final String vehicle) {
        final WaySection waySection = waysection(1,  1)
                .withTag(ACCESS.unwrap(), "no")
                .withTag(vehicle, "yes");
        final WaySection enhancedWay = applicableToTagRule.apply(waySection);
        assertTrue(enhancedWay.hasKey(APPLICABLE_TO.unwrap()));
        assertEquals(vehicle, enhancedWay.tags().get(APPLICABLE_TO.unwrap()));
    }

    @Test
    public void accessNoExist() {
        final WaySection waySection = waysection(1,  1)
                .withTag(ACCESS.unwrap(), "no");
        final WaySection enhancedWay = applicableToTagRule.apply(waySection);
        assertTrue(enhancedWay.hasKey(APPLICABLE_TO.unwrap()));
        assertEquals(String.join(";", applicableToValues), enhancedWay.tags().get(APPLICABLE_TO.unwrap()));
    }

    @Test
    @Parameters({"motorcar", "bus", "taxi", "hov", "foot", "truck", "delivery", "emergency", "motorcycle"})
    public void vehicleNoValue(final String vehicle) {
        final WaySection waySection = waysection(1,  1)
                .withTag(vehicle, "no");
        final WaySection enhancedWay = applicableToTagRule.apply(waySection);
        assertTrue(enhancedWay.hasKey(APPLICABLE_TO.unwrap()));
        assertThat(enhancedWay.tags().get(APPLICABLE_TO.unwrap()),
                not(isIn(applicableToValues
                        .stream()
                        .filter(v -> !v.equals(vehicle))
                        .collect(Collectors.toList()))));
    }

    @Test
    @Parameters({"bus", "taxi", "hov", "foot", "truck", "delivery", "emergency", "motorcycle"})
    public void vehicleYesMotorcarNo(final String vehicle) {
        final WaySection waySection = waysection(1,  1)
                .withTag(vehicle, "yes")
                .withTag("motorcar", "no");
        final WaySection enhancedWay = applicableToTagRule.apply(waySection);
        assertTrue(enhancedWay.hasKey(APPLICABLE_TO.unwrap()));
        assertEquals(applicableToValues
                .stream()
                .filter(v -> !v.equals(vehicle))
                .filter(v -> !v.equals(MOTORCAR.unwrap()))
                .collect(Collectors.joining(";")), enhancedWay.tags().get(APPLICABLE_TO.unwrap()));
    }
}