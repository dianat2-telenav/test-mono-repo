package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.SurfaceExtractor;
import com.telenav.adapter.component.mapper.OpenStreetMapToUniDbSurfaceMapper;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.TagAdjuster;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class WaySurfaceRuleTest extends BaseTest {
    WaysSurfaceRule waysSurfaceRule;

    @Before
    public void init() {
        waysSurfaceRule = new WaysSurfaceRule<>(new SurfaceExtractor(), new OpenStreetMapToUniDbSurfaceMapper());
    }

    @Test
    @Parameters({"unpaved","sand", "grass", "earth", "pebblestone", "ground", "gravel", "compacted", "mud",
    "dirt", "rock"})
    public void unpavedRoad(String tagValue) {
        final RawWay rawWay = rawWay(1)
                .withTag(OsmTagKey.SURFACE.unwrap(), tagValue);
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.SURFACE.unwrap(), tagValue);
        final RawWay enhancedRawWay = (RawWay) waysSurfaceRule.apply(rawWay);
        final WaySection enhancedWaySection = (WaySection) waysSurfaceRule.apply(waySection);
        assertTrue("Surface tag not present for unpaved surfaces.",
                enhancedRawWay.hasKey(UnidbTagKey.SURAFCE.unwrap()));
        assertTrue("Surface tag not present for unpaved surfaces.",
                enhancedWaySection.hasKey(UnidbTagKey.SURAFCE.unwrap()));
        assertEquals("Surface value not unified to 'unpaved' for unpaved surfaces.",
                UnidbTagValue.UNPAVED.unwrap(), enhancedRawWay.tags().get(UnidbTagKey.SURAFCE.unwrap()));
        assertEquals("Surface value not unified to 'unpaved' for unpaved surfaces.",
                UnidbTagValue.UNPAVED.unwrap(), enhancedWaySection.tags().get(UnidbTagKey.SURAFCE.unwrap()));
    }

    @Test
    @Parameters({"random1", "random2", "salt", "trees", "acrylic", "???", "paved"}) /* Only several of them since all non-whitelisted will be removed */
    public void randomValues(String tagValue) {
        final RawWay way = rawWay(1)
                .withTag(OsmTagKey.SURFACE.unwrap(), tagValue);
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.SURFACE.unwrap(), tagValue);
        final RawWay enhancedRawWay = (RawWay) waysSurfaceRule.apply(way);
        final WaySection enhancedWaySection = (WaySection) waysSurfaceRule.apply(waySection);
        assertFalse(String.format("Non-normalized '%s' surface values not removed.", tagValue),
                enhancedRawWay.hasKey(UnidbTagKey.SURAFCE.unwrap()));
        assertFalse(String.format("Non-normalized '%s' surface values not removed.", tagValue),
                enhancedWaySection.hasKey(UnidbTagKey.SURAFCE.unwrap()));
    }
}
