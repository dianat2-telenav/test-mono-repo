package com.telenav.adapter.utils;

import com.telenav.adapter.component.fc.FcInfo;
import com.telenav.adapter.component.fc.UsaTigerFccApproach;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * @author liviuc
 */
@RunWith(JUnitParamsRunner.class)
public class UsaTigerFccApproachTest {

    @Test
    @Parameters({"A00." + FcInfo.FC_FIFTH,
            "A11." + FcInfo.FC_THIRD,
            "A31." + FcInfo.FC_THIRD,
            "A41." + FcInfo.FC_FOURTH,
            "A70." + FcInfo.FC_FIFTH,
            "A31:A41." + FcInfo.FC_FOURTH,
            "A31:A41;A41." + FcInfo.FC_FOURTH,
    })
    public void test(final String parameter) {

        final UsaTigerFccApproach target = null;

        final String[] parameters = parameter.split("\\.");

        final String tigerCfcc = parameters[0];

        final String expectedFc = parameters[1];
        final String actualFc = target.tigerCfcc(tigerCfcc);
        assertEquals(expectedFc, actualFc);
    }
}
