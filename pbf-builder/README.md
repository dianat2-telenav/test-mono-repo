# PBF Builder

The purpose of this project is to convert map parquet files to pbf.
There are two jobs needed for this the PbfWriter and the Merger.
Two other jobs can be found here that are not related to the pbf-writing process, they should be moved in the future to
an approiate project; they are WaySectionerJob and WaysWithoutSectionsJob

## PbfWriterJob

This job takes as input the sectioned map, as well as the pristine parquet files. Sectioned map only contains roads, since the final pbf needs to contain also the other ways, like buildings, water, relations etc, we also need the pristine parquets.
As output the job will write a pbf for each geohash of size 4 in the region.
Each pbf in the geohash is not a complete pbf. The first node of each way decides in what geohash the way will be, so the way cannot be in 2 geohashes, it will be only in one geohash, and it will be missing completely from the other.

### Running job

```
spark-submit \
    --master yarn-cluster \
    --driver-memory 23G \
    --executor-memory 23G \
    --executor-cores 4 \
    --conf spark.default.parallelism=400 \
    --conf spark.kryoserializer.buffer.max=1800m \
    --conf spark.storage.memoryFraction=0.1 \
    --conf spark.driver.maxResultSize=0 \
    --conf spark.dynamicAllocation.enabled=true \
    --conf spark.shuffle.service.enabled=true \
    --conf spark.shuffle.io.retryWait=10s \
    --conf spark.yarn.executor.memoryOverhead=3000 \
    --conf spark.yarn.driver.memoryOverhead=3000 \
    --queue production \
    --class com.telenav.pbfwriter.PbfWriterJob \
    pbfwriter-1.0-SNAPSHOT-shaded.jar \
    output_file_dir=${OUTPUT_FILE_DIR} \
    node_parquet=${NODES_PARQUET}
    relation_parquet=${RELATIONS_PARQUET}
    run_local=true/false
    geohash_size=4 (2,3,4,5...)
```
In additional to the above parameters pbf writer can write pbf from 2 sources:

1. from raw ways only

    Params to specify:
```
    raw_ways_parquet
```
2. from ways no sections (also raw ways) and way sections

    Params to specify:
```
    way_sections_parquet
    ways_no_sections_parquet
```

## MergerJob

The second job is meant to mearge all these smal pbf's into a single one. It takes as input the small pbf's created by the writer job, and outputs one file, a merged pbf file.
Notice that this job is missing the --master setting. This is because we cannot run this job distrubted since our output is only one file.
So the job has master set to local in code, so you do not have to wory about that. If you use --master yarn-cluster, the job will not work.


### Running job

```
spark-submit \
    --driver-memory 10G \
    --executor-memory 10G \
    --executor-cores 4 \
    --conf spark.default.parallelism=400 \
    --queue default \
    --class com.telenav.merge.MergerJob \
    pbfwriter-1.0-SNAPSHOT-shaded.jar  \
    input_pbf_folder=${INPUT_PBF_FOLDER} \
    output_pbf=${OUTPUT_PBF}
    read_write_hdfs=true/false
```

## SectionerJob

This job is responsible for sectioning the ways. It takes as input the raw ways and nodes, and using map-interface API
it sections the ways based on a certain filter which is constructed using some wel defined rules.
The output of this job will be the input to many of the pipeline components

### Running Job

```
function way_sectioner() {
   spark-submit-2.3.0 \
    --master yarn \
    --deploy-mode cluster \
    --driver-memory 10G \
    --executor-memory 20G \
    --executor-cores 4 \
    --conf spark.default.parallelism=400 \
    --queue ${QUEUE} \
    --class com.telenav.sectioner.WaySectionerJob \
    ${PBF_BUILDER_JAR}  \
    output_dir=${SECTIONER_OUTPUT} \
    path_template=nameofthefile.osm.pbf.*.parquet
}
```

## WaysWithoutSectionsJob

The input for this job is the raw ways. This job will apply the filter used in sectioning but this time it will negate the filter,
thus the output will be only the ways that were not sectioned.

### Running the job

```
function ways_no_sections() {
   spark-submit-2.3.0 \
    --master yarn \
    --deploy-mode cluster \
    --driver-memory 5G \
    --executor-memory 10G \
    --executor-cores 4 \
    --conf spark.default.parallelism=400 \
    --queue ${QUEUE} \
    --class com.telenav.sectioner.WaysWithoutSectionsJob \
    ${PBF_BUILDER_JAR}  \
    output_dir=${WAYS_NO_SECTIONS_OUTPUT} \
    way_parquet=${WAY_PARQUET}
}
```

## PbfBuilderJob
This job sorts nodes and ways parquets by geohashes and then by id. This is an input for the compact serialization

### Running Job

	spark-submit-2.3.0 \
    --master yarn \
    --deploy-mode cluster \
    --driver-memory 5G \
    --executor-memory 10G \
    --class com.telenav.pbfbuilder.PbfBuilderJob \
    data-pipeline-pbf-builder-1.3.5-SNAPSHOT-shaded.jar \
    nodes_parquet=${nodes_parquet} \
    ways_parquet=${ways_parquet} \
    sorted_nodes_parquet=${sorted_nodes_parquet} \
    sorted_ways_parquet=${sorted_ways_parquet}
    geohash_size=4 (2,3,4,5...)

## PbfSerializerJob
This job serializers final nodes, ways and relations parquets (prepared by BuilderJob) into osm.pbf file by OsmosisSerializer 
using the specific batch limit (default batch_limit = 50000)

### Running Job

	spark-submit-2.3.0 \
    --master yarn \
    --deploy-mode cluster \
    --class com.telenav.pbfbuilder.PbfSerializerJob \
    data-pipeline-pbf-serializer-1.3.5-SNAPSHOT-shaded.jar \
    nodes_parquet=${nodes_parquet} \
    ways_parquet=${nodes_parquet} \
    relations_parquet=${nodes_parquet} \
    output_dir=${nodes_parquet} \
    batch_limit=${batch_limit} \
    output_file=${output_file}


