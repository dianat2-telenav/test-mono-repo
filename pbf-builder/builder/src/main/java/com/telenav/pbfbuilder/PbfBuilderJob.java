package com.telenav.pbfbuilder;

import ch.hsr.geohash.GeoHash;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.ost.ArgumentContainer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.expressions.UserDefinedFunction;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.*;

public class PbfBuilderJob {

    public static void main(String[] args) {

        final ArgumentContainer arguments = new ArgumentContainer(args);
        final String nodesParquet = arguments.getRequired("nodes_parquet");
        final String waysParquet = arguments.getRequired("ways_parquet");
        final String runLocal = arguments.getOptional("run_local", "false");
        final String sortedNodes = arguments.getRequired("sorted_nodes_parquet");
        final String sortedWays = arguments.getRequired("sorted_ways_parquet");
        int geohashSize = Integer.parseInt(arguments.getOptional("geohash_size", "4"));

        try(final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Pbf Build")
                .runLocal(Boolean.parseBoolean(runLocal))
                .build()) {

            UserDefinedFunction geoHash = udf(
                    (Double latitude, Double longitude, Integer hashSize) -> {
                        GeoHash hash = GeoHash.withCharacterPrecision(latitude, longitude, hashSize);
                        return hash.toBase32();
                    }, DataTypes.StringType
            );

            Dataset<Row> nodeDataset = jobInitializer.sparkSession().read().parquet(nodesParquet);
            Dataset<Row> wayDataset = jobInitializer.sparkSession().read().parquet(waysParquet);

            Dataset<Row> sortedNodeDataset =
                    nodeDataset
                            .withColumn("geohash", geoHash.apply(col("latitude"), col("longitude"), lit(geohashSize)))
                            .sort("geoHash", "id");
            sortedNodeDataset.write().mode(SaveMode.Overwrite).parquet(sortedNodes);

            Dataset<Row> sortedWayDataset =
                    wayDataset
                            .join(sortedNodeDataset,
                                    wayDataset.col("nodes").getItem(0).getItem("nodeId")
                                            .equalTo(sortedNodeDataset.col("id")))
                            .select(wayDataset.col("id"),
                                    wayDataset.col("tags"),
                                    wayDataset.col("nodes"),
                                    sortedNodeDataset.col("geohash"))
                            .sort("geohash", "id");

            sortedWayDataset.write().mode(SaveMode.Overwrite).parquet(sortedWays);
        }
    }
}