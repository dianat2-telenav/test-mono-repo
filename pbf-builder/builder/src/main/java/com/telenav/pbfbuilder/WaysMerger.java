package com.telenav.pbfbuilder;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.StandardEntity;
import com.telenav.map.entity.WaySection;
import com.telenav.map.storage.RawWaySchema;
import com.telenav.map.storage.read.RawRowToRawWayConverter;
import com.telenav.map.storage.read.RowToWaySectionConverter;
import com.telenav.map.storage.write.RawWayToRowConverter;
import com.telenav.ost.ArgumentContainer;
import com.telenav.ost.spark.storage.read.ParquetReader;
import com.telenav.ost.spark.storage.write.ParquetWriter;
import org.apache.spark.SparkException;
import org.apache.spark.api.java.JavaRDD;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author adrianal
 */
class WaysMerger {

    public static void main(final String[] args) throws SparkException, IOException {
        final ArgumentContainer arguments = new ArgumentContainer(args);
        final String waysNoSectionsPath = arguments.getRequired("ways_no_sections_parquet");
        final String waySectionsPath = arguments.getRequired("way_sections_parquet");
        final String outputPath = arguments.getRequired("output_dir");
        final String runLocal = arguments.getOptional("run_local", "false");

        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Merge Ways")
                .runLocal(Boolean.valueOf(runLocal))
                .build();

        final JavaRDD<WaySection> waySections = new ParquetReader<>(jobInitializer.javaSparkContext(),
                new RowToWaySectionConverter(), waySectionsPath).read();
        final JavaRDD<RawWay> waysNoSections = new ParquetReader<>(jobInitializer.javaSparkContext(),
                new RawRowToRawWayConverter(), waysNoSectionsPath).read();

        final JavaRDD<RawWay> waySectionToRawWay = waySections.map(waySection -> {
            final List<Long> nodeIds =
                    waySection.nodes().stream().map(StandardEntity::id).collect(Collectors.toList());
            return new RawWay(waySection.id(), nodeIds, waySection.tags());
        });
        final JavaRDD<RawWay> union = waysNoSections.union(waySectionToRawWay);

        new ParquetWriter.Builder<RawWay>()
                .sparkContext(jobInitializer.javaSparkContext())
                .converter(new RawWayToRowConverter())
                .schema(new RawWaySchema())
                .path(outputPath)
                .saveMode("Overwrite")
                .build()
                .write(union);
        jobInitializer.sparkSession().close();
    }
}