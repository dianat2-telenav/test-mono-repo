package com.telenav.pbfbuilder.common;

import crosby.binary.osmosis.OsmosisSerializer;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.openstreetmap.osmosis.osmbinary.file.BlockOutputStream;


/**
 * Class that handles initialization of attributes needed when writing, most important it initializes the osmosis
 * serializer (which actually writes the pbf file) to enable running locally but also in HDFS.
 *
 * @author odrab
 */
public class OsmWriter {

    protected final OsmosisSerializer serializer;
    private final String fileName;
    private final boolean readWriteHdfs;
    private final String outputPbfDir;
    private Configuration configuration;


    public OsmWriter(final String outputPbfDir, final String fileName, final boolean readWriteHdfs,
                        final Configuration configuration) throws IOException {
        if (outputPbfDir == null || fileName == null || configuration == null) {
            throw new IllegalArgumentException("All attributes must be set in order to construct a valid writer");
        }

        this.fileName = fileName;
        this.readWriteHdfs = readWriteHdfs;
        this.outputPbfDir = outputPbfDir;
        this.configuration = configuration;
        final String fullPathToFile = outputPbfDir + "/" + fileName;
        if (!readWriteHdfs) {
            this.serializer = initForLocalIo(fullPathToFile);
        } else {
            this.serializer = initForHdfsIo(fullPathToFile);
        }
    }

    private OsmosisSerializer initForHdfsIo(final String fullPathToFile) throws IOException {
        final OsmosisSerializer osmosisSerializer;
        final Path path = new Path(fullPathToFile);
        final FileSystem fs = FileSystem.get(configuration);
        final FSDataOutputStream dataOutputStream;
        if (!fs.exists(path)) {
            dataOutputStream = fs.create(path);
        } else {
            dataOutputStream = fs.append(path);
        }
        osmosisSerializer = new OsmosisSerializer(new BlockOutputStream(new BufferedOutputStream(dataOutputStream)));
        return osmosisSerializer;
    }

    private OsmosisSerializer initForLocalIo(final String fullPathToFile) throws IOException {
        final OsmosisSerializer osmosisSerializer;
        final java.nio.file.Path localFileSystemPath = Paths.get(fullPathToFile);
        if (!localFileSystemPath.getParent().toFile().exists()) {
            localFileSystemPath.getParent().toFile().mkdirs();
        }
        if (!localFileSystemPath.toFile().exists()) {
            final boolean status = localFileSystemPath.toFile().createNewFile();
            if (!status) {
                throw new IOException("Failed to create file " + localFileSystemPath.getFileName());
            }
        }
        osmosisSerializer = new OsmosisSerializer(
                new BlockOutputStream(Files.newOutputStream(localFileSystemPath, StandardOpenOption.APPEND)));
        return osmosisSerializer;
    }

    public String getFileName() {
        return fileName;
    }

    public boolean isReadWriteHdfs() {
        return readWriteHdfs;
    }

    public String getOutputPbfDir() {
        return outputPbfDir;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public OsmosisSerializer getSerializer() {
        return serializer;
    }
}