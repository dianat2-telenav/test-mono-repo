package com.telenav.pbfbuilder.common;

import com.telenav.map.entity.WaySection;

import java.util.List;


/**
 * Interface that describes the behavior that should be implemented by any writer that needs to write a pbf file only
 * with way sections.
 *
 * @author adrianal
 */
public interface OsmosisWaySectionWriter {
    void writeWaySections(List<WaySection> waySections);
}
