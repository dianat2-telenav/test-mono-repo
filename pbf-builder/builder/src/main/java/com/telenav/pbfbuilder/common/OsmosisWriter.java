package com.telenav.pbfbuilder.common;

import java.util.List;


/**
 * Interface that describes the behavior that should be implemented by any writer that needs to write a pbf file
 * composed of ways, nodes and relations.
 *
 * @author adrianal
 */
public interface OsmosisWriter<T, U, V> {
    void complete();

    void close();

    void writeNodes(final List<T> nodes);

    void writeWays(final List<U> ways);

    void writeRelations(final List<V> relations);
}