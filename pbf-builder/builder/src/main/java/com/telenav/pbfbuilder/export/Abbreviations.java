package com.telenav.pbfbuilder.export;

import java.util.HashMap;
import java.util.Map;

public class Abbreviations {

    private final static Map<String, String> abbreviations;

    static {
        abbreviations = new HashMap<>();
        // CAN
        abbreviations.put("Alberta", "AB");
        abbreviations.put("British Columbia", "BC");
        abbreviations.put("Manitoba", "MB");
        abbreviations.put("New Brunswick", "NB");
        abbreviations.put("Newfoundland and Labrador", "NF");
        abbreviations.put("Northwest Territories", "NT");
        abbreviations.put("Nova Scotia", "NS");
        abbreviations.put("Nunavut", "NU");
        abbreviations.put("Ontario", "ON");
        abbreviations.put("Prince Edward Island", "PE");
        abbreviations.put("Québec", "QC");
        abbreviations.put("Saskatchewan", "SK");
        abbreviations.put("Yukon", "YT");
        // MEX
        abbreviations.put("Aguascalientes", "AG");
        abbreviations.put("Baja California", "BC");
        abbreviations.put("Baja California Sur", "BCS");
        abbreviations.put("Chihuahua", "CH");
        abbreviations.put("Colima", "CL");
        abbreviations.put("Campeche", "CM");
        abbreviations.put("Coahuila", "CO");
        abbreviations.put("Chiapas", "CS");
        abbreviations.put("Distrito Federal", "DF");
        abbreviations.put("Durango", "DG");
        abbreviations.put("Guerrero", "GR");
        abbreviations.put("Guanajuato", "GT");
        abbreviations.put("Hidalgo", "HG");
        abbreviations.put("Jalisco", "JA");
        abbreviations.put("Michoacán", "MI");
        abbreviations.put("Morelos", "MO");
        abbreviations.put("Nayarit", "NA");
        abbreviations.put("Nuevo León", "NL");
        abbreviations.put("Oaxaca", "OA");
        abbreviations.put("Puebla", "PU");
        abbreviations.put("Quintana Roo", "QR");
        abbreviations.put("Querétaro", "QT");
        abbreviations.put("Sinaloa", "SI");
        abbreviations.put("San Luis Potosí", "SL");
        abbreviations.put("Sonora", "SO");
        abbreviations.put("Tabasco", "TB");
        abbreviations.put("Tlaxcala", "TL");
        abbreviations.put("Tamaulipas", "TM");
        abbreviations.put("Veracruz", "VE");
        abbreviations.put("Yucatán", "YU");
        abbreviations.put("Zacatecas", "ZA");
        abbreviations.put("México", "EM");
        // USA
        abbreviations.put("Alabama", "AL");
        abbreviations.put("Alaska", "AK");
        abbreviations.put("Arizona", "AZ");
        abbreviations.put("Arkansas", "AR");
        abbreviations.put("California", "CA");
        abbreviations.put("Colorado", "CO");
        abbreviations.put("Connecticut", "CT");
        abbreviations.put("Delaware", "DE");
        abbreviations.put("District of Columbia", "DC");
        abbreviations.put("Florida", "FL");
        abbreviations.put("Georgia", "GA");
        abbreviations.put("Hawaii", "HI");
        abbreviations.put("Idaho", "ID");
        abbreviations.put("Illinois", "IL");
        abbreviations.put("Indiana", "IN");
        abbreviations.put("Iowa", "IA");
        abbreviations.put("Kansas", "KS");
        abbreviations.put("Kentucky", "KY");
        abbreviations.put("Louisiana", "LA");
        abbreviations.put("Maine", "ME");
        abbreviations.put("Maryland", "MD");
        abbreviations.put("Massachusetts", "MA");
        abbreviations.put("Michigan", "MI");
        abbreviations.put("Minnesota", "MN");
        abbreviations.put("Mississippi", "MS");
        abbreviations.put("Missouri", "MO");
        abbreviations.put("Montana", "MT");
        abbreviations.put("Nebraska", "NE");
        abbreviations.put("Nevada", "NV");
        abbreviations.put("New Hampshire", "NH");
        abbreviations.put("New Jersey", "NJ");
        abbreviations.put("New Mexico", "NM");
        abbreviations.put("New York", "NY");
        abbreviations.put("North Carolina", "NC");
        abbreviations.put("North Dakota", "ND");
        abbreviations.put("Ohio", "OH");
        abbreviations.put("Oklahoma", "OK");
        abbreviations.put("Oregon", "OR");
        abbreviations.put("Pennsylvania", "PA");
        abbreviations.put("Rhode Island", "RI");
        abbreviations.put("South Carolina", "SC");
        abbreviations.put("South Dakota", "SD");
        abbreviations.put("Tennessee", "TN");
        abbreviations.put("Texas", "TX");
        abbreviations.put("Utah", "UT");
        abbreviations.put("Vermont", "VT");
        abbreviations.put("Virginia", "VA");
        abbreviations.put("Washington", "WA");
        abbreviations.put("West Virginia", "WV");
        abbreviations.put("Wisconsin", "WI");
        abbreviations.put("Wyoming", "WY");
    }

    public static String valueFor(String key) {
        return abbreviations.get(key);
    }

}
