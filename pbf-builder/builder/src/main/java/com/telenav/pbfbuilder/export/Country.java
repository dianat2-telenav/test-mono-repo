package com.telenav.pbfbuilder.export;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
@Data
public class Country implements Serializable {

    private final long adminPlaceId;
    private final String ISO;
    private final String names;
    private final String altNames;
    private final List<String> states;

    public Country(long adminPlaceId, String ISO, String names, String altNames) {
        this.adminPlaceId = adminPlaceId;
        this.ISO = ISO;
        this.names = names;
        this.altNames = altNames;
        this.states = new ArrayList<>();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("\t\t<country admin_place_id=\"").append(adminPlaceId).append("\"")
                .append(" iso=\"").append(ISO).append("\"");
        if (states.size() != 0 &&
                // Puerto Rico and Virgin Islands are too small, no states should be displayed
                !"VIR".equals(ISO) && !"PRI".equals(ISO)) {
            stringBuilder
                    .append(" state_cnt=\"").append(states.size()).append("\">");
        } else {
            stringBuilder.append(">");
        }
        if (!names.isEmpty()) {
            stringBuilder
                    .append("\n\t\t\t<names>").append(names).append("</names>");
        }
        if (!altNames.isEmpty()) {
            stringBuilder
                    .append("\n\t\t\t<alt_names>").append(altNames).append("</alt_names>");
        }
        if (states.size() != 0 &&
                // Puerto Rico and Virgin Islands are too small, no states should be displayed
                !"VIR".equals(ISO) && !"PRI".equals(ISO)) {
            states.sort(String::compareTo);
            stringBuilder.append("\n\t\t\t<states>");
            for (String state : states) {
                stringBuilder.append(state);
            }
            stringBuilder.append("\n\t\t\t</states>");
        }
        stringBuilder.append("\n\t\t</country>");
        return stringBuilder.toString();
    }
}
