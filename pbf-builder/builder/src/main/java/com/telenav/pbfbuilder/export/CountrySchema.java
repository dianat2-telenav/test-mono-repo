package com.telenav.pbfbuilder.export;

import com.telenav.ost.spark.storage.Column;
import com.telenav.ost.spark.storage.Schema;
import org.apache.spark.sql.types.ArrayType;
import org.apache.spark.sql.types.DataTypes;

import java.util.stream.Stream;

public class CountrySchema extends Schema {
    private static final Column adminPlaceId = new Column("adminPlaceId", DataTypes.LongType, false);
    private static final Column ISO = new Column("ISO", DataTypes.StringType, false);
    private static final Column names = new Column("names", DataTypes.StringType, false);
    private static final Column altNames = new Column("altNames", DataTypes.StringType, false);

    private static final ArrayType arrayType = DataTypes.createArrayType(DataTypes.StringType, false);
    private static final Column states = new Column("states", arrayType, false);

    protected Stream<Column> columns() {
        return Stream.of(adminPlaceId, ISO, names, altNames, states);
    }
}