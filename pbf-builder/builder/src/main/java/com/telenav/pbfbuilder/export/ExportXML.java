package com.telenav.pbfbuilder.export;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.map.storage.RawRelationSchema;
import com.telenav.ost.ArgumentContainer;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.spark.sql.functions.*;

public class ExportXML {

    private static final Long MAX_VALUE = Long.MAX_VALUE;

    public static void main(String[] args) {
        final ArgumentContainer arguments = new ArgumentContainer(args);
        final String relationsPath = arguments.getRequired("relations");
        final String outputPath = arguments.getRequired("output_dir");
        final String runLocal = arguments.getOptional("run_local", "false");

        try (final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Export XML")
                .runLocal(Boolean.parseBoolean(runLocal))
                .build()) {

            Dataset<Row> relationDataset = jobInitializer.sparkSession().read().parquet(relationsPath)
                    .map(row -> row, RowEncoder.apply(new RawRelationSchema().asStructType()));

            Dataset<Row> adminL1 = relationDataset.filter(array_contains(col("tags.key"), "admin_level"))
                    .filter(array_contains(col("tags.value"), "L1"));

            OutputSchema outputSchema = new OutputSchema();
            CountrySchema countrySchema = new CountrySchema();
            Dataset<Row> adminL1Converted = adminL1
                    .map(ExportXML::toCountry, Encoders.bean(Country.class))
                    .map(ExportXML::toString, RowEncoder.apply(outputSchema.asStructType()));

            Dataset<Row> adminL1WithCountrySchema = adminL1
                    .map(ExportXML::toCountry, Encoders.bean(Country.class))
                    .map(ExportXML::toCountrySchema, RowEncoder.apply(countrySchema.asStructType()));

            Dataset<Row> adminL2 = relationDataset.filter(array_contains(col("tags.key"), "admin_level"))
                    .filter(array_contains(col("tags.value"), "L2"));
            Dataset<Row> adminL2Converted = adminL2
                    .map(ExportXML::toState, Encoders.bean(State.class))
                    .map(ExportXML::toStringByAdminL1, RowEncoder.apply(outputSchema.asStructType()));

            Dataset<Row> result = adminL1WithCountrySchema
                    .join(adminL2Converted,
                            adminL1WithCountrySchema.col("adminPlaceId").equalTo(adminL2Converted.col("index")))
                    .groupBy("adminPlaceId")
                    .agg(first("ISO").as("ISO"),
                            first("names").as("names"),
                            first("altNames").as("altNames"),
                            collect_list(struct("text")).as("states"))
                    .map(ExportXML::toCountryWithStates, Encoders.bean(Country.class))
                    .map(ExportXML::toString, RowEncoder.apply(outputSchema.asStructType()));

            Dataset<Row> headerDataset = headerRows(jobInitializer, adminL1, outputSchema, adminL1Converted);

            Dataset<Row> footerDataset = footerRows(jobInitializer, outputSchema);

            headerDataset
                    .union(result)
                    .union(footerDataset)
                    .coalesce(1)
                    .orderBy(col("index"))
                    .drop("index")
                    .write().mode(SaveMode.Overwrite).text(outputPath);
        }
    }

    private static Dataset<Row> headerRows(JobInitializer jobInitializer, Dataset<Row> adminL1,
                                           OutputSchema outputSchema, Dataset<Row> adminL1Converted) {
        List<OutputEntity> header = new ArrayList<>();
        // we add index for each output string to preserver order of insert
        header.add(new OutputEntity(0L, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
        header.add(new OutputEntity(1L, "<regions>"));

        String regionName = getRegionName(adminL1);
        OutputEntity outputEntity = new OutputEntity(2L,
                "\t<region country_cnt=\"" + adminL1Converted.count() + "\" name=\"" + regionName + "\">");
        header.add(outputEntity);
        JavaRDD<Row> rowRDD = jobInitializer.javaSparkContext().parallelize(header)
                .map(row -> RowFactory.create(row.getIndex(), row.getText()));
        return jobInitializer.sparkSession().sqlContext()
                .createDataFrame(rowRDD, outputSchema.asStructType());
    }

    private static Dataset<Row> footerRows(JobInitializer jobInitializer, OutputSchema outputSchema) {
        List<OutputEntity> footer = new ArrayList<>();
        // we add an index large enough not to overlap with the admin_place_id
        // to preserve order of insert
        footer.add(new OutputEntity(MAX_VALUE - 1, "\t</region>"));
        footer.add(new OutputEntity(MAX_VALUE, "</regions>"));
        JavaRDD<Row> footerRDD = jobInitializer.javaSparkContext().parallelize(footer)
                .map(values -> RowFactory.create(values.getIndex(), values.getText()));
        return jobInitializer.sparkSession().sqlContext()
                .createDataFrame(footerRDD, outputSchema.asStructType());
    }

    private static String getRegionName(Dataset<Row> adminL1) {
        String regionName;
        Row regionNameRow = adminL1.filter(array_contains(col("tags.key"), "admin_level"))
                .filter(array_contains(col("tags.value"), "L1"))
                .filter(array_contains(col("tags.key"), "name:eng"))
                .filter(array_contains(col("tags.value"), "USA"))
                .first();

        if (regionNameRow == null) {
            regionName = "EU";
        } else {
            regionName = "NA";
        }
        return regionName;
    }

    private static Row toString(Country country) {
        return RowFactory.create(country.getAdminPlaceId(), country.toString());
    }

    private static Row toStringByAdminL1(State state) {
        return RowFactory.create(state.getAdminL1PlaceId(), state.toString());
    }

    private static Row toCountrySchema(Country country) {
        return RowFactory.create(country.getAdminPlaceId(),
                country.getISO(),
                country.getNames(),
                country.getAltNames(),
                country.getStates());
    }

    private static Country toCountry(Row row) {
        List<Row> tagRows = row.getList(row.fieldIndex("tags"));
        return new Country(row.getAs("id"), extractISO(tagRows),
                extractNameRelatedTags(tagRows, "name:"),
                extractNameRelatedTags(tagRows, "alt_name:"));
    }

    private static Country toCountryWithStates(Row row) {
        List<Row> states = row.getList(row.fieldIndex("states"));
        List<String> stringList = new ArrayList<>();
        for (Row state : states) {
            stringList.add(state.getAs("text"));
        }
        return new Country(row.getAs("adminPlaceId"),
                row.getAs("ISO"),
                row.getAs("names"),
                row.getAs("altNames"),
                stringList);
    }

    private static String extractISO(List<Row> tagRows) {
        for (Row tagRow : tagRows) {
            String key = tagRow.getAs("key");
            String value = tagRow.getAs("value");
            if ("iso".equals(key)) {
                return value;
            }
        }
        return "";
    }

    private static String extractNameRelatedTags(List<Row> tagRows, String name) {
        Pattern pattern = Pattern.compile(name + "(\\w+)");
        StringBuilder stringBuilder = new StringBuilder();
        for (Row tagRow : tagRows) {
            String key = tagRow.getAs("key");
            String value = tagRow.getAs("value");
            Matcher matcher = pattern.matcher(key);
            if (matcher.matches()) {
                String language = matcher.group(1);
                stringBuilder
                        .append(value)
                        .append(":")
                        .append(language.toUpperCase())
                        .append(";");
            }
        }
        String result = stringBuilder.toString();
        return result.endsWith(";") ? result.substring(0, result.length() - 1) : result;
    }

    private static State toState(Row row) {
        List<Row> tagRows = row.getList(row.fieldIndex("tags"));
        List<Row> memberRows = row.getList(row.fieldIndex("members"));
        return new State(row.getAs("id"),
                extractNameRelatedTags(tagRows, "name:"),
                extractNameRelatedTags(tagRows, "alt_name:"),
                extractAdminL1PlaceId(memberRows));
    }

    private static Long extractAdminL1PlaceId(List<Row> memberRows) {
        for (Row memberRow : memberRows) {
            Long id = memberRow.getAs("id");
            String role = memberRow.getAs("role");
            if ("country".equals(role)) {
                return id;
            }
        }
        return -1L;
    }

}
