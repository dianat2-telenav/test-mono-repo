package com.telenav.pbfbuilder.export;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Data
public class OutputEntity implements Serializable {

    private final long index;
    private final String text;

}
