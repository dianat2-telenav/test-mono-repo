package com.telenav.pbfbuilder.export;

import com.telenav.ost.spark.storage.Column;
import com.telenav.ost.spark.storage.Schema;
import org.apache.spark.sql.types.DataTypes;

import java.util.stream.Stream;

public class OutputSchema extends Schema {
    private static final Column index = new Column("index", DataTypes.LongType, false);
    private static final Column text = new Column("text", DataTypes.StringType, false);

    protected Stream<Column> columns() {
        return Stream.of(index, text);
    }
}