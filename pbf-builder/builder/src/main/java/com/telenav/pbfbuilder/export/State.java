package com.telenav.pbfbuilder.export;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Data
public class State implements Serializable {

    private final long adminPlaceId;
    private final String names;
    private final String altNames;
    private final long adminL1PlaceId;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("\n\t\t\t\t<state admin_place_id=\"").append(adminPlaceId).append("\"")
                .append(" abbr=\"").append(getAbbr(names)).append("\"")
                .append(">");
        if (!names.isEmpty()) {
            stringBuilder
                    .append("\n\t\t\t\t\t<names>").append(names).append("</names>");
        }
        if (!altNames.isEmpty()) {
            stringBuilder
                    .append("\n\t\t\t\t\t<alt_names>").append(altNames).append("</alt_names>");
        }
        stringBuilder.append("\n\t\t\t\t</state>");
        return stringBuilder.toString();
    }

    private String getAbbr(String names) {
        String firstValue = names.split(":")[0];
        return Abbreviations.valueFor(firstValue);
    }

}
