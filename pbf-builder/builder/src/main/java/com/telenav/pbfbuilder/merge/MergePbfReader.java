package com.telenav.pbfbuilder.merge;

import crosby.binary.osmosis.OsmosisReader;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openstreetmap.osmosis.core.container.v0_6.EntityContainer;
import org.openstreetmap.osmosis.core.domain.v0_6.Entity;
import org.openstreetmap.osmosis.core.domain.v0_6.Node;
import org.openstreetmap.osmosis.core.domain.v0_6.Relation;
import org.openstreetmap.osmosis.core.domain.v0_6.Way;
import org.openstreetmap.osmosis.core.task.v0_6.Sink;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * Class that is responsible for reading a pbf file.
 */
public class MergePbfReader {

    private static final Logger LOGGER = LogManager.getLogger(MergePbfReader.class);
    private final Path pbfPath;
    private final FileSystem fileSystem;
    private final List<Node> nodes;
    private final List<Way> ways;
    private final List<Relation> relations;


    MergePbfReader(final Path pbfPath, final FileSystem fileSystem) {
        this.nodes = new ArrayList<>();
        this.ways = new ArrayList<>();
        this.relations = new ArrayList<>();
        this.pbfPath = pbfPath;
        this.fileSystem = fileSystem;
    }

    void read() throws IOException {
        final String pbfName = pbfPath.getName();
        LOGGER.info("Reading " + pbfName + "...");
        final long startTime = Calendar.getInstance().getTimeInMillis();
        final OsmosisReader reader = new OsmosisReader(fileSystem.open(pbfPath).getWrappedStream());
        reader.setSink(new PbfEntityConsumer());
        reader.run();
        final long endTime = Calendar.getInstance().getTimeInMillis();
        final long durationInSeconds = TimeUnit.MILLISECONDS.toSeconds(endTime - startTime);
        LOGGER.info("Finished reading " + pbfName + " in " + durationInSeconds + " seconds.\n");
    }

    List<Node> nodes() {
        return nodes;
    }

    List<Way> ways() {
        return ways;
    }

    List<Relation> relations() {
        return relations;
    }

    private class PbfEntityConsumer implements Sink {

        @Override
        public void process(final EntityContainer entityContainer) {
            final Entity entity = entityContainer.getEntity();
            if (entity instanceof Node) {
                nodes.add((Node) entity);
            } else if (entity instanceof Way) {
                ways.add((Way) entity);
            } else if (entity instanceof Relation) {
                relations.add((Relation) entity);
            }
        }

        @Override
        public void initialize(final Map<String, Object> map) {
        }

        @Override
        public void complete() {
        }

        @Override
        public void close() {
        }
    }
}