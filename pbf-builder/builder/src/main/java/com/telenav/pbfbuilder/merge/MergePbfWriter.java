package com.telenav.pbfbuilder.merge;

import com.telenav.pbfbuilder.common.OsmosisWriter;
import com.telenav.pbfbuilder.common.PbfWriter;
import org.apache.hadoop.conf.Configuration;
import org.openstreetmap.osmosis.core.container.v0_6.NodeContainer;
import org.openstreetmap.osmosis.core.container.v0_6.RelationContainer;
import org.openstreetmap.osmosis.core.container.v0_6.WayContainer;
import org.openstreetmap.osmosis.core.domain.v0_6.Node;
import org.openstreetmap.osmosis.core.domain.v0_6.Relation;
import org.openstreetmap.osmosis.core.domain.v0_6.Way;

import java.io.IOException;
import java.util.List;


/**
 * Class that is responsible for writing a pbf file but it knows how to write only osmosis objects.
 * It is used by the merging process that merges small pbf files into a large final pbf.
 *
 * @author adrianal
 */
class MergePbfWriter extends PbfWriter implements OsmosisWriter<Node, Way, Relation> {

    private MergePbfWriter(final String outputPbfDir, final String fileName, final boolean readWriteHdfs,
                           final Configuration configuration) throws IOException {
        super(outputPbfDir, fileName, readWriteHdfs, configuration);
    }

    @Override
    public void complete() {
        this.serializer.complete();
    }

    @Override
    public void close() {
        this.serializer.close();
    }

    @Override
    public void writeNodes(final List<Node> nodes) {
        nodes.forEach(node -> serializer.process(new NodeContainer(node)));
    }

    @Override
    public void writeWays(final List<Way> ways) {
        ways.forEach(way -> serializer.process(new WayContainer(way)));
    }

    @Override
    public void writeRelations(final List<Relation> relations) {
        relations.forEach(relation -> serializer.process(new RelationContainer(relation)));
    }

    public static class Builder {

        private String outputPbfDir;
        private String fileName;
        private boolean readWriteHdfs;
        private Configuration configuration;

        public Builder outputPbfDir(final String outputPbfDir) {
            this.outputPbfDir = outputPbfDir;
            return this;
        }

        public Builder fileName(final String fileName) {
            this.fileName = fileName + ".osm.pbf";
            return this;
        }

        public Builder readWriteHdfs(final boolean readWriteHdfs) {
            this.readWriteHdfs = readWriteHdfs;
            return this;
        }


        public Builder configuration(final Configuration configuration) {
            this.configuration = configuration;
            return this;
        }

        public MergePbfWriter build() throws IOException {
            return new MergePbfWriter(outputPbfDir, fileName, readWriteHdfs, configuration);
        }
    }
}