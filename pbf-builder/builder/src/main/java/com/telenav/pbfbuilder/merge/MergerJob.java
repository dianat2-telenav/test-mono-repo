package com.telenav.pbfbuilder.merge;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.ost.ArgumentContainer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;


/**
 * @author adrianal
 */
public class MergerJob {
    private static final Logger LOGGER = LogManager.getLogger(MergerJob.class);

    public static void main(final String[] args) throws IOException {
        final ArgumentContainer arguments = new ArgumentContainer(args);
        final String pbfInputDirPath = arguments.getRequired("input_pbf_dir");
        final String pbfOutputDir = arguments.getRequired("output_pbf_dir");
        final String pbfName = arguments.getRequired("pbf_file_name");
        final String readWriteHdfs = arguments.getOptional("read_write_hdfs", "true");
        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Pbf Merger")
                .runLocal(true)
                .build();

        final Configuration configuration = jobInitializer.sparkSession().sparkContext().hadoopConfiguration();
        final FileSystem fs = FileSystem.get(configuration);

        LOGGER.info("Starting merge ...");
        final long startTime = Calendar.getInstance().getTimeInMillis();
        try {
            final MergePbfWriter writer = new MergePbfWriter.Builder()
                    .outputPbfDir(pbfOutputDir)
                    .fileName(pbfName)
                    .configuration(configuration)
                    .readWriteHdfs(Boolean.parseBoolean(readWriteHdfs))
                    .build();

            // We merge pbfs separately in order to keep nodes first, ways second and relations last
            // (compliant with OSM specifications)
            mergeNodes(pbfInputDirPath, fs, writer);

            mergeWays(pbfInputDirPath, fs, writer);

            mergeRelations(pbfInputDirPath, fs, writer);
            writer.close();
        } catch (final IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        LOGGER.info("End merge ...");
        final long endTime = Calendar.getInstance().getTimeInMillis();
        final long durationInSeconds = TimeUnit.MILLISECONDS.toSeconds(endTime - startTime);
        LOGGER.info("Finished merging in " + durationInSeconds + " seconds.\n");
    }

    private static void mergeNodes(String pbfInputDirPath, FileSystem fs, MergePbfWriter writer) throws IOException {
        LOGGER.info("Starting nodes merging ...");
        final String nodesInputPbfPath = pbfInputDirPath + "/nodes";
        final Path nodesInputPbfFolder = new Path(nodesInputPbfPath);
        final RemoteIterator<LocatedFileStatus> filesNodes = fs.listFiles(nodesInputPbfFolder, false);
        while (filesNodes.hasNext()) {
            final LocatedFileStatus file = filesNodes.next();
            final MergePbfReader reader = new MergePbfReader(file.getPath(), fs);
            try {
                reader.read();
            } catch (final IOException e) {
                e.printStackTrace();
            }
            LOGGER.info("Merging nodes from" + file.getPath().getName() + "...");
            final long start = Calendar.getInstance().getTimeInMillis();
            writer.writeNodes(reader.nodes());
            writer.complete();
            final long end = Calendar.getInstance().getTimeInMillis();
            final long durationInSeconds = TimeUnit.MILLISECONDS.toSeconds(end - start);
            LOGGER.info(
                    "Finished merging nodes from"
                            + file.getPath().getName() + " in " + durationInSeconds + " seconds.\n");
        }
    }

    private static void mergeWays(String pbfInputDirPath, FileSystem fs, MergePbfWriter writer) throws IOException {
        LOGGER.info("Starting ways merging ...");
        final String waysInputPbfPath = pbfInputDirPath + "/ways";
        final Path waysInputPbfFolder = new Path(waysInputPbfPath);
        final RemoteIterator<LocatedFileStatus> filesWays = fs.listFiles(waysInputPbfFolder, false);
        while (filesWays.hasNext()) {
            final LocatedFileStatus file = filesWays.next();
            final MergePbfReader reader = new MergePbfReader(file.getPath(), fs);
            try {
                reader.read();
            } catch (final IOException e) {
                e.printStackTrace();
            }
            LOGGER.info("Merging ways from " + file.getPath().getName() + "...");
            final long start = Calendar.getInstance().getTimeInMillis();
            writer.writeWays(reader.ways());
            writer.complete();
            final long end = Calendar.getInstance().getTimeInMillis();
            final long durationInSeconds = TimeUnit.MILLISECONDS.toSeconds(end - start);
            LOGGER.info(
                    "Finished merging ways from"
                            + file.getPath().getName() + " in " + durationInSeconds + " seconds.\n");
        }
    }

    private static void mergeRelations(String pbfInputDirPath, FileSystem fs, MergePbfWriter writer) {
        LOGGER.info("Starting relations merging ...");
        final String relationsInputPbfPath = pbfInputDirPath + "/relations.osm.pbf";
        final Path relationsInputPbfFolder = new Path(relationsInputPbfPath);
        final MergePbfReader reader = new MergePbfReader(relationsInputPbfFolder, fs);
        try {
            reader.read();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        LOGGER.info("Merging relations from " + relationsInputPbfFolder.getName() + "...");
        final long start = Calendar.getInstance().getTimeInMillis();
        writer.writeRelations(reader.relations());
        writer.complete();
        final long end = Calendar.getInstance().getTimeInMillis();
        final long durationInSeconds = TimeUnit.MILLISECONDS.toSeconds(end - start);
        LOGGER.info(
                "Finished merging relations from "
                        + relationsInputPbfFolder.getName() + " in " + durationInSeconds + " seconds.\n");
    }
}