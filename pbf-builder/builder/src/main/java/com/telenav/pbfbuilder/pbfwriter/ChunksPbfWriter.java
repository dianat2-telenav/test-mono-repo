package com.telenav.pbfbuilder.pbfwriter;

import com.telenav.map.entity.Node;
import com.telenav.map.entity.RawRelation;
import com.telenav.map.entity.Way;
import com.telenav.map.entity.WaySection;
import com.telenav.pbfbuilder.common.OsmosisWaySectionWriter;
import com.telenav.pbfbuilder.common.OsmosisWriter;
import com.telenav.pbfbuilder.common.PbfWriter;
import com.telenav.pbfbuilder.pbfwriter.converters.osmosis.Node2OsmosisNode;
import com.telenav.pbfbuilder.pbfwriter.converters.osmosis.RawRelation2OsmosisRelation;
import com.telenav.pbfbuilder.pbfwriter.converters.osmosis.Way2OsmosisWay;
import com.telenav.pbfbuilder.pbfwriter.converters.osmosis.WaySection2OsmosisWay;
import org.apache.spark.SerializableWritable;
import org.openstreetmap.osmosis.core.container.v0_6.NodeContainer;
import org.openstreetmap.osmosis.core.container.v0_6.RelationContainer;
import org.openstreetmap.osmosis.core.container.v0_6.WayContainer;

import java.io.IOException;
import java.util.List;


/**
 * The writer that will write the nodes, ways or relations, which are objects specific to the project into actual
 * pbf file using osmosis. Also writes way sections.
 *
 * @author adrianal
 */
class ChunksPbfWriter extends PbfWriter implements OsmosisWriter<Node, Way, RawRelation>, OsmosisWaySectionWriter {

    private ChunksPbfWriter(final String outputPbfsDir, final String fileName, final boolean readWriteHdfs,
                            final SerializableWritable configurationBroadcast) throws IOException {
        super(outputPbfsDir, fileName, readWriteHdfs, configurationBroadcast);
    }

    @Override
    public void complete() {
        this.serializer.complete();
    }

    @Override
    public void close() {
        this.serializer.close();
    }

    @Override
    public void writeNodes(final List<Node> nodes) {
        final Node2OsmosisNode node2OsmosisNode = new Node2OsmosisNode();
        nodes.forEach(node -> serializer.process(new NodeContainer(node2OsmosisNode.apply(node))));
    }

    @Override
    public void writeWaySections(final List<WaySection> waySections) {
        final WaySection2OsmosisWay waySection2OsmosisWay = new WaySection2OsmosisWay();
        waySections.forEach(waySection -> serializer
                .process(new WayContainer(waySection2OsmosisWay.apply(waySection))));
    }

    @Override
    public void writeWays(final List<Way> ways) {
        final Way2OsmosisWay way2OsmosisWay = new Way2OsmosisWay();
        ways.forEach(waySection -> serializer.process(new WayContainer(way2OsmosisWay.apply(waySection))));
    }

    @Override
    public void writeRelations(final List<RawRelation> rawRelations) {
        final RawRelation2OsmosisRelation rawRelation2OsmosisRelation = new RawRelation2OsmosisRelation();
        rawRelations.forEach(rawRelation -> serializer
                .process(new RelationContainer(rawRelation2OsmosisRelation.apply(rawRelation))));
    }

    public static class Builder {
        private String outputPbfsDir;
        private String fileName;
        private boolean readWriteHdfs;
        private SerializableWritable configurationBroadcast;

        public Builder outputPbfDir(final String outputPbfsDir) {
            this.outputPbfsDir = outputPbfsDir;
            return this;
        }

        public Builder fileName(final String fileName) {
            this.fileName = fileName + ".osm.pbf";
            return this;
        }

        public Builder readWriteHdfs(final boolean readWriteHdfs) {
            this.readWriteHdfs = readWriteHdfs;
            return this;
        }

        public Builder configurationBroadcast(final SerializableWritable configurationBroadcast) {
            this.configurationBroadcast = configurationBroadcast;
            return this;
        }

        public ChunksPbfWriter build() throws IOException {
            return new ChunksPbfWriter(outputPbfsDir, fileName, readWriteHdfs, configurationBroadcast);
        }
    }
}