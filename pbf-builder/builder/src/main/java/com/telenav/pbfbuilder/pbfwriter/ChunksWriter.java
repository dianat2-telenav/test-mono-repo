package com.telenav.pbfbuilder.pbfwriter;

import ch.hsr.geohash.GeoHash;
import com.telenav.map.compose.Composer;
import com.telenav.map.entity.*;
import com.telenav.map.filter.FilterFactory;
import com.telenav.ost.Filter;
import com.telenav.pbfbuilder.pbfwriter.entity.LocalizedNode;
import com.telenav.pbfbuilder.pbfwriter.entity.LocalizedWay;
import org.apache.spark.SerializableWritable;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.broadcast.Broadcast;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Class responsible to partition input by geohash when possible and write pbf's into small chunks, since osmosis
 * does not work in a distributed environment.
 *
 * @author adrianal
 */
class ChunksWriter implements Serializable {

    private final JavaRDD<Node> nodes;
    private final JavaRDD<RawWay> rawWays;
    private final JavaRDD<RawRelation> relations;
    private final String outputPath;
    private final Broadcast broadcast;
    private final Boolean readWriteHdfs;
    private final int geohashSize;

    private ChunksWriter(final JavaRDD<Node> nodes, final JavaRDD<RawWay> rawWays,
        final JavaRDD<RawRelation> relations, final String outputPath, final Broadcast broadcast,
        final Boolean runLocal, final int geohashSize) {
        if (nodes == null || rawWays == null || relations == null || outputPath == null
                || broadcast == null || runLocal == null) {
            throw new IllegalArgumentException("All attributes must be set in order to construct a valid ChunksWriter");
        }
        this.nodes = nodes;
        this.rawWays = rawWays;
        this.relations = relations;
        this.outputPath = outputPath;
        this.broadcast = broadcast;
        this.readWriteHdfs = runLocal;
        this.geohashSize = geohashSize;
    }

    /**
     * Writes all the nodes to pbf file. The nodes are splitted into geohashes, thus a pbf with nodes will be created for
     * each geohash. Data is appended to the pbf file using osmosis.
     */
    void writeNodes() {
        final JavaRDD<LocalizedNode> localizedNodes = nodes.map(node -> {
            final String geohash = GeoHash.geoHashStringWithCharacterPrecision(node.latitude().asDegrees(),
                node.longitude().asDegrees(), geohashSize);
            return new LocalizedNode(geohash, node);
        });
        localizedNodes.groupBy(LocalizedNode::getGeohash).foreach(pair -> {
            final List<Node> nodesPerGeohash = new ArrayList<>();
            pair._2().forEach(localizedNode -> nodesPerGeohash.add(localizedNode.getNode()));
            final List<Node> sorted = nodesPerGeohash.stream().sorted(Comparator.comparingLong(StandardEntity::id))
                    .collect(Collectors.toList());
            final ChunksPbfWriter writer = new ChunksPbfWriter.Builder()
                    .outputPbfDir(outputPath + "/nodes").fileName(pair._1())
                    .configurationBroadcast((SerializableWritable) broadcast.getValue())
                    .readWriteHdfs(readWriteHdfs)
                    .build();
            writer.writeNodes(sorted);
            writer.complete();
            writer.close();
        });
    }

    /**
     * Writes all the raw ways, without the way sections to pbf files. Filtering is applied so ways are not duplicated
     * in the output.  The ways are split into geohashes, thus a pbf with ways will be created for each geohash.
     * Data is appended to the pbf file using osmosis.
     */
    void writeRawWays() {
        final Composer composer = new Composer(true);
        final JavaRDD<Way> ways = composer.compose(rawWays, nodes);
        final Filter<Way> shapeFilter = FilterFactory.INSTANCE.validShapeFilter();
        final JavaRDD<Way> filteredWays = ways.filter(shapeFilter::accept);

        final JavaRDD<LocalizedWay> localizedWays = filteredWays.map(filteredWay -> {
            final Node firstNode = filteredWay.nodes().get(0);
            final String geohash = GeoHash.geoHashStringWithCharacterPrecision(firstNode.latitude().asDegrees(),
                firstNode.longitude().asDegrees(), geohashSize);
            return new LocalizedWay(geohash, filteredWay);
        });

        localizedWays.groupBy(LocalizedWay::getGeohash).foreach(pair -> {
            final List<Way> restOfWaysPerGeohash = new ArrayList<>();
            pair._2().forEach(localizedWay -> restOfWaysPerGeohash.add(localizedWay.getWay()));
            final List<Way> sorted = restOfWaysPerGeohash.stream().sorted(Comparator.comparingLong(Way::id)).collect(Collectors.toList());
            final ChunksPbfWriter writer = new ChunksPbfWriter.Builder()
                    .outputPbfDir(outputPath + "/ways").fileName(pair._1())
                    .configurationBroadcast((SerializableWritable) broadcast.getValue())
                    .readWriteHdfs(readWriteHdfs)
                    .build();
            writer.writeWays(sorted);
            writer.complete();
            writer.close();
        });
    }

    /**
     * Writes all the relations to pbf files. Since relations are small they are collected and written into one single
     * pbf file. Data is appended to the pbf file using osmosis.
     *
     * @throws IOException in case writing to pbf fails
     */
    void handleRelations() throws IOException {
        final ChunksPbfWriter writer = new ChunksPbfWriter.Builder()
                .outputPbfDir(outputPath).fileName("relations")
                .configurationBroadcast((SerializableWritable) broadcast.getValue())
                .readWriteHdfs(readWriteHdfs)
                .build();
        writer.writeRelations(relations.collect());
        writer.complete();
        writer.close();
    }

    public static class Builder {

        private JavaRDD<Node> nodes;
        private JavaRDD<RawWay> rawWays;
        private JavaRDD<RawRelation> relations;
        private String outputPath;
        private Broadcast broadcast;
        private Boolean readWriteHdfs;
        private int geohashSize;

        public Builder nodes(final JavaRDD<Node> nodes) {
            this.nodes = nodes;
            return this;
        }

        public Builder rawWays(final JavaRDD<RawWay> rawWays) {
            this.rawWays = rawWays;
            return this;
        }

        public Builder relations(final JavaRDD<RawRelation> relations) {
            this.relations = relations;
            return this;
        }

        public Builder outputPath(final String outputPath) {
            this.outputPath = outputPath;
            return this;
        }

        public Builder broadcast(final Broadcast broadcast) {
            this.broadcast = broadcast;
            return this;
        }

        public Builder readWriteHdfs(final Boolean localRun) {
            this.readWriteHdfs = localRun;
            return this;
        }

        public Builder geohashSize(final int geohashSize) {
            this.geohashSize = geohashSize;
            return this;
        }

        public ChunksWriter build() {
            return new ChunksWriter(nodes, rawWays, relations, outputPath, broadcast, readWriteHdfs, geohashSize);
        }
    }
}