package com.telenav.pbfbuilder.pbfwriter;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.RawRelation;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import com.telenav.map.storage.read.*;
import com.telenav.ost.ArgumentContainer;
import com.telenav.ost.spark.storage.read.ParquetReader;
import com.telenav.pbfbuilder.pbfwriter.converters.WaySectionToRawWayConverter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SerializableWritable;
import org.apache.spark.SparkException;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;

import java.io.IOException;
import java.util.Objects;


/**
 * @author adrianal
 */
public class PbfWriterJob {

    private static final String RAW_WAYS_PARQUET = "raw_ways_parquet";
    private static final String WAYS_NO_SECTION_PARQUET = "ways_no_sections_parquet";
    private static final String WAY_SECTIONS_PARQUET = "way_sections_parquet";
    private static final String NODE_PARQUET = "node_parquet";
    private static final String RELATION_PARQUET = "relation_parquet";
    private static final String RUN_LOCAL = "run_local";
    private static final String OUTPUT_DIR = "output_dir";
    private static final String GEOHASH_SIZE = "geohash_size";

    private static String nodeParquetPath;
    private static String relationsParquetPath;
    private static String rawWaysPath;
    private static String waysNoSectionsPath;
    private static String waySectionsPath;
    private static String outputFileDir;
    private static String runLocal;
    private static int geohashSize;


    private static JavaSparkContext javaSparkContext;
    private static Broadcast<SerializableWritable> broadcast;
    private static boolean readWriteHdfs = true;

    public static void main(final String[] args) throws SparkException {
        final ArgumentContainer arguments = new ArgumentContainer(args);
        readArguments(arguments);
        initializeLocalVariables();
        createOutputDir();

        /*read the data into rdd*/
        final JavaRDD<Node> nodes =
                new ParquetReader<>(javaSparkContext, new RawRowToNodeConverter(), nodeParquetPath).read();
        final JavaRDD<RawRelation> relations =
                new ParquetReader<>(javaSparkContext, new RawRowToRawRelationConverter(), relationsParquetPath).read();
        final JavaRDD<RawWay> rawWays = readWaysInput();

        writePbfChunks(nodes, relations, rawWays);
    }

    private static void writePbfChunks(final JavaRDD<Node> nodes, final JavaRDD<RawRelation> relations, final JavaRDD<RawWay> rawWays) {
        final ChunksWriter chunksWriter = new ChunksWriter.Builder()
                .nodes(nodes)
                .rawWays(rawWays)
                .relations(relations)
                .outputPath(outputFileDir)
                .broadcast(broadcast)
                .readWriteHdfs(readWriteHdfs)
                .geohashSize(geohashSize)
                .build();
        chunksWriter.writeNodes();
        chunksWriter.writeRawWays();

        try {
            chunksWriter.handleRelations();
        } catch (final IOException e) {
            throw new RuntimeException("Failed to write relations to hdfs", e);
        }
    }

    private static void readArguments(final ArgumentContainer arguments) {
        nodeParquetPath = arguments.getRequired(NODE_PARQUET);
        rawWaysPath = arguments.getOptional(RAW_WAYS_PARQUET, null);
        waysNoSectionsPath = arguments.getOptional(WAYS_NO_SECTION_PARQUET, null);
        waySectionsPath = arguments.getOptional(WAY_SECTIONS_PARQUET, null);
        relationsParquetPath = arguments.getRequired(RELATION_PARQUET);
        outputFileDir = arguments.getRequired(OUTPUT_DIR);
        runLocal = arguments.getOptional(RUN_LOCAL, "false");
        geohashSize = Integer.parseInt(arguments.getOptional(GEOHASH_SIZE, "4"));

        if (Objects.isNull(rawWaysPath) && (Objects.isNull(waySectionsPath) || Objects.isNull(waysNoSectionsPath))) {
            throw new IllegalArgumentException(
                    "Please specify " + RAW_WAYS_PARQUET + " argument or " + WAYS_NO_SECTION_PARQUET + ", " + WAY_SECTIONS_PARQUET + " pair");
        }
    }

    private static JavaRDD<RawWay> readWaysInput() {
        final JavaRDD<RawWay> rawWays;
        if (Objects.isNull(rawWaysPath)) {
            rawWays = combineToMakeRawWays();
        } else {
            rawWays = readRawWaysOnly();
        }
        return rawWays;
    }

    private static JavaRDD<RawWay> readRawWaysOnly() {
        return new ParquetReader<>(javaSparkContext, new RawRowToRawWayConverter(), rawWaysPath).read();
    }

    private static JavaRDD<RawWay> combineToMakeRawWays() {
        final JavaRDD<RawWay> waysNoSections =
                new ParquetReader<>(javaSparkContext, new RawRowToRawWayConverter(), waysNoSectionsPath).read();
        final JavaRDD<WaySection> waySections =
                new ParquetReader<>(javaSparkContext, new RowToWaySectionConverter(), waySectionsPath).read();

        return waySections
                .map(waySection -> new WaySectionToRawWayConverter().apply(waySection))
                .union(waysNoSections);
    }

    private static void initializeLocalVariables() {
        final JobInitializer jobInitializer =
                new JobInitializer.Builder().appName("Pbf Writer").runLocal(Boolean.valueOf(runLocal)).build();

        if (Boolean.valueOf(runLocal)) {
            readWriteHdfs = false;
        }

        final Configuration configuration = jobInitializer.sparkSession().sparkContext().hadoopConfiguration();
        javaSparkContext = jobInitializer.javaSparkContext();
        broadcast = javaSparkContext.broadcast(new SerializableWritable<>(configuration));
    }

    private static void createOutputDir() {
        final Path outputDir = new Path(outputFileDir);
        try {
            final FileSystem fs = FileSystem.get(javaSparkContext.hadoopConfiguration());
            Path nodeOut = new Path(outputDir, "nodes");
            if (!fs.exists(nodeOut)) {
                fs.mkdirs(nodeOut);
            }
            Path wayOut = new Path(outputDir, "ways");
            if (!fs.exists(wayOut)) {
                fs.mkdirs(wayOut);
            }
        } catch (final IOException e) {
            throw new RuntimeException("Failed to create directory " + outputFileDir, e);
        }
    }
}