package com.telenav.pbfbuilder.pbfwriter.converters;

import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.StandardEntity;
import com.telenav.map.entity.WaySection;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * @author adrianal
 */
public class WaySectionToRawWayConverter implements Function<WaySection, RawWay> {

    @Override
    public RawWay apply(final WaySection waySection) {
        final List<Long> onlyNodeIds = waySection.nodes().stream().map(StandardEntity::id).collect(Collectors.toList());
        return new RawWay(waySection.id(), onlyNodeIds, waySection.tags());
    }
}