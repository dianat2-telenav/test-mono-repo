package com.telenav.pbfbuilder.pbfwriter.converters.osmosis;

import com.telenav.map.entity.StandardEntity;
import org.openstreetmap.osmosis.core.domain.v0_6.CommonEntityData;
import org.openstreetmap.osmosis.core.domain.v0_6.OsmUser;
import org.openstreetmap.osmosis.core.domain.v0_6.Tag;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.stream.Collectors;


/**
 * Holds common converting of tag attributes si all map entities have tags, all converters will have to implement this.
 * Some of the attributes are hardcoded since they are required by the osmosis library but they are missing
 * from the raw parquets.
 *
 * @author adrianal
 */
interface EntityDataConverter {

    default CommonEntityData commonEntityData(final StandardEntity entity) {
        final Collection<Tag> tags =
                entity.tags().entrySet().stream().map(entry -> new Tag(entry.getKey(), entry.getValue()))
                        .collect(Collectors.toSet());
        return new CommonEntityData(entity.id(), 123, Date.valueOf(LocalDate.of(2017, Month.OCTOBER, 9)),
                new OsmUser(1, "Telenav"), 123L, tags);
    }
}