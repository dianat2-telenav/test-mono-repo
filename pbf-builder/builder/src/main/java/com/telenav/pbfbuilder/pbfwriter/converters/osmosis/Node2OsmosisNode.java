package com.telenav.pbfbuilder.pbfwriter.converters.osmosis;

import com.telenav.map.entity.Node;

import java.util.function.Function;


public class Node2OsmosisNode implements EntityDataConverter,
        Function<Node, org.openstreetmap.osmosis.core.domain.v0_6.Node> {

    @Override
    public org.openstreetmap.osmosis.core.domain.v0_6.Node apply(final Node node) {
        return new org.openstreetmap.osmosis.core.domain.v0_6.Node(commonEntityData(node), node.latitude().asDegrees(),
                node.longitude().asDegrees());
    }
}