package com.telenav.pbfbuilder.pbfwriter.converters.osmosis;

import com.telenav.map.entity.RawRelation;
import org.openstreetmap.osmosis.core.domain.v0_6.Relation;
import org.openstreetmap.osmosis.core.domain.v0_6.RelationMember;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * @author adrianal
 */
public class RawRelation2OsmosisRelation implements EntityDataConverter, Function<RawRelation, Relation> {

    @Override
    public Relation apply(final RawRelation rawRelation) {
        final RelationMember2OsmosisRelationMember relationMember2OsmosisRelationMember = new RelationMember2OsmosisRelationMember();
        final List<RelationMember> members =
                rawRelation.members().stream().map(relationMember2OsmosisRelationMember).collect(Collectors.toList());
        return new Relation(commonEntityData(rawRelation), members);
    }
}
