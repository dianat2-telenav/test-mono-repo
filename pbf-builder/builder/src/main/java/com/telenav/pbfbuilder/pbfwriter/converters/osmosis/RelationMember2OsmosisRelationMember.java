package com.telenav.pbfbuilder.pbfwriter.converters.osmosis;

import com.telenav.map.entity.RawRelation;
import org.openstreetmap.osmosis.core.domain.v0_6.EntityType;
import org.openstreetmap.osmosis.core.domain.v0_6.RelationMember;

import java.util.Objects;
import java.util.function.Function;


public class RelationMember2OsmosisRelationMember implements Function<RawRelation.Member, RelationMember> {

    @Override
    public RelationMember apply(final RawRelation.Member rawRelationMember) {
        final RawRelation.MemberType type = rawRelationMember.type();
        EntityType osmosisType = null;
        if (type.equals(RawRelation.MemberType.NODE)) {
            osmosisType = EntityType.Node;
        } else if (type.equals(RawRelation.MemberType.WAY)) {
            osmosisType = EntityType.Way;
        } else if (type.equals(RawRelation.MemberType.RELATION)) {
            osmosisType = EntityType.Relation;
        }
        if (Objects.isNull(osmosisType)) {
            throw new IllegalArgumentException("Invalid raw relation supplied, type is not correctly specified");
        }
        return new RelationMember(rawRelationMember.id(), osmosisType, rawRelationMember.role());
    }
}
