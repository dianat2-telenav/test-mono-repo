package com.telenav.pbfbuilder.pbfwriter.converters.osmosis;

import org.openstreetmap.osmosis.core.domain.v0_6.Way;
import org.openstreetmap.osmosis.core.domain.v0_6.WayNode;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * @author adrianal
 */
public class Way2OsmosisWay implements EntityDataConverter, Function<com.telenav.map.entity.Way, Way> {

    @Override
    public Way apply(final com.telenav.map.entity.Way way) {
        final List<WayNode> wayNodes =
                way.nodes().stream().map(node -> new WayNode(node.id())).collect(Collectors.toList());
        return new Way(commonEntityData(way), wayNodes);
    }
}
