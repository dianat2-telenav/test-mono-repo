package com.telenav.pbfbuilder.pbfwriter.converters.osmosis;

import com.telenav.map.entity.WaySection;
import org.openstreetmap.osmosis.core.domain.v0_6.Way;
import org.openstreetmap.osmosis.core.domain.v0_6.WayNode;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * @author adrianal
 */
public class WaySection2OsmosisWay implements EntityDataConverter, Function<WaySection, Way> {

    @Override
    public Way apply(final WaySection waySection) {
        final List<WayNode> wayNodes =
                waySection.nodes().stream().map(node -> new WayNode(node.id())).collect(Collectors.toList());
        return new Way(commonEntityData(waySection), wayNodes);
    }
}