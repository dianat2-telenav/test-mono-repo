package com.telenav.pbfbuilder.pbfwriter.entity;

import com.telenav.map.entity.Node;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;


/**
 * Holds information about a {@link Node} and the geohash this node belongs to.
 *
 * @author adrianal
 */
@AllArgsConstructor
@Getter
public class LocalizedNode implements Serializable {

    private String geohash;
    private Node node;
}
