package com.telenav.pbfbuilder.pbfwriter.entity;

import com.telenav.map.entity.Way;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;


/**
 * Holds information about a {@link Way} and the geohash this way belongs to.
 *
 * @author adrianal
 */
@AllArgsConstructor
@Getter
public class LocalizedWay implements Serializable {
    private String geohash;
    private Way way;
}
