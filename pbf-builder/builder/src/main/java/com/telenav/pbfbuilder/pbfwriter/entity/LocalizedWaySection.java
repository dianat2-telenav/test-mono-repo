package com.telenav.pbfbuilder.pbfwriter.entity;

import com.telenav.map.entity.WaySection;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;


/**
 * Holds information about a {@link WaySection} and the geohash this waysection belongs to.
 *
 * @author adrianal
 */
@AllArgsConstructor
@Getter
public class LocalizedWaySection implements Serializable {

    private String geohash;
    private WaySection waySection;
}
