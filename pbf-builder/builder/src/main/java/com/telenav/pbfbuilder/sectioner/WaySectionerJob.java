package com.telenav.pbfbuilder.sectioner;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.map.compose.Composer;
import com.telenav.map.entity.*;
import com.telenav.map.filter.FilterFactory;
import com.telenav.map.section.WaySectioner;
import com.telenav.map.section.WaySectionerConfiguration;
import com.telenav.map.storage.WaySectionSchema;
import com.telenav.map.storage.read.RawRowToNodeConverter;
import com.telenav.map.storage.read.RawRowToRawWayConverter;
import com.telenav.map.storage.write.WaySectionToRowConverter;
import com.telenav.ost.ArgumentContainer;
import com.telenav.ost.Filter;
import com.telenav.ost.spark.storage.read.ParquetReader;
import com.telenav.ost.spark.storage.write.ParquetWriter;
import org.apache.spark.api.java.JavaRDD;

import java.io.IOException;
import java.nio.file.Paths;


/**
 * @author adrianal
 */
public class WaySectionerJob {

    private static final String HIGHWAY_PATH = "sectioning_highway_values";
    private static String sectioningHighwayValuesFilePath;

    public static void main(final String[] args) {

        final ArgumentContainer arguments = new ArgumentContainer(args);
        final String nodesPath = arguments.getRequired("nodes");
        final String waysPath = arguments.getRequired("ways");
        final String outputDir = arguments.getRequired("output_dir");
        sectioningHighwayValuesFilePath = arguments.getRequired(HIGHWAY_PATH);
        final boolean runLocal = Boolean.parseBoolean(arguments.getOptional("run_local", "false"));

        try(final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Way Sectioner")
                .runLocal(runLocal)
                .build()) {

            final JavaRDD<Node> nodes = new ParquetReader<>(jobInitializer.javaSparkContext(), new RawRowToNodeConverter(),
                    nodesPath).read();
            final JavaRDD<RawWay> rawWays = new ParquetReader<>(jobInitializer.javaSparkContext(),
                    new RawRowToRawWayConverter(), waysPath).read();

            final Composer composer = new Composer(true);
            final JavaRDD<Way> composedWays = composer.compose(rawWays, nodes);

            final JavaRDD<Way> filteredWays = filterWays(composedWays);

            final JavaRDD<WaySection> sectioned = new WaySectioner(WaySectionerConfiguration.HIGHWAY_DEFAULT)
                    .section(filteredWays);

            new ParquetWriter.Builder<WaySection>()
                    .converter(new WaySectionToRowConverter())
                    .saveMode("ErrorIfExists")
                    .schema(new WaySectionSchema())
                    .path(outputDir)
                    .sparkContext(jobInitializer.javaSparkContext())
                    .build()
                    .write(sectioned);
        }
    }

    private static JavaRDD<Way> filterWays(final JavaRDD<Way> composedWays) {
        final Filter<Way> shapeFilter = FilterFactory.INSTANCE.validShapeFilter();
        final Filter<TagContainer> tagContainerFilter = constructTagFilter();
        final Filter<Way> filter = shapeFilter.and(tagContainerFilter);
        return composedWays.filter(filter::accept);
    }

    private static Filter<TagContainer> constructTagFilter() {
        final Filter<TagContainer> tagContainerFilter;
        try {
            tagContainerFilter = FilterFactory.INSTANCE.parseTagFilterFile(Paths.get(sectioningHighwayValuesFilePath));
        } catch (final IOException e) {
            throw new RuntimeException("Could not parse file " + sectioningHighwayValuesFilePath + " from "
                    + HIGHWAY_PATH + " parameter", e);
        }
        return tagContainerFilter;
    }
}