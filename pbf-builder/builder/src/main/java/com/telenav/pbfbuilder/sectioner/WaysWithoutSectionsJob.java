package com.telenav.pbfbuilder.sectioner;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.TagContainer;
import com.telenav.map.filter.FilterFactory;
import com.telenav.map.storage.RawWaySchema;
import com.telenav.map.storage.read.RawRowToRawWayConverter;
import com.telenav.map.storage.write.RawWayToRowConverter;
import com.telenav.ost.ArgumentContainer;
import com.telenav.ost.Filter;
import com.telenav.ost.spark.storage.read.ParquetReader;
import com.telenav.ost.spark.storage.write.ParquetWriter;
import org.apache.spark.api.java.JavaRDD;

import java.io.IOException;
import java.nio.file.Paths;


/**
 * Job that takes all the way sections and all the ways that have no sections, and merges them together
 *
 * @author adrianal
 */
public class WaysWithoutSectionsJob {
    private static final String HIGHWAY_PATH = "sectioning_highway_values";
    private static String sectioningHighwayValuesFilePath;

    public static void main(final String[] args) {

        final ArgumentContainer arguments = new ArgumentContainer(args);
        final String outputDir = arguments.getRequired("output_dir");
        final String rawWaysPath = arguments.getRequired("way_parquet");
        sectioningHighwayValuesFilePath = arguments.getRequired(HIGHWAY_PATH);
        final String runLocal = arguments.getOptional("run_local", "false");

        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Ways Without Sections")
                .runLocal(Boolean.valueOf(runLocal))
                .build();
        final JavaRDD<RawWay> rawWays = new ParquetReader<>(jobInitializer.javaSparkContext(),
                new RawRowToRawWayConverter(), rawWaysPath).read();

        final JavaRDD<RawWay> filteredWays = filterWays(rawWays);

        new ParquetWriter.Builder<RawWay>()
                .sparkContext(jobInitializer.javaSparkContext())
                .converter(new RawWayToRowConverter())
                .schema(new RawWaySchema())
                .path(outputDir).saveMode("ErrorIfExists")
                .build()
                .write(filteredWays);
    }

    private static JavaRDD<RawWay> filterWays(final JavaRDD<RawWay> rawWays) {
        final Filter<RawWay> shapeFilter = way -> way.nodeIds() != null && way.nodeIds().size() > 1;
        final Filter<TagContainer> tagContainerFilter = constructTagFilter();
        final Filter<RawWay> filter = shapeFilter.and(tagContainerFilter.negate());
        return rawWays.filter(filter::accept);
    }

    private static Filter<TagContainer> constructTagFilter() {
        final Filter<TagContainer> tagContainerFilter;
        try {
            tagContainerFilter = FilterFactory.INSTANCE.parseTagFilterFile(Paths.get(sectioningHighwayValuesFilePath));
        } catch (final IOException e) {
            throw new RuntimeException("Could not parse file " + sectioningHighwayValuesFilePath + " from "
                    + HIGHWAY_PATH + " parameter", e);
        }
        return tagContainerFilter;
    }
}