package com.telenav.pbfbuilder.stats;

import crosby.binary.osmosis.OsmosisReader;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openstreetmap.osmosis.core.container.v0_6.EntityContainer;
import org.openstreetmap.osmosis.core.domain.v0_6.Entity;
import org.openstreetmap.osmosis.core.domain.v0_6.Node;
import org.openstreetmap.osmosis.core.domain.v0_6.Relation;
import org.openstreetmap.osmosis.core.domain.v0_6.Way;
import org.openstreetmap.osmosis.core.task.v0_6.Sink;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * Stats from pbf file. Parse the pbf and count ways nodes and relations.
 *
 * @author adrianal
 */

public class PbfStats {

    private static final Logger LOGGER = LogManager.getLogger(PbfStats.class);
    private final Path pbfPath;
    private long nodes;
    private long ways;
    private long relations;
    private long tags;
    private long members;
    private long wayNodes;


    PbfStats(final Path pbfPath) {
        this.pbfPath = pbfPath;
    }

    public long getNodes() {
        return nodes;
    }

    public long getWays() {
        return ways;
    }

    public long getRelations() {
        return relations;
    }

    public long getTags() {
        return tags;
    }

    public long getMembers() {
        return members;
    }

    public long getWayNodes() {
        return wayNodes;
    }

    void read() throws IOException {
        LOGGER.info("Reading " + pbfPath + "...");
        final long startTime = Calendar.getInstance().getTimeInMillis();
        final OsmosisReader reader = new OsmosisReader(new FileInputStream(pbfPath.toString()));
        reader.setSink(new PbfEntityConsumer());
        reader.run();
        final long endTime = Calendar.getInstance().getTimeInMillis();
        final long durationInSeconds = TimeUnit.MILLISECONDS.toSeconds(endTime - startTime);
        LOGGER.info("Finished reading " + pbfPath + " in " + durationInSeconds + " seconds.\n");
    }

    private class PbfEntityConsumer implements Sink {

        @Override
        public void process(final EntityContainer entityContainer) {
            final Entity entity = entityContainer.getEntity();
            tags+=entity.getTags().size();
            if (entity instanceof Node) {
//                LOGGER.info("current entity is Node: " + entity.getId());
                nodes++;

            } else if (entity instanceof Way) {
//                LOGGER.info("current entity is Way: " + entity.getId());
                ways++;
                wayNodes+=((Way) entity).getWayNodes().size();
            } else if (entity instanceof Relation) {
//                LOGGER.info("current entity is Relation: " + entity.getId());
                relations++;
                members+=((Relation) entity).getMembers().size();
            }
        }

        @Override
        public void initialize(final Map<String, Object> map) {
        }

        @Override
        public void complete() {
        }

        @Override
        public void close() {
        }
    }
}