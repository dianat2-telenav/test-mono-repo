package com.telenav.pbfbuilder.stats;


import java.io.IOException;
import java.nio.file.Paths;
import com.telenav.ost.ArgumentContainer;


/**
 * Call the pbf stats and write the statistics to a file given as argument.
 *
 * @author odrab
 */
public class PbfStatsApplication {

    public static void main(final String[] args) throws IOException {
        final ArgumentContainer arguments = new ArgumentContainer(args);
        final String pbf = arguments.getRequired("pbf");
        final String output = arguments.getRequired("output");

        final PbfStats pbfStats = new PbfStats(Paths.get(pbf));
        pbfStats.read();

        String stats = "ways = " + pbfStats.getWays();
        stats += " nodes = " + pbfStats.getNodes();
        stats += " relations = " + pbfStats.getRelations();
        stats += " tags = " + pbfStats.getTags();
        stats += " wayNodes = " + pbfStats.getWayNodes();
        stats += " members = " + pbfStats.getMembers();
        System.out.println(stats);

    }
}