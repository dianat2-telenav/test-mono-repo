package com.telenav.pbfbuilder.stats;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.ost.ArgumentContainer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.*;


/**
 * The job is responsible for comparing aggregated info about tags key and values
 *
 * @author volodymyrl
 */
public class StatsCompareJob {

    /**
     * @param args - command line arguments in name=value format
     *             1. current_release_ways - path for current release data for ways
     *             2. previous_release_ways - path for previous release data for ways
     *             3. ways_keys - path for required for report keys for ways
     *             4. current_release_nodes - path for current release data for nodes
     *             5. previous_release_nodes - path for previous release data for nodes
     *             6. nodes_keys - path for required for report keys for nodes
     *             7. current_release_relations - path for current release data for relations
     *             8. previous_release_relations - path for previous release data for relations
     *             9. relations_keys - path for required for report keys for relations
     *             10. report_path - directory where report is stored
     *             11. report_type - type of report aggregating column, could be "key" or "key_value"
     *             12. save_only_changed - true if needed to save only records where diff greater than 0, true by default
     *             13. threshold - threshold for report rows to be saved, could be from 0 to 100, 0 by default (all records are saved)
     *             14. run_local - set to "true" for local run. Default value is "false"
     *             15. save_mode - set to "Overwrite" if you don't need to store output from the previous run
     */
    public static void main(String[] args) {
        final ArgumentContainer arguments = new ArgumentContainer(args);
        final String currentReleaseNodesPath = arguments.getOptional("current_release_nodes", null);
        final String previousReleaseNodesPath = arguments.getOptional("previous_release_nodes", null);
        final String nodesAttributesPath = arguments.getOptional("nodes_attributes_path", null);
        final String currentReleaseWaysPath = arguments.getOptional("current_release_ways", null);
        final String previousReleaseWaysPath = arguments.getOptional("previous_release_ways", null);
        final String waysAttributesPath = arguments.getOptional("ways_attributes_path", null);
        final String currentReleaseRelationsPath = arguments.getOptional("current_release_relations", null);
        final String previousReleaseRelationsPath = arguments.getOptional("previous_release_relations", null);
        final String relationsAttributesPath = arguments.getOptional("relations_attributes_path", null);
        final String reportPath = arguments.getRequired("report_path");
        final ReportType reportType = ReportType.valueOf(arguments.getRequired("report_type").toUpperCase());
        final Boolean saveOnlyChanged = Boolean.parseBoolean(arguments.getOptional("save_only_changed", "true"));
        final Double threshold = Double.parseDouble(arguments.getOptional("threshold", "0"));
        final String runLocal = arguments.getOptional("run_local", "false");
        final String saveMode = arguments.getOptional("save_mode", "Overwrite");

        JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("StatsCompareJob")
                .runLocal(Boolean.valueOf(runLocal))
                .build();

        try (SparkSession sparkSession = jobInitializer.sparkSession()) {
            collectAndSaveReport(currentReleaseNodesPath,
                    previousReleaseNodesPath,
                    nodesAttributesPath,
                    reportPath + "/nodes",
                    reportType,
                    saveOnlyChanged,
                    threshold,
                    saveMode,
                    sparkSession);

            collectAndSaveReport(currentReleaseWaysPath,
                    previousReleaseWaysPath,
                    waysAttributesPath,
                    reportPath + "/ways",
                    reportType,
                    saveOnlyChanged,
                    threshold,
                    saveMode,
                    sparkSession);

            collectAndSaveReport(currentReleaseRelationsPath,
                    previousReleaseRelationsPath,
                    relationsAttributesPath,
                    reportPath + "/relations",
                    reportType,
                    saveOnlyChanged,
                    threshold,
                    saveMode,
                    sparkSession);
        }
    }

    private static void collectAndSaveReport(String currentReleasePath,
                                             String previousReleasePath,
                                             String attributesListPath,
                                             String reportPath,
                                             ReportType reportType,
                                             Boolean saveOnlyChanged,
                                             Double threshold,
                                             String saveMode,
                                             SparkSession sparkSession) {
        if (currentReleasePath != null &&
                previousReleasePath != null) {

            Dataset<Row> report = createReport(previousReleasePath,
                    currentReleasePath,
                    reportType,
                    sparkSession
            ).cache();

            if (saveOnlyChanged) {
                report = StatsUtil.getChangedRows(report);
            }

            StatsUtil.getRowsOverThreshold(report, threshold)
                    .localCheckpoint()
                    .write()
                    .mode(saveMode)
                    .option("header", "true")
                    .csv(reportPath);

            report = sparkSession
                    .read()
                    .option("header", "true")
                    .csv(reportPath);
            if (attributesListPath != null) {
                Dataset<Row> filterKeys = readCsv(attributesListPath, sparkSession);
                report = StatsUtil.filterReportByKeys(report, broadcast(filterKeys).cache()).localCheckpoint();
            }
            report.coalesce(1)
                    .write()
                    .mode(saveMode)
                    .option("header", "true")
                    .csv(reportPath + "/merged");
        }
    }

    private static Dataset<Row> createReport(String firstPath,
                                             String secondPath,
                                             ReportType reportType,
                                             SparkSession sparkSession) {
        switch (reportType) {
            case KEY:
                return StatsUtil.getComparingReportByKey(readParquet(firstPath, sparkSession),
                        readParquet(secondPath, sparkSession));
            case KEY_VALUE:
                return StatsUtil.getComparingReportByKeyAndValue(readParquet(firstPath, sparkSession),
                        readParquet(secondPath, sparkSession));
            default:
                throw new IllegalArgumentException("Incorrect reportType, it must be key or key_value either");
        }
    }

    private static Dataset<Row> readParquet(String path, SparkSession sparkSession) {
        return sparkSession.read().parquet(path);
    }

    private static Dataset<Row> readCsv(String path, SparkSession sparkSession) {
        return sparkSession.read().option("header", "true").csv(path);
    }
}
