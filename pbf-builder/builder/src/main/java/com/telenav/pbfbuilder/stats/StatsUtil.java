package com.telenav.pbfbuilder.stats;

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.ArrayType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.Arrays;

import static org.apache.spark.sql.functions.*;

/**
 * The util for aggregating and collecting information about tags count
 *
 * @author volodymyrl
 */
public class StatsUtil {

    public final static StructType KEYS_SCHEMA;
    public static final String FIRST_COUNT = "first_count";
    public static final String SECOND_COUNT = "second_count";
    public static final String DIFF = "diff";

    private static final String TAGS = "tags";
    private static final String KEY = "key";
    private static final String VALUE = "value";
    private static final String COUNT = "count";
    private static final Column DIFF_FUNC = col(SECOND_COUNT).minus(col(FIRST_COUNT));
    private static final Column DIFF_IN_PERCENTS = col(DIFF)
            .multiply(100)
            .divide(
                    when(col(FIRST_COUNT).equalTo(0), col(SECOND_COUNT))
                            .otherwise(col(FIRST_COUNT))
            );
    private static final Column IS_CHANGED = abs(col(DIFF)).$greater$eq(lit(1));

    static {
        KEYS_SCHEMA = DataTypes.createStructType(new StructField[]{DataTypes.createStructField("key", DataTypes.StringType, false)});
    }

    private static boolean isColumnArray(Dataset<Row> data, String columnName) {
        return Arrays.stream(data.schema().fields())
                .anyMatch(field -> field.name().equals(columnName) && field.dataType() instanceof ArrayType);
    }

    private static Dataset<Row> explodeColumn(Dataset<Row> dataset, String columnName) {
        if (isColumnArray(dataset, columnName)) {
            return dataset
                    .select(explode(col(columnName)))
                    .select("col.*");
        } else {
            return dataset
                    .select(explode(col(columnName)));
        }
    }

    /**
     * @param dataset Dataset of rows that contain "tag" column of array or map type with "key" and "value" columns inside
     * @return Dataset counts of each pair of "key" and "value" inside input dataset
     */
    public static Dataset<Row> getSingleReportByKeyAndValue(Dataset<Row> dataset) {
        return explodeColumn(dataset, TAGS)
                .groupBy(KEY, VALUE)
                .agg(count("*").alias(COUNT))
                .cache();
    }

    /**
     * @param dataset Dataset of rows that contain "tag" column of array or map type with "key" column inside
     * @return Dataset counts of each "key" inside input dataset
     */
    public static Dataset<Row> getSingleReportByKey(Dataset<Row> dataset) {
        return explodeColumn(dataset, TAGS)
                .groupBy(KEY)
                .agg(count("*").alias(COUNT))
                .cache();
    }

    /**
     * @param firstDataset  Dataset of rows that contain "tag" column of array or map type with "key" and "value" columns inside
     * @param secondDataset Dataset of rows that contain "tag" column of array or map type with "key" and "value" columns inside
     * @return Dataset counts of each pair of "key" and "value" inside input datasets and diff between count for each pair
     */
    public static Dataset<Row> getComparingReportByKeyAndValue(Dataset<Row> firstDataset, Dataset<Row> secondDataset) {
        Dataset<Row> firstReport = getSingleReportByKeyAndValue(firstDataset);
        Dataset<Row> secondReport = getSingleReportByKeyAndValue(secondDataset);
        Column joinRule = firstReport.col(KEY).equalTo(secondReport.col(KEY)).and(
                firstReport.col(VALUE).equalTo(secondReport.col(VALUE)));
        return firstReport
                .joinWith(secondReport, joinRule, "fullouter")
                .select(coalesce(col("_1." + KEY), col("_2." + KEY)).as(KEY),
                        coalesce(col("_1." + VALUE), col("_2." + VALUE)).as(VALUE),
                        col("_1." + COUNT).as(FIRST_COUNT),
                        col("_2." + COUNT).as(SECOND_COUNT)
                )
                .na()
                .fill(0)
                .withColumn(DIFF, DIFF_FUNC);
    }

    /**
     * @param firstDataset  Dataset of rows that contain "tag" column of array or map type with "key" column inside
     * @param secondDataset Dataset of rows that contain "tag" column of array or map type with "key" column inside
     * @return Dataset counts of each "key" inside input datasets and diff between count for each "key"
     */
    public static Dataset<Row> getComparingReportByKey(Dataset<Row> firstDataset, Dataset<Row> secondDataset) {
        Dataset<Row> firstReport = getSingleReportByKey(firstDataset);
        Dataset<Row> secondReport = getSingleReportByKey(secondDataset);
        Column joinRule = firstReport.col(KEY).equalTo(secondReport.col(KEY));
        return firstReport
                .joinWith(secondReport, joinRule, "fullouter")
                .select(coalesce(col("_1." + KEY), col("_2." + KEY)).as(KEY),
                        col("_1." + COUNT).as(FIRST_COUNT),
                        col("_2." + COUNT).as(SECOND_COUNT)
                )
                .na()
                .fill(0)
                .withColumn(DIFF, DIFF_FUNC);
    }

    /**
     * @param comparingReport Dataset of rows that comparing report of two datasets and contains {@code "diff"} column
     * @return Comparing report which contains records where absolute diff is at least 1
     */
    public static Dataset<Row> getChangedRows(Dataset<Row> comparingReport) {
        return comparingReport.filter(IS_CHANGED);
    }

    /**
     * @param comparingReport Dataset of rows that comparing report of two datasets and contains {@code "diff"} column
     * @param filterKeys Dataset of rows that comparing report of two datasets and contains {@code "diff"} column
     * @return Comparing report which contains records filtered by filterKeys
     */
    public static Dataset<Row> filterReportByKeys(Dataset<Row> comparingReport, Dataset<Row> filterKeys) {
         return comparingReport.alias("report")
                .join(filterKeys.alias("keys"),
                        expr("report.key like keys.key"))
                .select("report.*");
    }

    /**
     * @param comparingReport Dataset of rows that comparing report of two datasets and contains {@code "diff"} column
     * @param threshold       Threshold for report rows to be filtered out, could be from 0 to 100 inclusive, if threshold is 100 - all records will be removed
     * @return Filtered comparing report
     */
    public static Dataset<Row> getRowsOverThreshold(Dataset<Row> comparingReport, Double threshold) {
        if (threshold >= 0D && threshold <= 100D) {
            return comparingReport.filter(abs(DIFF_IN_PERCENTS).$greater$eq(lit(threshold)));
        } else {
            throw new IllegalArgumentException("Threshold cannot be out of [0, 100] range");
        }
    }
}
