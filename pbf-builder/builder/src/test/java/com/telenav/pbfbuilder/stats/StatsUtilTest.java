package com.telenav.pbfbuilder.stats;

import com.telenav.datapipelinecore.JobInitializer;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author volodymyrl
 */
public class StatsUtilTest {

    private static Dataset<Row> dataset1;
    private static Dataset<Row> dataset2;
    private static Dataset<Row> emptyDataset;
    private static Dataset<Row> keysDataset1;
    private static Dataset<Row> keysDataset2;
    private static JobInitializer jobInitializer;
    private final static StructType schema;

    static {
        StructType tagsSchema = DataTypes.createStructType(new StructField[]{DataTypes.createStructField("key", DataTypes.StringType, false),
                DataTypes.createStructField("value", DataTypes.StringType, false)});

        schema = DataTypes.createStructType(new StructField[]{DataTypes.createStructField("id", DataTypes.StringType, false), DataTypes.createStructField("tags",
                DataTypes.createArrayType(tagsSchema, false),
                false)});
    }

    @org.junit.BeforeClass
    public static void setUp() {
        jobInitializer = new JobInitializer.Builder()
                .appName("StatsUtilTest")
                .runLocal(true)
                .build();

        dataset1 = jobInitializer.sparkSession().createDataFrame(Arrays.asList(
                RowFactory.create("id1", new Row[]{RowFactory.create("tag1", "value1"), RowFactory.create("tag2", "value2")}),
                RowFactory.create("id2", new Row[]{RowFactory.create("tag1", "value2"), RowFactory.create("tag3", "value3")}),
                RowFactory.create("id3", new Row[]{RowFactory.create("tag1", "value1"), RowFactory.create("tag4", "value4")}),
                RowFactory.create("id4", new Row[]{RowFactory.create("tag1", "value2"), RowFactory.create("tag4", "value4")})
        ), schema);

        dataset2 = jobInitializer.sparkSession().createDataFrame(Arrays.asList(
                RowFactory.create("id1", new Row[]{RowFactory.create("tag1", "value2"), RowFactory.create("tag2", "value2")}),
                RowFactory.create("id2", new Row[]{RowFactory.create("tag1", "value1"), RowFactory.create("tag3", "value3")}),
                RowFactory.create("id3", new Row[]{RowFactory.create("tag1", "value1"), RowFactory.create("tag4", "value4")}),
                RowFactory.create("id4", new Row[]{RowFactory.create("tag1", "value2"), RowFactory.create("tag5", "value5")})
        ), schema);

        keysDataset1 = jobInitializer.sparkSession().createDataFrame(Arrays.asList(
                RowFactory.create("tag1"),
                RowFactory.create("tag2")
        ), StatsUtil.KEYS_SCHEMA);

        keysDataset2 = jobInitializer.sparkSession().createDataFrame(Arrays.asList(
                RowFactory.create("tag%")
        ), StatsUtil.KEYS_SCHEMA);

        emptyDataset = jobInitializer.sparkSession().createDataFrame(Collections.emptyList(), schema);
    }

    @org.junit.AfterClass
    public static void tearDown() {
        jobInitializer.sparkSession().close();
    }

    @Test
    public void checkKeyStatsCorrectness() {
        List<Row> reportRows = StatsUtil.getSingleReportByKey(dataset1)
                .collectAsList();

        Map<String, Long> countByKey = reportRows
                .stream()
                .collect(Collectors.toMap(row -> row.getAs("key"), row -> row.<Long>getAs("count")));
        Map<String, Long> expectedCounts = new HashMap<>();
        expectedCounts.put("tag1", 4L);
        expectedCounts.put("tag2", 1L);
        expectedCounts.put("tag3", 1L);
        expectedCounts.put("tag4", 2L);
        assertThat(countByKey).containsExactlyInAnyOrderEntriesOf(expectedCounts);
    }

    @Test
    public void checkKeyAndValueStatsCorrectness() {
        List<Row> reportRows = StatsUtil.getSingleReportByKeyAndValue(dataset1)
                .collectAsList();

        Map<Pair<String, String>, Long> countByKeyAndValue = reportRows
                .stream()
                .collect(Collectors.toMap(row -> Pair.of(row.getAs("key"), row.getAs("value")), row -> row.<Long>getAs("count")));
        Map<Pair<String, String>, Long> expectedCounts = new HashMap<>();
        expectedCounts.put(Pair.of("tag1", "value1"), 2L);
        expectedCounts.put(Pair.of("tag1", "value2"), 2L);
        expectedCounts.put(Pair.of("tag2", "value2"), 1L);
        expectedCounts.put(Pair.of("tag3", "value3"), 1L);
        expectedCounts.put(Pair.of("tag4", "value4"), 2L);
        assertThat(countByKeyAndValue).containsExactlyInAnyOrderEntriesOf(expectedCounts);
    }

    @Test
    public void checkComparedKeyStatsCorrectness() {
        Dataset<Row> report = StatsUtil.getComparingReportByKey(dataset1, dataset2).cache();
        List<Row> reportRows = report
                .collectAsList();

        Map<String, Triple<Long, Long, Long>> countByKey = reportRows
                .stream()
                .collect(Collectors.toMap(row -> row.getAs("key"),
                        row -> Triple.of(row.getAs(StatsUtil.FIRST_COUNT), row.getAs(StatsUtil.SECOND_COUNT), row.getAs(StatsUtil.DIFF))));
        Map<String, Triple<Long, Long, Long>> expectedCounts = new HashMap<>();
        expectedCounts.put("tag1", Triple.of(4L, 4L, 0L));
        expectedCounts.put("tag2", Triple.of(1L, 1L, 0L));
        expectedCounts.put("tag3", Triple.of(1L, 1L, 0L));
        expectedCounts.put("tag4", Triple.of(2L, 1L, -1L));
        expectedCounts.put("tag5", Triple.of(0L, 1L, 1L));
        assertThat(countByKey).containsExactlyInAnyOrderEntriesOf(expectedCounts);

        assertThat(StatsUtil.getChangedRows(report).count()).isEqualTo(2);
    }

    @Test
    public void checkComparedKeyAndValueStatsCorrectness() {
        Dataset<Row> report = StatsUtil.getComparingReportByKeyAndValue(dataset1, dataset2).cache();
        List<Row> reportRows = report
                .collectAsList();

        Map<Pair<String, String>, Triple<Long, Long, Long>> countByKeyAndValue = reportRows
                .stream()
                .collect(Collectors.toMap(row -> Pair.of(row.getAs("key"), row.getAs("value")),
                        row -> Triple.of(row.getAs(StatsUtil.FIRST_COUNT), row.getAs(StatsUtil.SECOND_COUNT), row.getAs(StatsUtil.DIFF))));
        Map<Pair<String, String>, Triple<Long, Long, Long>> expectedCounts = new HashMap<>();
        expectedCounts.put(Pair.of("tag1", "value1"), Triple.of(2L, 2L, 0L));
        expectedCounts.put(Pair.of("tag1", "value2"), Triple.of(2L, 2L, 0L));
        expectedCounts.put(Pair.of("tag2", "value2"), Triple.of(1L, 1L, 0L));
        expectedCounts.put(Pair.of("tag3", "value3"), Triple.of(1L, 1L, 0L));
        expectedCounts.put(Pair.of("tag4", "value4"), Triple.of(2L, 1L, -1L));
        expectedCounts.put(Pair.of("tag5", "value5"), Triple.of(0L, 1L, 1L));
        assertThat(countByKeyAndValue).containsExactlyInAnyOrderEntriesOf(expectedCounts);

        assertThat(StatsUtil.getChangedRows(report).count()).isEqualTo(2);
    }

    @Test
    public void checkEmptyDatasetsComparingWithThreshold0() {
        Dataset<Row> report = StatsUtil.getComparingReportByKey(dataset1, emptyDataset);
        Map<String, Triple<Long, Long, Long>> countByKey = StatsUtil.getRowsOverThreshold(report, 0D)
                .collectAsList()
                .stream()
                .collect(Collectors.toMap(row -> row.getAs("key"),
                        row -> Triple.of(row.getAs(StatsUtil.FIRST_COUNT), row.getAs(StatsUtil.SECOND_COUNT), row.getAs(StatsUtil.DIFF))));

        Map<String, Triple<Long, Long, Long>> expectedCounts = new HashMap<>();
        expectedCounts.put("tag1", Triple.of(4L, 0L, -4L));
        expectedCounts.put("tag2", Triple.of(1L, 0L, -1L));
        expectedCounts.put("tag3", Triple.of(1L, 0L, -1L));
        expectedCounts.put("tag4", Triple.of(2L, 0L, -2L));
        assertThat(countByKey).containsExactlyInAnyOrderEntriesOf(expectedCounts);
    }

    @Test
    public void checkEmptyDatasetsComparingReversedWithThreshold0() {
        Dataset<Row> report = StatsUtil.getComparingReportByKey(emptyDataset, dataset1);
        Map<String, Triple<Long, Long, Long>> countByKey = StatsUtil.getRowsOverThreshold(report, 0D)
                .collectAsList()
                .stream()
                .collect(Collectors.toMap(row -> row.getAs("key"),
                        row -> Triple.of(row.getAs(StatsUtil.FIRST_COUNT), row.getAs(StatsUtil.SECOND_COUNT), row.getAs(StatsUtil.DIFF))));

        Map<String, Triple<Long, Long, Long>> expectedCounts = new HashMap<>();
        expectedCounts.put("tag1", Triple.of(0L, 4L, 4L));
        expectedCounts.put("tag2", Triple.of(0L, 1L, 1L));
        expectedCounts.put("tag3", Triple.of(0L, 1L, 1L));
        expectedCounts.put("tag4", Triple.of(0L, 2L, 2L));
        assertThat(countByKey).containsExactlyInAnyOrderEntriesOf(expectedCounts);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkOutOfRangeThreshold() {
        Dataset<Row> report = StatsUtil.getComparingReportByKey(dataset1, emptyDataset);
        StatsUtil.getRowsOverThreshold(report, 101D).show();
    }

    @Test
    public void testFilteringByKeys() {
        Dataset<Row> report = StatsUtil.getComparingReportByKey(dataset1, emptyDataset);
        Dataset<Row> filteredReport = StatsUtil.filterReportByKeys(report, keysDataset1);

        List<String> countByKey = filteredReport
                .collectAsList()
                .stream()
                .map(row -> row.<String>getAs("key"))
                .collect(Collectors.toList());

        List<String> expectedTags = new ArrayList<>();
        expectedTags.add("tag1");
        expectedTags.add("tag2");

        assertThat(countByKey).hasSameElementsAs(expectedTags);
    }

    @Test
    public void testFilteringByRegexKeys() {
        Dataset<Row> report = StatsUtil.getComparingReportByKey(dataset1, emptyDataset);
        Dataset<Row> filteredReport = StatsUtil.filterReportByKeys(report, keysDataset2);
        assertThat(report.collectAsList()).hasSameElementsAs(filteredReport.collectAsList());
    }
}