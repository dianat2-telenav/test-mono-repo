package com.telenav.pbfbuilder;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.ost.ArgumentContainer;
import com.telenav.pbfbuilder.common.OsmWriter;
import crosby.binary.osmosis.OsmosisSerializer;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.avro.generic.GenericRecord;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.parquet.avro.AvroParquetReader;
import org.apache.parquet.hadoop.ParquetReader;
import org.openstreetmap.osmosis.core.container.v0_6.NodeContainer;
import org.openstreetmap.osmosis.core.container.v0_6.RelationContainer;
import org.openstreetmap.osmosis.core.container.v0_6.WayContainer;
import org.openstreetmap.osmosis.core.domain.v0_6.CommonEntityData;
import org.openstreetmap.osmosis.core.domain.v0_6.EntityType;
import org.openstreetmap.osmosis.core.domain.v0_6.Node;
import org.openstreetmap.osmosis.core.domain.v0_6.OsmUser;
import org.openstreetmap.osmosis.core.domain.v0_6.Relation;
import org.openstreetmap.osmosis.core.domain.v0_6.RelationMember;
import org.openstreetmap.osmosis.core.domain.v0_6.Tag;
import org.openstreetmap.osmosis.core.domain.v0_6.Way;
import org.openstreetmap.osmosis.core.domain.v0_6.WayNode;


public class PbfSerializerJob {

  private static final Logger LOGGER = LogManager.getLogger(PbfSerializerJob.class);

  private static final int version = 123;
  private static final Date timestamp = Date.valueOf(LocalDate.of(2017, Month.OCTOBER, 9));
  private static final OsmUser user = new OsmUser(1, "Telenav");
  private static final long changesetId = 123L;

  private static OsmosisSerializer osmosisSerializer;

  public static void main(String[] args) throws IOException {

    final ArgumentContainer arguments = new ArgumentContainer(args);
    final String nodesParquet = arguments.getRequired("nodes_parquet");
    final String waysParquet = arguments.getRequired("ways_parquet");
    final String relationsParquet = arguments.getRequired("relations_parquet");
    final String pbfOutputDir = arguments.getRequired("output_dir");
    final String pbfFile = arguments.getRequired("output_file");
    final String runLocal = arguments.getOptional("run_local", "false");
    final String readWriteHdfs = arguments.getOptional("read_write_hdfs", "true");
    final String batchLimit = arguments.getOptional("batch_limit", "100000");

    try(final JobInitializer jobInitializer = new JobInitializer.Builder()
        .appName("Pbf Serializer")
        .runLocal(Boolean.parseBoolean(runLocal))
        .build()) {

      final Configuration configuration = jobInitializer.sparkSession().sparkContext().hadoopConfiguration();

      final OsmWriter osmWriter = new OsmWriter(pbfOutputDir, pbfFile, Boolean.parseBoolean(readWriteHdfs), configuration);
      osmosisSerializer = osmWriter.getSerializer();
      osmosisSerializer.configBatchLimit(Integer.parseInt(batchLimit));

      serializeNodes(nodesParquet);
      serializeWays(waysParquet);
      serializeRelations(relationsParquet);

      osmosisSerializer.close();

    }

  }

  private static void serializeNodes(String nodesParquet) throws IOException {
    LOGGER.info("Node serialization begins");
    Path nodePath = new Path(nodesParquet);
    try (ParquetReader<GenericRecord> reader = AvroParquetReader.<GenericRecord>builder(nodePath)
        .withConf(new Configuration()).build()) {
      GenericRecord record;
      while ((record = reader.read()) != null) {
        final List<GenericRecord> tagsRaw = (List<GenericRecord>) record.get("tags");
        final Collection<Tag> tags = new ArrayList<>(tagsRaw.size());
        for (GenericRecord tagRaw : tagsRaw) {
          GenericRecord element = (GenericRecord)tagRaw.get("element");
          tags.add(new Tag(element.get("key").toString(),element.get("value").toString()));
        }
        final CommonEntityData commonEntityData = new CommonEntityData((long) record.get("id"),
            version, timestamp, user, changesetId, tags);
        final Node node = new Node(commonEntityData, (double) record.get("latitude"),
            (double) record.get("longitude"));
        osmosisSerializer.process(new NodeContainer(node));
      }
    }
    osmosisSerializer.complete();
    LOGGER.info("Node serialization ends");
  }

  private static void serializeWays(String waysParquet) throws IOException {
    LOGGER.info("Way serialization begins");
    Path wayPath = new Path(waysParquet);
    try (ParquetReader<GenericRecord> reader = AvroParquetReader.<GenericRecord>builder(wayPath)
        .withConf(new Configuration()).build()) {
      GenericRecord record;
      while ((record = reader.read()) != null) {
        final List<GenericRecord> tagsRaw = (List<GenericRecord>) record.get("tags");
        final Collection<Tag> tags = new ArrayList<>(tagsRaw.size());
        for (GenericRecord tagRaw : tagsRaw) {
          GenericRecord element = (GenericRecord) tagRaw.get("element");
          tags.add(new Tag(element.get("key").toString(),element.get("value").toString()));
        }
        final List<GenericRecord> nodesRaw = (List<GenericRecord>) record.get("nodes");
        final List<WayNode> wayNodes = new ArrayList<>(tagsRaw.size());
        for (GenericRecord nodeRaw : nodesRaw) {
          GenericRecord element = (GenericRecord) nodeRaw.get("element");
          wayNodes.add(new WayNode((long)element.get("nodeId")));
        }
        final CommonEntityData commonEntityData = new CommonEntityData((long) record.get("id"),
            version, timestamp, user, changesetId, tags);
        final Way way = new Way(commonEntityData, wayNodes);
        osmosisSerializer.process(new WayContainer(way));
      }
    }
    osmosisSerializer.complete();
    LOGGER.info("Way serialization ends");
  }

  private static void serializeRelations(String relationsParquet) throws IOException {
    LOGGER.info("Relation serialization begins");
    Path relationPath = new Path(relationsParquet);
    try (ParquetReader<GenericRecord> reader = AvroParquetReader.<GenericRecord>builder(relationPath)
        .withConf(new Configuration()).build()) {
      GenericRecord record;
      while ((record = reader.read()) != null) {
        final List<GenericRecord> tagsRaw = (List<GenericRecord>) record.get("tags");
        final Collection<Tag> tags = new ArrayList<>(tagsRaw.size());
        for (GenericRecord tagRaw : tagsRaw) {
          GenericRecord element = (GenericRecord) tagRaw.get("element");
          tags.add(new Tag(element.get("key").toString(),element.get("value").toString()));
        }
        final List<GenericRecord> membersRaw = (List<GenericRecord>) record.get("members");
        final List<RelationMember> members = new ArrayList<>(membersRaw.size());
        for (GenericRecord nodeRaw : membersRaw) {
          GenericRecord element = (GenericRecord) nodeRaw.get("element");
          EntityType osmosisType = null;
          switch (element.get("type").toString()) {
            case  ("Node"):
            case  ("node"):
              osmosisType = EntityType.Node;
              break;
            case ("Way"):
            case ("way"):
              osmosisType = EntityType.Way;
              break;
            case ("Relation"):
            case ("relation"):
              osmosisType = EntityType.Relation;
              break;
            default:
              break;
          }
          assert osmosisType != null;
          members.add(new RelationMember((long) element.get("id"), osmosisType, element.get("role").toString()));
        }
        final CommonEntityData commonEntityData = new CommonEntityData((long) record.get("id"),
            version, timestamp, user, changesetId, tags);
        final Relation relation = new Relation(commonEntityData, members);
        osmosisSerializer.process(new RelationContainer(relation));
      }
    }
    osmosisSerializer.complete();
    LOGGER.info("Relation serialization ends");
  }

}